<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Session;
use App\Models\Cart;



class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        view()->composer('*', function ($view)
        {
            $view->with('cart', \Session::get('cart') );
        });


        if (!Session::has('cart')) {
            View()->share(['products' => null]);
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
//
//        dd($cart);
//
//        View()->share(['products' => $cart->items]);
//        View()->share(['totalPrice' => $cart->totalPrice]);
//        view()->composer('layouts.navbar', function ($view){
//
//            $view->with('cart', \App\Http\Controllers\ProductController::getCart() );
//        });
    }
}
