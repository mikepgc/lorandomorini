<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;


class Reservations extends Model
{
    use CrudTrait;

    protected $connection = 'caffeitalia';

    protected $table = 'reserveringen';

    protected $guarded = ['id'];



}

