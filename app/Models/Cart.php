<?php

namespace App\Models;
use Session;

class Cart
{
    public $items = null;
    public $totalQty = 0;
    public $totalPrice = 0;




    public function __construct($oldCart)
    {
        if ($oldCart) {
            $this->items = $oldCart->items;
            $this->totalQty = $this->getTotalQuantity($this->items);
            $this->totalPrice = $oldCart->totalPrice;
        }
    }

    public function getTotalQuantity($oldCart){
        $sum = 0;
        foreach($oldCart as $cart) {
                $sum += $cart['qty'];
        }
        return $sum;
    }
    public function getTotalPrice($items){
        $sum = 0;
        foreach($items as $cartItems) {
                $sum += $cartItems['price'];
        }
        return $sum;
    }

    public function add($item, $id) {
        $storedItem = ['qty' => $item->qty, 'price' => $item->price, 'item' => $item];
        if ($this->items) {
            if (array_key_exists($id, $this->items)) {
                $storedItem = $this->items[$id];
                $storedItem['qty'] += $item->qty;
            }
        }
        $storedItem['price'] = $item->price * $storedItem['qty'];
        $this->items[$id] = $storedItem;
        $this->totalQty = $this->getTotalQuantity($this->items);
        $this->totalPrice = $this->getTotalPrice($this->items);
    }

    public function reduceByOne($id) {
        $this->items[$id] ['qty']--;
        $this->items[$id] ['price'] -= $this->items[$id] ['item'] ['price'];
        $this->totalQty--;
        $this->totalPrice -= $this->items[$id] ['item'] ['price'];

        if ($this->items[$id]['qty'] <= 0) {
            unset($this->items[$id]);
        }
    }

    public function increaseByOne($id) {
        $this->items[$id] ['qty']++;
        $this->items[$id] ['price'] -= $this->items[$id] ['item'] ['price'];
        $this->totalQty++;
        $this->totalPrice += $this->items[$id] ['item'] ['price'];

        if ($this->items[$id]['qty'] <= 0) {
            unset($this->items[$id]);
        }
    }

     public function changeProductQty($item,$qty) {
        $this->items[$item->id] ['qty'] = $qty;
        $this->items[$item->id] ['price'] = $item->price * $qty;
        $this->totalQty = $this->getTotalQuantity($this->items);
        $this->totalPrice = $this->getTotalPrice($this->items);
        if ($this->items[$item->id]['qty'] <= 0) {
            unset($this->items[$item->id]);
        }
    }

    public function removeItem($id) {
          $this->totalQty -= $this->items[$id]['qty'];
          $this->totalPrice -= $this->items[$id]['price'];
        unset($this->items[$id]);

    }

}
