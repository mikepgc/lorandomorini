<?php

namespace App\Models;

use App\User;
use Backpack\Base\app\Notifications\ResetPasswordNotification as ResetPasswordNotification;
use Backpack\CRUD\CrudTrait;


class Users extends User
{
    use CrudTrait;

    protected $table = 'users';

    protected $guarded = ['id'];

    protected $fillable =[
//        'companyname',
//        'btw',
        'vip',
        'person',
        'name',
        'surname',
        'phone',
        'email',
        'password',
        'account',
//        'company',
//        'postcode',
//        'housenumber',
//        'housenumber2',
//        'address',
//        'city',
        'newsletter'
 

    ];

    /**
     * Send the password reset notification.
     *
     * @param string $token
     *
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    /**
     * Get the e-mail address where password reset links are sent.
     *
     * @return string
     */
    public function getEmailForPasswordReset()
    {
        return $this->email;
    }
}
