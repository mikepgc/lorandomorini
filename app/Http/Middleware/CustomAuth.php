<?php

namespace App\Http\Middleware;

class CustomAuth extends \Illuminate\Auth\Middleware\Authenticate
{
    public function handle($request, \Closure $next, ...$guards){
        if(env('TEST_ENV') && !$request->is('admin')){
            return $next($request);
        }
        return parent::handle($request, $next, ...$guards);
    }
}
