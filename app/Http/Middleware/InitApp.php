<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Cookie;
class InitApp
{
    public function handle($request, \Closure $next){
        if(!$request->is('admin/*')){
            \App\Governor::initApp($request, $next);
        }
        return $next($request);
    }
}
