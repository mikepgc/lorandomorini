<?php

namespace App\Http\Middleware;

use Closure;

class CheckIpMiddleware
{

    public $whiteIps = [
        '83.86.140.26',

        //mollie ip's
        '87.233.229.26',
        '87.233.229.27',
        '35.204.34.167',
        '35.204.72.248',
        '34.90.137.245',
        '34.90.10.225',
        '23.251.137.244',
        '146.148.31.21',
        '34.89.231.130',
        '35.246.254.59',



        // Tushar
        '103.250.145.237',


        //caffeitalia
        '77.161.202.51',


        ];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (env('APP_ENV') === 'development') {
            if (!in_array($request->ip(), $this->whiteIps)) {

                /*
                     You can redirect to any error page.
                */
                return response('No Access!!', 200);
            }
        }

        return $next($request);
    }
}
