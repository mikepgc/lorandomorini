<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\Users;
use App\Models\Orders;

class UserController extends Controller
{
    public function getSignup() {
        return view('pages.account.account-register');
    }

    public function postSignup(Request $request) {
        $this->validate($request, [

            'company' => '',
            'person' => '',
            'btw' => '',
            'companyname' => '',
            'name' => '',
            'surname' => '',
            'email' => '',
            'phone' => '',
            'account' => '',
            'password' => 'min:4'
        ]);

        $user = new Users([
            'company'=> $request->input('company'),
            'person'=> $request->input('person'),
            'btw'=> $request->input('btw'),
            'companyname'=> $request->input('companyname'),
            'name'=> $request->input('name'),
            'surname'=> $request->input('surname'),
            'email'=> $request->input('email'),
            'phone'=> $request->input('phone'),
            'password' => bcrypt($request->input('password')),
            'account' => '1',
        ]);
        $user->save();
        Auth::login($user);

        return redirect('account-profile');
    }

    public function UpdateProfile(Request $request) {

//
//            Auth::user()->email = $request->email;
//        Auth::user()->password($request->password);
//
//
//        \Auth::user()->save();


        \DB::table('users')->where('id', Auth::user()->id)->update([
            'company'=> $request->input('company'),
            'person'=> $request->input('person'),
            'btw'=> $request->input('btw'),
            'companyname'=> $request->input('companyname'),
            'name'=> $request->input('name'),
            'surname'=> $request->input('surname'),
            'newsletter'=> $request->input('newsletter'),
            'phone'=> $request->input('phone'),
//            'password' => bcrypt($request->input('password')),
        ]);


        return redirect('account-profile');
    }

    public function getSignin() {
        return view('pages.account.account-login');
    }

    public function GetPasswordReset() {
        return view('pages.account.account-password-recovery');
    }

    public function PostSignin(Request $request) {
        $this->validate($request, [
            //'email' => 'email[required|unique:users',
            'email' => 'required',
            'password' => 'required'

        ]);

        if (Auth::attempt(['email' => $request->input('email'), 'password' =>$request->input('password')])) {
            return redirect()->back();
        } else {
            // Authentication failed
            return redirect()->back()->withInput()->withErrors(['message' => 'Inloggen mislukt']);
        }
    }

    public function getProfile() {


        return view('pages.account.account-profile');
    }

    public function getOrders() {

        $this->viewdata = array(

                    'orders' => Orders::all(),

                );
    return view('pages.account.account-orders', $this->viewdata);
    }

    public function getSingleOrders($page) {

        $orders = Orders::all()
            ->sortBy('ordernumber')
            ->where ('ordernumber' , $page)
//            ->where('orderdata', unserialize('orderdata'))
            ->first();


$orderdata = Orders::all()

    ->sortBy('ordernumber')
    ->where ('ordernumber' , $page)
//     ->pluck(unserialize('orderdata'))
       ->first();

        $orderdetails = Orders::all()
            ->sortBy('orderdata')
            ->where('ordernumber', $page)
            ->pluck('orderdata')
            ->first();

            $orderdata =  unserialize($orderdetails);

//            $orderdata = json_encode($orderdata);

//            dd($orderdata); exit;

//            $orderdata = array_values($orderdata);


//            $orderdatas = $orderdata->transform(function($order, $key){
//
//                $order = unserialize($order);
//
//                return $order;
//            });







//dd ($orderdetails);exit;

    return view('pages.account.order-tracking', ['orders' => $orders , 'orderdata' => $orderdata]);
    }



     public function getSingleTicket() {
        return view('pages.account.account-single-ticket');
    }
     public function getTicketDetails() {
            return view('pages.account.account-ticket-details');
        }
        public function getTicket() {
            return view('pages.account.account-tickets');
        }
        public function getWishlist() {
            return view('pages.account.account-wishlist');
        }
      public function getAddress(Request $request)
      {
          if($request->isMethod('post')) {
              \DB::table('users')->where('id', Auth::user()->id)->update([
                  'postcode' => $request->input('postcode'),
                  'housenumber' => $request->input('housenumber'),
                  'housenumber2' => $request->input('housenumber2'),
                  'address' => $request->input('address'),
                  'city' => $request->input('city'),
                  'shipping_postcode'=> $request->input('sh-postcode'),
                  'shipping_housenumber'=> $request->input('sh-housenumber'),
                  'shipping_housenumber2'=> $request->input('sh-housenumber2'),
                  'shipping_address'=> $request->input('sh-address'),
                  'shipping_city'=> $request->input('sh-city'),
              ]);
          }

              return view('pages.account.account-address');

          }


    public function updateAddress(Request $request) {



        \DB::table('users')->where('id', Auth::user()->id)->update([
            'postcode' => $request->input('postcode'),
            'housenumber' => $request->input('housenumber'),
            'housenumber2' => $request->input('housenumber2'),
            'address' => $request->input('address'),
            'city' => $request->input('city'),
        ]);


        return redirect()->back();
    }

    public function getLogout() {
        Auth::logout();
//        return redirect()->back();
        return redirect('account-login');
    }
}
