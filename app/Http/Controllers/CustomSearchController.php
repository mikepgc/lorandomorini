<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Products;


class CustomSearchController extends Controller
{
    //

    function  index(Request $request)
    {

        if(request()->ajax())
        {
            if(!empty($request->filter_brand))
            {
                $data = Products::
                    select('sub_category_url', 'artnr', 'art_description_NL', 'image_url_1', 'stock', 'price', 'purchase_exbtw')
                    ->where('brand', $request->filter_brand)
                     ->get();
            }
            else{
                $data =  Products::
                    select('sub_category_url', 'artnr', 'art_description_NL', 'image_url_1', 'stock', 'price', 'purchase_exbtw')

                    ->get();
            }
            return datatables()->of($data)->make(true);
        }
        $brand = Products::select('brand')
        ->groupBy('brand')
        ->orderBy('Controller', 'ASC')
        ->get();

        return view('subcategory-grid', compact('brand'));
    }
}
