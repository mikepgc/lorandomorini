<?php

namespace App\Http\Controllers\Admin;

use App\Mail\NieuwsbriefCreated;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use DateTime;
// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\NieuwsbriefRequest as StoreRequest;
use App\Http\Requests\NieuwsbriefRequest as UpdateRequest;
use App\Models\Nieuwsbrief;

class NieuwsbriefCrudController extends CrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Nieuwsbrief');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/Nieuwsbrief');
        $this->crud->setEntityNameStrings('Nieuwsbrief', 'Nieuwsbrief');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

       // $this->crud->setFromDb();

        $this->crud->addColumn([
            'name' => 'naam',
            'label' => trans('Naam'),
            'type' => 'text',

        ]);

        $this->crud->addColumn([
            'name' => 'email',
            'label' => 'Email',
            'type' => 'text',
        ]);

        $this->crud->addColumn([
            'name' => 'updated_at',
            'label' => trans('Nieuwsbrief aangevraagd'),
            'type' => 'text',

        ]);


        $this->crud->addField([
            'name' => 'naam',
            'label' => trans('Naam'),
            'type' => 'text',

        ]);

        $this->crud->addField([
            'name' => 'email',
            'label' => 'Email',
            'type' => 'text',
        ]);

        $this->crud->addField([
            'name' => 'updated_at',
            'label' => trans('Nieuwsbrief aangevraagd'),
            'type' => 'text',

        ]);




        // ------ CRUD FIELDS
        // $this->crud->addField($options, 'update/create/both');
        // $this->crud->addFields($array_of_arrays, 'update/create/both');
        // $this->crud->removeField('name', 'update/create/both');
        // $this->crud->removeFields($array_of_names, 'update/create/both');

        // ------ CRUD COLUMNS
        // $this->crud->addColumn(); // add a single column, at the end of the stack
        // $this->crud->addColumns(); // add multiple columns, at the end of the stack
        // $this->crud->removeColumn('column_name'); // remove a column from the stack
        // $this->crud->removeColumns(['column_name_1', 'column_name_2']); // remove an array of columns from the stack
        // $this->crud->setColumnDetails('column_name', ['attribute' => 'value']); // adjusts the properties of the passed in column (by name)
        // $this->crud->setColumnsDetails(['column_1', 'column_2'], ['attribute' => 'value']);

        // ------ CRUD BUTTONS
        // possible positions: 'beginning' and 'end'; defaults to 'beginning' for the 'line' stack, 'end' for the others;
        // $this->crud->addButton($stack, $name, $type, $content, $position); // add a button; possible types are: view, model_function
        // $this->crud->addButtonFromModelFunction($stack, $name, $model_function_name, $position); // add a button whose HTML is returned by a method in the CRUD model
        // $this->crud->addButtonFromView($stack, $name, $view, $position); // add a button whose HTML is in a view placed at resources\views\vendor\backpack\crud\buttons
        // $this->crud->removeButton($name);
        // $this->crud->removeButtonFromStack($name, $stack);
        // $this->crud->removeAllButtons();
        // $this->crud->removeAllButtonsFromStack('line');

        // ------ CRUD ACCESS
        // $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);
        // $this->crud->denyAccess(['list', 'create', 'update', 'reorder', 'delete']);

        // ------ CRUD REORDER
        // $this->crud->enableReorder('label_name', MAX_TREE_LEVEL);
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('reorder');

        // ------ CRUD DETAILS ROW
        // $this->crud->enableDetailsRow();
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('details_row');
        // NOTE: you also need to do overwrite the showDetailsRow($id) method in your EntityCrudController to show whatever you'd like in the details row OR overwrite the views/backpack/crud/details_row.blade.php

        // ------ REVISIONS
        // You also need to use \Venturecraft\Revisionable\RevisionableTrait;
        // Please check out: https://laravel-backpack.readme.io/docs/crud#revisions
        // $this->crud->allowAccess('revisions');

        // ------ AJAX TABLE VIEW
        // Please note the drawbacks of this though:
        // - 1-n and n-n columns are not searchable
        // - date and datetime columns won't be sortable anymore
        // $this->crud->enableAjaxTable();

        // ------ DATATABLE EXPORT BUTTONS
        // Show export to PDF, CSV, XLS and Print buttons on the table view.
        // Does not work well with AJAX datatables.
         $this->crud->enableExportButtons();

        // ------ ADVANCED QUERIES
        // $this->crud->addClause('active');
        // $this->crud->addClause('type', 'car');
        // $this->crud->addClause('where', 'name', '==', 'car');
        // $this->crud->addClause('whereName', 'car');
        // $this->crud->addClause('whereHas', 'posts', function($query) {
        //     $query->activePosts();
        // });
        // $this->crud->addClause('withoutGlobalScopes');
        // $this->crud->addClause('withoutGlobalScope', VisibleScope::class);
        // $this->crud->with(); // eager load relationships
        // $this->crud->orderBy();
        // $this->crud->groupBy();
        // $this->crud->limit();
    }



    public function storeNieuwsbrief (Request $request)
        {

            $data = $this->Form_To_Array($request->all());




            $storenieuwsbrief = new Nieuwsbrief();
            $storenieuwsbrief->naam = $request->naam;
            $storenieuwsbrief->email = $request->email;
            $storenieuwsbrief->save();

            $saved = $storenieuwsbrief->save();

            if (!$saved) {


            } else {


                Mail::to("$storenieuwsbrief->email")
                    ->bcc('info@lorandomorini.nl')
                    ->send(new NieuwsbriefCreated($data));


                return view('pages.geluktNieuwsbrief');

            }
        }


    public function store(StoreRequest $request)

    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }



    private function Form_To_Array($arr){
        if ( !is_array($arr) ) return false;
        else {
            $return = array();

            foreach ($arr as $key => $value) {
                // TRY to decode $value to array
                $decoded_value = json_decode($value, true);

                // Check if Decoding Json worked without errorsg
                if ( json_last_error() == JSON_ERROR_NONE)
                    // $value is a Json. Put it in $return
                    $return[$key] = $decoded_value;
                else
                    // value is not a Json. Do nothing.
                    $return[$key] = $value;
            }

            return $return;
        }
    }
}
