<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ProductsRequest as StoreRequest;
use App\Http\Requests\ProductsRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class ProductsCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ProductsCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Products');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/Products');
        $this->crud->setEntityNameStrings('Product', 'Producten');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
//        $this->crud->setFromDb();

        // add asterisk for fields that are required in ProductsRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');

        $this->crud->addColumn(
            [ // image
                'label' => "id",
                'name' => "id",
                'type' => 'text',

            ]);
        $this->crud->addColumn(
            [ // image
                'label' => "Ean",
                'name' => "ean",
                'type' => 'text',
                'upload' => true,

            ]);

        $this->crud->addColumn(
            [ // image
                'label' => "Artikelnummer",
                'name' => "artnr",
                'type' => 'text',
                'upload' => true,

            ]);

        $this->crud->addColumn(
            [ // image
                'label' => "Naam",
                'name' => "name",
                'type' => 'text',

            ]);

        $this->crud->addColumn(
            [
                'label' => "Prijs",
                'name' => "price",
                'type' => 'text',

            ]);



        $this->crud->addField(
            [
                'label' => "EAN",
                'name' => "ean",
                'type' => 'text',

            ]);

        $this->crud->addField(
            [ // image
                'label' => "Naam",
                'name' => "name",
                'type' => 'text',

            ]);

        $this->crud->addField(
            [ // image
                'label' => "Artikelnummer",
                'name' => "artnr",
                'type' => 'text',

            ]);

        $this->crud->addField(
            [ // image
                'label' => "Voorraad",
                'name' => "stock",
                'type' => 'text',

            ]);


        $this->crud->addField([ // select_from_array
            'name' =>  "maincat",
            'label' => "Hoofdcategorie",
            'type' => 'select_from_array',
            'options' => ['Koffie' => 'Koffie', 'Servies' => 'Servies'],
            'allows_null' => false,
        ]);

        $this->crud->addField([ // select_from_array
            'name' =>  "maincat_url",
            'label' => "Hoofdcategorie URL",
            'type' => 'select_from_array',
            'options' => ['Koffie' => 'Koffie', 'Servies' => 'Servies'],
            'allows_null' => false,
        ]);

        $this->crud->addField(
            [ // image
                'label' => "Subcategorie",
                'name' => "subcat",
                'type' => 'text',

            ]);

        $this->crud->addField(
            [ // image
                'label' => "Prijs",
                'name' => "price",
                'type' => 'text',

            ]);

        $this->crud->addField(
            [ // image
                'label' => "Beschrijving",
                'name' => "description",
                'type' => 'wysiwyg',

            ]);

      
        $this->crud->addField([ // select_from_array
            'name' =>  "weight",
            'label' => "Gewicht",
            'type' => 'select_from_array',
            'options' => ['250 gram' => '250 gram', '500 gram' => '500 gram', '1 kilo' => '1 kilo', '2 kilo' => '2 kilo', '3 kilo' => '3 kilo', '4 kilo' => '4 kilo', '5 kilo' => '5 kilo'],
            'allows_null' => false,
        ]);

        $this->crud->addField(
           [ // image
                'label' => "Image",
               'name' => "image_1",
                'type' => 'image',
                'upload' => true,

            ]);

       $this->crud->addField(
            [
                'name'=> 'image_2',
                'label'=> 'Image 2',
                'type'=> 'image'
            ]
        );        $this->crud->addField(
            [
                'name'=> 'image_3',
                'label'=> 'Image 3',
                'type'=> 'image'
            ]
        );        $this->crud->addField(
            [
                'name'=> 'image_4',
                'label'=> 'Image 4',
                'type'=> 'image'
            ]
        );        $this->crud->addField(
            [
                'name'=> 'image_5',
                'label'=> 'Image 5',
                'type'=> 'image'
            ]
        );
        $this->crud->addField(
          [
              'name'=> 'visible',
              'label'=> 'Tonen op website',
              'type'=> 'checkbox'
          ]
        );
        $this->crud->addField(
            [ // image
                'label' => "id van gerelateerde product",
                'name' => "related_product",
                'type' => 'text',

            ]);
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
