<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

use Carbon\Carbon;
use App\Models\Category;
use App\Models\Products;
use Session;



class GetIndexController extends Controller
{

    public function getIndex($page)
    {
        switch ($page){



            case 'privacy':{

                $viewpage = 'pages.'.$page;
                $this->viewdata = array(


                );
            }; break;

            case 'voorwaarden':{

                $viewpage = 'pages.'.$page;
                $this->viewdata = array(


                );
            }; break;

            case 'account-password-recovery':{

                $viewpage = 'pages.account'.$page;
                $this->viewdata = array(

//                    'categorie' => Category::all(),

                );
            }; break;

//

        };

        return view($viewpage, $this->viewdata);

    }



        /*
         * Loop through $arr. Identify values that are Json, and convert it to an array again.
         * @param $arr Array()
         * @return array()
         */
    private function Form_To_Array($arr){
        if ( !is_array($arr) ) return false;
        else {
            $return = array();

            foreach ($arr as $key => $value) {
                // TRY to decode $value to array
                $decoded_value = json_decode($value, true);

                // Check if Decoding Json worked without errorsg
                if ( json_last_error() == JSON_ERROR_NONE)
                    // $value is a Json. Put it in $return
                    $return[$key] = $decoded_value;
                else
                    // value is not a Json. Do nothing.
                    $return[$key] = $value;
            }

            return $return;
        }
    }

};

