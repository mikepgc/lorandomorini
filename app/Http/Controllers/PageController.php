<?php

namespace App\Http\Controllers;

use App\Models\Slider;
use App\Models\Topcategory;
use App\Models\Users;
use Session;
use Jenssegers\Agent\Agent;
use App\Models\Orders;
use Auth;
use App\Models\Stockupdate;





class PageController extends Controller
{

    public function updateStatus(Request $request)
    {
        $switch = Users::findOrFail('1');
        $switch->switch = $request->switch;
        $switch->save();

        return response()->json(['message' => 'Switch status updated successfully.']);
    }


    public function home()
    {

        $agent = new Agent();
        $this->viewdata = array(


            'agent' => $agent,
            'sliders' => Slider::all(),
            'topcategories' => Topcategory::all(),


    );

//        if(!env('TEST_ENV'))
//        return view('pages.underconstruction', $this->viewdata, compact('agent'));
//        else
        return view('pages.home', $this->viewdata, compact('agent'));

    }


    public function aboutus()
    {

        $this->viewdata = array(




        );
//        if(!env('TEST_ENV'))
//        return view('pages.underconstruction', $this->viewdata, compact('agent'));
//        else
        return view('pages.about_us', $this->viewdata);

    }

    public function gallery()
    {

        $this->viewdata = array(


        );

        return view('pages.gallery', $this->viewdata);

    }

    public function b2b()
    {

        $this->viewdata = array(


        );

        return view('pages.b2b', $this->viewdata);

    }




//    public function home2()
//    {
//
//        $agent = new Agent();
//        $this->viewdata = array(
//
//
//            'agent' => $agent,
//            'sliders' => Slider::all(),
//            'topcategories' => Topcategory::all(),
//
//        );
//        return view('pages.home', $this->viewdata, compact('agent'));
//    }
//




}
