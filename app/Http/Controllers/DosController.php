<?php

namespace App\Http\Controllers;

use App\Models\Factuurnummer;
use App\Models\Reservations;
use App\Models\Users;
use Illuminate\Http\Request;
use App\Models\Orders;
use Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Session;
use App\Mail\Orderconfirmation;
use App\Mail\Orderconfirmed;
use Mollie\Laravel\Facades\Mollie;
use Illuminate\Support\Facades\Redirect;
use App\Mail\CancelReservation;
use App\Mail\Reservation;
use App\Mail\FinalReservation;



class DosController extends Controller
{

    public function controlpanel() {

        $date = \Carbon\Carbon::today()->subDays(5);
        $Last7days = Reservations::where('datum', '>=', $date)->get();

        $date1 = \Carbon\Carbon::today()->subDays(-1);
        $day = Reservations::where('datum', '>=', $date1)->get();

        $orders = Orders::all();
        $reservations = Reservations::all();


        return view('pages.dos', [
            'reservations' => $reservations,
            'reserveringen7' => $Last7days,
            'reserveringen1' => $day,
            'orders' => $orders,

            ]);

    }


    public function goedkeuring(Request $request)
    {

        $data = Reservations::find($request->id);
        $email = $data['email'];
        $naam = $data['naam'];

        $data->goedkeuring = 1;
        $data->update();
//        $bevestiging = DB::table('emailBCC')->first()->email;
        $bevestiging = 'info@caffeitalia.nl';
//        $bevestiging2 = DB::table('emailBCC')->first()->email2;
//        $bevestiging2 = 'bigyanbhattarai@hotmail.com';

        Mail::to("$email")
//            ->bcc(["$bevestiging", "$bevestiging2"])
//             ->bcc("$bevestiging")



            ->send(new FinalReservation(array(

                        'naam' => $data['naam'],
                        'personen' => $data['personen'],
                        'datum' => $data['datum'],
                        'tijd' => $data['tijd'],
                        'email' => $data['email'],
                        'telefoon' => $data['telefoon'],
                        'opmerking' => $data['opmerking'],


                    )


                )
            );

        \Slack::to('#caffeitalia')->send("$naam goedgekeurd");



        return redirect()->back()->with('message', 'gelukt');
    }

    public function afkeuring(Request $request)
    {

        $data = Reservations::find($request->id);
        $email = $data['email'];
//        $bevestiging = DB::table('emailBCC')->first()->email;
//        $bevestiging = 'info@caffeitalia.nl';
//        $bevestiging2 = DB::table('emailBCC')->first()->email2;
//        $bevestiging2 = 'bigyanbhattarai@hotmail.com';

        $naam = $data['naam'];



        $data->goedkeuring = 2;
        $data->update();

        Mail::to("$email")
//            ->bcc(["$bevestiging", "$bevestiging2"])
//             ->bcc("$bevestiging")



            ->send(new CancelReservation(array(

                        'naam' => $data['naam'],
                        'personen' => $data['personen'],
                        'datum' => $data['datum'],
                        'tijd' => $data['tijd'],
                        'email' => $data['email'],
                        'telefoon' => $data['telefoon'],
                        'opmerking' => $data['opmerking'],

                    )
                )
            );

        \Slack::to('#caffeitalia')->send("$naam afgekeurd");


        return redirect()->back()->with('message', 'gelukt');
    }
    public function orders()
    {

        $date = \Carbon\Carbon::today()->subDays(5);
        $Last7days = Reservations::where('datum', '>=', $date)->get();

        $date1 = \Carbon\Carbon::today()->subDays(-1);
        $day = Reservations::where('datum', '>=', $date1)->get();

        $orders = Orders::all();
        $reservations = Reservations::all();

        $users = Users::all();

        return view('pages.dos', [
            'reserveringen' => $reservations,
            'reserveringen7' => $Last7days,
            'reserveringen1' => $day,
            'order' => $orders,
            'users' => $users,

        ]);

    }

    public function getVip(Request $request)
    {

        Users::where('id', $request->id)->update([
            'vip' => '1',

        ]);


        return redirect()->back()->with('alert2', 'VIP opgeslagen');

    }
    public function PDFgenerator($order) {

        $ordernummer = $order;

        $orderId = Orders::where('ordernumber', $ordernummer)->pluck('ordernumber')->first();
        $orderdata = Orders::where('ordernumber', $ordernummer)->pluck('orderdata')->first();
        $ordertotal = Orders::where('ordernumber', $ordernummer)->pluck('ordertotal')->first();
        $shippingcost = Orders::where('ordernumber', $ordernummer)->pluck('shipping_cost')->first();
        $userId= Orders::where('ordernumber', $ordernummer)->pluck('user_id')->first();
        $userEmail = Orders::where('ordernumber', $ordernummer)->pluck('user_email')->first();
        $orderDate = Orders::where('ordernumber', $ordernummer)->pluck('created_at')->first();
        $userName = Users::where('id' , $userId)->pluck('name')->first();
        $surName = Users::where('id' , $userId)->pluck('surname')->first();
        $companyName = Users::where('id' , $userId)->pluck('companyname')->first();
        $company = Users::where('id' , $userId)->pluck('company')->first();
        $postcode = Users::where('id' , $userId)->pluck('postcode')->first();
        $housenumber = Users::where('id' , $userId)->pluck('housenumber')->first();
        $housenumber2 = Users::where('id' , $userId)->pluck('housenumber2')->first();
        $address = Users::where('id' , $userId)->pluck('address')->first();
        $city = Users::where('id' , $userId)->pluck('city')->first();
        $shippingPostcode = Users::where('id' , $userId)->pluck('shipping_postcode')->first();
        $shippinghousenumber = Users::where('id' , $userId)->pluck('shipping_housenumber')->first();
        $shippinghousenumber2 = Users::where('id' , $userId)->pluck('shipping_housenumber2')->first();
        $shippingAddress = Users::where('id' , $userId)->pluck('shipping_address')->first();
        $shippingCity = Users::where('id' , $userId)->pluck('shipping_city')->first();
        $factuurnummer = Factuurnummer::select('factuur_nummer', 'id')->orderBy('id', 'desc')->pluck('factuur_nummer')->first();
        $currentDate = date('d-m-Y');

        Orders::where('ordernumber', $orderId)->update([
            'order_sent' => '1'
        ]);


        $data = array(
            'orderId' => $orderId,
            'orderdata' => unserialize($orderdata),
            'ordertotal' => $ordertotal,
            'shippingcost' => $shippingcost,
            'userId' => $userId,
            'userEmail' => $userEmail,
            'userName' => $userName,
            'surName' => $surName,
            'companyName' => $companyName,
            'company' => $company,
            'postcode' => $postcode,
            'housenumber' => $housenumber,
            'housenumber2' => $housenumber2,
            'address' => $address,
            'city' => $city,
            'shippingPostcode' => $shippingPostcode ,
            'shippinghousenumber' => $shippinghousenumber,
            'shippinghousenumber2' => $shippinghousenumber2 ,
            'shippingAddress' => $shippingAddress,
            'shippingCity' => $shippingCity,
            'Orderdate' => $orderDate,
            'factuurnummer' => $factuurnummer,
            'currentDate' => $currentDate


        );

        $pdf = \App::make('snappy.pdf.wrapper');

        // below filename needs to be matched with Mail/Orderconfirmation.php line 38

        $fileName = 'Factuur-'.$factuurnummer.'.pdf';

        $pdf = $pdf->loadView('invoices.invoice' , $data)->output();

        // increase factuurnumber by 1
        $orderId1 = Factuurnummer::latest('id')->pluck('factuur_nummer')->first() + 1;

//        save factuurnummer to factuurnummer table with increased  factuurnummer
        $save_ordersincr = new Factuurnummer();
        $save_ordersincr->factuur_nummer = $orderId1;
        $save_ordersincr->save();

        Storage::disk('invoices')->put($fileName, $pdf);

// als test_env op true staat, dan kan je op lorandomorini.nl/pdf de pdf genereren zonder het naar klant te sturen

        if(env('TEST_ENV') == TRUE) {
            Mail::to('info@jae-it.nl')

                ->send(new Orderconfirmed($data

                ));
        }
        else {
            Mail::to($userEmail)
                ->bcc('info@lorandomorini.nl')

                ->send(new Orderconfirmed($data

                ));
        }



        return redirect()->route('dos');



    }


}
