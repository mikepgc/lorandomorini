<?php

namespace App\Http\Controllers;

use App\Models\LM;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Products;
use Session;
use App\Models\Cart;
use App\Models\Category;
use App\Models\Orders_increment;
use App\Models\Orders;
use Illuminate\Support\Facades\URL;
use Jenssegers\Agent\Agent;
use Illuminate\Support\Facades\DB;





class ProductController extends Controller
{


    public function getCategoryMenu(\Illuminate\Http\Request $request)
    {
        $categories = LM::groupBy('maincat')->get();

        $subcategories = LM::groupBy('subcat')->get();

        return view('category.categoryall', ['categories' => $categories, 'subcategories' => $subcategories]);

    }

    public function getContact()
    {
        $categories = LM::groupBy('maincat')->get();

        $subcategories = LM::groupBy('subcat')->get();

        return view('pages.contacts', ['categories' => $categories, 'subcategories' => $subcategories]);

    }
    public function getCategory($page)
    {
        $category = LM::select('id', 'maincat','maincat_url', 'subcat', 'maincat', 'subcat','description' , 'stock', 'artnr', 'image_1', 'price')
            ->groupBy('maincat_url')
            ->get($page);

        $categories = LM::groupBy('maincat')->get();

        $subcategories = LM::groupBy('subcat')->get();



        return view('category.category', ['categories' => $categories, 'subcategories' => $subcategories, 'category' => $category]);
    }
    public function getCategoryall()
    {
//        $products = Products::all();
        $category = Category::select('id' , 'category', 'sub_category', 'category_url', 'sub_category_url')
            ->groupBy('category')
            ->get();

        $Subcategory = Category::select('id' , 'category', 'sub_category', 'sub_category_url')
            ->groupBy('sub_category')
            ->get();




        return view('category.categoryall', ['category' => $category, 'Subcategory' => $Subcategory]);
    }
    public function getSubCategoryGrid($page)
    {

        $categories = LM::groupBy('maincat')->get();

        $subcategories = LM::where('maincat', 'LIKE', '%' . $page . '%')->paginate(18);

        $gridUrl = URL::to('/').'/category/subcategory-grid/'.\Request::segment(3);
        $listUrl = URL::to('/').'/category/subcategory-list/'.\Request::segment(3);
        return view('category.subcategory-grid', ['categories' => $categories, 'subcategories' => $subcategories, 'page' => $page,'gridUrl'=>$gridUrl,'listUrl'=>$listUrl]);
    }
    public function getFilterProduct(Request $request)
    {
        $categories = LM::groupBy('maincat')->get();
        $products = LM::select('*');
        if($request->has('filterPriceMin') && $request->has('filterPriceMax')){
            $products->whereBetween('price', [(int) $request->get('filterPriceMin'), (int) $request->get('filterPriceMax')]);
        }
        if($request->get('subcat')){
            $products->whereIn('subcat', $request->get('subcat'));
        }

        $productsList = $products->get();
        $queries = \DB::getQueryLog();
        $gridUrl = route('pages.filter-product-grid',$request->all());
        $listUrl = route('pages.filter-product-list',$request->all());
        if($request->segment(2) == 'filter-grid'){
            $view = 'category.subcategory-grid';
        }else{
            $view = 'category.subcategory-list';
        }
        return view($view, ['categories' => $categories, 'subcategories' => $productsList,'gridUrl'=>$gridUrl,'listUrl'=>$listUrl]);
    }

    public function getSearch($page)
    {
//        $products = Products::all();
        $grid = LM::select('id', 'maincat', 'subcat', 'description' , 'stock', 'artnr', 'image_1', 'price')
            ->groupBy('maincat')
            ->get();

        $categories = LM::groupBy('maincat')->get();

        $subcategories = LM::all();


        if ($page <> null) {

        $itemsQ = LM::select('id',  'maincat', 'subcat','description' , 'stock', 'artnr', 'image_1', 'price', 'ean', 'maincat_url', 'name', 'artnr')
            ->where(function ($query) use ($page) {


        $query
            ->where('ean', 'LIKE', '%' . $page . '%')
            ->OrWhere('artnr', 'LIKE', '%' . $page . '%')
            ->OrWhere('name', 'LIKE', '%' . $page . '%')
            ->OrWhere('maincat', 'LIKE', '%' . $page . '%')
            ->OrWhere('subcat', 'LIKE', '%' . $page . '%')
            ->OrWhere('ean', 'LIKE', '%' . $page . '%');
    });


           if(request()->sort){
               $itemsQ->orderBy(request()->sort, request()->direction);
           }

            $items = $itemsQ->paginate(9);


       } else {
            $items = null;

            }


        $lm = ( 'maincat_url');



        return view('category.search', ['item' => $items, 'subcategories' => $subcategories, 'grid' => $grid, 'lm' => $lm, 'page' => $page, 'categories' => $categories]);
    }

    public function getSubCategoryList($page)
    {
        $categories = LM::groupBy('maincat')->get();

        $subcategories = LM::where('maincat', 'LIKE', '%' . $page . '%')->paginate(6);

        $gridUrl = URL::to('/').'/category/subcategory-grid/'.\Request::segment(3);
        $listUrl = URL::to('/').'/category/subcategory-list/'.\Request::segment(3);

        return view('category.subcategory-list', [ 'categories' => $categories, 'subcategories' => $subcategories,'gridUrl'=>$gridUrl,'listUrl'=>$listUrl]);
    }
    public function getIndex()
    {
//        $products = Products::all();
        $category = Category::select('id' , 'category', 'sub_category', 'category_url')
        ->groupBy('category')
        ->get();

        $Subcategory = Category::select('id' , 'category', 'sub_category')
        ->groupBy('sub_category')
        ->get();



        $products = Products::select('benel.*', 'category_url', 'category', 'sub_category')
            ->join('category','category.web_category', '=' , 'benel.category_NL')
            ->groupBy('benel.category_NL')
            ->paginate(15);


        return view('pages.shop-grid-ls', ['products' => $products, 'category' => $category, 'Subcategory' => $Subcategory]);
    }



    public function getIndex2()
    {
//        $products = Products::all();
        $category = Category::select('id' , 'category', 'sub_category', 'category_url')
            ->groupBy('category')
            ->get();

        $Subcategory = Category::select('id' , 'category', 'sub_category')
            ->groupBy('sub_category')
            ->get();

//        var_dump($Subcategory); exit;


        $products = Products::select('benel.*', 'category_url', 'category', 'sub_category')
            ->join('category','category.web_category', '=' , 'benel.category_NL')
            ->groupBy('benel.category_NL')
            ->paginate(15);

//
//        $url = Category::pluck('category_url')->get();
//
//dd($key);



        return view('pages.shop-list-ls', ['products' => $products, 'category' => $category, 'Subcategory' => $Subcategory]);
    }

    public function viewProduct($page)
    {
        $productId = LM::all()
            ->sortBy('artnr')
            ->where('artnr' , $page)
            ->first();
        $relatedProduct = [];
        if(isset($productId->related_product) && $productId->related_product != '' && $productId->related_product != null){
            $relatedProduct = LM::find($productId->related_product);
        }
//dd(explode('–',$relatedProduct->name));
        $subart = LM::all()
            ->where('artnr' , $page)
            ->pluck('artnr');

//        dd($subart); exit;

        $categories = LM::groupBy('maincat')->get();



//        dd($productId);exit;
        return view('pages.single-product', ['subart' => $subart, 'productId' => $productId, 'categories' => $categories,'relatedProduct'=>$relatedProduct] );
    }
//
//    Public function postAddToCart(Request $request) {
//        $id = $request->input('id');
//        $product = Products::find($id);
//        $oldCart = $request->Session()->has('cart') ? $request->Session()->get('cart') : null;
//        $cart = new Cart($oldCart);
//        $cart->add($product, $product->id);
//
//        $request->session()->put('cart', $cart);
//        dd($request->session()->get('cart'));
//        return redirect("shop-grid-ns");
//        return response()->json([
//            'success' => 'true',
//        ]);
//
//    }


    /**
     * Add products to the Cart
     * @param   string $productId (?)
     * @return  string // HTML content of confirmation message
     */

    public function getAddToCart(Request $request,$id = null)
    {
        if($request->ajax()){
            $response = [];
            $id = $request->get('product');
            $qty = $request->get('qty');
            $product = LM::find($id);
            $product->qty = $qty;
            $oldCart = $request->Session()->has('cart') ? $request->Session()->get('cart') : null;
            $cart = new Cart($oldCart);
            $cart->add($product, $product->id);
            $request->session()->put('cart', $cart);
            $cartData = $this->getMiniCartView();
            $response['cartData'] = $cartData;
            return response()->json([
                'success' => 'true',
                'response'=> $response
            ]);
        }else{
            //        $id = $request->input('id');
            $product = LM::find($id);
            $oldCart = $request->Session()->has('cart') ? $request->Session()->get('cart') : null;
            $cart = new Cart($oldCart);
            $cart->add($product, $product->id);

            $request->session()->put('cart', $cart);
    //        dd($request->session()->get('cart', $cart));
            return redirect()->back();
            return response()->json([
                'success' => 'true',
            ]);
        }

    }

    public function getCart(request $request) {
        if (!Session::has('cart')) {
            return view('pages.shoppingcart', ['products' => null]);
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
//
        $agent = new Agent();
//
//        $total = $cart->items;
//
//        dd($total['item']['gross_incbtw']); exit;


        $AllSessionData = $request->session()->all();

//        $test = \App\Governor::$productsCart;
//
//        dd($AllSessionData);
        return view('pages.shoppingcart', ['products' => $cart->items, 'totalPrice' => $cart->totalPrice, 'agent' => $agent]);

    }


    public function destroyCart(){
        if(Session::has('cart')){
            Session::forget('cart');
        }
        return redirect()->route('shoppingcart');
    }
    public function getReduceByOne($id) {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->reduceByOne($id);
        if (count($cart->items) > 0) {
            Session::put('cart', $cart);
        } else {
            Session::forget('cart');
        }
        return redirect()->route('shoppingcart');
    }

    public function changeProductQty(request $request) {
        $response = [];
        $id = $request->get('product');
        $qty = $request->get('qty');

        $product = LM::find($id);
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->changeProductQty($product,$qty);
        if (count($cart->items) > 0) {
            Session::put('cart', $cart);
        } else {
            Session::forget('cart');
        }
        $cartData = $this->getMiniCartView();
        $response['cartData'] = $cartData;
        $newCart = Session::has('cart') ? Session::get('cart') : null;
        $newCartData = $newCart->items[$product->id];
        $subTotal = $newCart->totalPrice;
        return response()->json([
            'success' => 'true',
            'response'=> $response,
            'cartData'=> $newCartData,
            'cartSubtotal'=>$subTotal
        ]);
    }

    public function getIncreaseByOne($id) {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;

        $cart = new Cart($oldCart);
        $cart->increaseByOne($id);
        if (count($cart->items) > 0) {
            Session::put('cart', $cart);
        } else {
            Session::forget('cart');
        }
        return redirect()->route('shoppingcart');
    }

    public function getRemoveItem($id) {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->removeItem($id);
        if (count($cart->items) > 0) {
            Session::put('cart', $cart);
        } else {
            Session::forget('cart');
        }
        return redirect()->route('shoppingcart');
    }



    public function getMiniCartView() {

        $view =  view('layouts.minicart')->render();
        return $view;
    }

    public function getCheckout() {
        if (!Session::has('cart')) {
            return view('shop.winkelwagen');
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        $total = $cart->totalPrice;
        return view('shop.checkout', ['total' => $total]);
    }

    public function postCheckout(Request $request)
    {
        if (!Session::has('cart')) {
            return redirect()->route('shop.winkelwagen');
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        $total = $cart->totalPrice;

//        make ordernumber increase by one
        $orderId1 = Orders_increment::latest('id')->pluck('ordernr')->first() + 1;

//        save ordernr to ordersincrementtable
        $save_ordersincr = new Orders_increment;
        $save_ordersincr->ordernr = $orderId1;
        $save_ordersincr->save();

//        generate ordernumber
        $orderId = 'JES-' . $orderId1 ;


        $payment = mollie()->payments()->create([
            'amount' => [
                'currency' => 'EUR',
                'value' => (string) number_format($total, 2, '.', ''), // You must send the correct number of decimals, thus we enforce the use of strings
            ],
            'description' => 'Lorando&Morini betaling order #' . $orderId,
            'webhookUrl' => url('/example/webhook'),
            'redirectUrl' => url('/checkout-complete/' . $orderId),
        ]);
        Orders::where('id', $orderId)->update([
            'mollie_id' => $payment->id,
        ]);
        $payment = mollie()->payments()->get($payment->id);
//        return response()->json([
//            'orderId' => $orderId,
//            'paymentUrl' => $payment->getCheckoutUrl()
//        ]);
        // redirect customer to Mollie checkout page
        return redirect($payment->getCheckoutUrl(), 303);




    }



}
