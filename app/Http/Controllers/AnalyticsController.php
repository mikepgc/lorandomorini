<?php

namespace App\Http\Controllers;

use Backpack\PageManager\app\Models\Page;
use Analytics;
use Spatie\Analytics\Period;
use Carbon\Carbon;
use App\Mail\Statistics;
use Illuminate\Support\Facades\DB;
use App\Models\AnalyticsStore;



class AnalyticsController extends Controller
{

    public function storeAnalytics()
    {

        $today = Carbon::now();

        $analyticsData7 = Analytics::fetchTotalVisitorsAndPageViews(Period::create($today, $today));
//        dd($analyticsData7);exit;

        $analytics = new AnalyticsStore();
        $analytics->visitors = $analyticsData7['0']['visitors'];
        $analytics->pagesviews = $analyticsData7['0']['pageViews'];
        $analytics->date = $analyticsData7['0']['date'];
        $analytics->save();

        echo('Analytics opgeslagen!!');


    }

    public function MailStatistics(\Illuminate\Http\Request $request)
    {

        $startDate = $request->startDate ? Carbon::parse($request->startDate) : Carbon::now()->startOfMonth();
        $endDate = $request->endDate ? Carbon::parse($request->endDate) : Carbon::now()->addDays(1);

           $analyticsData = Analytics::fetchVisitorsAndPageViews(Period::create($startDate, $endDate));


        Mail::to("mikepgc@hotmail.com")

            ->send(new Statistics(

                $analyticsData

                )
            );

    }

    public function pageviews(\Illuminate\Http\Request $request)
    {
        /*$analyticsData = Analytics::fetchVisitorsAndPageVDD($analyticsData);views(Period::days(7));*/
        $startDate = $request->startDate ? Carbon::parse($request->startDate) : Carbon::now()->startOfMonth();
        $endDate = $request->endDate ? Carbon::parse($request->endDate) : Carbon::now()->addDays(1);

        $startDate = $request->startDate ? Carbon::parse($request->startDate) : Carbon::now()->startOfMonth();
        $startWeek = $request->startWeek ? Carbon::parse($request->startWeek) : Carbon::now()->startOfWeek();
        $endWeek = $request->endWeek ? Carbon::parse($request->endWeek) : Carbon::now()->endOfWeek();

        $vandaagstart = Carbon::now()->startOfDay();
        $vandaagend = Carbon::now()->addDays(1);

        $today = Carbon::now();

        $analyticsData = Analytics::fetchVisitorsAndPageViews(Period::create($startDate, $endDate));
        $analyticsData2 = Analytics::fetchTopBrowsers(Period::create($startDate, $endDate));
        $analyticsData3 = Analytics::performQuery(
            Period::create($startDate, $endDate),
            'ga:sessions',
            [
                'metrics' => 'ga:entrances,ga:bounces',
                'dimensions' => 'ga:landingPagePath',
                'sort'=>'-ga:entrances',
            ]
        );
        $analyticsData4 = Analytics::fetchTopReferrers(Period::create($startDate, $endDate));
        $analyticsData5 = Analytics::fetchUserTypes(Period::create($startDate, $endDate));
        $analyticsData6 = Analytics::fetchTotalVisitorsAndPageViews(Period::create($startDate, $endDate));
        $analyticsData7 = Analytics::fetchTotalVisitorsAndPageViews(Period::create($today, $today));

        $analyticsData14 = Analytics::performQuery(
            Period::create($today, $today),
            'ga:sessions',
            [
                'metrics' => 'ga:sessions,ga:pageviews, ga:bounces',
            ]
        );

        // Bouncerate
        $analyticsData8 = Analytics::performQuery(
            Period::create($startDate, $endDate),
            'ga:sessions',
            [
                'metrics' => 'ga:sessions, ga:bounces, ga:bounceRate',
                'dimensions' => 'ga:yearMonth'
            ]
        );

        // Sessions by Country
        $analyticsData9 = Analytics::performQuery(
            Period::create($startDate, $endDate),
            'ga:sessions',
            [
                'metrics' => 'ga:sessions',
                'dimensions' => 'ga:country',
                'sort'=>'-ga:sessions',
            ]
        );

        // Top Exit Pages
        $analyticsData10 = Analytics::performQuery(
            Period::create($startDate, $endDate),
            'ga:sessions',
            [
                'dimensions' => 'ga:exitPagePath',
                'metrics' => 'ga:exits,ga:pageviews',
                'sort'=>'-ga:exits',
            ]
        );

        // Device Category
        $analyticsData11 = Analytics::performQuery(
            Period::create($startDate, $endDate),
            'ga:sessions',
            [
                'dimensions' => 'ga:deviceCategory',
                'metrics' => 'ga:sessions,ga:bounceRate',
                'sort'=>'-ga:sessions',
            ]
        );

        // Info mobiel apparaat
        $analyticsData12 = Analytics::performQuery(
            Period::create($startDate, $endDate),
            'ga:sessions',
            [
                'dimensions' => 'ga:mobileDeviceInfo,ga:source',
                'metrics' => 'ga:sessions,ga:pageviews',
                'sort'=>'-ga:sessions',
            ]
        );

        // Total Sessies & pageviews
        $analyticsData13 = Analytics::performQuery(
            Period::create($startDate, $endDate),
            'ga:sessions',
            [
                'metrics' => 'ga:sessions,ga:pageviews',
            ]
        );




        return view('pages.dashboard',[
            'analyticsData' => $analyticsData,
            'analyticsData2'=> $analyticsData2,
            'analyticsData3'=> $analyticsData3,
            'analyticsData4'=> $analyticsData4,
            'analyticsData5'=> $analyticsData5,
            'analyticsData6'=> $analyticsData6,
            'analyticsData7'=> $analyticsData7,
            'analyticsData8'=> $analyticsData8,
            'analyticsData9'=> $analyticsData9,
            'analyticsData10'=> $analyticsData10,
            'analyticsData11'=> $analyticsData11,
            'analyticsData12'=> $analyticsData12,
            'analyticsData13'=> $analyticsData13,
            'analyticsData14'=> $analyticsData14,
            'startdate' => $startDate,
            'enddate' => $endDate,
            'start' => $startDate,
            'end' => $endDate,
            'startweek'=>$startWeek,
            'endweek'=>$endWeek,




        ]);

    }


}