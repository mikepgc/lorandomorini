<?php

namespace App\Http\Controllers;

use App\Mail\BenelStockStarted;
use App\Models\Factuurnummer;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Products;
use Illuminate\Support\Facades\Mail;
use Session;
use App\Models\Cart;
use App\Models\Users;
use App\Models\Category;
use App\Models\Orders_increment;
use App\Models\Orders;
use App\Mail\Orderconfirmation;
use App\Mail\OrderconfirmationFailed;
use Auth;
use Mollie\Laravel\Facades\Mollie;




class    OrderController extends Controller
{

    public function getCheckoutAddress(Request $request) {


        return view('checkout.checkout-address');
    }

    public function updateCheckoutAddress(Request $request)
    {


        if($request->input('password')) {

            $account = 1;
        }
        else {
            $account = 0;
        }

        if (!Auth::check()){

            $user = new Users([
                'name' => $request->input('name'),
                'surname' => $request->input('surname'),
                'company' => $request->input('company'),
                'person' => $request->input('person'),
                'btw' => $request->input('btw'),
                'companyname' => $request->input('companyname'),
                'email' => $request->input('email'),
                'phone' => $request->input('phone'),
                'postcode' => $request->input('postcode'),
                'housenumber' => $request->input('housenumber'),
                'housenumber2' => $request->input('housenumber2'),
                'address' => $request->input('address'),
                'same_address' => $request->input('same_address'),
                'city' => $request->input('city'),
                'shipping_postcode'=> $request->input('sh-postcode'),
                'shipping_housenumber'=> $request->input('sh-housenumber'),
                'shipping_housenumber2'=> $request->input('sh-housenumber2'),
                'shipping_address'=> $request->input('sh-address'),
                'shipping_city'=> $request->input('sh-city'),
                'password' => bcrypt($request->input('password')),
                'account' => $account,


            ]);

        $user->save();


        Auth::login($user);
        return redirect('checkout-afronding');
    }

        if (Auth::check()) {

            \DB::table('users')->where('id', Auth::user()->id)->update([
                'name'=> $request->input('name'),
                'surname'=> $request->input('surname'),
                'company'=> $request->input('company'),
                'person'=> $request->input('person'),
                'btw'=> $request->input('btw'),
                'companyname'=> $request->input('companyname'),
                'email'=> $request->input('email'),
                'phone'=> $request->input('phone'),
                'postcode'=> $request->input('postcode'),
                'housenumber'=> $request->input('housenumber'),
                'housenumber2'=> $request->input('housenumber2'),
                'address'=> $request->input('address'),
                'same_address' => $request->input('same_address'),
                'city'=> $request->input('city'),
                'shipping_postcode'=> $request->input('sh-postcode'),
                'shipping_housenumber'=> $request->input('sh-housenumber'),
                'shipping_housenumber2'=> $request->input('sh-housenumber2'),
                'shipping_address'=> $request->input('sh-address'),
                'shipping_city'=> $request->input('sh-city'),
            ]);
            return redirect('checkout-afronding');
        }

    }



    public function getAfronding() {

        if (!Session::has('cart')) {
            return view('pages.shoppingcart', ['products' => null]);
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
//        $AllSessionData = $request->session()->all();
//
////        $test = \App\Governor::$productsCart;
////
//        dd($AllSessionData);
        return view('checkout.checkout-afronding', ['products' => $cart->items, 'totalPrice' => $cart->totalPrice]);
    }

    public function postCheckoutAddress(Request $request) {


        if(!Auth::check()) {

            $user = new Users([
                'name' => $request->input('name'),
                'surname' => $request->input('surname'),
                'email' => $request->input('email'),
                'phone' => $request->input('phone'),
                'password' => bcrypt($request->input('password')),
                'account' => $request->input('account'),
                'company' => $request->input('company'),
                'postcode' => $request->input('postcode'),
                'housenumber' => $request->input('housenumber'),
                'housenumber2' => $request->input('housenumber2'),
                'address' => $request->input('address'),
                'city' => $request->input('city'),
                'account' => $request->input('account'),

            ]);
            $user->save();


            Auth::login($user);


            if ($user) {

//                return response('gelukt!');
                return redirect('checkout-afronding');
            }
        }

        else{
            return redirect('checkout-afronding');


        }
}


    public function doOrder(Request $request ){
        {

            if (!Session::has('cart')) {
                return redirect()->route('shop.winkelwagen');
            }
            $oldCart = Session::get('cart');
            $cart = new Cart($oldCart);
            $total1 = $cart->totalPrice;
            if($total1 <= 30)
                $total2 = 6.50;
            else
                $total2 = 0;

            $normaaltotaal = $total1 + $total2;
            $kortingstotaal = $normaaltotaal - ($normaaltotaal * 0.1);

            if(Auth::user()->vip == '1'){

                $total = $kortingstotaal;

            }
            else{
                $total = $normaaltotaal;
            }




//        make ordernumber increase by one
            $orderId1 = Orders_increment::latest('id')->pluck('ordernr')->first() + 1;

//        save ordernr to ordersincrementtable
            $save_ordersincr = new Orders_increment;
            $save_ordersincr->ordernr = $orderId1;
            $save_ordersincr->save();

//        generate ordernumber
            $orderId = 'LM-' . $orderId1 ;


            $cartitems = array_values($cart->items);


            $payment = mollie()->payments()->create([
                'amount' => [
                    'currency' => 'EUR',
                    'value' => (string) number_format($total, 2, '.', ''), // You must send the correct number of decimals, thus we enforce the use of strings
                ],
//                'metadata' => json_encode($metadata),
                'description' => 'Lorando&Morini betaling order #' . $orderId,
                'webhookUrl' => url('/orderhook'),
                'redirectUrl' => url('/checkout-complete/' . $orderId),
            ]);
            Orders::where('id', $orderId)->update([
                'mollie_id' => $payment->id,
            ]);

                $save_order = new Orders;
                $save_order->ordernumber = $orderId;
                $save_order->orderdata = serialize($cartitems);
                $save_order->ordertotal = $total;
                $save_order->shipping_cost = $total2;
                $save_order->mollie_id = $payment->id;
                $save_order->user_id = Auth::user()->id;
                $save_order->user_email = Auth::user()->email;
                $save_order->save();

                $useremail = Auth::user()->email;

            \Slack::to('#lorando')->send("Order started!!!!! $orderId, $save_order->user_email");

//            var_dump(http_response_code(200));

                return redirect($payment->getCheckoutUrl(), 303);

        }

    }

    public function doHook(Request $request){
        if (! $request->has('id')) {
                return;
            }

    $payment = Mollie::api()->payments()->get($request->id);


// $payment = json_decode($payment->metadata());
// dd($payment);

    $paid = 0;

    if($payment->isPaid() && !$payment->hasRefunds() && !$payment->hasChargebacks()) {
        $paid = 1;

        Orders::where('mollie_id', $payment->id)->update([
            'payment' => $paid
        ]);


        $paymentsuccess = Orders::where('mollie_id', $payment->id)->pluck('payment')->first();
        $orderId = Orders::where('mollie_id', $payment->id)->pluck('ordernumber')->first();
        $orderdata = Orders::where('mollie_id', $payment->id)->pluck('orderdata')->first();
        $ordertotal = Orders::where('mollie_id', $payment->id)->pluck('ordertotal')->first();
        $shippingcost = Orders::where('mollie_id', $payment->id)->pluck('shipping_cost')->first();
        $userId= Orders::where('mollie_id', $payment->id)->pluck('user_id')->first();
        $userEmail = Orders::where('mollie_id', $payment->id)->pluck('user_email')->first();
        $userName = Users::where('id' , $userId)->pluck('name')->first();
        $surName = Users::where('id' , $userId)->pluck('surname')->first();
        $companyName = Users::where('id' , $userId)->pluck('companyname')->first();
        $company = Users::where('id' , $userId)->pluck('company')->first();
        $postcode = Users::where('id' , $userId)->pluck('postcode')->first();
        $housenumber = Users::where('id' , $userId)->pluck('housenumber')->first();
        $housenumber2 = Users::where('id' , $userId)->pluck('housenumber2')->first();
        $address = Users::where('id' , $userId)->pluck('address')->first();
        $city = Users::where('id' , $userId)->pluck('city')->first();
        $shippingPostcode = Users::where('id' , $userId)->pluck('shipping_postcode')->first();
        $shippinghousenumber = Users::where('id' , $userId)->pluck('shipping_housenumber')->first();
        $shippinghousenumber2 = Users::where('id' , $userId)->pluck('shipping_housenumber2')->first();
        $shippingAddress = Users::where('id' , $userId)->pluck('shipping_address')->first();
        $shippingCity = Users::where('id' , $userId)->pluck('shipping_city')->first();
        $factuurnummer = Factuurnummer::select('factuur_nummer', 'id')->orderBy('id', 'desc')->pluck('factuur_nummer')->first();
        $currentDate = date('d-m-Y');
        $emailsent = Orders::where('mollie_id', $payment->id)->pluck('order_sent')->first();



        $data = array(
            'factuurnummer' => $factuurnummer,
            'orderId' => $orderId,
            'orderdata' => unserialize($orderdata),
            'ordertotal' => $ordertotal,
            'shippingcost' => $shippingcost,
            'userId' => $userId,
            'userEmail' => $userEmail,
            'userName' => $userName,
            'surName' => $surName,
            'companyName' => $companyName,
            'company' => $company,
            'postcode' => $postcode,
            'housenumber' => $housenumber,
            'housenumber2' => $housenumber2,
            'address' => $address,
            'city' => $city,
            'shippingPostcode' => $shippingPostcode ,
            'shippinghousenumber' => $shippinghousenumber,
            'shippinghousenumber2' => $shippinghousenumber2 ,
            'shippingAddress' => $shippingAddress,
            'shippingCity' => $shippingCity,
            'currentDate' => $currentDate,
            'emailsent' => $emailsent,

        );


        if($paymentsuccess == 1) {

//            \Slack::to('#lorando')->send(" Order $orderId , $userName , $ordertotal PAID!!!!! ");

      // increase factuurnumber by 1
//            $orderId1 = Factuurnummer::latest('id')->pluck('factuur_nummer')->first() + 1;

     // save factuurnummer to factuurnummer table with increased  factuurnummer
//            $save_ordersincr = new Factuurnummer();
//            $save_ordersincr->factuur_nummer = $orderId1;
//            $save_ordersincr->save();
//
//            $pdf = \App::make('snappy.pdf.wrapper');

            // below filename needs to be matched with Mail/Orderconfirmation.php line 38

//            $fileName = 'Factuur-'.$factuurnummer.'.pdf';
//
//            $pdf = $pdf->loadView('invoices.invoice' , $data)->output();


            // is stored in /public/uploads/invoices
//            Storage::disk('invoices')->put($fileName, $pdf);

            // email if email is filled in

            if($userEmail)
            {

            //   if test_env is true, you can make testorder. dont forget set molliekey to test

                    if(env('TEST_ENV') == true) {

                            if(!$emailsent == '1' ) {
                                Mail::to($userEmail)->bcc('info@jae-it.nl')
                                    ->send(new Orderconfirmation($data

                                    ));
                                \Slack::to('#lorando')->send("**TEST** you have an TEST Order  $orderId , $userName , $ordertotal!!!!! ");

                                Orders::where('mollie_id', $payment->id)->update([
                                    'test' => 1
                                ]);
                                exit;
                            }
                            else {
                                 //     do nothing
                            }

                        }
                    else {
                        if(!$emailsent == '1' ) {
                            Mail::to($userEmail)->bcc('info@lorandomorini.nl')
                                ->send(new Orderconfirmation($data

                                ));
                            \Slack::to('#lorando')->send("You have an Order  $orderId , $userName , $ordertotal!!!!! ");
                            exit;
                        }
                            else{
                                //  do nothing
                            }
                        }

            //save sent email status,
//                Orders::where('ordernumber', $orderId)->update([
//                    'order_sent' => '1'
//                ]);


            }

                //            Session::forget('cart');
        }

    }

    elseif($payment->IsOpen() && $payment->isPending() && $payment->IsFailed() && $payment->isExpired() && $payment->isCanceled()){
//    elseif($payment->IsFailed()){

        $orderId = Orders::where('mollie_id', $payment->id)->pluck('ordernumber')->first();
        $orderdata = Orders::where('mollie_id', $payment->id)->pluck('orderdata')->first();
        $ordertotal = Orders::where('mollie_id', $payment->id)->pluck('ordertotal')->first();
        $shippingcost = Orders::where('mollie_id', $payment->id)->pluck('shipping_cost')->first();
        $userId= Orders::where('mollie_id', $payment->id)->pluck('user_id')->first();
        $userEmail = Orders::where('mollie_id', $payment->id)->pluck('user_email')->first();
        $userName = Users::where('id' , $userId)->pluck('name')->first();
        $surName = Users::where('id' , $userId)->pluck('surname')->first();
        $companyName = Users::where('id' , $userId)->pluck('companyname')->first();
        $postcode = Users::where('id' , $userId)->pluck('postcode')->first();
        $housenumber = Users::where('id' , $userId)->pluck('housenumber')->first();
        $housenumber2 = Users::where('id' , $userId)->pluck('housenumber2')->first();
        $address = Users::where('id' , $userId)->pluck('address')->first();
        $city = Users::where('id' , $userId)->pluck('city')->first();
        $shippingPostcode = Users::where('id' , $userId)->pluck('shipping_postcode')->first();
        $shippinghousenumber = Users::where('id' , $userId)->pluck('shipping_housenumber')->first();
        $shippinghousenumber2 = Users::where('id' , $userId)->pluck('shipping_housenumber2')->first();
        $shippingAddress = Users::where('id' , $userId)->pluck('shipping_address')->first();
        $shippingCity = Users::where('id' , $userId)->pluck('shipping_city')->first();
        $factuurnummer = Factuurnummer::select('factuur_nummer', 'id')->orderBy('id', 'desc')->pluck('factuur_nummer')->first();
        $currentDate = date('d-m-Y');

        $data = array(
            'factuurnummer' => $factuurnummer,
            'orderId' => $orderId,
            'orderdata' => unserialize($orderdata),
            'ordertotal' => $ordertotal,
            'shippingcost' => $shippingcost,
            'userId' => $userId,
            'userEmail' => $userEmail,
            'userName' => $userName,
            'surName' => $surName,
            'companyName' => $companyName,
            'postcode' => $postcode,
            'housenumber' => $housenumber,
            'housenumber2' => $housenumber2,
            'address' => $address,
            'city' => $city,
            'shippingPostcode' => $shippingPostcode ,
            'shippinghousenumber' => $shippinghousenumber,
            'shippinghousenumber2' => $shippinghousenumber2 ,
            'shippingAddress' => $shippingAddress,
            'shippingCity' => $shippingCity,
            'currentDate' => $currentDate,

        );

            Mail::to($userEmail)
                ->bcc('mikeypgc@gmail.com')

                ->send(new OrderconfirmationFailed($data

                ));
            \Slack::to('#lorando')->send("Order Failed!!!!!! ");

    }


}






}
