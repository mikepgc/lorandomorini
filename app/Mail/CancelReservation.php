<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CancelReservation extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // hieronder email van afzender19
        return $this->from('info@jae-it.nl' , 'Caffé Italia')
            ->subject('Helaas hebben wij geen plek meer op door u gekozen tijdstip')

            ->view('email.Cancelreservation', $this->data);
    }
}
