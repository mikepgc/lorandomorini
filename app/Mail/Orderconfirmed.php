<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Orderconfirmed extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this

        ->from('info@lorandomorini.nl' , "Lorandomorini.nl")
        ->view('email.Orderconfirmed')
        ->subject("'Lorandomorini.nl' - Factuur")
        ->attach("uploads/invoices/Factuur-{$this->data['factuurnummer']}.pdf");

    }
}
