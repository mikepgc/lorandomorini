<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EDCStockUpdated extends Mailable
{
    use Queueable, SerializesModels;

    public $now;

    public function __construct($now)
    {
        $this->now = $now;

    }
    public function build()
    {
        // hieronder email van afzender
        return $this->from('website@jae-studio.nl')
            ->subject("EDC Stockfile Update $this->now")
            ->view('email.EDCStockUpdated' );

    }
}
