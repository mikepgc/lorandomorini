<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FinalReservation extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // hieronder email van afzender19
        return $this->from('info@jae-it.nl', 'Caffé Italia')
            ->subject('Uw reservering is definitief en bevestigd door Caffé Italia')

            ->view('email.Finalreservation', $this->data);
    }
}
