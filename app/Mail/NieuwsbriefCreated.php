<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NieuwsbriefCreated extends Mailable
{
    use Queueable, SerializesModels;


    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // hieronder email van afzender
        return $this->from('info@lorandomorini.nl')
            ->subject("Leuk! Bedankt voor het aanmelden voor onze nieuwsbrief.")

            ->view('email.nieuwsbriefemail',$this->data);
    }
}
