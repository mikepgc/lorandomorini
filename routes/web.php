<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});


$router->get('/checkout-complete/{ordernr}', function () use ($router) {
    return view('checkout.checkout-complete');
    exit;
});

//Route::get('/status/update', 'PageController@updateStatus')->name('switch.update.same_address');

//Route::middleware(['checkIp'])->group(function () {
//
//    Route::get('/', function () {
//        return view('pages.underconstruction');
//    });

    Route::get('/', "PageController@home");
    Route::get('/feeds', "FeedController@feedGenerator");



    Route::get('/monitor', "PageController@monitor");
    Route::post('/succes', "DosController@post_orderbevestiging");


    Route::get("dashboard", "AnalyticsController@pageviews");
    Route::get('/statistics', function () {
        $import = new \App\Http\Controllers\AnalyticsController();
        $import->MailStatistics();
        exit;
    });

//
//Route::get('/pdf',function(){
//    return view('pages.pdf');
//
//});

    Route::post('succes', 'DosController@goedkeuring');
    Route::post('fail', 'DosController@afkeuring');

    Route::get('/about_us', "PageController@aboutus");
    Route::get('/gallery', "PageController@gallery");
    Route::get('/b2b', "PageController@b2b");
    Route::get('/dos', "DosController@orders")->name('dos');
    Route::any("/getvip/{id}", "DosController@getVip");
    Route::any("/productswitch", "DosController@productSwitch")->name('switch.update.status');

    Route::get('/feed', "FeedController@makeFeed");


    Route::any('/dos/{order}', function ($order) {
        $import = new \App\Http\Controllers\DosController();
        $import->PDFgenerator($order);
        exit;
    });

    Route::get('/storeanalytics', function () {
        $import = new \App\Http\Controllers\AnalyticsController();
        $import->storeAnalytics();
        exit;
    });
    Route::get('/refreshMonitor', function () {
        $import = new \App\Http\Controllers\ImportController();
        $import->refreshMonitor();
        exit;
    });


    Route::resource('subcategory-grid', 'index@CustomSearchController');


    Route::get('/mollie', [
            'uses' => 'OrderController@doOrder'
        ]
    );

    Route::post('/orderhook', 'OrderController@doHook');

    Route::get('/healthcheck', function () {
        $import = new \App\Http\Controllers\ImportController();
        $import->healthcheck();

    });

    Route::get('/feed', function () {
        $import = new \App\Http\Controllers\FeedController();
        $import->feedGenerator();
        exit;
    });



    Route::any('/order-tracking/{page}', [
        'uses' => 'UserController@getSingleOrders',
        'as' => 'singleorder.track'
    ]);

    Route::get('/checkout', [
        'uses' => 'ProductController@getCheckout',
        'as' => 'shop.checkout'
    ]);

    Route::any('/category/subcategory-grid/single-product/{page}', [
        'uses' => 'ProductController@viewProduct',
        'as' => 'pages.single-product-grid'
    ]);

    Route::any('/category/subcategory-list/single-product/{page}', [
        'uses' => 'ProductController@viewProduct',
        'as' => 'pages.single-product-list'
    ]);
//    Route::any('/category/{id}', [
//        'uses' => 'ProductController@getCategory',
//        'as' => 'category.category'
//    ]);

    Route::any('/category/{page}', [
        'uses' => 'ProductController@getCategory',
        'as' => 'category.category'
    ]);

    Route::any('/contacts', [
        'uses' => 'ProductController@getContact',
        'as' => 'category.category'
    ]);


    Route::any('/categoryall', [
        'uses' => 'ProductController@getCategoryMenu',
        'as' => 'category.category'
    ]);

    Route::any('/category/subcategory-grid/{page}', [
        'uses' => 'ProductController@getSubCategoryGrid',
        'as' => 'category.category'
    ]);

    Route::any('/category/search/{page}', [
        'uses' => 'ProductController@getSearch',
        'as' => 'category.category'
    ]);


    Route::any('/category/subcategory-list/{page}', [
        'uses' => 'ProductController@getSubCategoryList',
        'as' => 'category.category'
    ]);

    Route::get('/product/filter-list', [
        'uses' => 'ProductController@getFilterProduct',
        'as' => 'pages.filter-product-list'
    ]);
    Route::get('/product/filter-grid', [
        'uses' => 'ProductController@getFilterProduct',
        'as' => 'pages.filter-product-grid'
    ]);


    Route::get('/shop-grid-ls', [
        'uses' => 'ProductController@getIndex',
        'as' => 'product.page'
    ]);
    Route::get('/shop-list-ls', [
        'uses' => 'ProductController@getIndex2',
        'as' => 'product.page'
    ]);



    Route::get('/shoppingcart', [
        'uses' => 'ProductController@getCart',
        'as' => 'shoppingcart'
    ]);

//Route::get('/', [
//    'uses' => 'ProductController@getCartHeader',
//    'as' => 'shoppingcart.header'
//]);

    Route::get('/addToCart/{id}', [
        'uses' => 'ProductController@getAddToCart',
        'as' => 'product.addToCart'

    ]);
    Route::post('/addToCart', [
        'uses' => 'ProductController@getAddToCart',
        'as' => 'product.addingToCart'

    ]);

    Route::get('/reduce/{id}', [
        'uses' => 'ProductController@getReduceByOne',
        'as' => 'product.reduceByOne'
    ]);
    Route::get('/increase/{id}', [
        'uses' => 'ProductController@getIncreaseByOne',
        'as' => 'product.increaseByOne'
    ]);
    Route::post('/change-product-quantity', [
        'uses' => 'ProductController@changeProductQty',
        'as' => 'product.changeProductQuantity'
    ]);

    Route::get('/remove/{id}', [
        'uses' => 'ProductController@getRemoveItem',
        'as' => 'product.remove'
    ]);


    Route::get('/checkout-afronding', [
        'uses' => 'OrderController@getAfronding',
        'as' => 'checkout-afronding',

    ]);

    Route::any('/checkout-address', [
        'uses' => 'OrderController@getCheckoutAddress',
        'as' => 'checkout-address',

    ]);

    Route::post('/checkout-address-update', [
        'uses' => 'OrderController@updateCheckoutAddress',
        'as' => 'checkout-address-update',

    ]);

    Route::post('formulierbandenGebruikt-verzenden', [
        'uses' => 'PageController@post_formulierbandenGebruikt_verzenden'
    ]);


    Route::group(['prefix' => ''], function () {

        Route::get('/account-register', [
            'uses' => 'UserController@getSignup',
            'as' => 'account-register',
            'middleware' => 'guest'

        ]);


        Route::post('/account-register', [
            'uses' => 'UserController@postSignup',
            'as' => 'account-register',
            'middleware' => 'guest'

        ]);

        Route::get('/account-login', [
            'uses' => 'UserController@getSignin',
            'as' => 'account-login',
            'middleware' => 'guest'

        ]);
//
//    Route::get('/account-login', [
//        'uses' => 'UserController@getSignin',
//        'as' => 'account-login',
//        'middleware' => 'auth'
//
//    ]);

        Route::post('/account-login', [
            'uses' => 'UserController@postSignin',
            'as' => 'account-login',
            'middleware' => 'guest'

        ]);

        Route::get('/account-profile', [
            'uses' => 'UserController@getProfile',
            'as' => 'account.profile',
            'middleware' => 'auth'
        ]);


        Route::post('/account-profile-update', [
            'uses' => 'UserController@UpdateProfile',
            'as' => 'Update.Profile',
            'middleware' => 'auth'

        ]);

        Route::get('/account-password-recovery', [
            'uses' => 'UserController@GetPasswordReset',
            'as' => 'Update.Profile',
            'middleware' => 'guest'

        ]);

        Route::get('/account-orders', [
            'uses' => 'UserController@getOrders',
            'as' => 'account.orders',
            'middleware' => 'auth'

        ]);
        Route::get('/account-single-ticket', [
            'uses' => 'UserController@getSingleTicket',
            'as' => 'account.single-ticket',
            'middleware' => 'auth'

        ]);
        Route::get('/account-ticket-details', [
            'uses' => 'UserController@getTicketDetails',
            'as' => 'account.ticket-details',
            'middleware' => 'auth'

        ]);
        Route::get('/account-tickets', [
            'uses' => 'UserController@getTicket',
            'as' => 'account.account-tickets',
            'middleware' => 'auth'

        ]);
        Route::any('/account-address', [
            'uses' => 'UserController@getAddress',
            'as' => 'account.account-address',
            'middleware' => 'auth'

        ]);
        Route::any('/account-address-update', [
            'uses' => 'UserController@updateAddress',
            'as' => 'account.account-address',
            'middleware' => 'auth'

        ]);
        Route::get('/account-wishlist', [
            'uses' => 'UserController@getWishlist',
            'as' => 'account.account-wishlist',
            'middleware' => 'auth'

        ]);


        Route::get('/logout', [
            'uses' => 'UserController@getLogout',
            'as' => 'logout',
            'middleware' => 'auth'

        ]);
    });

    Route::get('/cart/destroy', [
        'uses' => 'ProductController@destroyCart',
        'as' => 'product.destroyCart'
    ]);


    Route::get('{page}', 'GetIndexController@getIndex');
    Route::post('/storeNieuwsbrief', 'Admin\NieuwsbriefCrudController@storeNieuwsbrief');


//});
