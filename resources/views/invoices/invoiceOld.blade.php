<!doctype html>
<html lang="en" >
<head>
    <meta charset="UTF-8">

    <title>d</title>

    <style>
        .invoice-box {
            max-width: 800px;
            margin: auto;
            padding: 30px;
            border: 1px solid #eee;
            box-shadow: 0 0 10px rgba(0, 0, 0, .15);
            font-size: 16px;
            line-height: 24px;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            color: #555;
        }

        .invoice-box table {
            width: 100%;
            line-height: inherit;
            text-align: left;
        }

        .invoice-box table td {
            padding: 5px;
            vertical-align: top;
        }

        .invoice-box table tr td:nth-child(2) {
            text-align: right;
        }

        .invoice-box table tr.top table td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.top table td.title {
            font-size: 45px;
            line-height: 45px;
            color: #333;
        }

        .invoice-box table tr.information table td {
            padding-bottom: 40px;
        }

        .invoice-box table tr.heading td {
            background: #eee;
            border-bottom: 1px solid #ddd;
            font-weight: bold;
        }

        .invoice-box table tr.details td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.item td{
            border-bottom: 1px solid #eee;
        }

        .invoice-box table tr.item.last td {
            border-bottom: none;
        }

        .invoice-box table tr.total td:nth-child(2) {
            border-top: 2px solid #eee;
            font-weight: bold;
        }

        @media only screen and (max-width: 600px) {
            .invoice-box table tr.top table td {
                width: 100%;
                display: block;
                text-align: center;
            }

            .invoice-box table tr.information table td {
                width: 100%;
                display: block;
                text-align: center;
            }
        }

        /** RTL **/
        .rtl {
            direction: rtl;
            font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        }

        .rtl table {
            text-align: right;
        }

        .rtl table tr td:nth-child(2) {
            text-align: left;
        }
    </style>
</head>

<body>
<div class="invoice-box">
    <table cellpadding="0" cellspacing="0" width="100%" style="width:100%">
        <tr class="top">
            <td colspan="2">
                <table>
                    <tr>
                        <td class="title">
                            <img src="https://development.jouwfotografieshop.nl/img/logoXS.png" style="width:300px;">
                        </td>

                        <td>
                            Factuurnummer:# <br>
                            Ordernummer:#{{$orderId}} <br>
                            Datum: January 1, 2015<br>

                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr class="information">
            <td colspan="2">
                <table>
                    <tr>
                        <td>
                            @if($company == 'on')
                                <p style="vertical-align: top;">{{$companyName}},</p>
                            @else
                                <p style="vertical-align: top;">{{$userName}}  {{$surName}},</p>
                            @endif<br>
                            {{$address}} {{$housenumber}}{{$housenumber2}}<br>
                            {{$shippingAddress}} {{$housenumber}}{{$housenumber2}}
                        </td>

                        <td>
                            Jouwfotografieshop.nl<br>
                            Info@jouwfotografieshop.nl<br>

                            IBan: NL78 BUNQ 2036 2872 63 <br>
                            Kvk: 75489287<br>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>





        <tr class="heading">
            <td>
                Artikelomschrijving
            </td>
            <td>
                Aantal
            </td>
            <td>
                Totaal
            </td>
        </tr>


        @foreach($orderdata as  $order)
            <tr class="item">
                <td>
                    {{$order['item']['art_description_NL']}}<br>
                    Artikelnummer: {{$order['item']['artnr']}}
                </td>
                <td>
                    {{$order['qty']}}
                </td>
                <td>
                    € {{$order['qty'] * $order['item']['price']}}
                </td>
            </tr>

        @endforeach


        <tr class="total">
            <td></td>
            <td></td>
            <td>
                Totaal (incl btw): € {{(round($ordertotal / 121 * 21, 2))}}
            </td>
        </tr>
    </table>
</div>
</body>
</html>
