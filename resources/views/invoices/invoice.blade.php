<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>factuur </title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <style>
        @font-face {
            font-family: 'Source Sans Pro';
            font-style: normal;
            font-weight: normal;
            src: local('Source Sans Pro'), local('SourceSansPro-Regular'), url(http://themes.googleusercontent.com/static/fonts/sourcesanspro/v7/ODelI1aHBYDBqgeIAH2zlNzbP97U9sKh0jjxbPbfOKg.ttf) format('truetype');
        }
        @font-face {
            font-family: 'Source Sans Pro';
            font-style: normal;
            font-weight: bold;
            src: local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(http://themes.googleusercontent.com/static/fonts/sourcesanspro/v7/toadOcfmlt9b38dHJxOBGLsbIrGiHa6JIepkyt5c0A0.ttf) format('truetype');
        }
        @font-face {
            font-family: 'Source Sans Pro';
            font-style: italic;
            font-weight: normal;
            src: local('Source Sans Pro Italic'), local('SourceSansPro-It'), url(http://themes.googleusercontent.com/static/fonts/sourcesanspro/v7/M2Jd71oPJhLKp0zdtTvoM0DauxaEVho0aInXGvhmB4k.ttf) format('truetype');
        }
        @font-face {
            font-family: 'Source Sans Pro';
            font-style: italic;
            font-weight: bold;
            src: local('Source Sans Pro Bold Italic'), local('SourceSansPro-BoldIt'), url(http://themes.googleusercontent.com/static/fonts/sourcesanspro/v7/fpTVHK8qsXbIeTHTrnQH6Edtd7Dq2ZflsctMEexj2lw.ttf) format('truetype');
        }
        html{
            width: 100%;
            height: 100%;
            padding: 0;
            margin: 0;
        }
        body {
            font-family: sans-serif;
            font-family: 'Source Sans Pro', sans-serif;
            /*font-size: 0.8rem;*/
            color: #666;
        }
        .table{
            color: #666;
        }
        .h4, h4{
            /*font-size: 1.3rem;*/
        }
        p, h1 {
            font-size: 2em;
            background: #eee;
            padding: 1em;
            font-family: 'Source Sans Pro', sans-serif;
        }
        .themed{
            color: transparent;
        }
        .themed-bg{
            background: transparent;
            color: white;
        }
        #head{
            /*display: flex;*/
            align-items: center;
            padding: 50px 0;
        }
        #head{
            height: 200px;
        }
        #head > h2{
            font-size: 45px;
            font-weight: 100;
            padding-left: 22px;
        }
        #head > div > b{
            font-weight: 100;
            font-size: 55px;
            color: white;
        }
        b, h4{
            color: #666;
        }
        #products tr.borderless > td{
            border: none;
        }
        #products tbody tr.vat > td:nth-of-type(2), #products tbody tr.vat > td:last-of-type{
            border-top: 1px solid #dee2e6;
        }
        #products tbody tr:last-of-type > td:nth-of-type(2), #products tbody tr:last-of-type > td:last-of-type{
            border-top: 2px solid #999;
        }
        .table thead th{
            border-bottom: 1px;
        }
    </style>
</head>
<body>
<div class="themed-bg" id="head" style="position: relative">
    <img src="https://www.lorandomorini.nl/img/logo/logo-black.png" style="width:150px;">
    <div class="text-right" style="width: auto; position: absolute; right: 22px; top: 50%; -webkit-transform: translateY(-50%); color:#333">

        <table class="table" style="border: 1px solid black">
            <tbody>
            <tr>
                <td style="border: none"><b>Factuurnummer:</b></td>
                <td style="border: none">{{$factuurnummer}}</td>
            </tr>
            <tr>
                <td><b>Factuurdatum:</b></td>
                <td >{{$currentDate}}</td>
            </tr>
{{--            @if($btw_number)--}}
{{--                <tr>--}}
{{--                    <td style="border: none"><b>BTW-nummer:</b></td>--}}
{{--                    <td style="border: none">{{$btw_number}}</td>--}}
{{--                </tr>--}}
{{--            @endif--}}
            </tbody>
        </table>

    </div>
    <div style="clear: both; display: block"></div>
</div>
<div class="p-4">
    <div class="">
        <div class="float-left" style="width: 66.666%">
            <div class="p-3">
                <div>
                    @if($company == 'on')
                        {{$companyName}}<br>
                    @else
                        {{$userName}}  {{$surName}}<br>
                    @endif
                    {{$address}} {{$housenumber}}{{$housenumber2}}<br>
                        {{$postcode}}  {{$city}}<br>
                </div>
            </div>
        </div>

        <div style="clear: both; display: block"></div>
    </div>
    <div style="clear: both; display: block"></div>
    <br>
    <div>
        <table class="table" id="products">
            <thead>
            <tr>
                <th style="min-width: 400px">Artikelomschrijving</th>
                <th class="text-center">Aantal</th>
                <th class="text-right">Prijs</th>
                <th class="text-right">Subtotaal</th>
            </tr>
            </thead>
            <tbody>
            @foreach($orderdata as  $order)                        <tr>
                <td>{{$order['item']['name']}}</td>
                <td class="text-center"> {{$order['qty']}}</td>

                <td class="text-right">&euro;  {{number_format($order['item']['price'], 2)}}</td>
                <td class="text-right">&euro;  {{number_format($order['qty'] * $order['item']['price'], 2)}}</td>
            </tr>
            @endforeach
<br><br>
            <tr class="borderless vat">
                <td colspan="2"></td>
                <td class="text-right">VAT/BTW 9%:</td>
                <td class="text-right">&euro;{{(number_format(($ordertotal - $shippingcost) / 109 * 9, 2))}}</td>
            </tr>
            <tr class="borderless ">
                <td colspan="2"></td>
                <td class="text-right">Verzendkosten</td>
                @if($shippingcost > 1 )
                    <td class="text-right" style="vertical-align: top; padding: 4px 6px 4px 0; text-align: right;" align="right" valign="top">€{{(number_format($shippingcost, 2))}}</td>
                @else
                    <td class="text-right" style="vertical-align: top; padding: 4px 6px 4px 0; text-align: right; color: green" align="right" valign="top">Gratis</td>
                @endif
            </tr>
            <tr class="borderless">
                <td colspan="2"></td>
                <td class="text-right"><b>Subtotaal (incl btw):</b></td>
                <td class="text-right">&euro;  {{(number_format($ordertotal , 2))}}</td>
            </tr>

            </tbody>
        </table>
    </div>
    <div class="p-3">
        <br>

        <br><br>
        Bankgegevens: <br>IBAN: NL59 INGB 0009 3097 94  <br>
        SWIFT/BIC: INGBNL2A

    </div>
    <br>
    <br>
    <hr style="margin-bottom: 0;">
    <div class="text-center">
        <b style="color: #888">Lorandomorini.nl | BTW/VAT# NL144493717B03| Company Reg/KvK#: 64137325</b>
    </div>
</div>
<br>


<div style="clear: both; display: block"></div>

</body>
</html>
