@extends('layouts.master2')


@section('content')


    @include('layouts.sidebar')

    @include('layouts.topbar')

    @include ('layouts.navbar')
    <!-- Off-Canvas Wrapper-->
    <div class="offcanvas-wrapper">
        <!-- Page Title-->
        <div class="page-title">
            <div class="container">
                <div class="column">
                    <h1>Afronding</h1>
                </div>
                <div class="column">
                    <ul class="breadcrumbs">
                        <li><a href="/">Home</a>
                        </li>
                        <li class="separator">&nbsp;</li>
                        <li>Afronding</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Page Content-->
        <div class="container padding-bottom-3x mb-2">
            <div class="row">
                <!-- Checkout Adress-->
                @if(!\App\Governor::$agent->isMobile())

                <div class="col-xl-9 col-lg-8">
                    <div class="checkout-steps">
                        @if (empty (Session :: get ('cart')))
                            <a>Compleet</a>
                            <a class="active"><span class="angle"></span>Afronding</a>
                            <a><span class="angle"></span>Adresgegevens</a>
                            <a href="shoppingcart"><span class="angle"></span>Mijn Winkelwagen</a></div>

                    @else
                        <a href="#">Compleet</a>
                        <a class="active" href="checkout-afronding"><span class="angle"></span>Afronding</a>
                        <a href="/checkout-address"><span class="angle"></span>Adresgegevens</a>
                        <a href="/shoppingcart"><span class="angle"></span>Mijn Winkelwagen</a>
                    @endif
                </div>
                @endif
                <hr>

                <div class="row" style="padding-top: 10px; padding-bottom: 10px">

                    <div class="col-8">
                        <h4 style="padding-top: 10px; color: whitesmoke;">Samenvatting van uw order</h4>
                    </div>
                    <div class="col-4 float-right">
                        <a class="btn btn-sm btn-outline-danger" href="/shoppingcart">Winkelwagen
                            bewerken</a>
                    </div>


                </div>




                <div class="table-responsive shopping-cart">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Product</th>
                            <th class="text-center">Aantal</th>
                            <th class="text-center">Prijs</th>
                            <th class="text-center">Totaal</th>
                            <th class="text-center"></th>


                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($products as $product)
                            <tr>
                                <td>
                                    <div class="product-item"><a class="product-thumb"
                                                                 href="category/subcategory-grid/single-product/{{$product['item']['artnr']}}"><img
                                                    src="{{$product['item'] ['image_1']}}" alt="Product"></a>
                                        <div class="product-info">
                                            <h4 class="product-title"><a
                                                        href="category/subcategory-grid/single-product/{{$product['item']['artnr']}}" style="color: whitesmoke">{{$product['item'] ['name']}}</a>
                                            </h4>

{{--                                            <span><em>Merk:</em> {{$product['item'] ['brand']}}</span>--}}
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center  text-lg text-medium">

                                    <span class="badge" style="color: whitesmoke; font-size: 14px">{{$product['qty']}}</span>

                                </td>
                                {{--                <td class="text-center text-lg text-medium">€{{$product['item']['gross_incbtw']}}</td>--}}
                                <td class="text-center text-lg text-medium" style="color: whitesmoke">
                                    €{{(round($product['item']['price'] , 4 ))}}</td>
                                <td class="text-center text-lg text-medium" style="color: whitesmoke">
                                    €{{(round($product['item']['price'] , 4 ) * $product['qty'])}}</td>
                                {{--@if($discount > '0' )--}}

                                {{--<td class="text-center text-lg text-medium">€{{(($product['item']['discountPercentage'] / 100 * $product['item']['price'] )) * $product['qty'] }}</td>--}}
                                {{--@else--}}
                                <td class="text-center text-lg text-medium"></td>
                                {{--@endif               --}}
                                {{--<td class="text-center"><a class="remove-from-cart" href="{{ route('product.remove', ['id'=> $product['item']['id']])}}" data-toggle="tooltip" title="Remove item"><i class="icon-cross"></i></a></td>--}}
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
                <div class="shopping-cart-footer">
                    <div class="column"></div>
                    {{--<div class="column text-lg">Subtotaal:--}}
                    {{--@if(!empty($totalPrice))--}}
                    {{--<span class="text-medium">€{{(round($totalPrice, 2)) - ((($product['item']['discountPercentage'] / 100 * $product['item']['price'] )) * $product['qty'])}}</span>--}}
                    {{--@else--}}
                    {{--<span class="text-medium">€ 0</span>--}}
                    {{--@endif--}}
                    {{--</div>--}}
                </div>
                <div class="row padding-top-1x mt-3">
                    <div class="col-sm-6">
                        <h5>Verzenden naar:</h5>

                        <ul class="list-unstyled">
                            @if(Auth::user()->company == 'on')
                            <li style="color: whitesmoke"><span class="text-muted" >Bedrijfsnaam:</span>{{Auth::user()->companyname}}</li>
                            @endif
                                <li style="color: whitesmoke"><span class="text-muted" >Naam:</span>{{Auth::user()->name}}</li>
                            <li style="color: whitesmoke">
                                <span class="text-muted" >Adres:</span>{{Auth::user()->shipping_address}} {{Auth::user()->shipping_housenumber}} {{Auth::user()->shipping_housenumber2}}
                            </li>
                            <li style="color: whitesmoke"><span class="text-muted">Postcode:</span>{{Auth::user()->shipping_postcode}}</li>
                            <li style="color: whitesmoke"><span  class="text-muted">Plaats:</span>{{Auth::user()->shipping_city}}</li>
                        </ul>
                    </div>
                    <div class="col-sm-6">
                        <h5>Betalingsmethode:</h5>
                        <ul class="list-unstyled">
                            <li><span class="text-muted"><img src="img/ideal.png" width="50px" alt="Payment Methods"> of <img
                                            src="img/paypal.png" width="80px" alt="Payment Methods"></span></li>
                            <li><u style="font-size: 10px">***U kunt een optie kiezen als u verder gaat naar
                                    betalen***</u></li>
                        </ul>
                    </div>
                </div>
                <div class="checkout-footer margin-top-1x">
                    <div class="column hidden-xs-down"><a class="btn btn-outline-secondary" href="checkout-address"><i
                                    class="icon-arrow-left"></i>&nbsp;Ga terug</a></div>
                    <div class="column"><a class="btn btn-primary" href="mollie">Ga naar betalen</a></div>
                </div>
            </div>
            <!-- Sidebar          -->
            <div class="col-xl-3 col-lg-4">
                <aside class="sidebar">
                    <div class="padding-top-2x hidden-lg-up"></div>
                    <!-- Order Summary Widget-->
                    <section class="widget widget-order-summary">
                        <h3 class="widget-title">Order totaal</h3>
                        <table class="table">
                            <?php
                            $subtotal = (round($totalPrice, 2)) - ((($product['item']['discountPercentage'] / 100 * $product['item']['price'])) * $product['qty']);

                            if ($subtotal <= 0)
                                $total = $subtotal + 6.5;

                            else
                                $total = $subtotal;

                            //var_dump($product['item']['discountPercentage'] )

                            ?>
                            <tr>
                                <td>Subtotaal:</td>
                                <td class="text-medium">
                                    €{{(round($totalPrice, 2)) - ((($product['item']['discountPercentage'] / 100 * $product['item']['price'] )) * $product['qty'])}}</td>
                            </tr>

                                <?php

                                if($subtotal <= 30){
                                    $total = $total + 6.5;

                                }
                                else{
                                    $total = $total;
                                }
                                $kortingsprijs = $total - ($total * 0.1)

                                ?>
                            <tr>
                                <td>Verzendkosten:</td>

                                    @if($subtotal <= 30)
                                        <td class="text-medium">€ 6,50</td>
                                    @else
                                        <td class="text-medium"><span style="color: green; font-weight: bold">Gratis</span>
                                        </td>
                                    @endif

                            </tr>
                            <tr>
                                <td>BTW 9% (incl)</td>
                                <td>€{{(round($total / 109 * 9, 2))}}</td>
                                {{--<td class="text-medium">€3.42</td>--}}
                            </tr>



                            @if(Auth::user()->vip == '1')

                                <tr>
                                    <td style="color:yellow">VIP Korting(10%):</td>
                                    <td>€-{{$total * 0.1}}</td>


                                </tr>
                                @endif
                                @if(Auth::user()->vip == '1')

                                 <tr>
                                        <td style="font-weight: bold">Totaal:</td>
                                        <td style="font-weight: bold">€ {{$kortingsprijs}}</td>
                                 </tr>
                                @else
                                    <tr>
                                        <td style="font-weight: bold">Totaal:</td>
                                        <td style="font-weight: bold">€ {{$total}}</td>
                                    </tr>
                                @endif
                        </table>
                    </section>
                    <!-- Featured Products Widget-->
                    {{--<section class="widget widget-featured-products">--}}
                    {{--<h3 class="widget-title">Recently Viewed</h3>--}}
                    {{--<!-- Entry-->--}}
                    {{--<div class="entry">--}}
                    {{--<div class="entry-thumb"><a href="shop-single.html"><img src="img/shop/widget/01.jpg" alt="Product"></a></div>--}}
                    {{--<div class="entry-content">--}}
                    {{--<h4 class="entry-title"><a href="shop-single.html">Oakley Kickback</a></h4><span class="entry-meta">$155.00</span>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--<!-- Entry-->--}}
                    {{--<div class="entry">--}}
                    {{--<div class="entry-thumb"><a href="shop-single.html"><img src="img/shop/widget/02.jpg" alt="Product"></a></div>--}}
                    {{--<div class="entry-content">--}}
                    {{--<h4 class="entry-title"><a href="shop-single.html">Top-Sider Fathom</a></h4><span class="entry-meta">$90.00</span>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--<!-- Entry-->--}}
                    {{--<div class="entry">--}}
                    {{--<div class="entry-thumb"><a href="shop-single.html"><img src="img/shop/widget/03.jpg" alt="Product"></a></div>--}}
                    {{--<div class="entry-content">--}}
                    {{--<h4 class="entry-title"><a href="shop-single.html">Vented Straw Fedora</a></h4><span class="entry-meta">$49.50</span>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--<!-- Entry-->--}}
                    {{--<div class="entry">--}}
                    {{--<div class="entry-thumb"><a href="shop-single.html"><img src="img/shop/widget/04.jpg" alt="Product"></a></div>--}}
                    {{--<div class="entry-content">--}}
                    {{--<h4 class="entry-title"><a href="shop-single.html">Big Wordmark Tote</a></h4><span class="entry-meta">$29.99</span>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</section>--}}
                    {{--<!-- Promo Banner-->--}}
                    {{--<section class="promo-box" style="background-image: url(img/banners/02.jpg);"><span class="overlay-dark" style="opacity: .4;"></span>--}}
                    {{--<div class="promo-box-content text-center padding-top-2x padding-bottom-2x">--}}
                    {{--<h4 class="text-light text-thin text-shadow">New Collection of</h4>--}}
                    {{--<h3 class="text-bold text-light text-shadow">Sunglasses</h3><a class="btn btn-outline-white btn-sm" href="shop-grid-ls.html">Shop Now</a>--}}
                    {{--</div>--}}
                    {{--</section>--}}
                </aside>
            </div>
        </div>
    </div>
    <!-- Site Footer-->
    @include('layouts.footer')

    @endsection
    </div>
    <!-- Back To Top Button--><a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
    <!-- Backdrop-->
    <div class="site-backdrop"></div>
    <!-- JavaScript (jQuery) libraries, plugins and custom scripts-->
    <script src="js/vendor.min.js"></script>
    <script src="js/scripts.min.js"></script>
    </body>
    </html>
