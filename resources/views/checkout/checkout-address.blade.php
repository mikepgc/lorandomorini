
@extends('layouts.master2')

@section('content')

    @include('layouts.sidebar')

    @include('layouts.topbar')

    @include ('layouts.navbar')
    <!-- Off-Canvas Wrapper-->
    <div class="offcanvas-wrapper">
      <!-- Page Title-->
      <div class="page-title">
        <div class="container">
          <div class="column">
            <h1>Adresgegevens</h1>
          </div>
          <div class="column">
            <ul class="breadcrumbs">
              <li><a href="/">Home</a>
              </li>
              <li class="separator">&nbsp;</li>
              <li>Adresgegevens</li>
            </ul>
          </div>
        </div>
      </div>
      <!-- Page Content-->
      <div class="container padding-bottom-3x mb-2">

          <!-- Checkout Adress-->
          @if(!\App\Governor::$agent->isMobile())

              <div class="col-xl-12 col-lg-8">

                              <div class="checkout-steps">
                                  @if (empty (Session :: get ('cart')))
                                      <a>Compleet</a>
                                      <a><span class="angle"></span>Afronding</a>
                                      <a class="active"><span class="angle"></span>Adresgegevens</a>
                                      <a href="shoppingcart"><span class="angle"></span>Mijn Winkelwagen</a>
                              </div>
                                  @elseif(!Auth::user())
                                  <a>Compleet</a>
                                  <a><span class="angle"></span>Afronding</a>
                                  <a class="active"><span class="angle"></span>Adresgegevens</a>
                                  <a href="shoppingcart"><span class="angle"></span>Mijn Winkelwagen</a>


                                  @else
                                      <a href="#">Compleet</a>
                                      <a href="checkout-afronding"><span class="angle"></span>Afronding</a>
                                      <a class="active" href="checkout-address"><span class="angle"></span>Adresgegevens</a>
                                      <a href="shoppingcart"><span class="angle"></span>Mijn Winkelwagen</a>
                                  @endif
              </div>
          @endif


          <h4>Persoonlijke gegevens</h4>
            <hr class="padding-bottom-1x">
          {{--@if(Auth::check())--}}
              {{--<form action="checkout-address-update" method="post">--}}

          {{--@else--}}
              <form class="address" action="checkout-address-update" id="checkout-address-update" method="post">
          {{--@endif--}}

            <div class="row">
                              <div class="col-sm-2">
                                  <div class="form-group">
                                      <div class="custom-control custom-checkbox">
                                          @if (Auth::check())
                                              @if(Auth::user()->person == '0')
                                                  <input class="status person custom-control-input" type="checkbox"
                                                         id="person" name="person" checked>
                                              @else
                                                  <input class="status person custom-control-input" type="checkbox"
                                                         id="person" name="person">
                                              @endif
                                          @else
                                              <input class="status person custom-control-input" type="checkbox"
                                                     id="person" name="person">
                                          @endif
                                              <label class="custom-control-label" for="person">Particulier</label>
                                      </div>

                                  </div>
                              </div>
                              <div class="col-sm-2">
                                  <div class="form-group">
                                      <div class="custom-control custom-checkbox">
                                          @if (Auth::check())
                                              @if( Auth::user()->company == 'on')

                                                  <input class="status company custom-control-input" type="checkbox" id="company" name="company" checked>
                                              @else
                                                  <input class="status company custom-control-input" type="checkbox" id="company" name="company"  >
                                              @endif
                                          @else
                                              <input class="status company custom-control-input" type="checkbox" id="company" name="company"  >
                                          @endif




                                              <label class="custom-control-label" for="company">Bedrijf</label>
                                      </div>
                                  </div>
                              </div>
                  </div>


                  @if(Auth::check() && Auth::user()->company == 'on')

                  <div class="row" id="company-details" >
                      @else

                      <div class="row" id="company-details" style="display: none">
                          @endif


                      <div class="col-sm-6">
                          <div class="form-group">
                              <label for="checkout-ln">Bedrijfsnaam</label>
                              @if(Auth::check())
                                  <input class="form-control" type="text" id="companyname" name="companyname" value="{{Auth::user()->companyname}}" >
                              @else
                                  <input class="form-control" type="text" id="companyname" name="companyname" >
                              @endif
                          </div>
                      </div>

                      <div class="col-sm-6">
                          <div class="form-group">
                              <label for="checkout-ln">BTW-nummer</label>
                              @if(Auth::check())
                                  <input class="form-control" type="text" id="btw" name="btw" value="{{Auth::user()->btw}}" >
                              @else
                                  <input class="form-control" type="text" id="btw" name="btw" >
                              @endif
                          </div>
                      </div>

                      <hr class="padding-bottom-1x">
                  </div>


            <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">
                                  <label for="checkout-fn">Voornaam</label>
                                    @if(Auth::check())
                                  <input class="form-control" type="text" id="name" name="name" value="{{Auth::user()->name}}" required>
                                    @else
                                  <input class="form-control" type="text" id="name" name="name" value="" required>
                                    @endif

                                </div>
                          </div>


                          <div class="col-sm-6">
                            <div class="form-group">
                              <label for="checkout-ln">Achternaam</label>
                                @if(Auth::check())
                              <input class="form-control" type="text" id="surname" name="surname" value="{{Auth::user()->surname}}" required>
                                @else
                              <input class="form-control" type="text" id="surname" name="surname" required>
                                @endif
                            </div>
                          </div>
              </div>

            <div class="row">
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label for="checkout-email">E-mail Adres</label>
                                @if(Auth::check())
                              <input class="form-control" type="email" id="email" name="email" value="{{Auth::user()->email}}" required>
                                @else
                              <input class="form-control" type="email" id="email" name="email" required>
                                @endif

                            </div>
                        </div>

                          <div class="col-sm-6">
                            <div class="form-group">
                              <label for="checkout-phone">Telefoonnummer</label>
                                @if(Auth::check())
                              <input class="form-control" type="text" id="phone" name="phone" value="{{Auth::user()->phone}}">
                                @else
                              <input class="form-control" type="text" id="phone" name="phone" >
                                @endif

                            </div>
                          </div>
            </div>

            <div class="row">
{{--                      <b style="padding: 0 0 15px 25px">Vul uw postcode en huisnummer in en uw adres wordt automatisch en foutloos aangevuld. Handmatig invullen kan natuurlijk ook.</b>--}}
                  </div>
                          <div class="billing-address">
                  <div class="row">
                      <div class="col-sm-6">
                          <div class="form-group">

                              <label for="checkout-zip">Postcode</label>

                              @if(Auth::check())
                                  <input class="form-control" type="text" id="postcode" name="postcode"  value="{{Auth::user()->postcode}}" required>
                              @else
                                  <input class="form-control" type="text" id="postcode"  name="postcode" required>
                              @endif
                          </div>
                      </div>

                      <div class="col-sm-5">
                          <div class="form-group">
                              <label for="checkout-address1">Huisnummer <span class="message" style="color: red"></span><br></label>
                              @if(Auth::check())
                                  <input class="form-control" type="text" id="housenumber" name="housenumber" value="{{Auth::user()->housenumber}}" required>
                              @else
                                  <input class="form-control" type="text" id="housenumber" name="housenumber" required>
                              @endif
                          </div>
                      </div>
                      <div class="col-sm-1">
                          <div class="form-group">
                              <label for="checkout-address1">Toevoeging </label>
                              @if(Auth::check())
                                  <input class="form-control" type="text" id="housenumber2" name="housenumber2"  class="housenumber2" value="{{Auth::user()->housenumber2}}" >
                              @else
                                  <input class="form-control" type="text" id="housenumber2" name="housenumber2" class="housenumber2" >
                              @endif
                          </div>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-sm-6">
                          <div class="form-group">
                              <label for="checkout-address1">Adres </label>
                              @if(Auth::check())
                                  <input class="form-control"  type="text" id="address" name="address" value="{{Auth::user()->address}}"  required>
                              @else
                                  <input class="form-control" type="text" id="address" name="address" required>
                              @endif
                          </div>
                      </div>
                      <div class="col-sm-6">
                          <div class="form-group">
                              <label for="checkout-city">Stad</label>
                              @if(Auth::check())
                                  <input class="form-control" type="text" id="city" name="city"  value="{{Auth::user()->city}}" required>
                              @else
                                  <input class="form-control" type="text" id="city" name="city"  required>
                              @endif
                          </div>
                      </div>

                  </div>
                          </div>

                          <div class="form-group">
                              <div class="custom-control custom-checkbox">
                                  @if(Auth::check())
                                      @php
                                          $sameAddressChecked = Auth::user()->same_address === 1;
                                      @endphp
                                      <input class="same_address custom-control-input" type="checkbox" id="same_address" name="same_address" value="1" {{ $sameAddressChecked ? 'checked' : '' }}>
                                  @else
                                      <input class="same_address custom-control-input" type="checkbox" id="same_address" name="same_address">
                                  @endif

                                  <label class="custom-control-label" for="same_address">Verzenden naar hetzelfde adres</label>
                              </div>
                          </div>


                  </div>



            <div id="different_address">
                  <h4>Bezorgen op een ander adres</h4>
                  <hr class="padding-bottom-1x">

                <div class="shipping-address">
                  <div class="row">
                      <div class="col-sm-6">
                          <div class="form-group">

                              <label for="checkout-zip">Postcode</label>

                              @if(Auth::check())
                                  @if(!empty(Auth::user()->shipping_postcode))
                                      <input class="form-control" type="text" id="sh-postcode" name="sh-postcode"
                                             value="{{Auth::user()->shipping_postcode}}" required>
                                  @else
                                      <input class="form-control" type="text" id="sh-postcode" name="sh-postcode" required>

                                  @endif

                               @else
                                  <input class="form-control" type="text" id="sh-postcode" name="sh-postcode" required>
                              @endif
                          </div>
                      </div>

                      <div class="col-sm-5">
                          <div class="form-group">
                              <label for="checkout-address1">Huisnummer <span class="message" style="color: red"></span></label>
                              @if(Auth::check())
                                      <input class="form-control" type="text" id="sh-housenumber" name="sh-housenumber"
                                             value="{{Auth::user()->shipping_housenumber}}" required>


                                  @else
                                  <input class="form-control" type="text" id="sh-housenumber" name="sh-housenumber" required>
                              @endif
                          </div>
                      </div>
                      <div class="col-sm-1">
                          <div class="form-group">
                              <label for="checkout-address1">Toevoeging </label>
                              @if(Auth::check())
                                  @if(!empty(Auth::user()->shipping_housenumber2))
                                      <input class="form-control" type="text" id="sh-housenumber2"
                                             name="sh-housenumber2" value="{{Auth::user()->shipping_housenumber2}}">
                                  @else
                                      <input class="form-control" type="text" id="sh-housenumber2" name="sh-housenumber2" >
                                  @endif
                              @else
                                  <input class="form-control" type="text" id="sh-housenumber2" name="sh-housenumber2" >
                              @endif
                          </div>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-sm-6">
                          <div class="form-group">
                              <label for="checkout-address1">Adres </label>
                              @if(Auth::check())
                                  @if(!empty(Auth::user()->shipping_address))
                                      <input class="form-control" type="text" id="sh-address"
                                             name="sh-address" value="{{Auth::user()->shipping_address}}">
                                  @else
                                      <input class="form-control" type="text" id="sh-address" name="sh-address" required>

                                  @endif
                              @else
                                  <input class="form-control" type="text" id="sh-address" name="sh-address" required>
                              @endif
                          </div>
                      </div>
                      <div class="col-sm-6">
                          <div class="form-group">
                              <label for="checkout-city">Stad</label>
                              @if(Auth::check())
                                  @if(!empty(Auth::user()->shipping_city))
                                      <input class="form-control city" type="text" id="sh-city" name="sh-city"
                                             value="{{Auth::user()->shipping_city}}" required>
                                  @else
                                      <input class="form-control" type="text" id="sh-city" name="sh-city" required>
                                  @endif
                              @else
                                  <input class="form-control" type="text" id="sh-city" name="sh-city" required>
                              @endif
                          </div>
                      </div>
                  </div>
                  </div>
             </div>

                      @if(!(Auth::check()) || Auth::user()->account == Null)
<div>
                              <div class="form-group">
                                  <div class="custom-control custom-checkbox">
                                      <input class="account custom-control-input " type="checkbox" id="account" name="account" >

                                      <label class="custom-control-label" for="account">Maak een account aan voor
                                          uw volgende bezoek</label><br>
                                      <i>Sneller afrekenen</i><br>
{{--                                      <i>Meerdere verzendadressen opslaan</i><br>--}}
                                      <i>Bestellingen bekijken en volgen</i><br>
{{--                                      <i>Persoonlijk verlanglijstje bijhouden</i><br>--}}
                                      <i>Aan- en afmelden van nieuwsbriefabonnementen</i>

                                  </div>
                              </div>



    <div class="row" id="passwordfield">

                              <div class="col-sm-6">
                                  <div class="form-group">
                                      <label for="reg-pass">Wachtwoord</label>
                                      <input class="form-control" type="password" id="password" name="password">
                                  </div>
                              </div>
                              <div class="col-sm-6">
                                  <div class="form-group">
                                      <label for="reg-pass-confirm">Herhaal wachtwoord</label>


                                      <input class="form-control" type="password" id="herhaalpassword"  onChange="checkPasswordMatch()">
                                  </div>
                              </div>
                          </div>
</div>
                              <div class="registrationFormAlert" id="divCheckPasswordMatch" style="font-size: 15px; color: red"></div>

                          @endif
            <div class="checkout-footer">
              <div class="column"><a class="btn btn-outline-secondary" href="shoppingcart"><i class="icon-arrow-left"></i><span class="hidden-xs-down">&nbsp;Terug naar Winkelwagen</span></a></div>
              <div class="column">
                  <button class="btn btn-primary margin-bottom-none" type="submit" href="/checkout-afronding"><span class="hidden-xs-down">Ga verder met bestellen&nbsp;</span><i class="icon-arrow-right"></i></button>
              </div>
            </div>

          {{ csrf_field() }}


      </form>
          <!-- Sidebar          -->
          {{--<div class="col-xl-3 col-lg-4">--}}
            {{--<aside class="sidebar">--}}
              {{--<div class="padding-top-2x hidden-lg-up"></div>--}}
              {{--<!-- Order Summary Widget-->--}}
              {{--<section class="widget widget-order-summary">--}}
                {{--<h3 class="widget-title">Order Summary</h3>--}}
                {{--<table class="table">--}}
                  {{--<tr>--}}
                    {{--<td>Cart Subtotal:</td>--}}

                      {{--@if(!empty($totalPrice))--}}
                          {{--<td class="text-medium"> €{{(round($totalPrice, 2)) - ((($products['item']['discountPercentage'] / 100 * $products['item']['price'] )) * $products['qty'])}}</td>--}}
                      {{--@else--}}
                          {{--<td class="text-medium"> € 0</td>--}}
                      {{--@endif--}}
                  {{--</tr>--}}

                {{--</table>--}}
              {{--</section>--}}
              {{--<!-- Featured Products Widget-->--}}
              {{--<section class="widget widget-featured-products">--}}
                {{--<h3 class="widget-title">Recently Viewed</h3>--}}
                {{--<!-- Entry-->--}}
                {{--<div class="entry">--}}
                  {{--<div class="entry-thumb"><a href="shop-single.html"><img src="img/shop/widget/01.jpg" alt="Product"></a></div>--}}
                  {{--<div class="entry-content">--}}
                    {{--<h4 class="entry-title"><a href="shop-single.html">Oakley Kickback</a></h4><span class="entry-meta">$155.00</span>--}}
                  {{--</div>--}}
                {{--</div>--}}
                {{--<!-- Entry-->--}}
                {{--<div class="entry">--}}
                  {{--<div class="entry-thumb"><a href="shop-single.html"><img src="img/shop/widget/02.jpg" alt="Product"></a></div>--}}
                  {{--<div class="entry-content">--}}
                    {{--<h4 class="entry-title"><a href="shop-single.html">Top-Sider Fathom</a></h4><span class="entry-meta">$90.00</span>--}}
                  {{--</div>--}}
                {{--</div>--}}
                {{--<!-- Entry-->--}}
                {{--<div class="entry">--}}
                  {{--<div class="entry-thumb"><a href="shop-single.html"><img src="img/shop/widget/03.jpg" alt="Product"></a></div>--}}
                  {{--<div class="entry-content">--}}
                    {{--<h4 class="entry-title"><a href="shop-single.html">Vented Straw Fedora</a></h4><span class="entry-meta">$49.50</span>--}}
                  {{--</div>--}}
                {{--</div>--}}
                {{--<!-- Entry-->--}}
                {{--<div class="entry">--}}
                  {{--<div class="entry-thumb"><a href="shop-single.html"><img src="img/shop/widget/04.jpg" alt="Product"></a></div>--}}
                  {{--<div class="entry-content">--}}
                    {{--<h4 class="entry-title"><a href="shop-single.html">Big Wordmark Tote</a></h4><span class="entry-meta">$29.99</span>--}}
                  {{--</div>--}}
                {{--</div>--}}
              {{--</section>--}}
              {{--<!-- Promo Banner-->--}}
              {{--<section class="promo-box" style="background-image: url(img/banners/02.jpg);"><span class="overlay-dark" style="opacity: .4;"></span>--}}
                {{--<div class="promo-box-content text-center padding-top-2x padding-bottom-2x">--}}
                  {{--<h4 class="text-light text-thin text-shadow">New Collection of</h4>--}}
                  {{--<h3 class="text-bold text-light text-shadow">Sunglasses</h3><a class="btn btn-outline-white btn-sm" href="shop-grid-ls.html">Shop Now</a>--}}
                {{--</div>--}}
              {{--</section>--}}
            {{--</aside>--}}
          {{--</div>--}}
        </div>
      </div>
      <!-- Site Footer-->
        @include('layouts.footer')
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var sameAddressCheckbox = document.getElementById('same_address');

            // Add event listener to dynamically update checkbox state based on database value
            sameAddressCheckbox.addEventListener('change', function() {
                if (this.checked) {
                    this.value = '1'; // Set value to '1' when checked
                } else {
                    this.value = ''; // Set value to '' (or remove value attribute) when unchecked
                }
            });
        });
    </script>

    <script>

        // Create closure to keep namespace clean and hide implementation.
        (function($) {
            'use strict';

            // Trigger on '5408xb' and on '5408 XB'
            var NL_SIXPP_REGEX = /[0-9]{4,4}\s?[a-zA-Z]{2,2}/;
            var NL_STREETNUMBER_REGEX = /[0-9]+/;
            var NL_STREETNUMBER_WITH_ADDITION_REGEX = /[0-9]+ ?([0-9a-zA-Z]{0,3})/;



            $.fn.applyAutocomplete = function(options) {
                if (!options) {
                    options = {};
                }

                var default_args = {
                    // Wait for 10 seconds before giving up on communication with
                    // Pro6PP.
                    'timeout' : 10000,
                    // When set to 'true', the script will required a valid address
                    // present in the Pro6PP database.
                    // Set to 'false' to allow entering a custom address.
                    'enforce_validation' : true,
                    // When set to 'true', the script will never block the process in
                    // case of
                    // trouble communicating with Pro6PP.
                    // Set to 'false' to prevent users from entering a custom address.
                    'gracefully_degrade' : true,
                    // Allow splitting streetnumber additions from streetnumbers when
                    // no specific streetnumber addition field is provided.
                    'allow_streetnumber_additions_split' : false
                }

                var instance = this;

                for (var index in default_args) {
                    if (typeof options[index] === "undefined") {
                        instance[index] = default_args[index];
                    }
                }

                function getConfig(field) {
                    if (typeof options[field] === 'undefined') {
                        // Use default field class name
                        return instance.find('.' + field);
                    } else {
                        // Developer chose to specify form field manually.
                        return $(options[field]);
                    }
                }

                instance.postcode = getConfig('postcode');
                instance.streetnumber = getConfig('streetnumber');
                instance.extension = getConfig('extension');
                instance.street = getConfig('street');
                instance.streets = getConfig('streets');
                instance.city = getConfig('city');
                instance.municipality = getConfig('municipality');
                instance.province = getConfig('province');
                instance.lat = getConfig('lat');
                instance.lng = getConfig('lng');
                instance.areacode = getConfig('areacode');
                instance.message = getConfig('message');
                instance.spinner = getConfig('spinner');

                // Turn off browser autocompletion for the postcode field.
                // Because javascript is unable to catch an event when a user clicks a
                // previously filled-in value that the browser may suggest.
                // The autocomplete attribute became official in HTML5, but used to work
                // long before that.
                instance.postcode.attr('autocomplete', 'off');
                instance.postcode.keyup(function() {
                    autocomplete(instance);
                });
                instance.streetnumber.attr('autocomplete', 'off');
                instance.streetnumber.blur(function() {
                    autocomplete(instance);
                });
                instance.extension.attr('autocomplete', 'off');
                instance.extension.blur(function() {
                    autocomplete(instance);
                });
                // Bind event handler to street selectbox.
                if (typeof instance.streets !== 'undefined') {
                    // When pressing tab, make a selection.
                    instance.streets.blur(function() {
                        show_street(instance);
                    });
                }

                instance.callback = options.callback;
            };

            var pro6pp_cache = {};
            function pro6pp_cached_get(obj, url, params, callback) {
                var key = url + $.param(params);
                if (pro6pp_cache.hasOwnProperty(key)) {
                    if (typeof callback !== 'undefined') {
                        callback(obj, pro6pp_cache[key]);
                    }
                } else {
                    obj.spinner.show();
                    $.ajax({
                        crossDomain : true,
                        dataType : 'jsonp',
                        timeout : obj.timeout,
                        url : url,
                        data : params,
                        success : function(data, textStatus, jqXHR) {
                            pro6pp_cache[key] = data;
                            if (typeof callback !== 'undefined') {
                                callback(obj, data);
                            }
                        },
                        error : function(jqXHR, textStatus, errorThrown) {
                            var message = "Unable to contact Pro6PP validation service";
                            showErrorMessage(obj, message);
                        },
                        complete : function(jqXHR, textStatus) {
                            obj.spinner.hide();
                        }
                    });
                }
            }

            // Request geo-data from nl_sixpp
            function autocomplete(obj) {
                obj.message.hide().empty();
                var postcode = obj.postcode.val();
                var streetnumber = obj.streetnumber.val();
                // Streetnumber is only required when there's an input field defined for
                // it.
                // There may be use-cases where the streetnumber is not required.
                if (NL_SIXPP_REGEX.test(postcode)
                    && ((typeof streetnumber === 'undefined' || NL_STREETNUMBER_REGEX.test(streetnumber))
                        ||
                        (typeof obj.addition === 'undefined' && NL_STREETNUMBER_WITH_ADDITION_REGEX.test(streetnumber)
                            && obj.allow_streetnumber_additions_split === true))
                ) {
                    show_street(obj);
                    var url = 'https://api.pro6pp.nl/v1/autocomplete';
                    var params = {
                        auth_key: pro6pp_auth_key,
                        nl_sixpp: postcode
                    };
                    // Streetnumber field is not required
                    if (typeof obj.streetnumber !== 'undefined') {
                        params.streetnumber = obj.streetnumber.val();
                        // Extension field is not required
                        if (typeof obj.extension !== 'undefined' && obj.extension.length) {
                            params.extension = obj.extension.val();
                        } else if (obj.allow_streetnumber_additions_split === true) {
                            var match = NL_STREETNUMBER_WITH_ADDITION_REGEX.exec(streetnumber);
                            if (match !== null) {
                                params.extension = match[1];
                            }
                        }
                    }
                    pro6pp_cached_get(obj, url, params, fillin);
                } else {
                    obj.street.empty();
                    obj.street.empty();
                }
            }

            function show_street(obj) {
                obj.street.show();
                obj.streets.hide();
                // Copy over the selected value (if any) and remember the choice of
                // streets.
                var streetname = obj.streets.val();
                if (typeof streetname !== "undefined" && streetname !== "") {
                    obj.street.val(streetname);
                    obj.street.data('old_streetname', streetname);
                }
            }
            function show_streets(obj) {
                obj.street.hide();
                obj.streets.show();
            }

            function escapeHtml(unsafe) {
                // Some characters that are received from the webservice should be
                // escaped when used in HTML
                return unsafe.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;")
                    .replace(/'/g, "&#039;");
            }

            function fillin(obj, json) {
                if (typeof obj.callback !== 'undefined') {
                    obj.callback(json);
                }

                if (json.status === 'ok') {
                    if (json.results.length === 1) {
                        obj.street.val(json.results[0].street);
                    } else {
                        var streets = obj.streets;
                        streets.empty();
                        $.each(json.results, function(i, street) {
                            // Some characters like the quote in "'s-Gravenweg" need to
                            // be escaped before it can be
                            // put in the value field.
                            var escapedStreet = escapeHtml(street.street);

                            // If a street was selected, a new streetnumber was filled
                            // in, the street selector will be filled again,
                            // therefore the old street selection has been remembered
                            // and set a preferred street here.
                            var selected = "";
                            if (obj.street.data('old_streetname') === escapedStreet) {
                                selected = " selected='selected'";
                            }

                            var newOption = $("<option value='" + escapedStreet + "'" + selected + ">" + street.street
                                + "</option>");
                            streets.append(newOption);
                            // IE doesn't react well on having the click handler
                            // attached to the
                            // selectbox itself. It needs to be attached to the
                            // individual options.
                            newOption.click(function() {
                                show_street(obj);
                            });
                        });
                        show_streets(obj);
                    }
                    obj.city.val(json.results[0].city);
                    // You might also want to add these extra fields
                    obj.municipality.val(json.results[0].municipality);
                    obj.province.val(json.results[0].province);
                    obj.lat.val(json.results[0].lat);
                    obj.lng.val(json.results[0].lng);
                    obj.areacode.val(json.results[0].areacode);
                    if (json.results[0].streetnumbers) {
                        obj.streetnumbers = json.results[0].streetnumbers;

                        var extensions = [];
                        var splitStreetnumbers = json.results[0].streetnumbers.split(';')
                        var streetnumber = obj.streetnumber.val();
                        $.each(splitStreetnumbers, function(i, streetnumberWithExtension) {
                            var index = streetnumberWithExtension.indexOf(streetnumber + ' ');
                            if (index > -1) {
                                extensions.push(streetnumberWithExtension.slice(index + streetnumber.length + 1));
                            }
                        });
                        obj.extensions = extensions;
                    }
                } else {
                    showErrorMessage(obj, json.error.message);
                }
            }

            function showErrorMessage(obj, message) {
                var translated_message = message;
                // See if we've got a translation available
                switch(message) {
                    case 'nl_sixpp not found':
                        translated_message = 'Onbekende postcode';
                        releaseReadOnlyFields(obj, true, false);
                        break;
                    case 'Invalid nl_sixpp format':
                        translated_message = 'Ongeldig postcode formaat';
                        break;
                    case 'streetnumber is missing a number':
                        translated_message = 'Vul een geldig huisnummer in';
                        break;
                        case 'Streetnumber without extension not found':
                        translated_message = 'Vul ook huisnummer toevoeging in';
                        break;
                    case 'Streetnumber not found':
                        translated_message = 'Onbekend huisnummer';
                        releaseReadOnlyFields(obj, true, false);
                        break;
                    case 'extension not found':
                        translated_message = 'Onbekende huisnummer toevoeging';
                        releaseReadOnlyFields(obj, true, false);
                        break;
                    case 'Invalid nl_fourpp format':
                        translated_message = 'Ongeldig postcode formaat';
                        break;
                    case 'Invalid be_fourpp format':
                        translated_message = 'Ongeldig postcode formaat';
                        break;
                    case "Unable to contact Pro6PP validation service":
                        translated_message = 'Geen verbinding met validatieserver';
                        releaseReadOnlyFields(obj, false, true);
                        break;
                }
                // Show message to user
                obj.message.html(translated_message).show();
            }

            function releaseReadOnlyFields(obj, not_found, comm_error) {
                if (comm_error === true && obj.gracefully_degrade === false) {
                    return;
                }
                if (not_found === true && obj.enforce_validation === true) {
                    return;
                }
                // Make input fields writable.
                obj.street.removeAttr('readonly');
                obj.city.removeAttr('readonly');
            }
            //
            // end of closure
            //
        })(jQuery);

        // hide shipping address options when shippingaddress exist in database
        @if(Auth::check())
            @if(Auth::user()->shipping_address)
            $('#different_address').show();
            $('#sh-postcode').val($('#postcode').val()).prop( true);
            $('#sh-address').val($('#address').val()).prop( true);
            $('#sh-housenumber').val($('#housenumber').val()).prop(true);
            $('#sh-housenumber2').val($('#housenumber2').val()).prop(true);
            $('#sh-city').val($('#city').val()).prop(true);
            @endif
        @endif

                // if check checkbox same address, below action will copy input field to shippingaddress fields

        $(".same_address").change(function() {
            if(this.checked) {
                $('#different_address').show();
                $('#sh-postcode').val($('#postcode').val()).prop( true);
                $('#sh-address').val($('#address').val()).prop( true);
                $('#sh-housenumber').val($('#housenumber').val()).prop(true);
                $('#sh-housenumber2').val($('#housenumber2').val()).prop(true);
                $('#sh-city').val($('#city').val()).prop(true);

            }
            else {
                $('#different_address').show();
                $('#sh-postcode').val('').prop('disabled', false).show();
                $('#sh-address').val('').prop('disabled', false).show();
                $('#sh-housenumber').val('').prop('disabled', false).show();
                $('#sh-housenumber2').val('').prop('disabled', false).show();
                $('#sh-city').val('').prop('disabled', false).show();
            }

        });

        $(document).ready(function() {
            function updateShippingAddress() {
                if ($('.same_address').is(':checked')) {
                    $('#sh-postcode').val($('#postcode').val());
                    $('#sh-address').val($('#address').val());
                    $('#sh-housenumber').val($('#housenumber').val());
                    $('#sh-housenumber2').val($('#housenumber2').val());
                    $('#sh-city').val($('#city').val());
                }
            }

            // On checkbox change
            $(".same_address").change(function() {
                if (this.checked) {
                    $('#different_address').hide();
                    updateShippingAddress();
                } else {
                    $('#different_address').show();
                    $('#sh-postcode').val('').prop('disabled', false);
                    $('#sh-address').val('').prop('disabled', false);
                    $('#sh-housenumber').val('').prop('disabled', false);
                    $('#sh-housenumber2').val('').prop('disabled', false);
                    $('#sh-city').val('').prop('disabled', false);
                }
            });

            // On any main address field change
            $('#postcode, #address, #housenumber, #housenumber2, #city').on('input', function() {
                updateShippingAddress();
            });
        });

        $(document).ready(function() {
            // Check the initial state of the same_address checkbox
            var sameAddressChecked = $('#same_address').prop('checked');

            // If same_address is checked on page load, hide the different_address section
            if (sameAddressChecked) {
                $('#different_address').hide();
            } else {
                $('#different_address').show();
            }

            // Handle change event of the same_address checkbox
            $(".same_address").change(function() {
                if (this.checked) {
                    $('#different_address').hide();
                } else {
                    $('#different_address').show();
                }
            });
        });



        //        when company is checked, then show company fields

        $(".company").change(function() {
            if(this.checked) {
                $('#company-details').show();
                           }
            else {
                $('#company-details').hide();

            }


        });

        $('#passwordfield').show();

       // when make account is checked, show password fields

        $(".account").change(function() {
            if(this.checked) {
                $('#passwordfield').show();
                           }
            else {
                $('#passwordfield').hide();

            }


        });

//        when person is checked, then uncheck company
        $(".person").change(function() {
            if(this.checked) {
                $('#company-details').hide();
                           }
        });

        $('input.status').on('change', function() {
            $('input.status').not(this).prop('checked', false);
        });



        $('.account').prop('checked', 1);


        $(document).ready(function(){
            $('.js-switch').change(function () {
                let account = $(this).prop('checked') === true ? 1 : 0;

            });
        });

        // wachtwoord controle

        function checkPasswordMatch() {
            var password = $("#password").val();
            var confirmPassword = $("#herhaalpassword").val();

            if (password != confirmPassword)
                $("#divCheckPasswordMatch").html("Wachtwoord niet zelfde!");
            else
                $("#divCheckPasswordMatch").html("");
        }

        $(document).ready(function () {
            $("#txtConfirmPassword").keyup(checkPasswordMatch);
        });
    </script>



    @endsection
    <!-- Back To Top Button--><a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
    <!-- Backdrop-->
    <div class="site-backdrop"></div>

