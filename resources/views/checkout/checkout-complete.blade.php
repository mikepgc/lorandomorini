
@extends('layouts.master2')


@section('content')


    @include('layouts.sidebar')

    @include('layouts.topbar')

    @include ('layouts.navbar')
    <!-- Off-Canvas Wrapper-->
    <div class="offcanvas-wrapper">
      <!-- Page Title-->
      <div class="page-title">
        <div class="container">
          <div class="column">
            <h1>Compleet</h1>
          </div>
          <div class="column">
            <ul class="breadcrumbs">
              <li><a href="/">Home</a>
              </li>
              <li class="separator">&nbsp;</li>
              <li>Compleet</li>

            </ul>
          </div>
        </div>
      </div>
      <!-- Page Content-->
        @if(!\App\Governor::$agent->isMobile())
            <div class="container padding-bottom-3x mb-2">
                  <div class="checkout-steps">
                    @if (empty (Session :: get ('cart')))
                      <a >Compleet</a>
                      <a><span class="angle"></span>Afronding</a>
                      <a><span class="angle"></span>Adresgegevens</a>

                      <a class="active" href="shoppingcart"><span class="angle"></span>Mijn Winkelwagen</a></div>

                    @else

                  <a  class="active"  href="/checkout-review">Compleet</a>
                  <a class="active" href="#"><span class="angle"></span>Afronding</a>
                  <a class="active" href="#"><span class="angle"></span>Adresgegevens</a>
                  <a class="active" href="/shoppingcart"><span class="angle"></span>Mijn Winkelwagen</a>

                    @endif
            </div>
        @endif
        <div class="card text-center">
          <div class="card-body padding-top-2x">
            <h3 class="card-title" style="color: black">Bedankt voor uw bestelling!</h3>
            <p class="card-text" style="color:black">Uw bestelling is geplaatst en zal zo snel mogelijk worden verwerkt.</p>
            <p class="card-text" style="color:black">Uw ordernummer is:  <span class="text-medium">{!! request()->segment(count(request()->segments())) !!}</span></p>
            <p class="card-text" style="color:black">U ontvangt binnenkort een e-mail met bevestiging van uw bestelling.

            </p>
            <div class="padding-top-1x padding-bottom-1x"><a class="btn btn-outline-secondary" href="/">Verder shoppen</a>
                <a class="btn btn-outline-primary" href="/account-orders"><i class="icon-location"></i>&nbsp;Track order</a></div>
          </div>
        </div>
      </div>
      <!-- Site Footer-->
        @include('layouts.footer')

        @endsection
    </div>
    <!-- Back To Top Button--><a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
    <!-- Backdrop-->
    <div class="site-backdrop"></div>
    <!-- JavaScript (jQuery) libraries, plugins and custom scripts-->
    <script src="js/vendor.min.js"></script>
    <script src="js/scripts.min.js"></script>
