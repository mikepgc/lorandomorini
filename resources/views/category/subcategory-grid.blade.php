
@extends('layouts.master2')


@section('content')


    @include('layouts.sidebar')

    @include('layouts.topbar')

    @include ('layouts.navbar')
    <!-- Off-Canvas Wrapper-->
    <div class="offcanvas-wrapper" style="background-color: #6e5e4e">
        <!-- Page Title-->
        <div class="page-title" style="background-color: #6e5e4e">
            <div class="container">
                <div class="column">
                    {{--Takes from end of url and remove underscore--}}
                    <h1 style="color:whitesmoke;">{!! str_replace('_', ' ' , request()->segment(count(request()->segments()))) !!}</h1>
                </div>
                <div class="column">
                    <ul class="breadcrumbs">
                        <li><a href="index.html">Home</a>
                        </li>
                        <li class="separator">&nbsp;</li>
                        <li>Categorie</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Page Content-->
        <div class="container padding-bottom-3x mb-1">
            <div class="row">
                <!-- Products-->
                <div class="col-xl-9 col-lg-8 order-lg-2">
                    <!-- Shop Toolbar-->
                    <div class="shop-toolbar padding-bottom-1x mb-2">
{{--                        <div class="column">--}}
{{--                            <div class="shop-sorting">--}}
{{--                                <label for="sorting">Sort by:</label>--}}
{{--                                <select class="form-control" id="sorting">--}}
{{--                                    <option>Popularity</option>--}}
{{--                                    <option>Low - High Price</option>--}}
{{--                                    <option>High - Low Price</option>--}}
{{--                                    <option>Avarage Rating</option>--}}
{{--                                    <option>A - Z Order</option>--}}
{{--                                    <option>Z - A Order</option>--}}
{{--                                </select><span class="text-muted">Showing:&nbsp;</span><span>1 - 12 items</span>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                        <div class="column">
                            <div class="shop-view"><a class="grid-view active" href="{{ $gridUrl }}"><span></span><span></span>
                                    <span></span></a><a class="list-view" href="{{ $listUrl }}"><span></span><span></span><span></span></a></div>
                        </div>
                    </div>

                    <div class="isotope-grid cols-3 mb-2">
                        <div class="gutter-sizer"></div>
                        <div class="grid-sizer"></div>
                        <!-- Product-->
                        {{--{{$subcategories}}--}}

                        @foreach($subcategories  as $category)
                            @if($category->visible == 1)
                            <div class="grid-item">
                                <div class="product-card" style="min-height: 510px">
                                    {{--@if(!empty($product->btw_percentage))--}}
                                    {{--<div class="product-badge text-danger">{{$product->btw_percentage}}% Korting</div>--}}

                                    {{--@endif--}}
                                    <h3 class="product-title" style="min-height: 75px ; color: white; font-size: 20px">{{$category->name}}</h3>
                                    <br>
                                    <a class="product-thumb" href="{{ route('pages.single-product-grid',['page'=>$category->artnr]) }}"><img class="thumb" src="{{$category->image_1}}" alt="Product" ></a>
                                    {{--<h3 class="product-title" style="font-size: 12px"><a href="single-product/{{$category->artnr}}">{{$category->description}}</a></h3>--}}

                                    <div class="product-title">
                                    @if($category->stock >= 1 && $category->stock <= 5)
                                        {{--@if(env('TEST_ENV'))--}}
                                            {{--<div style="color: green; font-weight: bold; font-size: 20px" title="beperkt op voorraad {{$product->stock}}">--}}
                                                {{--<i class="fa fa-square" style="font-size:20px;color:green"></i>--}}
                                                {{--<i class="fa fa-square" style="font-size:20px;color:grey"></i>--}}

                                                {{--<i class="fa fa-square" style="font-size:20px;color:grey"></i>--}}
                                            {{--</div>--}}
                                        {{--@else--}}

                                            <div style="color: green; font-weight: bold; font-size: 20px" title="beperkt op voorraad">
                                                <i class="fa fa-square" style="font-size:20px;color:green"></i>
                                                <i class="fa fa-square" style="font-size:20px;color:grey"></i>
                                                <i class="fa fa-square" style="font-size:20px;color:grey"></i>

                                            </div>
                                        {{--@endif--}}



                                    @elseif($category->stock > 5 && $category->stock <= 10)
                                        {{--@if(env('TEST_ENV'))--}}
                                            {{--<div style="color: green; font-weight: bold; font-size: 20px" title="voldoende op voorraad {{$product->stock}}">--}}
                                                {{--<i class="fa fa-square" style="font-size:20px;color:green"></i>--}}
                                                {{--<i class="fa fa-square" style="font-size:20px;color:green"></i>--}}
                                                {{--<i class="fa fa-square" style="font-size:20px;color:grey"></i>--}}
                                            {{--</div>--}}
                                        {{--@else--}}

                                            <div style="color: green; font-weight: bold; font-size: 20px" title="voldoende op voorraad">
                                                <i class="fa fa-square" style="font-size:20px;color:green"></i>
                                                <i class="fa fa-square" style="font-size:20px;color:green"></i>
                                                <i class="fa fa-square" style="font-size:20px;color:grey"></i>
                                            </div>
                                        {{--@endif--}}



                                    @elseif($category->stock >= 10)
                                        {{--@if(env('TEST_ENV'))--}}
                                            {{--<div style="color: green; font-weight: bold; font-size: 20px" title="ruim op voorraad {{$product->stock}}">--}}
                                                {{--<i class="fa fa-square" style="font-size:20px;color:green"></i>--}}
                                                {{--<i class="fa fa-square" style="font-size:20px;color:green"></i>--}}
                                                {{--<i class="fa fa-square" style="font-size:20px;color:green"></i>--}}
                                            {{--</div>--}}
                                        {{--@else--}}


                                            <div style="color: green; font-weight: bold; font-size: 20px" title="ruim op voorraad">
                                                <i class="fa fa-square" style="font-size:20px;color:green"></i>
                                                <i class="fa fa-square" style="font-size:20px;color:green"></i>
                                                <i class="fa fa-square" style="font-size:20px;color:green"></i>
                                            </div>
                                        {{--@endif--}}
                                    @elseif($category->stock < 1)
                                        {{--@if(env('TEST_ENV'))--}}
                                            {{--<div style="color: red;  font-weight: bold; font-size: 20px" title="niet op voorraad {{$product->stock}}">--}}
                                                {{--<i class="fa fa-square" style="font-size:20px;color:red"></i>--}}
                                                {{--<i class="fa fa-square" style="font-size:20px;color:red"></i>--}}
                                                {{--<i class="fa fa-square" style="font-size:20px;color:red"></i>--}}
                                            {{--</div>--}}
                                        {{--@else--}}
                                            <div style="color: red;  font-weight: bold; font-size: 20px" title="niet op voorraad">
                                                <i class="fa fa-square" style="font-size:20px;color:red"></i>
                                                <i class="fa fa-square" style="font-size:20px;color:red"></i>
                                                <i class="fa fa-square" style="font-size:20px;color:red"></i>

                                            </div>
                                        {{--@endif--}}

                                    @endif
                                    </div>
                                    <h4 class="product-price" style="color:white;">
                                        {{--@if(!empty($product->btw_percentage))--}}

                                        {{--<del>€{{$product->gross_incbtw}}</del>--}}

                                        {{--€{{$product->gross_incbtw - ($product->btw_percentage / 100 *$product->gross_incbtw )}}--}}
                                        {{--@else--}}
                                        <span style="color: gold; font-size: 18px"> €{{$category->price}},-</span>
                                        <del style="color: red" >€{{$category->price_discount}}</del>
                                        <br>
{{--                                        €{{number_format($category->price, 2)}}(incl btw)<br>--}}
                                        {{--@if(env('TEST_ENV'))--}}

                                        {{--€{{number_format($product->purchase_exbtw, 2)}} (inkoop ex)<br>--}}
                                        {{--@endif--}}
                                        {{--{{$product->category_url}}<br>--}}
                                        {{--{{$product->category}}<br>--}}
                                        {{--{{$product->sub_category}}<br>--}}

                                        {{--@endif--}}
                                    </h4>
                                    <div class="product-buttons">
                                        <form action="javascript:;" id="addToCartForm_{{ $category->id }}">
                                            <div class="change_quantity">
                                                <a style="text-decoration: blink; color: white;font-size: 25px" onclick="change_qty(this,'decrese')" href="javascript:;">
                                                    -
                                                </a>
                                                <input type="text" name="qty" value="1" style="width: 40px;height: 40px;display: inline-block; border:0 ; padding: 0; text-decoration: blink; color: white; font-size: 22px; background-color: transparent; text-align: center"/>
                                                <input type="hidden" name="product" value="{{ $category->id }}" />
                                                {{ csrf_field() }}
                                                <a style="text-decoration: blink; color: white; font-size: 25px" onclick="change_qty(this,'increase')" href="javascript:;">
                                                    +
                                                </a>
                                            </div>
                                            {{--<button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip" title="Whishlist"><i class="icon-heart"></i></button>--}}
                                            <a href="javascript:;" onclick="addToCart(this)" ><button  class="btn btn-outline-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Product"data-toast-message="{{$category->name}} toegevoegd in winkelwagen!">Winkelwagen</button></a>
                                          </form>
                                    </div>
                                </div>
                            </div>
                            @endif
                        @endforeach

{{--                        {{$subcategories->links()}}--}}
                    </div>

{{--                    @if($subcategories)--}}
{{--                        {{$subcategories->links('vendor.pagination.bootstrap-4')}}--}}
{{--                    @endif--}}
                    <!-- Products Grid-->
                    {{--<div class="isotope-grid cols-3 mb-2">--}}
                        {{--<div class="gutter-sizer"></div>--}}
                        {{--<div class="grid-sizer"></div>--}}
                        {{--<!-- Product-->--}}

                        {{--@foreach($category->where('sub_category_url' , request()->segment(count(request()->segments()))) as $grid)--}}
{{--                        @foreach ($products->where('category_url' , 'Camera_Accesoires') as  $product)--}}
                            {{--@foreach($Subcategory->where('category' , $grid->category) as $grid)--}}

                            {{--<div class="grid-item">--}}
                                {{--<div class="product-card">--}}
                                    {{--<a href="{{$grid->sub_category_url}}">--}}
                                    {{--@if(!empty($product->btw_percentage))--}}{{--f--}}
                                    {{--<div class="product-badge text-danger">{{$product->btw_percentage}}% Korting</div>--}}

                                    {{--@endif--}}
                                    {{--<br>--}}
{{--                                    <a class="product-thumb" href="category/{{$grid->id}}"><img src="" alt="Product"></a>--}}
                                    {{--<h4 class="product-price">--}}

                                        {{--{{$grid->sub_category}}<br>--}}

                                        {{--@endif--}}
                                    {{--</h4>--}}
                                    {{--</a>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                                {{--@endforeach--}}
                        {{--@endforeach--}}
                    {{--</div>--}}
                    <!-- Pagination-->


                    {{--<nav class="pagination">--}}
                        {{--<ul class="pages">--}}
                        {{--</ul>--}}
                        {{--<div class="column text-right hidden-xs-down"><a class="btn btn-outline-secondary btn-sm" href="#">Next&nbsp;<i class="icon-arrow-right"></i></a></div>--}}
                    {{--</nav>--}}
                </div>
                <!-- Sidebar          -->
                @include('layouts.menusidebar')
            </div>
        </div>
        <!-- Site Footer-->
        @include('layouts.footer')

        @endsection
    </div>
    <!-- Back To Top Button--><a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
    <!-- Backdrop-->
    <div class="site-backdrop"></div>
    <!-- JavaScript (jQuery) libraries, plugins and custom scripts-->
    <script src="js/vendor.min.js"></script>
    <script src="js/scripts.min.js"></script>
@section('js')
<script type="text/javascript">
    $(document).ready(function(){
        $('input[name=qty]').change(function(){
            if($(this).val() < 0){
                $(this).val('1')
            }
        });
    });
    function addToCart(obj){
        var url = "{{ route('product.addingToCart') }}";
        var form = $(obj).parent('form');
        $('#loader').fadeIn(100);
        $.ajax({
           type: "POST",
           url: url,
           data: form.serialize(), // serializes the form's elements.
           success: function(data)
           {
                $('#cart-toolbar').html(data.response.cartData); // show response from the php script.
                $('#loader').fadeOut(100);
           }
        });
    }
    function change_qty(obj,type){
        var target_ele = $(obj).siblings('input[name="qty"]');
        var current_qty = target_ele.val();
        if(type == 'increase'){
            current_qty++;
            target_ele.val(current_qty);
        }else{
            if(current_qty == 1){
                return ;
            }else{
                current_qty--;
                target_ele.val(current_qty);
            }
        }
    }
    function filterData(){
        @if(\Request::segment(2) == 'filter-list')
            var filterURL = '{{ route("pages.filter-product-list") }}';
        @else
            var filterURL = '{{ route("pages.filter-product-grid") }}';
        @endif

        var minPrice = $(document.querySelector('.ui-range-value-min input')).val();
        var maxPrice = $(document.querySelector('.ui-range-value-max input')).val();
        var queryString = $('input[name="subcat[]"]:checked').serialize();
        var queryStringParam = '';
        if(queryString != ''){
            queryStringParam = '&'+queryString;
        }
        var filterURL = filterURL+'?filterPriceMin='+minPrice+"&filterPriceMax="+maxPrice+queryStringParam;
        window.location.href=filterURL;
    }
</script>

<style>

	img.thumb {
    position:relative;
    top:0px;
    transition:all 1s ease;

}

img.thumb:hover {
    position:relative;
    top:-15px;
    background:transparent;
    transition:all 1s ease;

}
	</style>
@endsection
