
@extends('layouts.master2')


@section('content')


    @include('layouts.sidebar')

    @include('layouts.topbar')

    @include ('layouts.navbar')

    <!-- Off-Canvas Wrapper-->
    <div class="offcanvas-wrapper">
        <!-- Page Title-->
        <div class="page-title">
            <div class="container">
                <div class="column">
                    <h1>Alle Categorieën</h1>
                </div>
                <div class="column">
                    <ul class="breadcrumbs">
                        <li><a href="/">Home</a>
                        </li>
                        <li class="separator">&nbsp;</li>
                        <li>Categorieën</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Page Content-->
        <div class="container padding-bottom-3x mb-1">
            <div class="row">
                <!-- Products-->
                <div class="col-xl-9 col-lg-8 order-lg-2">
                    <!-- Shop Toolbar-->
                    {{--<div class="shop-toolbar padding-bottom-1x mb-2">--}}
                        {{--<div class="column">--}}
                            {{--<div class="shop-sorting">--}}
                                {{--<label for="sorting">Sort by:</label>--}}
                                {{--<select class="form-control" id="sorting">--}}
                                    {{--<option>Popularity</option>--}}
                                    {{--<option>Low - High Price</option>--}}
                                    {{--<option>High - Low Price</option>--}}
                                    {{--<option>Avarage Rating</option>--}}
                                    {{--<option>A - Z Order</option>--}}
                                    {{--<option>Z - A Order</option>--}}
                                {{--</select><span class="text-muted">Showing:&nbsp;</span><span>1 - 12 items</span>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="column">--}}
                            {{--<div class="shop-view"><a class="grid-view active" href="shop-grid-ls"><span></span><span></span><span></span></a><a class="list-view" href="shop-list-ls"><span></span><span></span><span></span></a></div>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    <!-- Products Grid-->
                    <div class="isotope-grid cols-4 mb-2">
                        <div class="gutter-sizer"></div>
                        <div class="grid-sizer"></div>
                        <!-- Product-->

                        @foreach($categories as $category)

                            <div class="grid-item">
                                <div class="product-card">
                                    <a href="/category/{{$category->maincat}}">

                                    <br>
                                    <h4 class="product-price" style="color:#C8584E;">

                                        {{$category->maincat}}        </h4><br>
                                        {{--@foreach ($subcategories->where('maincatid' , $category->maincatid) as $subcategory)--}}
                                            {{--{{$subcategory->subcat}}--}}

                                            {{--@endforeach--}}


                                    </a>

                                    <a class="product-thumb" href="/category/single-product/{{$category->artnr}}"><img src="{{$category->mainpic_url}}" alt="Product" ></a>

                                </div>
                            </div>
                        @endforeach
                    </div>

                </div>

                @include('layouts.menusidebar')

            </div>
        </div>
        <!-- Site Footer-->
        @include('layouts.footer')

        @endsection
    </div>
    <!-- Back To Top Button--><a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
    <!-- Backdrop-->
    <div class="site-backdrop"></div>
    <!-- JavaScript (jQuery) libraries, plugins and custom scripts-->
    <script src="js/vendor.min.js"></script>
    <script src="js/scripts.min.js"></script>
