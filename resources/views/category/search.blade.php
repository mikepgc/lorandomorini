
@extends('layouts.master2')


@section('content')


    @include('layouts.sidebar')

    @include('layouts.topbar')

    @include ('layouts.navbar')
    <!-- Off-Canvas Wrapper-->
    <div class="offcanvas-wrapper">
        <!-- Page Title-->
        <div class="page-title">
            <div class="container">
                <div class="column">
                    {{--Takes from end of url and remove underscore--}}
                    <h1>Gezocht naar: " {!! str_replace('_', ' ' , request()->segment(count(request()->segments()))) !!}"</h1>
                </div>
                <div class="column">
                    <ul class="breadcrumbs">
                        <li><a href="index.html">Home</a>
                        </li>
                        <li class="separator">&nbsp;</li>
                        <li>Zoeken</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Page Content-->
        <div class="container padding-bottom-3x mb-1">
            <div class="row">
                <!-- Products-->
                <div class="col-xl-9 col-lg-8 order-lg-2">
                    <!-- Shop Toolbar-->
                    <div class="shop-toolbar padding-bottom-1x mb-2">
{{--                        <div class="column">--}}
{{--                            <div class="shop-sorting">--}}
{{--                                <label for="sorting">Sort by:</label>--}}
{{--                                <select class="form-control" id="sorting">--}}
{{--                                    <option>Popularity</option>--}}
{{--                                    <option>Low - High Price</option>--}}
{{--                                    <option>High - Low Price</option>--}}
{{--                                    <option>Avarage Rating</option>--}}
{{--                                    <option>A - Z Order</option>--}}
{{--                                    <option>Z - A Order</option>--}}
{{--                                </select><span class="text-muted">Showing:&nbsp;</span><span>1 - 12 items</span>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                        {{--<div class="column">--}}
                            {{--<div class="shop-view"><a class="grid-view active" href="/category/subcategory-grid/{{request()->segment(count(request()->segments()))}}"><span></span><span></span>--}}
                                    {{--<span></span></a><a class="list-view" href="/category/subcategory-list/{{request()->segment(count(request()->segments()))}}"><span></span><span></span><span></span></a></div>--}}
                        {{--</div>--}}
                    </div>
                    @if($item)
                        {{$item->links('vendor.pagination.bootstrap-4')}}
                    @endif
                    <div class="isotope-grid cols-3 mb-2">
                        <div class="gutter-sizer"></div>
                        <div class="grid-sizer"></div>
                        <!-- Product-->
                        {{--{{$subcategories}}--}}
                        @foreach($item as $category)

                            <div class="grid-item">
                                <div class="product-card" style="background-color: #060606">
                                    {{--@if(!empty($product->btw_percentage))--}}
                                    {{--<div class="product-badge text-danger">{{$product->btw_percentage}}% Korting</div>--}}

                                    {{--@endif--}}
                                    <h3 class="product-title"  style="min-height: 40px">{{$category->name}}</h3>
                                    <br>
                                    <a class="product-thumb" href="/category/subcategory-grid/single-product/{{$category->artnr}}"><img src="{{$category->image_1}}" alt="Product" ></a>
                                    {{--<h3 class="product-title" style="font-size: 12px"><a href="single-product/{{$category->artnr}}">{{$category->description}}</a></h3>--}}

                                    <div class="product-title">
                                    @if($category->stock >= 1 && $category->stock <= 5)
                                        {{--@if(env('TEST_ENV'))--}}
                                            {{--<div style="color: green; font-weight: bold; font-size: 20px" title="beperkt op voorraad {{$product->stock}}">--}}
                                                {{--<i class="fa fa-square" style="font-size:20px;color:green"></i>--}}
                                                {{--<i class="fa fa-square" style="font-size:20px;color:grey"></i>--}}

                                                {{--<i class="fa fa-square" style="font-size:20px;color:grey"></i>--}}
                                            {{--</div>--}}
                                        {{--@else--}}

                                            <div style="color: green; font-weight: bold; font-size: 20px" title="beperkt op voorraad">
                                                <i class="fa fa-square" style="font-size:20px;color:green"></i>
                                                <i class="fa fa-square" style="font-size:20px;color:grey"></i>
                                                <i class="fa fa-square" style="font-size:20px;color:grey"></i>

                                            </div>
                                        {{--@endif--}}



                                    @elseif($category->stock > 5 && $category->stock <= 10)
                                        {{--@if(env('TEST_ENV'))--}}
                                            {{--<div style="color: green; font-weight: bold; font-size: 20px" title="voldoende op voorraad {{$product->stock}}">--}}
                                                {{--<i class="fa fa-square" style="font-size:20px;color:green"></i>--}}
                                                {{--<i class="fa fa-square" style="font-size:20px;color:green"></i>--}}
                                                {{--<i class="fa fa-square" style="font-size:20px;color:grey"></i>--}}
                                            {{--</div>--}}
                                        {{--@else--}}

                                            <div style="color: green; font-weight: bold; font-size: 20px" title="voldoende op voorraad">
                                                <i class="fa fa-square" style="font-size:20px;color:green"></i>
                                                <i class="fa fa-square" style="font-size:20px;color:green"></i>
                                                <i class="fa fa-square" style="font-size:20px;color:grey"></i>
                                            </div>
                                        {{--@endif--}}



                                    @elseif($category->stock >= 10)
                                        {{--@if(env('TEST_ENV'))--}}
                                            {{--<div style="color: green; font-weight: bold; font-size: 20px" title="ruim op voorraad {{$product->stock}}">--}}
                                                {{--<i class="fa fa-square" style="font-size:20px;color:green"></i>--}}
                                                {{--<i class="fa fa-square" style="font-size:20px;color:green"></i>--}}
                                                {{--<i class="fa fa-square" style="font-size:20px;color:green"></i>--}}
                                            {{--</div>--}}
                                        {{--@else--}}


                                            <div style="color: green; font-weight: bold; font-size: 20px" title="ruim op voorraad">
                                                <i class="fa fa-square" style="font-size:20px;color:green"></i>
                                                <i class="fa fa-square" style="font-size:20px;color:green"></i>
                                                <i class="fa fa-square" style="font-size:20px;color:green"></i>
                                            </div>
                                        {{--@endif--}}
                                    @elseif($category->stock < 1)
                                        {{--@if(env('TEST_ENV'))--}}
                                            {{--<div style="color: red;  font-weight: bold; font-size: 20px" title="niet op voorraad {{$product->stock}}">--}}
                                                {{--<i class="fa fa-square" style="font-size:20px;color:red"></i>--}}
                                                {{--<i class="fa fa-square" style="font-size:20px;color:red"></i>--}}
                                                {{--<i class="fa fa-square" style="font-size:20px;color:red"></i>--}}
                                            {{--</div>--}}
                                        {{--@else--}}
                                            <div style="color: red;  font-weight: bold; font-size: 20px" title="niet op voorraad">
                                                <i class="fa fa-square" style="font-size:20px;color:red"></i>
                                                <i class="fa fa-square" style="font-size:20px;color:red"></i>
                                                <i class="fa fa-square" style="font-size:20px;color:red"></i>

                                            </div>
                                        {{--@endif--}}

                                    @endif
                                    </div>
                                    <h4 class="product-price">
                                        {{--@if(!empty($product->btw_percentage))--}}

                                        {{--<del>€{{$product->gross_incbtw}}</del>--}}

                                        {{--€{{$product->gross_incbtw - ($product->btw_percentage / 100 *$product->gross_incbtw )}}--}}
                                        {{--@else--}}
                                        €{{$category->price}} (incl btw)<br>
{{--                                        €{{number_format($category->price, 2)}}(incl btw)<br>--}}
                                        {{--@if(env('TEST_ENV'))--}}

                                        {{--€{{number_format($product->purchase_exbtw, 2)}} (inkoop ex)<br>--}}
                                        {{--@endif--}}
                                        {{--{{$product->category_url}}<br>--}}
                                        {{--{{$product->category}}<br>--}}
                                        {{--{{$product->sub_category}}<br>--}}

                                        {{--@endif--}}
                                    </h4>
                                    <div class="product-buttons">
                                        {{--<button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip" title="Whishlist"><i class="icon-heart"></i></button>--}}
                                        <a href="{{ route('product.addToCart', ['id' => $category->id]) }}"><button class="btn btn-outline-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Winkelwagen</button></a>
                                    </div>
                                </div>
                            </div>
                        @endforeach

{{--                        {{$subcategories->links()}}--}}
                    </div>
                    <!-- Products Grid-->
                    {{--<div class="isotope-grid cols-3 mb-2">--}}
                        {{--<div class="gutter-sizer"></div>--}}
                        {{--<div class="grid-sizer"></div>--}}
                        {{--<!-- Product-->--}}

                        {{--@foreach($category->where('sub_category_url' , request()->segment(count(request()->segments()))) as $grid)--}}
{{--                        @foreach ($products->where('category_url' , 'Camera_Accesoires') as  $product)--}}
                            {{--@foreach($Subcategory->where('category' , $grid->category) as $grid)--}}

                            {{--<div class="grid-item">--}}
                                {{--<div class="product-card">--}}
                                    {{--<a href="{{$grid->sub_category_url}}">--}}
                                    {{--@if(!empty($product->btw_percentage))--}}{{--f--}}
                                    {{--<div class="product-badge text-danger">{{$product->btw_percentage}}% Korting</div>--}}

                                    {{--@endif--}}
                                    {{--<br>--}}
{{--                                    <a class="product-thumb" href="category/{{$grid->id}}"><img src="" alt="Product"></a>--}}
                                    {{--<h4 class="product-price">--}}

                                        {{--{{$grid->sub_category}}<br>--}}

                                        {{--@endif--}}
                                    {{--</h4>--}}
                                    {{--</a>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                                {{--@endforeach--}}
                        {{--@endforeach--}}
                    {{--</div>--}}
                    <!-- Pagination-->


                    {{--<nav class="pagination">--}}
                        {{--<ul class="pages">--}}
                        {{--</ul>--}}
                        {{--<div class="column text-right hidden-xs-down"><a class="btn btn-outline-secondary btn-sm" href="#">Next&nbsp;<i class="icon-arrow-right"></i></a></div>--}}
                    {{--</nav>--}}
                    @if($item)
                        {{$item->links('vendor.pagination.bootstrap-4')}}
                    @endif


                </div>
                <!-- Sidebar          -->
                @include('layouts.menusidebar')
            </div>
        </div>
        <!-- Site Footer-->
        @include('layouts.footer')

        @endsection
    </div>
    <!-- Back To Top Button--><a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
    <!-- Backdrop-->
    <div class="site-backdrop"></div>
    <!-- JavaScript (jQuery) libraries, plugins and custom scripts-->
    <script src="js/vendor.min.js"></script>
    <script src="js/scripts.min.js"></script>
