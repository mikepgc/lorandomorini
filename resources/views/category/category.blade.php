
@extends('layouts.master2')


@section('content')


    @include('layouts.sidebar')

    @include('layouts.topbar')

    @include ('layouts.navbar')
    <!-- Off-Canvas Wrapper-->
    <div class="offcanvas-wrapper">
        <!-- Page Title-->
        <div class="page-title">
            <div class="container">
                <div class="column">
                    {{--Takes from end of url and remove underscore--}}
                    <h1>{!! str_replace('_', ' ' , request()->segment(count(request()->segments()))) !!}</h1>
                </div>
                <div class="column">
                    <ul class="breadcrumbs">
                        <li><a href="index.html">Home</a>
                        </li>
                        <li class="separator">&nbsp;</li>
                        <li>{{request()->segment(count(request()->segments()))}}</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Page Content-->
        <div class="container padding-bottom-3x mb-1">
            <div class="row">
                <!-- Products-->
                <div class="col-xl-9 col-lg-8 order-lg-2">
                    <!-- Shop Toolbar-->

                    <div class="isotope-grid cols-5 mb-2">
                        <div class="gutter-sizer"></div>
                        <div class="grid-sizer"></div>
                        <!-- Product-->

                        @foreach($subcategories->where('maincat_url' , request()->segment(count(request()->segments()))) as $category)

                            <div class="grid-item">
                                <div class="product-card">

                                    <h3 class="product-title" style="max-height: 100px; width: auto">{{$category->subcat}}</h3>
                                    <br>
                                    <a class="product-thumb" href="/category/subcategory-grid/{{$category->subcat_url}}"><img src="{{$category->mainpic_url}}" alt="Product" ></a>

                                </div>
                            </div>
                        @endforeach
                    </div>
                    <!-- Products Grid-->
                {{--<div class="isotope-grid cols-3 mb-2">--}}
                {{--<div class="gutter-sizer"></div>--}}
                {{--<div class="grid-sizer"></div>--}}
                {{--<!-- Product-->--}}

                {{--@foreach($category->where('sub_category_url' , request()->segment(count(request()->segments()))) as $grid)--}}
                {{--                        @foreach ($products->where('category_url' , 'Camera_Accesoires') as  $product)--}}
                {{--@foreach($Subcategory->where('category' , $grid->category) as $grid)--}}

                {{--<div class="grid-item">--}}
                {{--<div class="product-card">--}}
                {{--<a href="{{$grid->sub_category_url}}">--}}
                {{--@if(!empty($product->btw_percentage))--}}{{--f--}}
                {{--<div class="product-badge text-danger">{{$product->btw_percentage}}% Korting</div>--}}

                {{--@endif--}}
                {{--<br>--}}
                {{--                                    <a class="product-thumb" href="category/{{$grid->id}}"><img src="" alt="Product"></a>--}}
                {{--<h4 class="product-price">--}}

                {{--{{$grid->sub_category}}<br>--}}

                {{--@endif--}}
                {{--</h4>--}}
                {{--</a>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--@endforeach--}}
                {{--@endforeach--}}
                {{--</div>--}}
                <!-- Pagination-->


                    {{--<nav class="pagination">--}}
                    {{--<ul class="pages">--}}
                    {{--</ul>--}}
                    {{--<div class="column text-right hidden-xs-down"><a class="btn btn-outline-secondary btn-sm" href="#">Next&nbsp;<i class="icon-arrow-right"></i></a></div>--}}
                    {{--</nav>--}}
                </div>
                <!-- Sidebar          -->
{{--                @include('layouts.menusidebar')--}}
            </div>
        </div>
        <!-- Site Footer-->
        @include('layouts.footer')

        @endsection
    </div>
    <!-- Back To Top Button--><a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
    <!-- Backdrop-->
    <div class="site-backdrop"></div>
    <!-- JavaScript (jQuery) libraries, plugins and custom scripts-->
    <script src="js/vendor.min.js"></script>
    <script src="js/scripts.min.js"></script>
