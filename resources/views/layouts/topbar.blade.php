 <!-- Topbar-->
 @include('cookieConsent::index')

 <div class="topbar" style="background-color: #6e5e4e ; ">

     @if(!\App\Governor::$agent->isMobile() && !\App\Governor::$agent->isTablet())

     <div class="topbar-column"><a class="hidden-md-down" href="mailto:info@lorandomorini.nl" style="color: ghostwhite">
          <i class="icon-mail"></i>&nbsp;
          info@lorandomorini.nl </a>

        <a class="hidden-md-down" href="tel:0031623235509" style="color: ghostwhite">  <i class="icon-bell"></i>&nbsp; +31 6 23 23 55 09</a>
         @if(\App\Governor::$agent->isMobile())


       @else
          <a class="social-button sb-facebook shape-none sb-dark" href="#" target="_blank">
          <i class="socicon-facebook"></i></a><a class="social-button sb-twitter shape-none sb-dark" href="#" target="_blank">
          <i class="socicon-twitter"></i></a><a class="social-button sb-instagram shape-none sb-dark" href="#" target="_blank">
          <i class="socicon-instagram"></i></a><a class="social-button sb-pinterest shape-none sb-dark" href="#" target="_blank">
                 <span style="color:whitesmoke ; font-weight: bold; font-size: 14px; padding-right: 50px">   Gratis verzending boven &euro;30,- </span>
             {{--              <span style="color:Gold ; font-weight: bold; font-size: 20px; padding-right: 50px">Altijd 10% korting als VIP member, word nu gratis VIP!!</span>--}}

             {{--          <i class="socicon-pinterest"></i></a>--}}
         @endif

      </div>


{{--          <span style="color:#C8584E ; font-weight: bold; font-size: 14px">Voor 23:00 besteld, morgen geleverd!</span>--}}
{{--          <span style="color:whitesmoke ; font-weight: bold; font-size: 14px; padding-right: 50px">Vanaf 5 kilo 10 % korting!!</span>--}}
{{--          <span style="color:whitesmoke ; font-weight: bold; font-size: 14px; padding-right: 50px">Snel bij u bezorgd! </span>--}}
{{--              <a href="https://caffeitalia.nl"><span style="color:Gold ; font-weight: bold; font-size: 20px;">Caffeitalia.nl : Word Lid van Ons Team: We Zijn Op Zoek naar Koks en Bedieningsmedewerkers!</span></a>--}}




              <a></a>

{{--              <a class="hidden-md-down" href="/contacts" style="color: ghostwhite">Contact</a>--}}
{{--        <div class="lang-currency-switcher-wrap">--}}
{{--          <div class="lang-currency-switcher dropdown-toggle"><span class="language"><img alt="English" src="/img/flags/GB.png"></span><span class="currency">€  EUR</span></div>--}}
{{--          <div class="dropdown-menu">--}}
{{--            <div class="currency-select">--}}
{{--              <select class="form-control form-control-rounded form-control-sm">--}}
{{-- 		<option value="usd">€ EUR</option>--}}
{{--                --}}{{----}}{{--<option value="usd">$ USD</option>--}}
{{--                --}}{{----}}{{--<option value="usd">£ UKP</option>--}}
{{--                --}}{{----}}{{--<option value="usd">¥ JPY</option>--}}
{{--              </select>--}}
{{--            </div><a class="dropdown-item" href="#"><img src="img/flags/NL.png" alt="Nederlands">Nederlands</a>--}}
{{--            --}}{{--<a class="dropdown-item" href="#">--}}
{{--              --}}{{--<img src="img/flags/DE.png" alt="Deutsch">Deutsch</a>--}}
{{--            --}}{{--<a class="dropdown-item" href="#"><img src="img/flags/IT.png" alt="Italiano">Italiano</a>--}}
{{--          </div>--}}
{{--        </div>--}}
      </div>


 @endif

{{-- @if(\App\Governor::$agent->isMobile())--}}
{{--     <div class="row" style="padding: 10px">--}}
{{--     <a href="https://caffeitalia.nl"><span style="color:Gold ; font-weight: bold; font-size: 12px ;padding-left: 10px; padding-right: 10px;">--}}
{{--                          Caffeitalia.nl : Word Lid van Ons Team: <br>We Zijn Op Zoek naar Koks en Bedieningsmedewerkers!</span></a>--}}
{{--     </div>--}}
{{-- @endif--}}
 </div>
