<a href="/shoppingcart"></a><i class="icon-bag"></i>
<span class="count" style="font-size: 18px">
    {{ Session::has('cart') ? Session::get('cart')->totalQty : '' }}
</span>
<span class="subtotal" style="font-size: 18px">€ {{ round(Session::has('cart') ? Session::get('cart')->totalPrice : '' , 2)}}</span>
<div class="toolbar-dropdown" id="cart-toolbar" style="background-color: #191919">
    @if (!Session::has('cart'))
        <img src="/img/emptycart.png" style="padding-bottom: 20px"><br>
        <span>Winkelwagen is leeg</span>
    @else

        @foreach (Session::get('cart')->items as $product)

            <div class="dropdown-product-item">
                <span class="dropdown-product-remove">
                   <a class="remove-from-cart" href="{{ route('product.remove', ['id'=> $product['item']['id']])}}" data-toggle="tooltip"
                      title="Remove item"><i class="icon-cross"></i>
                   </a>
                </span>

                <a class="dropdown-product-thumb" href="category/subcategory-grid/single-product/{{$product['item']['artnr']}}">
                    <img src="{{$product['item'] ['image_1']}}" alt="Product">
                </a>
                <div class="dropdown-product-info">
                    <a class="dropdown-product-title"
                       href="{{$product['item']['artnr']}}">{{$product['item'] ['name']}}</a>
                    <span class="dropdown-product-details">{{$product['qty']}}
                        x €{{(round($product['item']['price'] , 2 ))}}</span>
                </div>
            </div>
        @endforeach
     @endif
     @if(Session::has('cart'))
        <div class="toolbar-dropdown-group" >
          <div class="column"><span class="text-lg">Totaal:</span></div>
          <div class="column text-right"><span class="text-lg text-medium">€{{ number_format((Session::has('cart')) ? Session::get('cart')->totalPrice : '' , 2)}}&nbsp;</span></div>
        </div>
    @endif
    <div class="toolbar-dropdown-group">
        <div class="column"><a class="btn btn-sm btn-block btn-secondary" href="/shoppingcart" style="font-size: 11px">Winkelwagen</a></div>
        @if (!Session::has('cart'))
        @else
            <div class="column"><a class="btn btn-sm btn-block btn-success" href="/checkout-address" style="font-size: 11px">Bestellen</a></div>
        @endif
    </div>
</div>