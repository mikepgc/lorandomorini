@yield('content')

{{--    <div class="navbar">--}}
{{--        <button onclick="openCity(event, 'cryptos');" class="button crypto" id="defaultOpen" style="font-size: 10px" >Cryptos</button>--}}
{{--        <button onclick="openCity(event, 'mike')" class="button mike"  id="mike" style="font-size: 10px">Mike</button>--}}
{{--        <button onclick="openCity(event, 'sammy')" class="button sammy" id="sammy" style="font-size: 10px">Sammy</button>--}}
{{--        <button onclick="openCity(event, 'binance')" class="button sammy" id="sammy" style="font-size: 10px">Binance</button>--}}
{{--    </div>--}}
    <nav class="mobile-bottom-nav">
    <div class="mobile-bottom-nav__item mobile-bottom-nav__item--active">
        <div class="mobile-bottom-nav__item-content">
{{--            <a onclick="openCity(event, 'lorando');" ><span class="iconify" data-icon="grommet-icons:coffee" data-width="25">--}}
            <span onclick="openCity(event, 'lorando');"  id="defaultOpen" >
                <span class="iconify" data-icon="grommet-icons:coffee" data-width="25">
{{--                <span class="iconify" data-icon="mdi:bitcoin"  data-width="25">--}}
                </span>
            </span>
            <span style="font-size: 10px">Lorando & Morini</span>
        </div>
    </div>
    <div class="mobile-bottom-nav__item">
        <div class="mobile-bottom-nav__item-content">
            <span  style="border:0"onclick="openCity(event, 'users')"   id="mike"><span class="iconify" data-icon="clarity:users-line" data-width="25"></span></span>
            <span style="font-size: 10px">Lorando Gebruikers</span>
        </div>
    </div>
        <div class="mobile-bottom-nav__item">
        <div class="mobile-bottom-nav__item-content">
            <span  style="border:0"onclick="openCity(event, 'caffeitalia')"   id="mike"><span class="iconify" data-icon="grommet-icons:restaurant" data-width="25"></span></span>
            <span style="font-size: 10px">Caffé Italia</span>
        </div>
    </div>

</nav>
<script>
    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // default open on "id"

    document.getElementById("defaultOpen").click();
</script>
<style>
    /* Place the navbar at the bottom of the page, and make it stick */
    .navbar {
        background-color: #333;
        overflow: hidden;
        position: fixed;
        padding-bottom: 25px;
        bottom: 0;
        width: 100%;

    }

    .button{
        border:0;
        float: left;
        display: block;
        color: #f2f2f2;
        text-align: center;
        padding: 14px 16px;
        text-decoration: none;
        font-size: 17px;
        margin: 0;
        border-radius: 10px;
        background: transparent;
    }

    .button.active {
        background-color: rgba(218, 165, 45, 0.2);
        color: white;

    }

    .mike.active{

        background-color: blue;
    }
    .sammy.active{

        background-color: pink;
    }
</style>

<script>
    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // default open on "id"

    document.getElementById("defaultOpen").click();
</script>
<script src="https://code.iconify.design/2/2.0.3/iconify.min.js"></script>
