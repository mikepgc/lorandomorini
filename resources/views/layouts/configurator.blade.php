<div id="filterbox" class="no_padding">
    <div id="filter" class="{{\App\Governor::$car ? 'collapsed' : ''}}">
        <div class="item  active">
            <div class="tab ">
                <span class="caricon "></span>
                {{trans('menu.Zoek op auto')}}            </div>
            <div class="content" id="bycar">
                <form method="POST" id="search_car_by_dropdown_form">
                    <div class="selbox col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                        <div class="select_wrapper">
                            <select name="merk" id="merk" class="active">
                                <option value="" id="emptyModel">{{trans('menu.selecteer merk')}}</option>
                            </select>
                            <span class="arrow">▼</span>
                        </div>
                    </div>

                    <div class="selbox col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                        <div class="select_wrapper">
                            <select name="model" id="model"><option value="" id="emptyModel">{{trans('menu.selecteer model')}}</option></select><span class="arrow">▼</span>
                        </div>
                    </div>

                    <div class="selbox col-xs-12 col-sm-12 col-md-4 last col-lg-4 co-xl-4 last">
                        <div class="select_wrapper">
                            <select name="motorinhoud" id="motorinhoud" required ><option value="">{{trans('menu.selecteer motorinhoud')}}</option></select><span class="arrow">▼</span>
                        </div>
                    </div>

                    <div class="explanation">
                        <span>{{trans('menu.Vul de gegevens van uw auto in om te zien welke banden en velgen er op uw auto passen.')}}</span>
                        <button type="submit" id="bycar_submit" class="button">{{trans('menu.Zoek mijn wielen')}}</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="item ">
            <div class="tab">
                <span class="plateicon "></span>
                {{trans('menu.Zoek op kenteken')}}</div>
            <div class="content" id="byplate">
                <form method="POST">



                    <!-- Generator: Adobe Illustrator 19.2.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 40 60" style="enable-background:new 0 0 40 60;" xml:space="preserve">
<rect style="fill:#2B4999;" width="40" height="60"/>
                        <g>
                            <g>
                                <g id="star-rate">
                                    <polygon style="fill:#FFD200;" points="20.1,9.4 21.4,10.4 20.8,8.8 22.3,7.7 20.5,7.7 20.1,6.1 19.5,7.7 17.9,7.7 19.2,8.8
				18.6,10.4 			"/>
                                </g>
                            </g>
                            <g>
                                <g id="star-rate_1_">
                                    <polygon style="fill:#FFD200;" points="8,21.4 9.4,22.3 8.9,20.6 10.3,19.7 8.6,19.7 8,18.1 7.4,19.7 5.8,19.7 7.1,20.6
				6.7,22.3 			"/>
                                </g>
                            </g>
                            <g>
                                <g id="star-rate_2_">
                                    <polygon style="fill:#FFD200;" points="19.9,33.4 21.4,34.4 20.8,32.8 22.1,31.9 20.5,31.9 19.9,30.1 19.5,31.9 17.7,31.9
				19.2,32.8 18.6,34.4 			"/>
                                </g>
                            </g>
                            <g>
                                <g id="star-rate_3_">
                                    <polygon style="fill:#FFD200;" points="32,21.5 33.3,22.5 32.9,20.9 34.2,19.8 32.6,19.8 32,18.2 31.4,19.8 29.7,19.8 31.1,20.9
				30.6,22.5 			"/>
                                </g>
                            </g>
                        </g>
                        <g>
                            <g>
                                <g id="star-rate_7_">
                                    <polygon style="fill:#FFD200;" points="28.3,12.8 29.7,13.9 29.1,12.2 30.6,11.2 28.8,11.2 28.3,9.5 27.7,11.2 26.1,11.2
				27.4,12.2 26.8,13.9 			"/>
                                </g>
                            </g>
                            <g>
                                <g id="star-rate_6_">
                                    <polygon style="fill:#FFD200;" points="11.3,12.8 12.7,13.7 12.2,12.1 13.6,11.2 11.9,11.2 11.3,9.5 10.7,11.2 9.1,11.2
				10.4,12.1 10,13.7 			"/>
                                </g>
                            </g>
                            <g>
                                <g id="star-rate_5_">
                                    <polygon style="fill:#FFD200;" points="11.1,29.7 12.6,30.7 12,29.1 13.4,28.2 11.7,28.2 11.1,26.4 10.7,28.2 8.9,28.2
				10.4,29.1 9.8,30.7 			"/>
                                </g>
                            </g>
                            <g>
                                <g id="star-rate_4_">
                                    <polygon style="fill:#FFD200;" points="28.2,29.8 29.6,30.9 29.1,29.2 30.5,28.2 28.8,28.2 28.2,26.5 27.6,28.2 26,28.2
				27.3,29.2 26.9,30.9 			"/>
                                </g>
                            </g>
                        </g>
                        <polygon style="fill:#FFFFFF;" points="10.8,53.5 10.8,41.5 12.8,41.5 17.2,49.3 17.2,41.5 19.3,41.5 19.3,53.5 17.2,53.5
	12.7,45.8 12.7,53.5 "/>
                        <polygon style="fill:#FFFFFF;" points="21.7,41.5 24,41.5 24,51.4 29.4,51.4 29.4,53.5 21.7,53.5 "/>
</svg>


                    <input type="text" name="license_plate" placeholder="{{trans('menu.vul hier uw kenteken in')}}"><button id="license-search-button" name="searchByLicense" class="hidden-xs button">{{trans('menu.Zoek mijn wielen')}}</button>
                </form>
                <div class="explanation">
                    <span>{{trans('menu.Vul uw kenteken in en wij vinden velgen en banden die passen bij uw auto.')}}</span>
                    <button name="searchByLicense" class="visible-xs button" onclick="$('#license-search-button').click()">{{trans('menu.Zoek mijn wielen')}}</button>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="tab ">
                <span class="keywordicon "></span>
                {{trans('menu.Vrij zoeken')}}            </div>
            <div class="content" id="bykeyword">
                <form method="GET" action="{{URL::route('search')}}">
                    <input type="text" name="searchQuery" placeholder="{{trans('menu.Typ hier uw zoekterm in')}}" value="">
                    <button id="free-search-button" class="hidden-xs button">{{trans('menu.Zoeken')}}</button>
                </form>
                <div class="explanation">
                    <span>{{trans('menu.Bijvoorbeeld een automerk, modelnaam of een inchmaat etc.')}}</span>
                    <button onclick="$('#free-search-button').click()" name="searchFree" class="visible-xs button">{{trans('menu.Zoeken')}}</button>
                </div>
            </div>
        </div>
        <div class="toggle" title="Verberg">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve"><g><polygon style="fill:#5D5D5D;" points="49,40.3 25,10 1,40.3 	"></polygon></g></svg>
        </div>
        <div class="current">
            <span>
                <strong>{{trans('menu.Uw auto :')}}</strong>
                @if(\App\Governor::$car)
                    <a class="lb_car_info">
                        {{\App\Governor::$car->fullName}}
                        <span class="info">i</span>
                    </a>
                @else
                    {{trans('menu.u heeft geen auto geselecteerd')}}
                @endif
            </span>
            @if(\App\Governor::$car)
                <span>
                <a class="remove_car">{{trans('menu.verwijderauto')}}</a>|
                <a class="edit_car">{{trans('menu.andereautoselecteren')}}</a>
            </span>
            @endif
        </div>
    </div>
    @if(\App\Governor::$car)
        <div id="cboxLoadedContent" style="display: none; width: 1170px; padding: 0;">
            <div class="details"><div class="header"><h2>Autogegevens</h2></div>
                <div class="cbox_content">
                    <table>
                        <tbody><tr>
                            <td><strong>Merk / model / type</strong></td>
                            <td>{{\App\Governor::$car->fullName}}</td>
                            <td><strong>Brandstof type proces</strong></td>
                            <td>{{\App\Governor::$car->fuelTypeProcess}}</td>
                        </tr>

                        <tr>
                            <td><strong>Gebouwd van/tot</strong></td>
                            <td>{{\App\Governor::$car->constructionFrom}} - {{\App\Governor::$car->constructionTo}}</td>
                            <td><strong>Kw</strong></td>
                            <td>{{\App\Governor::$car->powerKwFrom}}</td>
                        </tr>

                        <tr>
                            <td><strong>Constructietype</strong></td>
                            <td>{{\App\Governor::$car->constructionType}}</td>
                            <td><strong>PK</strong></td>
                            <td>{{\App\Governor::$car->powerHpFrom}}</td>
                        </tr>
                        <tr>
                            <td><strong>Motorcode</strong></td>
                            <td>{{\App\Governor::$car->motorType}}</td>
                            <td><strong>CC</strong></td>
                            <td>{{\App\Governor::$car->cylinderCapacityCcm}}</td>
                        </tr>
                        <tr>
                            <td><strong>Brandstof type</strong></td>
                            <td>{{\App\Governor::$car->fuelType}}</td>
                            <td><strong>Cylinder</strong></td>
                            <td>{{\App\Governor::$car->cylinder}}</td>
                        </tr>
                        </tbody></table>
                </div>

                <div class="header">
                    <h2>Bandenmaten</h2>
                </div>

                <div class="cbox_content">
                    <table>
                        <tbody>
                        @foreach(\App\Models\Driveright\Upstep::where('chassis_instance_id', \App\Governor::$car->chassis->instanceId)->orderBy('wheel_size')->get() as $upstep)
                            <tr>
                                <td class="borderless"><strong>{{$upstep->wheelSize}}</strong></td>
                                <td class="borderless tyre_col">{{$upstep->tyre1}}</td>
                                <td class="borderless tyre_col">{{$upstep->tyre2}}</td>
                                <td class="borderless tyre_col">{{$upstep->tyre3}}</td>
                                <td class="borderless tyre_col">{{$upstep->tyre4}}</td>
                                <td class="borderless tyre_col">{{$upstep->tyre5}}</td>
                                <td class="borderless tyre_col">{{$upstep->tyre6}}</td>
                                <td class="borderless tyre_col">{{$upstep->tyre7}}</td>
                                <td class="borderless tyre_col">{{$upstep->tyre8}}</td>
                                <td class="borderless tyre_col">{{htmlspecialchars($upstep->comments)}}</td>
                                <td class="borderless tyre_col">{{$upstep->upstepId}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    @endif
</div>
