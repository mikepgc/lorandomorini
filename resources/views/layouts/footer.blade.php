<!-- Site Footer-->
      <footer class="site-footer" style="background-color: #6e5e4e; border-top: 1px solid white">
        <div class="container">
          <div class="row">
            <div class="col-lg-3 col-md-6">
              <!-- Contact Info-->
              <section class="widget widget-light-skin">
                <h3 class="widget-title">Contactgegevens</h3>
              {{--  <p class="text-white">Phone: 00 33 169 7720</p>
                <ul class="list-unstyled text-sm text-white">
                  <li><span class="opacity-50">Monday-Friday:</span>9.00 am - 8.00 pm</li>
                  <li><span class="opacity-50">Saturday:</span>10.00 am - 6.00 pm</li>
                </ul>--}}
                <p><i class="icon-mail"></i><a class="navi-link-light" href="mailto:info@lorandomorini.nl"> Lorando & Morini</a><br><i class="icon-bell"></i><a class="navi-link-light" > +31 6 23 23 55 09</a></p>
                <p><a class="navi-link-light" >Kiotoweg 133<br>
                        3047BG Rotterdam</a></p>
                  <a class="social-button shape-circle sb-facebook" href="https://www.facebook.com/sharer/sharer.php?u={{url()->current()}}" target="_blank" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="socicon-facebook"></i></a>
                  <a class="social-button shape-circle sb-twitter" href="https://twitter.com/intent/tweet?text=LorandoMorini&url={{url()->current()}}" target="_blank"data-toggle="tooltip" data-placement="top" title="Twitter"><i class="socicon-twitter"></i></a>
              </section>
            </div>
       {{--     <div class="col-lg-3 col-md-6">
              <!-- Mobile App Buttons-->
              <section class="widget widget-light-skin">
                <h3 class="widget-title">Our Mobile App</h3><a class="market-button apple-button mb-light-skin" href="#"><span class="mb-subtitle">Download on the</span><span class="mb-title">App Store</span></a><a class="market-button google-button mb-light-skin" href="#"><span class="mb-subtitle">Download on the</span><span class="mb-title">Google Play</span></a><a class="market-button windows-button mb-light-skin" href="#"><span class="mb-subtitle">Download on the</span><span class="mb-title">Windows Store</span></a>
              </section>
            </div>--}}
            <div class="col-lg-3 col-md-6">
              <!-- About Us-->
{{--              <section class="widget widget-links widget-light-skin">--}}
{{--                <h3 class="widget-title">Over ons</h3>--}}
{{--                <ul>--}}
{{--                  <li><a href="#">Over Jouwfotografieshop.nl</a></li>--}}
{{--                  <li><a href="contacts">Contact</a></li>--}}
{{--                </ul>--}}
{{--              </section>--}}
            </div>
            <div class="col-lg-3 col-md-6">
              <!-- Account / Shipping Info-->
              <section class="widget widget-links widget-light-skin">
                <h3 class="widget-title">Account &amp;  Info</h3>
                <ul>
                  @if(Auth::check())
                  <li><a href="/account-profile">Uw Account</a></li>
                  @else
                    <li><a href="/account-login">Uw Account</a></li>

                  @endif
                    <li><a href="/voorwaarden">Algemene voorwaarden</a></li>
                    <li><a href="/privacy">Privacy policy</a></li>
{{--                  <li><a href="#">Refunds & Replacements</a></li>--}}
{{--                  <li><a href="#">Delivery Info</a></li>--}}
                </ul>
              </section>
            </div>
          </div>
          <hr class="hr-light mt-2 margin-bottom-2x">
          <div class="row">
            <div class="col-md-7 padding-bottom-1x">
              <!-- Payment Methods-->
              <b style="color: #fff">Veilig betalen:</b><br> <div class="margin-bottom-1x" style="max-width: 615px;">
                    <img src="/img/ideal.png" width="50px" alt="Payment Methods"> &nbsp;&nbsp;
                    <img src="/img/paypal.png" width="80px" alt="Payment Methods">
{{--                    <img src="/img/docs/thuiswinkel.png">--}}

                </div>
            </div>
            <div class="col-md-5 padding-bottom-1x">
              <div class="margin-top-1x hidden-md-up"></div>
              <!--Subscription-->
              <form class="subscribe-form" action="storeNieuwsbrief" method="post" novalidate>
                <div class="clearfix">
                  <div class="input-group input-light padding-bottom-1x">
                    <input class="form-control" type="Naam" id="naam" name="naam" placeholder="Uw naam"><span class="input-group-addon" style="padding-bottom: 20px "><i class="icon-head"></i></span>
                  </div>

                  <div class="input-group input-light">
                    <input class="form-control" type="email" name="email" id="email" placeholder="Uw e-mail"><span class="input-group-addon"><i class="icon-mail"></i></span>
                  </div>
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                  <div style="position: absolute; left: -5000px;" aria-hidden="true">
                    <input type="text" name="b_c7103e2c981361a6639545bd5_1194bb7544" tabindex="-1">
                  </div>
                  <button class="btn btn-primary" type="submit"><i class="icon-check"></i></button>
                </div><span class="form-text text-sm text-white opacity-50">Schijf u in voor onze nieuwsbrief, aanbiedingen en het laatste nieuws.</span>
              </form>
            </div>
          </div>
          <!-- Copyright-->
            <p class="footer-copyright">Alle rechten voorbehouden. Photography & Webshop by <a href="https://jae-it.nl" target="_blank">JAE-IT</a> 2024 © </p>
        </div>
      </footer>
