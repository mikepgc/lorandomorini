<div class="col-xl-3 col-lg-4 order-lg-1">
    <button class="sidebar-toggle position-left" data-toggle="modal" data-target="#modalShopFilters"><i class="icon-layout"></i></button>
    <aside class="sidebar sidebar-offcanvas" style="padding-top: 75px">

        <!-- Widget Price Range-->

        <section class="widget widget-categories">
            <h3 class="widget-title">Prijs</h3>
{{--            <form action="javascript:;" class="price-range-slider" method="post" data-start-min="{{ \Request::get('filterPriceMin','0') }}" data-start-max="{{ \Request::get('filterPriceMax','50') }}" data-min="0" data-max="50" data-step="1">--}}
                <div class="ui-range-slider"></div>
                <footer class="ui-range-slider-footer">
                    <div class="column">
                        <button class="btn btn-outline-primary btn-sm" onclick="filterData()" type="submit">Filter</button>
                    </div>
                    <div class="column">
                        <div class="ui-range-values">
                            <div class="ui-range-value-min">&euro;<span></span>
                                <input type="hidden">
                            </div>&nbsp;-&nbsp;
                            <div class="ui-range-value-max">&euro;<span></span>
                                <input type="hidden">
                            </div>
                        </div>
                    </div>
                </footer>
{{--            </form>--}}
        </section>


        <section class="widget widget-categories">
            <h3 class="widget-title">Filter
                @if(\Request::segment(2) == 'filter-list')
                    <button class="btn btn-outline-primary btn-sm" onclick='window.location.href= "{{ route("pages.filter-product-list") }}"' type="button" style="font-size: 10px; height: 22px;padding: 0 9px; border-radius: 12px;line-height: 14px">RESET</button>
                @else
                    <button class="btn btn-outline-primary btn-sm" onclick='window.location.href= "{{ route("pages.filter-product-grid") }}"' type="button"  style="font-size: 10px; height: 22px;padding: 0 9px; border-radius: 12px;line-height: 14px">RESET</button>
                @endif
            </h3>

            @if(!empty(\Request::get('subcat')))
                <div class="column">
                    @foreach(\Request::get('subcat') as $subcategory)
                        <button class="btn btn-outline-primary btn-sm" style="font-size: 10px; height: 22px;padding: 0 9px; border-radius: 12px;line-height: 14px"onclick="return $('input#{{ $subcategory }}').trigger('click')" type="button"> x {{ $subcategory }}</button>
                    @endforeach
                </div>
            @endif
            <ul>

                @foreach($categories->where('visible' , '1') as $category)
                    <li class="has-children expanded" ><a href="javascript:;">{{$category->maincat}}</a>
                        <ul>

                            @foreach(App\Models\LM::where('maincat' , $category->maincat)->groupBy('subcat')->pluck('subcat') as $subcategory)
                                <li>
                                    <?php
                                    $checked = '';
                                    if(!empty(\Request::get('subcat'))){
                                        $checked = in_array($subcategory,\Request::get('subcat'))?"checked":"";
                                    }

                                    ?>
                                    <input type="checkbox" id="{{ $subcategory }}" {{$checked }} onchange="filterData()" value="{{ $subcategory }}" name="subcat[]" />
                                    <label for="{{ $subcategory }}">
                                        <a href="javascript:;"><span>{{$subcategory}}</span></a>
                                    </label>
                                </li>
                            @endforeach
                        </ul>
                    </li>

                @endforeach


            </ul>
        </section>


        <!-- Promo Banner-->

        {{--<section class="promo-box" style="background-image: url(https://www.benel.nl/images/webshop/matin-smartphone-adapter-cr3-m-7123-full-169769-1-38160-114.jpg);">--}}
        {{--<!-- Choose between .overlay-dark (#000) or .overlay-light (#fff) with default opacity of 50%. You can overrride default color and opacity values via 'style' attribute.--><span class="overlay-dark" style="opacity: .45;"></span>--}}
        {{--<div class="promo-box-content text-center padding-top-3x padding-bottom-2x">--}}
        {{--<h4 class="text-light text-thin text-shadow">Nieuw!!</h4>--}}
        {{--<h3 class="text-bold text-light text-shadow">Smartphone fotografie</h3><a class="btn btn-sm btn-primary" href="/category/subcategory-list/Smartphone_Accessoires" >Shop Nu</a>--}}
        {{--</div>--}}
        {{--</section>--}}


        {{--<br><br>--}}


        {{--<!-- Promo2 Banner-->--}}

        {{--<section class="promo-box" style="background-image: url(https://www.benel.nl/images/webshop/falcon-eyes-led-lamp-set-dimbaar-dv-160v-met-statieven-full-2204-1-36074-116.jpg);">--}}
        {{--<!-- Choose between .overlay-dark (#000) or .overlay-light (#fff) with default opacity of 50%. You can overrride default color and opacity values via 'style' attribute.--><span class="overlay-dark" style="opacity: .45;"></span>--}}
        {{--<div class="promo-box-content text-center padding-top-3x padding-bottom-2x">--}}
        {{--<h4 class="text-light text-thin text-shadow">Creeër je perfecte foto met de juiste belichting waar je ook bent!!</h4>--}}
        {{--<h5 class="text-bold text-light text-shadow">Falcon Eyes Soft LED Lamp Set DV-80SL incl. 2 accu's en handvat</h5><a class="btn btn-sm btn-primary" href="/category/subcategory-list/single-product/2228" >Shop Nu</a>--}}
        {{--</div>--}}
        {{--</section>--}}


    </aside>
</div>
<style>

    a {
        color: white
    }
</style>
<script>

    $(document).ready(function(){

        fill_datatable();

        function fill_datatable(filter_brand = '')
        {
            var dataTable = $('#customer_data').DataTable({
                processing: true,
                serverSide: true,
                ajax:{
                    url: "{{ route('subcategory-grid.index') }}",
                    data:{filter_brand:filter_brand}
                },
                columns: [
                    {
                        data:'sub_category_url',
                        name:'sub_category_url'
                    },
                    {
                        data:'artnr',
                        name:'artnr'
                    },
                    {
                        data:'art_description_NL',
                        name:'art_description_NL'
                    },
                    {
                        data:'image_url_1',
                        name:'image_url_1'
                    },
                    {
                        data:'stock',
                        name:'stock'
                    },
                    {
                        data:'price',
                        name:'price'
                    },
                    {
                        data:'purchase_exbtw',
                        name:'purchase_exbtw'
                    }
                ]
            });
        }

        $('#filter').click(function(){
            var filter_brand = $('#filter_brand').val();


            if(filter_gender != '')
            {
                $('#customer_data').DataTable().destroy();
                fill_datatable(filter_brand);
            }
            else
            {
                alert('Select Both filter option');
            }
        });

        $('#reset').click(function(){
            $('#filter_brand').val('');
            $('#brand').DataTable().destroy();
            fill_datatable();
        });

    });
</script>
