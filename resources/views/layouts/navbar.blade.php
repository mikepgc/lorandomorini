      <!-- Main Navigation-->
{{--    backgroundcolor:  #9f1b32;--}}
      <!-- Navbar-->
      <!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
      <header class="navbar navbar-sticky" style="background-color: #6e5e4e">
          <!-- Search-->
          <form class="site-search" method="post" style="background-color: ghostwhite" onsubmit="window.location = '/category/search/' + search.value; return false;">
              <input type="text" name="search"  placeholder="Zoeken...">
              <div class="search-tools">
                  <span class="clear-search">Wissen</span>
                  <span class="close-search"><i class="icon-cross" style="color: #0a0a0a"></i></span>
              </div>

              {{csrf_token()}}
          </form>
          <div class="site-branding">
              <div class="inner">
                  <!-- Off-Canvas Toggle (#shop-categories)-->
                  {{--<a class="offcanvas-toggle cats-toggle" href="#shop-categories" data-toggle="offcanvas"></a>--}}
                  <!-- Off-Canvas Toggle (#mobile-menu)-->

                  @if(\App\Governor::$agent->isMobile())
                      <a class="offcanvas-toggle menu-toggle" href="#mobile-menu" data-toggle="offcanvas"></a>

                      <a href="/"><img src="/img/logo/logo-white-small.png" class="img-fluid" style="width: 120px; padding-left: 20px" alt="Responsive image"> </a>
                  @endif


              </div>
          </div>
          <nav class="site-menu">
              <ul>
                  <li class="has-megamenu"><a href="/about_us"><span style="font-size: 16px">Over ons</span></a></li>
                  <li><a href="/category/subcategory-grid/koffie"><span style="font-size: 16px">Koffie</span></a></li>
                  <li class="has-megamenu"><a href="/"><span style="font-size: 16px">Accessoires</span></a></li>

              @if(\App\Governor::$agent->isDesktop())

                      <li class="has-megamenu">
                          <a href="/"><img src="/img/logo/logo-white.png" class="img-fluid"
                                           style="height: 83px; padding-left: 20px" alt="Responsive image"> </a>
                      </li>

                  @endif
                  <li><a href="/b2b"><span style="font-size: 16px">Voor Bedrijven</span></a></li>
                  <li><a href="/gallery"><span style="font-size: 16px">Gallery</span></a></li>
                  <li><a href="/contacts"><span style="font-size: 16px">Contact</span></a></li>

              </ul>
      </nav>


      <!-- Toolbar-->
      <div class="toolbar">
        <div class="inner">
          <div class="tools">
              @if(Auth::check() && Auth::user()->account == '1')
            <div class="search"><img src="/img/icon-search.png"></div>

              @else
                  <div class="search"><i class="icon-search"></i></div>
              @endif
            <div class="account">

                <a href="/account-profile"></a><i class="icon-head"></i>
{{--                <a href="/account-profile"  style="background-color: #C8584F; "><i class="icon-head"></i></a>--}}

              <ul class="toolbar-dropdown">
                  @if(Auth::check() && Auth::user()->account == '1')

                      <li class="sub-menu-user">
                          @if(Auth::user()->profilePic)
                          <div class="user-ava"><img src="{{Auth::user()->profilePic}}" alt="{{Auth::user()->name}} {{Auth::user()->surname}}">
                          </div>
                          @endif
                          <div class="user-info">
                              <h6 class="user-name" style="color:black">{{Auth::user()->name}} {{Auth::user()->surname}}</h6>
                              {{--<span class="text-xs text-muted">290 Reward points</span>--}}
                          </div>
                      </li>
                      <li><a href="/account-profile">Mijn profiel</a></li>
                      <li><a href="/account-orders">Mijn orders</a></li>
{{--                      <li><a href="/account-wishlist">Mijn wensenlijst</a></li>--}}
                      <li class="sub-menu-separator"></li>
                      <li><a href="{{route('logout')}}"> <i class="icon-unlock"></i>Uitloggen</a></li>

                  @else

                      <li><a href="{{ route('logout') }}">Inloggen</a></li>
                      <li><a href="{{ route('account-register') }}">Registreren</a></li>
                  @endif

              </ul>
            </div>


            <div class="cart" id="cart-toolbar" >
                <a href="/shoppingcart"></a><i class="icon-bag"></i><span class="count" style="font-size: 18px">
                    {{ Session::has('cart') ? Session::get('cart')->totalQty : '' }}
                </span>
                <span class="subtotal" style="font-size: 18px">€ {{ round(Session::has('cart') ? Session::get('cart')->totalPrice : '' , 2)}}</span>
                <div class="toolbar-dropdown" style="background-color: #191919">

        {{--{{ \App\Governor::$productsCart }}--}}

                  @if (!Session::has('cart'))
                      <img src="/img/emptycart.png" style="padding-bottom: 20px"><br>
                      <span>Winkelwagen is leeg</span>
                  @else

                      @foreach (Session::get('cart')->items as $product)

                          <div class="dropdown-product-item">
                              <span class="dropdown-product-remove">
                                 <a class="remove-from-cart" href="{{ route('product.remove', ['id'=> $product['item']['id']])}}" data-toggle="tooltip"
                                    title="Remove item"><i class="icon-cross"></i>
                                 </a>
                              </span>

                              <a class="dropdown-product-thumb" href="/category/subcategory-grid/single-product/{{$product['item']['artnr']}}">
                                  <img src="{{$product['item'] ['image_1']}}" alt="Product">
                              </a>
                              <div class="dropdown-product-info">
                                  <a class="dropdown-product-title"
                                     href="/category/subcategory-grid/single-product/{{$product['item']['artnr']}}">{{$product['item'] ['name']}}</a>
                                  <span class="dropdown-product-details">{{$product['qty']}}
                                      x €{{(round($product['item']['price'] , 2 ))}}</span>
                              </div>
                          </div>
                      @endforeach
                  @endif
                  @if(Session::has('cart'))
                <div class="toolbar-dropdown-group" >
                  <div class="column"><span class="text-lg">Totaal:</span></div>
                    @if (Session::get('cart')->totalPrice <= 30)
                  <div class="column text-right"><span class="text-lg text-medium">€{{ number_format((Session::has('cart')) ? Session::get('cart')->totalPrice : '' , 2) + 4.5}}&nbsp;</span></div>
                        @else
                        <div class="column text-right"><span class="text-lg text-medium">€{{ number_format((Session::has('cart')) ? Session::get('cart')->totalPrice : '' , 2)}}&nbsp;</span></div>

                    @endif


                </div>
                  @endif
                <div class="toolbar-dropdown-group">
                  <div class="column"><a class="btn btn-sm btn-block btn-secondary" href="/shoppingcart" style="font-size: 11px ;color: whitesmoke">Winkelwagen</a></div>
                        @if (!Session::has('cart'))
                        @else
                  <div class="column"><a class="btn btn-sm btn-block btn-success" href="/checkout-address" style="font-size: 11px">Bestellen</a></div>
                        @endif

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
    <!-- Off-Canvas Wrapper-->
