
<head>

    <link href="/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link id="mainStyles" rel="stylesheet" media="screen" href="/css/styles.min.css">
    <link rel="stylesheet" media="screen" href="/css/vendor.min.css">


    <!-- Loading Template CSS -->
    <link href="/css2/style.css" rel="stylesheet">
    <link href="/css2/animate.css" rel="stylesheet">
    <link href="/css2/style-magnific-popup.css" rel="stylesheet">

    <!-- Fonts -->
    <link href="/css2/font-awesome.min.css" rel="stylesheet">
    <link href="/css2/icons-main.css" rel="stylesheet">
    <link href="/css2/icons-helper.css" rel="stylesheet">

    <!-- RS5.0 Main Stylesheet -->
    <link rel="stylesheet" type="text/css" href="/revolution/css/settings.css">

    <!-- RS5.0 Layers and Navigation Styles -->
    <link rel="stylesheet" type="text/css" href="/revolution/css/layers.css">
    <link rel="stylesheet" type="text/css" href="/revolution/css/navigation.css">

    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Tangerine:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lora:400,400italic,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

    <!-- Font Favicon -->
    <link rel="shortcut icon" href="images/favicon.ico">

    <script src="/js/html5shiv.js"></script>
    <script src="/js/respond.min.js"></script>
    <script src="/js/scripts.min.js"></script>
</head>

<body>


@include('templates.banner')

<!--begin section-white-->
<div class="section-white" style="background-color: #6e5e4e">

    <!--begin container-->
    <div class="container">

        <!--begin row-->
        <div class="row">

            <!--begin col-md-4 -->
            <div class="col-md-4 margin-bottom-30">

                <!--begin popup image -->
                <div class="popup-wrapper">
                    <div class="popup-gallery">
                        <a class="popup3" href="/img/gallery/gallery1.jpg">
                            <img src="/img/gallery/gallery1.jpg" class="width-100" alt="pic">
                            <span class="eye-wrapper">
                                <i class="pe-7s-expand1 eye-icon"></i>
                            </span>
                        </a>
                    </div>
                </div>
                <!--begin popup image -->

            </div>
            <!--end col-md-4-->

            <!--begin col-md-4 -->
            <div class="col-md-4 margin-bottom-30">

                <!--begin popup image -->
                <div class="popup-wrapper">
                    <div class="popup-gallery">
                        <a class="popup3" href="/img/gallery/gallery2.jpg">
                            <img src="/img/gallery/gallery2.jpg" class="width-100" alt="pic">
                            <span class="eye-wrapper">
                                <i class="pe-7s-expand1 eye-icon"></i>
                            </span>
                        </a>
                    </div>
                </div>
                <!--begin popup image -->

            </div>
            <!--end col-md-4-->

            <!--begin col-md-4 -->
            <div class="col-md-4 margin-bottom-30">

                <!--begin popup image -->
                <div class="popup-wrapper">
                    <div class="popup-gallery">
                        <a class="popup3" href="/img/gallery/gallery3.jpg">
                            <img src="/img/gallery/gallery3.jpg" class="width-100" alt="pic">
                            <span class="eye-wrapper">
                                <i class="pe-7s-expand1 eye-icon"></i>
                            </span>
                        </a>
                    </div>
                </div>
                <!--begin popup image -->

            </div>
            <!--end col-md-4-->

        </div>
        <!--end row-->

        <!--begin row-->
        <div class="row">

            <!--begin col-md-4 -->
            <div class="col-md-4 margin-bottom-30">

                <!--begin popup image -->
                <div class="popup-wrapper">
                    <div class="popup-gallery">
                        <a class="popup3" href="/img/gallery/gallery4.jpg">
                            <img src="/img/gallery/gallery4.jpg" class="width-100" alt="pic">
                            <span class="eye-wrapper">
                                <i class="pe-7s-expand1 eye-icon"></i>
                            </span>
                        </a>
                    </div>
                </div>
                <!--begin popup image -->

            </div>
            <!--end col-md-4-->

            <!--begin col-md-4 -->
            <div class="col-md-4 margin-bottom-30">

                <!--begin popup image -->
                <div class="popup-wrapper">
                    <div class="popup-gallery">
                        <a class="popup3" href="/img/gallery/gallery5.jpg">
                            <img src="/img/gallery/gallery5.jpg" class="width-100" alt="pic">
                            <span class="eye-wrapper">
                                <i class="pe-7s-expand1 eye-icon"></i>
                            </span>
                        </a>
                    </div>
                </div>
                <!--begin popup image -->

            </div>
            <!--end col-md-4-->

            <!--begin col-md-4 -->
            <div class="col-md-4 margin-bottom-30">

                <!--begin popup image -->
                <div class="popup-wrapper">
                    <div class="popup-gallery">
                        <a class="popup3" href="/img/gallery/gallery6.jpg">
                            <img src="/img/gallery/gallery6.jpg" class="width-100" alt="pic">
                            <span class="eye-wrapper">
                                <i class="pe-7s-expand1 eye-icon"></i>
                            </span>
                        </a>
                    </div>
                </div>
                <!--begin popup image -->

            </div>
            <!--end col-md-4-->


            <!--begin col-md-4 -->
            <div class="col-md-4 margin-bottom-30">

                <!--begin popup image -->
                <div class="popup-wrapper">
                    <div class="popup-gallery">
                        <a class="popup3" href="/img/gallery/gallery9.jpg">
                            <img src="/img/gallery/gallery9.jpg" class="width-100" alt="pic">
                            <span class="eye-wrapper">
                                <i class="pe-7s-expand1 eye-icon"></i>
                            </span>
                        </a>
                    </div>
                </div>
                <!--begin popup image -->


            </div>
            <!--end col-md-4-->

            <!--begin col-md-4 -->
            <div class="col-md-4 margin-bottom-30">

                <!--begin popup image -->
                <div class="popup-wrapper">
                    <div class="popup-gallery">
                        <a class="popup3" href="/img/gallery/gallery7.jpg">
                            <img src="/img/gallery/gallery7.jpg" class="width-100" alt="pic">
                            <span class="eye-wrapper">
                                <i class="pe-7s-expand1 eye-icon"></i>
                            </span>
                        </a>
                    </div>
                </div>
                <!--begin popup image -->

            </div>
            <!--end col-md-4-->

            <!--begin col-md-4 -->
            <div class="col-md-4 margin-bottom-30">

                <!--begin popup image -->
                <div class="popup-wrapper">
                    <div class="popup-gallery">
                        <a class="popup3" href="/img/gallery/gallery8.jpg">
                            <img src="/img/gallery/gallery8.jpg" class="width-100" alt="pic">
                            <span class="eye-wrapper">
                                <i class="pe-7s-expand1 eye-icon"></i>
                            </span>
                        </a>
                    </div>
                </div>
                <!--begin popup image -->

            </div>
            <!--end col-md-4-->


            </div>
        <!--end row-->

        <!--begin row-->
        <div class="row">


            <!--begin col-md-4 -->
            <div class="col-md-4 margin-bottom-30">

                <!--begin popup image -->
                <div class="popup-wrapper">
                    <div class="popup-gallery">
                        <a class="popup3" href="/img/gallery/gallery10.jpeg">
                            <img src="/img/gallery/gallery10.jpeg" class="width-100" alt="pic">
                            <span class="eye-wrapper">
                                <i class="pe-7s-expand1 eye-icon"></i>
                            </span>
                        </a>
                    </div>
                </div>
                <!--begin popup image -->

            </div>
            <!--end col-md-4-->
            <!--begin col-md-4 -->
            <div class="col-md-4 margin-bottom-30">

                <!--begin popup image -->
                <div class="popup-wrapper">
                    <div class="popup-gallery">
                        <a class="popup3" href="/img/gallery/gallery18.jpeg">
                            <img src="/img/gallery/gallery18.jpeg" class="width-100" alt="pic">
                            <span class="eye-wrapper">
                                <i class="pe-7s-expand1 eye-icon"></i>
                            </span>
                        </a>
                    </div>
                </div>
                <!--begin popup image -->

            </div>
            <!--end col-md-4-->
            <!--begin col-md-4 -->
            <div class="col-md-4 margin-bottom-30">

                <!--begin popup image -->
                <div class="popup-wrapper">
                    <div class="popup-gallery">
                        <a class="popup3" href="/img/gallery/gallery19.jpeg">
                            <img src="/img/gallery/gallery19.jpeg" class="width-100" alt="pic">
                            <span class="eye-wrapper">
                                <i class="pe-7s-expand1 eye-icon"></i>
                            </span>
                        </a>
                    </div>
                </div>
                <!--begin popup image -->

            </div>
            <!--end col-md-4-->
            <!--begin col-md-4 -->
            <div class="col-md-4 margin-bottom-30">

                <!--begin popup image -->
                <div class="popup-wrapper">
                    <div class="popup-gallery">
                        <a class="popup3" href="/img/gallery/gallery20.jpeg">
                            <img src="/img/gallery/gallery20.jpeg" class="width-100" alt="pic">
                            <span class="eye-wrapper">
                                <i class="pe-7s-expand1 eye-icon"></i>
                            </span>
                        </a>
                    </div>
                </div>
                <!--begin popup image -->

            </div>
            <!--end col-md-4-->

            <!--begin col-md-4 -->
            <div class="col-md-4 margin-bottom-30">

                <!--begin popup image -->
                <div class="popup-wrapper">
                    <div class="popup-gallery">
                        <a class="popup3" href="/img/gallery/gallery11.jpeg">
                            <img src="/img/gallery/gallery11.jpeg" class="width-100" alt="pic">
                            <span class="eye-wrapper">
                                <i class="pe-7s-expand1 eye-icon"></i>
                            </span>
                        </a>
                    </div>
                </div>
                <!--begin popup image -->

            </div>
            <!--end col-md-4-->




            <!--begin col-md-4 -->
            <div class="col-md-4 margin-bottom-30">

                <!--begin popup image -->
                <div class="popup-wrapper">
                    <div class="popup-gallery">
                        <a class="popup3" href="/img/gallery/gallery22.jpeg">
                            <img src="/img/gallery/gallery22.jpeg" class="width-100" alt="pic">
                            <span class="eye-wrapper">
                                <i class="pe-7s-expand1 eye-icon"></i>
                            </span>
                        </a>
                    </div>
                </div>
                <!--begin popup image -->

            </div>
            <!--end col-md-4-->


            <!--begin col-md-4 -->
            <div class="col-md-4 margin-bottom-30">

                <!--begin popup image -->
                <div class="popup-wrapper">
                    <div class="popup-gallery">
                        <a class="popup3" href="/img/gallery/gallery15.jpeg">
                            <img src="/img/gallery/gallery15.jpeg" class="width-100" alt="pic">
                            <span class="eye-wrapper">
                                <i class="pe-7s-expand1 eye-icon"></i>
                            </span>
                        </a>
                    </div>
                </div>
                <!--begin popup image -->

            </div>
            <!--end col-md-4-->
            <!--begin col-md-4 -->
            <div class="col-md-4 margin-bottom-30">

                <!--begin popup image -->
                <div class="popup-wrapper">
                    <div class="popup-gallery">
                        <a class="popup3" href="/img/gallery/gallery16.jpeg">
                            <img src="/img/gallery/gallery16.jpeg" class="width-100" alt="pic">
                            <span class="eye-wrapper">
                                <i class="pe-7s-expand1 eye-icon"></i>
                            </span>
                        </a>
                    </div>
                </div>
                <!--begin popup image -->

            </div>
            <!--end col-md-4-->
            <!--begin col-md-4 -->
            <div class="col-md-4 margin-bottom-30">

                <!--begin popup image -->
                <div class="popup-wrapper">
                    <div class="popup-gallery">
                        <a class="popup3" href="/img/gallery/gallery17.jpeg">
                            <img src="/img/gallery/gallery17.jpeg" class="width-100" alt="pic">
                            <span class="eye-wrapper">
                                <i class="pe-7s-expand1 eye-icon"></i>
                            </span>
                        </a>
                    </div>
                </div>
                <!--begin popup image -->

            </div>
            <!--end col-md-4-->


            <!--begin col-md-4 -->
            <div class="col-md-4 margin-bottom-30">

                <!--begin popup image -->
                <div class="popup-wrapper">
                    <div class="popup-gallery">
                        <a class="popup3" href="/img/gallery/gallery21.jpeg">
                            <img src="/img/gallery/gallery21.jpeg" class="width-100" alt="pic">
                            <span class="eye-wrapper">
                                <i class="pe-7s-expand1 eye-icon"></i>
                            </span>
                        </a>
                    </div>
                </div>
                <!--begin popup image -->

            </div>
            <!--end col-md-4-->
            <!--begin col-md-4 -->
            <div class="col-md-4 margin-bottom-30">

                <!--begin popup image -->
                <div class="popup-wrapper">
                    <div class="popup-gallery">
                        <a class="popup3" href="/img/gallery/gallery13.jpeg">
                            <img src="/img/gallery/gallery13.jpeg" class="width-100" alt="pic">
                            <span class="eye-wrapper">
                                <i class="pe-7s-expand1 eye-icon"></i>
                            </span>
                        </a>
                    </div>
                </div>
                <!--begin popup image -->

            </div>
            <!--end col-md-4-->

            <!--begin col-md-4 -->
            <div class="col-md-4 margin-bottom-30">

                <!--begin popup image -->
                <div class="popup-wrapper">
                    <div class="popup-gallery">
                        <a class="popup3" href="/img/gallery/gallery12.jpeg">
                            <img src="/img/gallery/gallery12.jpeg" class="width-100" style="height: 200px" alt="pic">
                            <span class="eye-wrapper">
                                <i class="pe-7s-expand1 eye-icon"></i>
                            </span>
                        </a>
                    </div>
                </div>
                <!--begin popup image -->

            </div>
            <!--end col-md-4-->

            <!--begin col-md-4 -->
            <div class="col-md-4 margin-bottom-30">

                <!--begin popup image -->
                <div class="popup-wrapper">
                    <div class="popup-gallery">
                        <a class="popup3" href="/img/gallery/gallery14.jpeg">
                            <img src="/img/gallery/gallery14.jpeg" class="width-100" style="height: 200px" alt="pic">
                            <span class="eye-wrapper">
                                <i class="pe-7s-expand1 eye-icon"></i>
                            </span>
                        </a>
                    </div>
                </div>
                <!--begin popup image -->

            </div>
            <!--end col-md-4-->

        </div>
        <!--end row-->

    </div>
    <!--end container-->

</div>
<!--end section-white-->




<script src="/js/jquery-1.11.3.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/bootstrap-select.js"></script>
<script src="/js/bootstrap-switch.js"></script>
<script src="/js/jquery.magnific-popup.min.js"></script>
<script src="/js/jquery.nav.js"></script>
<script src="/js/jquery.scrollTo-min.js"></script>
<script src="/js/SmoothScroll.js"></script>
<script src="/js/wow.js"></script>
<script src="/js/instafeed.js"></script>
<script src="/js/instagram.js"></script>

<script type="text/javascript" src="/revolution/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="/revolution/js/jquery.themepunch.revolution.min.js"></script>

<script type="text/javascript" src="/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="/revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="/revolution/js/extensions/revolution.extension.carousel.min.js"></script>

<!-- begin custom script-->
<script src="/js/custom.js"></script>
<script src="/js/plugins.js"></script>

<script type="text/javascript" src="/revolution/js/extensions/revolution.extension.video.min.js"></script>
<script type="text/javascript" src="/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>

</body>

