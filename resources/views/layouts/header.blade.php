<header id="header" class="compensate-for-scrollbar">
    <div id="action_bar">
        <div class="container">
            @if(!\Auth::check() || \Auth::user()->settings->showLogo)
            <a href="{{URL::asset('/')}}" id="logo" style="background-image: url('{{URL::asset(\App\Governor::$website->logo)}}')">
                {{--<a href="/"><img src="{{URL::asset(\App\Governor::$website->logo)}}"></a>--}}
            </a>
            @endif

            <div id="menu_hamburger">
                <!--?xml version="1.0" encoding="utf-8"?--><!-- Generator: Adobe Illustrator 19.2.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  --><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve"><path style="fill:#5B5B5B;" d="M49,10.5c0,1.1-0.9,2-2,2H3c-1.1,0-2-0.9-2-2v-1c0-1.1,0.9-2,2-2h44c1.1,0,2,0.9,2,2V10.5z"></path><path style="fill:#5B5B5B;" d="M49,25.5c0,1.1-0.9,2-2,2H3c-1.1,0-2-0.9-2-2v-1c0-1.1,0.9-2,2-2h44c1.1,0,2,0.9,2,2V25.5z"></path><path style="fill:#5B5B5B;" d="M49,40.5c0,1.1-0.9,2-2,2H3c-1.1,0-2-0.9-2-2v-1c0-1.1,0.9-2,2-2h44c1.1,0,2,0.9,2,2V40.5z"></path></svg></div>



            <div class="right ">
                <!-- SEARCH -->
                <form method="GET" id="search" class="action_el" action="/search">

                    <input type="text" name="searchQuery" placeholder="{{trans('menu.Voer zoekterm in')}}">
                    <input type="submit" value=""></form>



                <div id="user_btns">

                    <div id="lang_select" class="expandable" data-locale="{{LaravelLocalization::getCurrentLocale()}}">
                        <div class="item lang close-expandable" name="{{LaravelLocalization::getCurrentLocale()}}">
                            @if(LaravelLocalization::getCurrentLocale() == 'nl_NL')
                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve"><rect x="1" y="9" style="fill:#21468B;" width="48" height="32"></rect><rect x="1" y="9" style="fill:#FFFFFF;" width="48" height="21.3"></rect><rect x="1" y="9" style="fill:#d81e05;" width="48" height="10.7"></rect></svg>
                            @endif
                            @if(LaravelLocalization::getCurrentLocale() == 'en_EN')
                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve"><path style="fill:#00247D;" d="M1,13v24h48V13H1z"></path><polygon style="fill:#FFFFFF;" points="25,27.2 44.5,37 49,37 49,34.8 29.5,25 49,15.2 49,13 44.5,13 25,22.8 5.5,13 1,13 1,15.2
	20.5,25 1,34.8 1,37 5.5,37 "></polygon><polygon style="fill:#CF142B;" points="1,14.3 22.3,25 1,35.7 1,37 3.7,37 25,26.3 46.3,37 49,37 49,35.6 27.7,25 49,14.4 49,13
	46.3,13 25,23.7 3.7,13 1,13 1,13 "></polygon><polygon style="fill:#FFFFFF;" points="29,37 21,37 21,29 1,29 1,21 21,21 21,13 29,13 29,21 49,21 49,29 29,29 "></polygon><polygon style="fill:#CF142B;" points="27.4,37 22.6,37 22.6,27.4 1,27.4 1,22.6 22.6,22.6 22.6,13 27.4,13 27.4,22.6 49,22.6
	49,27.4 27.4,27.4 "></polygon></svg>
                            @endif
                            @if(LaravelLocalization::getCurrentLocale() == 'fr_FR')
                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve"><style type="text/css">
                                        .st0{fill:#ED2939;}
                                        .st1{fill:#FFFFFF;}
                                        .st2{fill:#002395;}
                                    </style><rect x="1" y="9" class="st0" width="48" height="32"></rect><rect x="1" y="9" class="st1" width="32" height="32"></rect><rect x="1" y="9" class="st2" width="16" height="32"></rect></svg>
                            @endif
                        </div>
                        @foreach(LaravelLocalization::getLocalesOrder() as $locale)
                            @if($locale['regional'] == LaravelLocalization::getCurrentLocale())
                                @continue
                            @endif
                            <div class="item lang" name="{{$locale['regional']}}">
                            <a href="{{ LaravelLocalization::getLocalizedURL($locale['regional'], null, [], true) }}" class=" not-selected">
                                @if($locale['regional'] == 'nl_NL')
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve"><rect x="1" y="9" style="fill:#21468B;" width="48" height="32"></rect><rect x="1" y="9" style="fill:#FFFFFF;" width="48" height="21.3"></rect><rect x="1" y="9" style="fill:#d81e05;" width="48" height="10.7"></rect></svg>
                                @endif
                                @if($locale['regional'] == 'en_EN')
                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve"><path style="fill:#00247D;" d="M1,13v24h48V13H1z"></path><polygon style="fill:#FFFFFF;" points="25,27.2 44.5,37 49,37 49,34.8 29.5,25 49,15.2 49,13 44.5,13 25,22.8 5.5,13 1,13 1,15.2
	20.5,25 1,34.8 1,37 5.5,37 "></polygon><polygon style="fill:#CF142B;" points="1,14.3 22.3,25 1,35.7 1,37 3.7,37 25,26.3 46.3,37 49,37 49,35.6 27.7,25 49,14.4 49,13
	46.3,13 25,23.7 3.7,13 1,13 1,13 "></polygon><polygon style="fill:#FFFFFF;" points="29,37 21,37 21,29 1,29 1,21 21,21 21,13 29,13 29,21 49,21 49,29 29,29 "></polygon><polygon style="fill:#CF142B;" points="27.4,37 22.6,37 22.6,27.4 1,27.4 1,22.6 22.6,22.6 22.6,13 27.4,13 27.4,22.6 49,22.6
	49,27.4 27.4,27.4 "></polygon></svg>
                                @endif
                                @if($locale['regional'] == 'fr_FR')
                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve"><style type="text/css">
                                                .st0{fill:#ED2939;}
                                                .st1{fill:#FFFFFF;}
                                                .st2{fill:#002395;}
                                            </style><rect x="1" y="9" class="st0" width="48" height="32"></rect><rect x="1" y="9" class="st1" width="32" height="32"></rect><rect x="1" y="9" class="st2" width="16" height="32"></rect></svg>
                                @endif
                            </a>
                            </div>
                        @endforeach

                </div>

                    <a id="shopping_car_parent" href="/checkout">
                        <div id="shopping_car">
                            <img src="/svg/cart.svg"><span id="shopping_car_count">{{\App\Governor::$shoppingcart ? \App\Governor::$shoppingcart->count : 0}}</span>
                        </div>
                    </a>

                    <!-- LOGIN -->
                    @if(!\Auth::check())
                        <div id="login" class="expandable">
                            <a class="user_btn login close-expandable"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 50 50" xml:space="preserve"><g><path d="M45.1,38.9L45,39.1C39.6,45.5,32.6,49,25,49c-7.6,0-14.6-3.5-20-9.9l-0.2-0.2l0-0.2
		c1-6.7,4.5-12.1,9.9-15.2l0.4-0.2l0.3,0.3c2.5,2.6,5.9,4.1,9.5,4.1s7-1.5,9.5-4.1l0.3-0.3l0.4,0.2c5.3,3.1,8.8,8.5,9.9,15.2
		L45.1,38.9z M25,25c6.6,0,12-5.4,12-12S31.6,1,25,1S13,6.4,13,13S18.4,25,25,25z"></path></g></svg>login <span class="fa fa-sort-desc"></span>
                            </a>
                            <div id="loginform">
                                <form id="form-login" method="POST" action="{{ LaravelLocalization::localizeURL('login') }}">
                                    {{ csrf_field() }}
                                    {{--<span>Verkeerd e-mail adres en/of watchwoord</span>--}}
                                    <fieldset>
                                        <ol>
                                            <li>
                                                <label for="loginDebnr"></label>
                                                <input type="text" name="email" placeholder="Email" id="loginDebnr"></li>
                                            <li>
                                                <label for="loginPassword"></label>
                                                <input type="password" name="password" placeholder="Wachtwoord" id="loginPassword"></li>
                                            <li>
                                                <input type="submit" class="button" value="Inloggen">
                                            </li>
                                        </ol>
                                    </fieldset>
                                </form>
                                <a class="forgot_pass_link" href="/forgot-password">{{trans('menu.Wachtwoord vergeten')}}</a>
                            </div>
                        </div>
                    @else
                        <div id="user_dropdown" class="expandable">
                            <a class="user_btn loggedin close-expandable">
                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 50 50" xml:space="preserve"><g><path d="M45.1,38.9L45,39.1C39.6,45.5,32.6,49,25,49c-7.6,0-14.6-3.5-20-9.9l-0.2-0.2l0-0.2
		c1-6.7,4.5-12.1,9.9-15.2l0.4-0.2l0.3,0.3c2.5,2.6,5.9,4.1,9.5,4.1s7-1.5,9.5-4.1l0.3-0.3l0.4,0.2c5.3,3.1,8.8,8.5,9.9,15.2
		L45.1,38.9z M25,25c6.6,0,12-5.4,12-12S31.6,1,25,1S13,6.4,13,13S18.4,25,25,25z"></path></g></svg>
                                {{trans('menu.Mijn account')}}                            <span class="fa fa-sort-desc"></span>
                                <span class="username">{{Auth::user()->name}}</span>
                            </a>
                            <div id="usermenu">
                                <ul><li class="debiteurnummer"><a>Debt. nr. <b>{{Auth::user()->debnr}}</b></a></li>
                                    <li class="profiel">
                                        <a href="/profile">{{trans('menu.Profiel')}}</a>
                                    </li>
                                    <li class="instellingen">
                                        <a href="/settings">{{trans('menu.Instellingen')}}</a>
                                    </li>
                                    <li class="ordergeschiedenis">
                                        <a href="/orders">{{trans('menu.Ordergeschiedenis')}}</a>
                                    </li>
                                    <li>
                                        <a href="/offers">{{trans('menu.Offertes')}}</a>
                                    </li>
                                    <li>
                                        <a href="/rma">{{trans('menu.RMA')}}</a>
                                    </li>
                                    <li class="uitloggen">
                                        <a href="/logout">{{trans('menu.Uitloggen')}}</a>
                                    </li>
                                </ul></div>
                        </div>
                    @endif
                </div>

        </div>
    </div>

<?php
        $menu = [
            ['route' => url('/' . LaravelLocalization::getCurrentLocale()), 'name' => 'Home'],
            ['route' => URL::route('wheels'), 'name' =>  trans('menu.Wielen')],
            ['route' => URL::route('tyres'), 'name' =>  trans('menu.Banden')],
            ['route' => url('/' . LaravelLocalization::getCurrentLocale() . '/tpms'), 'name' => trans('menu.tpms')],
            ['route' => URL::route('configurator'), 'name' =>  trans('menu.configurator')],
            ['route' => url('/' . LaravelLocalization::getCurrentLocale() . '/tpms'), 'name' => trans('menu.accessoires'), 'subitems' => [
                ['route' => url('/' . LaravelLocalization::getCurrentLocale() . '/tpms'), 'name' => trans('menu.tpms')]
            ]],
            ['route' => url('/' . LaravelLocalization::getCurrentLocale() . '/services'), 'name' => trans('menu.Diensten')],
            ['route' => url('/' . LaravelLocalization::getCurrentLocale() . '/about_us'), 'name' => trans('menu.Over Ons')],
            ['route' => url('/' . LaravelLocalization::getCurrentLocale() . '/downloads'), 'name' => trans('menu.Downloads')],
            ['route' => url('/' . LaravelLocalization::getCurrentLocale() . '/news'), 'name' => trans('menu.nieuws')],
            ['route' => url('/' . LaravelLocalization::getCurrentLocale() . '/gallery'), 'name' => trans('menu.Gallery')],
            ['route' => url('/' . LaravelLocalization::getCurrentLocale() . '/contact'), 'name' => trans('menu.contact')],
        ];
        if(\App\Governor::$website->id != 1){
            $menu = [
                ['route' => url('/' . LaravelLocalization::getCurrentLocale()), 'name' => 'Home'],
                ['route' => URL::route('wheels'), 'name' =>  trans('menu.Wielen')],
                ['route' => url('/' . LaravelLocalization::getCurrentLocale() . '/tpms'), 'name' => trans('menu.tpms')],
                ['route' => URL::route('configurator'), 'name' =>  trans('menu.configurator')],
//                ['route' => url('/' . LaravelLocalization::getCurrentLocale() . '/contact'), 'name' => trans('menu.contact')],
            ];
        }
?>

    <div id="menu_bar" class="col-lg-12">
        <div class="container">
            <nav id="menu" data-current-url="{{URL::current()}}">
                <ul>
                    @foreach($menu as $key => $menuItem)
                        <li @if(URL::current() == $menuItem['route']) class="active" @endif>
                            <a href="{{$menuItem['route']}}">{{$menuItem['name']}}</a>
                                @if(!empty($menuItem['subitems']))
                                    <ul>
                                        @foreach($menuItem['subitems'] as $subitem)
                                            <li @if(URL::current() == $subitem['route']) class="active" @endif>
                                                <a href="{{$subitem['route']}}">{{$subitem['name']}}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                @endif
                        </li>
                    @endforeach
                    {{--<li class="home">--}}
                        {{--<a href="/" >Home</a>--}}
                    {{--</li>--}}
                    {{--<li class="wielen">--}}
                        {{--<a href="{{URL::route('wheels')}}">{{ trans('menu.Wielen')}}</a>--}}
                    {{--</li>--}}

                    {{--<li class="banden">--}}
                        {{--<a href="{{URL::route('tyres')}}">{{ trans('menu.Banden')}}</a>--}}
                    {{--</li>--}}

                    {{--<li class="tpms">--}}
                        {{--<a href="/tpms">{{ trans('menu.tpms')}}</a>--}}
                    {{--</li>--}}

                    {{--<li class="configurator">--}}
                        {{--<a href="{{URL::route('configurator')}}">{{ trans('menu.configurator')}}</a>--}}
                    {{--</li>--}}

                    {{--<li class="accessoires">--}}
                        {{--<a href="/tpms">{{ trans('menu.accessoires')}}</a>--}}
                        {{--<ul>--}}
                            {{--<li class="tpms">--}}
                                {{--<a href="/tpms">{{ trans('menu.tpms')}}</a>--}}
                            {{--</li>--}}

                        {{--</ul>--}}
                    {{--</li>--}}
                    {{--<li class="diensten">--}}
                        {{--<a href="/services">{{ trans('menu.Diensten')}}</a>--}}
                    {{--</li>--}}

                    {{--<li class="over-ons">--}}
                        {{--<a href="/about_us">{{ trans('menu.Over Ons')}}</a>--}}

                    {{--</li>--}}

                    {{--<li class="downloads">--}}
                        {{--<a href="/downloads">{{ trans('menu.Downloads')}}</a>--}}
                    {{--</li>--}}

                    {{--<li class="news">--}}
                        {{--<a href="/news">{{ trans('menu.nieuws')}}</a>--}}
                    {{--</li>--}}

                    {{--<li class="gallery">--}}
                        {{--<a href="/gallery">{{ trans('menu.Gallery')}}</a>--}}
                    {{--</li>--}}

                    {{--<li class="contact">--}}
                        {{--<a href="/contact">{{ trans('menu.contact')}}</a>--}}
                    {{--</li>--}}
                </ul>

            </nav>
            <div id="mob_close"><!--?xml version="1.0" encoding="utf-8"?--><!-- Generator: Adobe Illustrator 19.2.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  --><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve"><g><path d="M32.7,25L48,9.7c1.3-1.3,1.3-3.4,0-4.7L44.9,2c-1.3-1.3-3.4-1.3-4.7,0L25,17.3L9.8,2.1
		c-1.3-1.3-3.4-1.3-4.7,0L2,5.2C0.7,6.5,0.7,8.5,2,9.8L17.2,25L2,40.2c-1.3,1.3-1.3,3.4,0,4.7L5.1,48c1.3,1.3,3.4,1.3,4.7,0L25,32.8
		L40.2,48c1.3,1.3,3.4,1.3,4.7,0l3.1-3.1c1.3-1.3,1.3-3.4,0-4.7L32.7,25z"></path></g></svg></div>
        </div>
    </div>

    </div>
</header>
