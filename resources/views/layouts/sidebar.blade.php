<!-- Off-Canvas Category Menu-->
<div class="offcanvas-container" id="shop-categories">
    <div class="offcanvas-header">
        <h3 class="offcanvas-title"></h3>
    </div>
    <nav class="offcanvas-menu">
        <ul class="menu">
            <li ><span><a href="/categoryall">Alle categorieën</a><span class="sub-menu-toggle"></span></span>
                {{--<ul class="offcanvas-submenu">--}}
                    {{--<li><a href="#">Sneakers</a></li>--}}
                    {{--<li><a href="#">Loafers</a></li>--}}
                    {{--<li><a href="#">Boat Shoes</a></li>--}}
                    {{--<li><a href="#">Sandals</a></li>--}}
                    {{--<li><a href="#">View All</a></li>--}}
                {{--</ul>--}}
            </li>


            <li><span><a href="#">Contact</a><span class="sub-menu-toggle"></span></span>
                {{--<ul class="offcanvas-submenu">--}}
                    {{--<li><a href="#">Sunglasses</a></li>--}}
                    {{--<li><a href="#">Hats</a></li>--}}
                    {{--<li><a href="#">Watches</a></li>--}}
                    {{--<li><a href="#">Jewelry</a></li>--}}
                    {{--<li><a href="#">Belts</a></li>--}}
                    {{--<li><a href="#">View All</a></li>--}}
                {{--</ul>--}}
            </li>
        </ul>
    </nav>
</div>
<!-- Off-Canvas Mobile Menu-->
<div class="offcanvas-container" id="mobile-menu">
    @if(Auth::check())
        <a href="{{route('logout')}}"> <b class="float-right" style="color: white;font-size: 18px; padding-right: 20px"><i class="icon-unlock"></i>Uitloggen</b></a>
    @endif
    <a class="account-link" href="account-orders"></a>
        @if(Auth::check())
        <div class="user-ava">
            <img src="{{Auth::user()->profilePic}}" alt="{{Auth::user()->name}} {{Auth::user()->surname}}">
        </div>
        <div class="user-info">
            <b style="color: white;font-size: 18px; padding-left: 20px">{{Auth::user()->name}} {{Auth::user()->surname}}</b>

        </div>
        @else
            <a href="{{ route('account-login') }}"><b style="color: white;font-size: 18px; padding-left: 20px">Inloggen</b></a>
            <a href="{{ route('account-register') }}"><b style="color: white;font-size: 18px; padding-left: 20px">Registreren</b></a>
        @endif
    <nav class="offcanvas-menu">
        <ul class="menu">

            <li></li>


                <li>
                    <span><a href="/about_us"> Over Ons</a></span>
                </li>
            <li>
                <span><a href="/category/subcategory-grid/koffie">Koffie</a></span>
            </li>
            {{--<li>--}}
                {{--<span><a href="/category/subcategory-grid/servies">Servies</a></span>--}}
            {{--</li>--}}
            <li>
                <span><a href="/b2b">Voor bedrijven</a></span>
            </li>
            <li>
                <span><a href="/gallery">Gallery</a></span>
            </li>

            {{--<li class="has-children"><span><a href="#">Acties</a><span class="sub-menu-toggle"></span></span>--}}
            {{--<ul class="offcanvas-submenu">--}}
            {{--<li><a href="#">Sandals</a></li>--}}
            {{--<li><a href="#">Flats</a></li>--}}
            {{--<li><a href="#">Sneakers</a></li>--}}
            {{--<li><a href="#">Heels</a></li>--}}
            {{--<li><a href="#">View All</a></li>--}}
            {{--</ul>--}}
            {{--</li>--}}
            {{--<li class="has-children"><span><a href="#">Men's Clothing</a><span class="sub-menu-toggle"></span></span>--}}
            {{--<ul class="offcanvas-submenu">--}}
            {{--<li><a href="#">Shirts &amp; Tops</a></li>--}}
            {{--<li><a href="#">Pants</a></li>--}}
            {{--<li><a href="#">Jackets</a></li>--}}
            {{--<li><a href="#">View All</a></li>--}}
            {{--</ul>--}}
            {{--</li>--}}
            {{--<li class="has-children"><span><a href="#">Women's Clothing</a><span class="sub-menu-toggle"></span></span>--}}
            {{--<ul class="offcanvas-submenu">--}}
            {{--<li><a href="#">Dresses</a></li>--}}
            {{--<li><a href="#">Shirts &amp; Tops</a></li>--}}
            {{--<li><a href="#">Shorts</a></li>--}}
            {{--<li><a href="#">Swimwear</a></li>--}}
            {{--<li><a href="#">View All</a></li>--}}
            {{--</ul>--}}
            {{--</li>--}}
            {{--<li class="has-children"><span><a href="#">Kid's Shoes</a><span class="sub-menu-toggle"></span></span>--}}
            {{--<ul class="offcanvas-submenu">--}}
            {{--<li><a href="#">Boots</a></li>--}}
            {{--<li><a href="#">Sandals</a></li>--}}
            {{--<li><a href="#">Crib Shoes</a></li>--}}
            {{--<li><a href="#">Loafers</a></li>--}}
            {{--<li><a href="#">View All</a></li>--}}
            {{--</ul>--}}
            {{--</li>--}}
            {{--<li class="has-children"><span><a href="#">Bags</a><span class="sub-menu-toggle"></span></span>--}}
            {{--<ul class="offcanvas-submenu">--}}
            {{--<li><a href="#">Handbags</a></li>--}}
            {{--<li><a href="#">Backpacks</a></li>--}}
            {{--<li><a href="#">Luggage</a></li>--}}
            {{--<li><a href="#">Wallets</a></li>--}}
            {{--<li><a href="#">View All</a></li>--}}
            {{--</ul>--}}
            {{--</li>--}}
            <li><span><a href="#">Contact</a><span class="sub-menu-toggle"></span></span>
                {{--<ul class="offcanvas-submenu">--}}
                {{--<li><a href="#">Sunglasses</a></li>--}}
                {{--<li><a href="#">Hats</a></li>--}}
                {{--<li><a href="#">Watches</a></li>--}}
                {{--<li><a href="#">Jewelry</a></li>--}}
                {{--<li><a href="#">Belts</a></li>--}}
                {{--<li><a href="#">View All</a></li>--}}
                {{--</ul>--}}
            </li>
        </ul>
    </nav>
</div>
