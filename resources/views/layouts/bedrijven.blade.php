
<head>

    <link href="/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link id="mainStyles" rel="stylesheet" media="screen" href="/css/styles.min.css">
    <link rel="stylesheet" media="screen" href="/css/vendor.min.css">


    <!-- Loading Template CSS -->
    <link href="/css2/style.css" rel="stylesheet">
    <link href="/css2/animate.css" rel="stylesheet">
    <link href="/css2/style-magnific-popup.css" rel="stylesheet">

    <!-- Fonts -->
    <link href="/css2/font-awesome.min.css" rel="stylesheet">
    <link href="/css2/icons-main.css" rel="stylesheet">
    <link href="/css2/icons-helper.css" rel="stylesheet">

    <!-- RS5.0 Main Stylesheet -->
    <link rel="stylesheet" type="text/css" href="/revolution/css/settings.css">

    <!-- RS5.0 Layers and Navigation Styles -->
    <link rel="stylesheet" type="text/css" href="/revolution/css/layers.css">
    <link rel="stylesheet" type="text/css" href="/revolution/css/navigation.css">

    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Tangerine:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lora:400,400italic,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

    <!-- Font Favicon -->
    <link rel="shortcut icon" href="images/favicon.ico">

    <script src="/js/html5shiv.js"></script>
    <script src="/js/respond.min.js"></script>
    <script src="/js/scripts.min.js"></script>
</head>

<body>


<section id="about" class="section-grey" style="background-color: #6e5e4e; border-bottom: 1px solid #e1e7ec;">

    <!--begin container-->
    <div class="container">

        <!--begin row-->
        <div class="row">

            <!--begin col-md-6-->
            <div class="col-md-6 text-center padding-top-50 padding-bottom-50 wow slideInLeft">

                <span class="comic-text" style="color: whitesmoke">Lorando & Morini</span>

                <h2 class="section-title" style="color: antiquewhite; padding-top: 60px">Voor jouw bedrijf</h2>

                <p style="font-size: 16px; color: whitesmoke">
                    Koffiebranderij en leverancier van de meest verse en unieke single state origin Arabica koffie van Nederland. Lorando en Morini levert dagelijks vers gebrande koffie en koffiebonen aan bedrijven, horeca, restaurants en hotels.

                    <br><br>
                    Vanuit onze ambachtelijke koffiebranderij in Rotterdam delen wij onze passie en ontdekkingen van de lekkerste koffiesoorten met echte liefhebbers.
                    <br><br>

                    Kortingen voor grote afnames:
                    <br><br>

                    5 Kilo (per week) – 7,5% korting<br>
                    10 Kilo (per week) – 10% korting<br><br>
                    Wij leveren ook in zakken van 5 kilo.

                </p>
                <img src="/img/whitelabel-1.jpg" alt="picture" class="width-100" style="padding-top: 170px">





            </div>
            <!--end col-md-6-->

            <!--begin col-md-6-->
            <div class="col-md-6 wow slideInRight text-center padding-top-50 padding-bottom-50 wow" >

                <img src="/img/lorandomorini-5.jpg" style="height: 50%;"  alt="picture" class="width-100">


                <br>
                <br>
                <br>   <br>
                <br>
                <br>

                <span class="comic-text" style="color:whitesmoke" >Private Label</span>

                <h2 class="section-title" style="color: antiquewhite; padding-top: 60px">Voor jouw gebrand met jouw logo</h2>


                <p style="font-size: 16px; color: whitesmoke">

                    Altijd al een eigen koffie-merk willen starten? <br><br>
                    Dat kan in samenwerking met Lorando & Morini. In opdracht van jou kunnen we samen met jou koffie branden.
                    Vervolgens kan de koffie geleverd worden in zakken met jouw logo er op. <br><br>
                    Benieuwd naar prijzen en de mogelijkheden? Neem dan contact op!

                </p>


            </div>
            <!--end col-md-6-->

        </div>
        <!--end row-->

    </div>
    <!--end container-->

</section>





<script src="/js/jquery-1.11.3.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/bootstrap-select.js"></script>
<script src="/js/bootstrap-switch.js"></script>
<script src="/js/jquery.magnific-popup.min.js"></script>
<script src="/js/jquery.nav.js"></script>
<script src="/js/jquery.scrollTo-min.js"></script>
<script src="/js/SmoothScroll.js"></script>
<script src="/js/wow.js"></script>
<script src="/js/instafeed.js"></script>
<script src="/js/instagram.js"></script>

<script type="text/javascript" src="/revolution/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="/revolution/js/jquery.themepunch.revolution.min.js"></script>

<script type="text/javascript" src="/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="/revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="/revolution/js/extensions/revolution.extension.carousel.min.js"></script>

<!-- begin custom script-->
<script src="/js/custom.js"></script>
<script src="/js/plugins.js"></script>

<script type="text/javascript" src="/revolution/js/extensions/revolution.extension.video.min.js"></script>
<script type="text/javascript" src="/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>

</body>

