<!doctype html>
<html lang="en">
<head>

    <!-- Facebook Pixel Code -->
<!--
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script','https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '323050382779805');
        fbq('track', 'PageView');
    </script>
    <noscript>
        <img height="1" width="1" style="display:none"
             src="https://www.facebook.com/tr?id=323050382779805&ev=PageView&noscript=1"
        />
    </noscript>
-->
    <!-- End Facebook Pixel Code -->

    <meta name="google-site-verification" content="E7QQP-7yu0q4gqth-wv8-YuUupNNG6tgGivO19Ww24c" />
    <meta name="facebook-domain-verification" content="3xk9meze905cx6ycghh0jmf9o3m17g" />
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-168812147-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-168812147-1');
    </script>

    <!-- Smartsupp Live Chat script -->
    <script type="text/javascript">
        var _smartsupp = _smartsupp || {};
        _smartsupp.key = 'fffce1f0bce6ea1560bbbdccc6229578a6e97cda';
        window.smartsupp||(function(d) {
            var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];
            s=d.getElementsByTagName('script')[0];c=d.createElement('script');
            c.type='text/javascript';c.charset='utf-8';c.async=true;
            c.src='https://www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);
        })(document);
    </script>

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '947007065870664');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=947007065870664&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->


    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{\App\Governor::$website->name}}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
        <link rel="stylesheet" media="screen" href="/css/logo.css">

        <link rel="stylesheet" href="{{str_replace('.css', '', mix('/css/app.css'))}}">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-91229735-4">
    </script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-91229735-4');
    </script>

    <script src="{{mix('/js/app.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.cycle2/2.1.6/jquery.cycle2.min.js"></script>
</head>
<body>
<div id="loader-2018" @if(strpos(URL::current(), URL::route('configurator')) === false) style="display: none;" @endif>
    <div class="lds-css ng-scope">
        <div style="width:100%;height:100%" class="lds-rolling">
            <div>
            </div>
        </div>
    </div>
</div>
<div id="energy_label_container">
    <div id="fuel_label"></div>
    <div id="brake_label"></div>
    <div id="decibel_label"></div>
</div>
{{--@if(!Request::is('*forgot-password*'))--}}
    @include('layouts.header')
{{--@endif--}}
    <main data-auth="{{Auth::check() ? 'true' : 'false'}}" data-api-url="{{ LaravelLocalization::localizeURL('ajax') }}" @if(Request::is('*forgot-password*')) class="loginFullContainer" @endif>
        @yield('content')
        <div class="clearfix"></div>
    </main>
<br><br>
{{--@if(!Request::is('*forgot-password*'))--}}
@include('layouts.footer')
{{--@endif--}}
<div id="notification" style="right: 0; height: auto; top: 50px;display: none">
    <div id="notificationTranslations">
        <div id="cartTrans" data-title="Succesvol aan uw winkelwagen toegevoegd" data-content=""></div>
    </div>

    <div id="notificationHeader">
        <div class="icon icon_cart_{{\App\Governor::$website->iconClass}}"></div>
        <div class="title">Product(en) toegevoegd</div>
        <div id="notificationClose"><!--?xml version="1.0" encoding="utf-8"?--><!-- Generator: Adobe Illustrator 19.2.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve"><g><path d="M32.7,25L48,9.7c1.3-1.3,1.3-3.4,0-4.7L44.9,2c-1.3-1.3-3.4-1.3-4.7,0L25,17.3L9.8,2.1
		c-1.3-1.3-3.4-1.3-4.7,0L2,5.2C0.7,6.5,0.7,8.5,2,9.8L17.2,25L2,40.2c-1.3,1.3-1.3,3.4,0,4.7L5.1,48c1.3,1.3,3.4,1.3,4.7,0L25,32.8
		L40.2,48c1.3,1.3,3.4,1.3,4.7,0l3.1-3.1c1.3-1.3,1.3-3.4,0-4.7L32.7,25z"></path></g></svg></div>
    </div>

    <div class="notificationContent" style="height: auto;">
        <a id="notificationLink" href="{{URL::route('checkout')}}" data-default="{{url('')}}" data-cart="{{URL::route('checkout')}}">
            <div id="notificationContent">
            </div>
        </a>
    </div>
</div>
<div id="cookieWarning">

</div>
</body>
</html>
