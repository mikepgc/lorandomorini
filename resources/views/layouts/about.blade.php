
<head>

    <link href="/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link id="mainStyles" rel="stylesheet" media="screen" href="/css/styles.min.css">
    <link rel="stylesheet" media="screen" href="/css/vendor.min.css">


    <!-- Loading Template CSS -->
    <link href="/css2/style.css" rel="stylesheet">
    <link href="/css2/animate.css" rel="stylesheet">
    <link href="/css2/style-magnific-popup.css" rel="stylesheet">

    <!-- Fonts -->
    <link href="/css2/font-awesome.min.css" rel="stylesheet">
    <link href="/css2/icons-main.css" rel="stylesheet">
    <link href="/css2/icons-helper.css" rel="stylesheet">

    <!-- RS5.0 Main Stylesheet -->
    <link rel="stylesheet" type="text/css" href="/revolution/css/settings.css">

    <!-- RS5.0 Layers and Navigation Styles -->
    <link rel="stylesheet" type="text/css" href="/revolution/css/layers.css">
    <link rel="stylesheet" type="text/css" href="/revolution/css/navigation.css">

    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Tangerine:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lora:400,400italic,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

    <!-- Font Favicon -->
    <link rel="shortcut icon" href="images/favicon.ico">

    <script src="/js/html5shiv.js"></script>
    <script src="/js/respond.min.js"></script>
    <script src="/js/scripts.min.js"></script>
</head>

<body>


{{--@include('templates.banner')--}}
@include('templates.video')

@include('templates.quote')

@include('templates.story')
{{--@include('templates.productdetails')--}}





<script src="/js/jquery-1.11.3.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/bootstrap-select.js"></script>
<script src="/js/bootstrap-switch.js"></script>
<script src="/js/jquery.magnific-popup.min.js"></script>
<script src="/js/jquery.nav.js"></script>
<script src="/js/jquery.scrollTo-min.js"></script>
<script src="/js/SmoothScroll.js"></script>
<script src="/js/wow.js"></script>
<script src="/js/instafeed.js"></script>
<script src="/js/instagram.js"></script>

<script type="text/javascript" src="/revolution/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="/revolution/js/jquery.themepunch.revolution.min.js"></script>

<script type="text/javascript" src="/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="/revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="/revolution/js/extensions/revolution.extension.carousel.min.js"></script>

<!-- begin custom script-->
<script src="/js/custom.js"></script>
<script src="/js/plugins.js"></script>

<script type="text/javascript" src="/revolution/js/extensions/revolution.extension.video.min.js"></script>
<script type="text/javascript" src="/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>

</body>

