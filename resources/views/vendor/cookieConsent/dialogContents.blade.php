
    <div class="js-cookie-consent cookie-consent" style="background-color: #C8584E; color:white; padding-top: 8px; font-weight: 500;padding-left: 20%">

    <span class="cookie-consent__message">
        {!! trans('cookieConsent::texts.message') !!}
    </span>

    <button class="js-cookie-consent-agree cookie-consent__agree btn" style="color: lightgrey">
        {{ trans('cookieConsent::texts.agree') }}
    </button>

</div>
