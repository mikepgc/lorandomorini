<!-- begin revolution slider -->
<div id="rev_slider_110_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="rotating-words82" style="background-color:#e5e5e5;padding:0px;">
    <!-- START REVOLUTION SLIDER 5.0.7 fullscreen mode -->
    <div id="rev_slider_110_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.0.7">
        <ul>

            <!-- SLIDE  -->
            <li data-index="rs-333" data-transition="fade" data-slotamount="default"  data-easein="Power3.easeInOut" data-easeout="Power3.easeInOut" data-masterspeed="1500"  data-rotate="0"  data-saveperformance="off"  data-title="Intro" data-description="">
                <!-- MAIN IMAGE -->
                <img src="/img/coffee/machine2"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                <!-- LAYERS -->

                <!-- BACKGROUND VIDEO LAYER -->
                <div class="rs-background-video-layer"
                     data-forcerewind="on"
                     data-volume="mute"
                     data-videowidth="100%"
                     data-videoheight="100%"
                     data-videomp4="/img/video/roasters.mp4"
                     data-videopreload="preload"
                     data-videoloop="loopandnoslidestop"
                     data-forceCover="1"
                     data-aspectratio="16:9"
                     data-autoplay="true"
                     data-autoplayonlyfirsttime="false"
                     data-nextslideatend="true"
                ></div>

                <!-- LAYER NR. 1 -->
                <div class="tp-caption tp-shape tp-shapewrapper   rs-parallaxlevel-0"
                     id="slide-333-layer-10"
                     data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                     data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                     data-width="full"
                     data-height="full"
                     data-whitespace="nowrap"
                     data-transform_idle="o:1;"

                     data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;"
                     data-transform_out="s:300;s:300;"
                     data-start="0"
                     data-basealign="slide"
                     data-responsive_offset="on"
                     data-responsive="off"

{{--                     Wijzigen transparatie video in Background-color moet je 0.7 wijzigen--}}
                     style="z-index: 5;background-color:rgba(41, 46, 49, 0.7);border-color:rgba(0, 0, 0, 0.50);">
                </div>

                <!-- LAYER NR. 2 -->
                <div class="tp-caption RotatingWords-TitleWhite   tp-resizeme  toblur rs-parallaxlevel-0"
                     id="slide-333-layer-1"
                     data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                     data-y="['middle','middle','middle','middle']" data-voffset="['-50','-50','-152','-152']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"
                     data-transform_idle="o:1;"

                     data-transform_in="x:-50px;opacity:0;s:1500;e:Power3.easeInOut;"
                     data-transform_out="s:300;s:300;"
                     data-start="750"
                     data-splitin="none"
                     data-splitout="none"
                     data-responsive_offset="on"


                     style="z-index: 6; white-space: nowrap;">WE LOVE <span style="color:#9f1b32;">COFFEE</span>
                </div>

                <!-- LAYER NR. 3 -->
                <div class="tp-caption RotatingWords-Button rev-btn  rs-parallaxlevel-0"
                     id="slide-333-layer-7"
                     data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                     data-y="['middle','middle','middle','middle']" data-voffset="['57','27','20','20']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"
                     data-transform_idle="o:1;"
                     data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;"
                     data-style_hover="c:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);cursor:pointer;"

                     data-transform_in="y:100px;sX:1;sY:1;opacity:0;s:2000;e:Power3.easeInOut;"
                     data-transform_out="y:50px;opacity:0;s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                     data-start="750"
                     data-splitin="none"
                     data-splitout="none"
                     data-actions='[{"event":"click","action":"scrollbelow","offset":"-76px"}]'
                     data-responsive_offset="on"
                     data-responsive="off"

                     style="z-index: 11; white-space: nowrap;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;">Ontdek ons verhaal
                </div>

            </li>
        </ul>
        <div class="tp-static-layers"></div>
        <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
    </div>
</div>
<!-- end revolution slider -->
