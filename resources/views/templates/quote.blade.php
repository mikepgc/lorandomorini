<div class="image-section">

    <!--begin image-overlay -->
    <div class="image-overlay"></div>
    <!--end image-overlay -->

    <!--begin container-->
    <div class="container image-section-inside">

        <!--begin row-->
        <div class="row">

            <!--begin col-md-10-->
            <div class="col-md-10 col-md-offset-1 text-center">

                <p class="hero-text2 white wow bounceIn">"
                    De kracht van Lorando en Morini is de praktijkervaring uit de horeca en vele jaren opgebouwde expertise van koffie cuppen en branden.
                    Deze expertise wordt met passie toegepast op een breed en uitdagend scala aan koffiesoorten."</p>

            </div>
            <!--end col-md-6-->

        </div>
        <!--end row-->

    </div>
    <!--end container-->

</div>
