<!-- Top Categories-->
{{--<body style="background-color: #191919" >--}}
<section class="container padding-top-3x" style="background-color: #988068; margin-top: 80px; padding:80px;   border-radius: 2%;
" >
    {{--<h3 class="text-center mb-30">Populair</h3>--}}
    <div class="row">

        @foreach($topcategories as $topcategory)
            <div class="col-md-6 col-sm-6">
                <div class="card mb-30" style="background-color: transparent">
                    <a class="card-img-tiles" href="{{$topcategory->url}}">
                        <div class="inner" style="min-height: 300px">
                            <div class="main-img  ">
                                <div class="text-center">
                                <img  src="{{$topcategory->mainImg}}" style="width: {{$topcategory->width}}px; " alt="Category">
                                </div>
                            </div>
{{--                            <div class="thumblist">--}}
{{--                                <img src="{{$topcategory->thumbImg1}}" style="width: 100px" alt="Category">--}}
{{--                                <br>--}}
{{--                                <img src="{{$topcategory->thumbImg2}}" style="width: 100px" alt="Category">--}}
{{--                            </div>--}}
                        </div>
                    </a>
                    <div class="card-body text-center" style="min-height: 190px">
                        <h4 class="card-title">{{$topcategory->categoryName}}</h4>
                        <p style="color: whitesmoke; font-size: 16px">Vanaf €{{$topcategory->prijs}}</p>
                        <a class="btn btn-outline-primary btn-sm" href="{{$topcategory->url}}">Ga naar producten</a>
                    </div>
                </div>
            </div>
        @endforeach

    </div>
{{--         <div class="text-center"><a class="btn btn-outline-secondary margin-top-none" href="/categoryall">Alle Categorieën</a></div>--}}
</section>
{{--</body>--}}
