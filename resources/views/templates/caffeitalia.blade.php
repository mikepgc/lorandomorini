
<div id="table-wrapper" style="">
    <div id="table-scroll" >


        <table class="table table-bordered table-dark text-center" id="vrd" >
            <thead>
            <tr>
                <th scope="col"><img src="/img/logo/caffeitalia-logo.png" width="180px"></th>
                {{--                        <th scope="col">Datum</th>--}}
                {{--                        <th scope="col">Gegevens</th>--}}
                {{--                        <th scope="col">Betaling</th>--}}
                {{--                        <th scope="col">Orderverzend bevestiging</th>--}}
                {{--                        <th scope="col">Ordertotaal</th>--}}
            </tr>
            </thead>

            <tbody>

            @foreach ($reserveringen->where('goedkeuring','0')  as $reservering)

                  <tr>
                     <td>
                            <input type="hidden" name="id" value="{{$reservering->id}}"/>
                                <p class="box-description mb-3">{{$reservering->naam}} / {{$reservering->personen}}pers.
                                    <br> {{date('D d-m-Y', strtotime($reservering->datum))}}<br>{{$reservering->tijd}}
                                </p>
                                <p style="white-space: normal; font-size: 12px; margin:0" >{{$reservering->opmerking}}</p>


                       <table>
                           <tr>
                               <td style="border-color: transparent">
                                   <form method="post"  onsubmit="return checkForm(this);" action="/succes">
                                       {{ csrf_field() }}
                                       <button  id="submit" class="btn btn-info btn-sm">
                                           <input type="hidden" name="id" value="{{$reservering->id}}"/>
                                           Goedkeuren

                                       </button>
                                   </form>
                               </td>
                               <td style="border-color: transparent">
                                   <form method="post" onsubmit="return checkForm(this);" action="/fail">
                                       {{ csrf_field() }}

                                       <button  id="submit2" class="btn btn-danger btn-sm">
                                           <input type="hidden" name="id"  value="{{$reservering->id}}"/>
                                           Afkeuren
                                       </button>
                                   </form>
                               </td>
                           </tr>
                       </table>

                      </td>
                    </tr>

            @endforeach

            @foreach ($reserveringen7->where('goedkeuring','1')->sortBy('datum')  as $reservering)

                <tr>
                    <td style="background-color: green">
                        <input type="hidden" name="id" value="{{$reservering->id}}"/>
                        <p class="box-description mb-3">{{$reservering->naam}} / {{$reservering->personen}}pers.
                            <br> {{date('D d-m-Y', strtotime($reservering->datum))}}<br>{{$reservering->tijd}}
                        </p>
                        <p style="white-space: normal; font-size: 12px; margin:0" >{{$reservering->opmerking}}</p>




                    </td>
                </tr>

            @endforeach

            @foreach ($reserveringen->where('goedkeuring','2')->sortByDesc('datum')  as $reservering)

                <tr>
                    <td style="background-color: red">
                        <input type="hidden" name="id" value="{{$reservering->id}}"/>
                        <p class="box-description mb-3">{{$reservering->naam}} / {{$reservering->personen}}pers.
                            <br> {{date('D d-m-Y', strtotime($reservering->datum))}}<br>{{$reservering->tijd}}
                        </p>
                        <p style="white-space: normal; font-size: 12px; margin:0" >{{$reservering->opmerking}}</p>


                    </td>
                </tr>

            @endforeach

            </tbody>
        </table>
    </div>
</div>


{{--            <div class="col-md-4">--}}
{{--                <div class=" text-center inner-space-2x">--}}
{{--                    <h3 class="box-title">afgekeurd</h3>--}}
{{--                    @foreach ($reserveringen7->where('goedkeuring','2')  as $reservering)--}}
{{--                        <div class="row line" >--}}
{{--                            <div class="col-md-12">--}}
{{--                                <button class="button-red btn btn-lg"  >--}}
{{--                                    <p class="box-description mb-3">{{$reservering->naam}} / {{$reservering->personen}}pers. <br> {{date('D d-m-Y', strtotime($reservering->datum))}} / {{$reservering->tijd}}</p>--}}
{{--                                    <p style="white-space: normal; font-size: 10px" >{{$reservering->opmerking}}</p>--}}
{{--                                </button>--}}
{{--                            </div>--}}
{{--                            <br>--}}

{{--                        </div>--}}
{{--                    @endforeach--}}
{{--                </div><!-- / icon-block -->--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</section>--}}
