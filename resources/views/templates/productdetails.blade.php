<section id="discover" class="section-white small-padding-bottom">

    <!--begin container-->
    <div class="container">

        <!--begin row-->
        <div class="row margin-bottom-40">

            <!--begin col-md-12-->
            <div class="col-md-12 text-center">

                <div class="text-center" style="padding-top: 20px">

                    <span class="comic-text wow fadeIn">Tasty Coffee</span>

                    <h2 class="section-title wow bounceIn">FRESHLY ROASTED</h2>

                </div>

            </div>
            <!--end col-md-12-->

        </div>
        <!--end row-->

        <!--begin row-->
        <div class="row">

            <!--begin col-md-4 -->
            <div class="col-md-3 wow bounceInUp">

                <!--begin blog-item -->
                <div class="blog-item">

                    <!--begin popup image -->
                    <div class="popup-wrapper">
                        <div class="popup-gallery">
                            <a href="http://placehold.it/800x530" class="popup3 blog-item-pic"><img src="http://placehold.it/800x530" class="width-100" alt="pic"><span class="eye-wrapper"><i class="pe-7s-expand1 eye-icon"></i></span></a>
                        </div>
                    </div>
                    <!--begin popup image -->

                    <!--begin blog-item_inner -->
                    <div class="blog-item-inner">

                        <h3 class="blog-title"><a href="#">Arabica Brazilië Blend</a></h3>


                        <p>De kenmerken van deze koffie op een rijtje:

                            Kenmerken:
                            Verwerking: Pulped op natuurlijk wijze
                            Regio: Mantiqueira de Minas
                            Hoogte: 900 – 1200 meter</p>

                        <a href="#" class="btn btn-lg btn-yellow-small scrool">DISCOVER MORE</a>

                    </div>
                    <!--end blog-item-inner -->

                </div>
                <!--end blog-item -->

            </div>
            <!--end col-md-4-->

            <!--begin col-md-4 -->
            <div class="col-md-3 wow bounceInUp">

                <!--begin blog-item -->
                <div class="blog-item">

                    <!--begin popup image -->
                    <div class="popup-wrapper">
                        <div class="popup-gallery">
                            <a href="http://placehold.it/800x530" class="popup3 blog-item-pic"><img src="http://placehold.it/800x530" class="width-100" alt="pic"><span class="eye-wrapper"><i class="pe-7s-expand1 eye-icon"></i></span></a>
                        </div>
                    </div>
                    <!--begin popup image -->

                    <!--begin blog-item_inner -->
                    <div class="blog-item-inner">

                        <h3 class="blog-title"><a href="#">Arabica Colombia</a></h3>


                        <p>De kenmerken van deze koffie op een rijtje:

                            Smaakkenmerken: evenwichtige fruitig met een delicate body sweet en met lichte tonen van fruit, en eindigt met een bitterzoete, donkere cacao noot.
                            Smaakbeschrijving: Schone kop, medium aciditeit, medium body.
                            Verwerking: Handgepikt, Volledig gewassen, gedeeltelijk in de zon gedroogd en silo gedroogd, zwaartekracht selectie
                            Regio: Risaralda
                            Hoogte:1500 – 1700 meter</p>

                        <a href="#" class="btn btn-lg btn-yellow-small scrool">DISCOVER MORE</a>

                    </div>
                    <!--end blog-item-inner -->

                </div>
                <!--end blog-item -->

            </div>
            <!--end col-md-4-->

            <!--begin col-md-4 -->
            <div class="col-md-3 wow bounceInUp">

                <!--begin blog-item -->
                <div class="blog-item">

                    <!--begin popup image -->
                    <div class="popup-wrapper">
                        <div class="popup-gallery">
                            <a href="http://placehold.it/800x530" class="popup3 blog-item-pic"><img src="http://placehold.it/800x530" class="width-100" alt="pic"><span class="eye-wrapper"><i class="pe-7s-expand1 eye-icon"></i></span></a>
                        </div>
                    </div>
                    <!--begin popup image -->

                    <!--begin blog-item_inner -->
                    <div class="blog-item-inner">

                        <h3 class="blog-title"><a href="#">Arabica Guatemala</a></h3>


                        <p>De kenmerken van deze koffie op een rijtje:

                            Kenmerken: Aroma, Donkere chocolade, Smaak, Wijnige citrus, Veel body, Vol en Zurig
                            Chaff inhoud: Gemiddeld hoog
                            Verwerking: volledig gewassen, in de zon gedroogd op patio’s
                            Regio: Cobán
                            Hoogte: 1200-1450 meter</p>

                        <a href="#" class="btn btn-lg btn-yellow-small scrool">DISCOVER MORE</a>

                    </div>
                    <!--end blog-item-inner -->

                </div>
                <!--end blog-item -->

            </div>
            <!--end col-md-4-->            <!--begin col-md-4 -->
            <div class="col-md-3 wow bounceInUp">

                <!--begin blog-item -->
                <div class="blog-item">

                    <!--begin popup image -->
                    <div class="popup-wrapper">
                        <div class="popup-gallery">
                            <a href="http://placehold.it/800x530" class="popup3 blog-item-pic"><img src="http://placehold.it/800x530" class="width-100" alt="pic"><span class="eye-wrapper"><i class="pe-7s-expand1 eye-icon"></i></span></a>
                        </div>
                    </div>
                    <!--begin popup image -->

                    <!--begin blog-item_inner -->
                    <div class="blog-item-inner">

                        <h3 class="blog-title"><a href="#">Arabica Honduras</a></h3>


                        <p> Kenmerken: Medium body, nootachtig, kruidig, knapperig met rustieke zoetheid, uitstekende balans – zeer soepel
                            Boon-maat: gemiddeld
                            Chaff inhoud: Gemiddeld hoog
                            Dominant Cup Characteristic: Knapperig en kruidig met karamelafwerking.
                            Smaakkenmerken: rustieke suiker (bruine suiker), karamel, nootachtig en kruidig
                            Hardheid van de Boon: Very Hard (SHG)
                            Verwerking: volledig gewassen en in de zon gedroogd
                            Regio: San Juan, Western region of Intibuca
                            Hoogte: 1100 – 1500 meter</p>

                        <a href="#" class="btn btn-lg btn-yellow-small scrool">DISCOVER MORE</a>

                    </div>
                    <!--end blog-item-inner -->

                </div>
                <!--end blog-item -->

            </div>
            <!--end col-md-4-->

        </div>
        <!--end row-->

    </div>
    <!--end container-->

</section>
