<section id="about" class="section-grey" style="background-color: #6e5e4e;  border-bottom: 1px solid #e1e7ec;">

    <!--begin container-->
    <div class="container">

        <!--begin row-->
        <div class="row">

            <!--begin col-md-6-->
            <div class="col-md-6 text-center padding-top-50 padding-bottom-50 wow slideInLeft" style="color: whitesmoke">

                <span class="comic-text" style="color:whitesmoke">Over Lorando & Morini</span>

                <h2 class="section-title" style="color: antiquewhite; padding-top: 60px">Ons verhaal</h2>

                <p style="font-size: 16px; color: whitesmoke;">
                    Al sinds zijn jeugd is de heer Karnaz op zoek naar de lekkerste soorten koffie.
                    Vanuit zijn passie voor koffie, ontdekkingen van smaak en heerlijke aroma’s heeft hij zichzelf geschoold als barista en koffiebrander.
                    Hij reisde hiervoor de hele wereld over, bezocht koffie producerende landen in Azië, Afrika en Zuid-Amerika.<br><br>

                    In zijn eerste eetcafe in Den Haag begon hij met professionele apparatuur en cupping.
                    Hoe kon het dat de verschillende soorten koffie die hij voor zijn klanten maakte anders of beter van smaak waren?
                    De vele factoren die hier van invloed zijn overtuigden hem om op onderzoek te gaan.
                    Hij schonk eerst Illy’s pure Arabica melange en later Segafredo’s.
                    Met deze partners ging hij op zoek naar nieuwe smaken, melanges, aroma’s en koffiesoorten.
                    Hij leerde hoe je als goede barista het verschil maakt en welke factoren je zelf kunt beïnvloeden om te komen tot de beste koffie voor de klant.<br><br>

                    Jarenlang testte hij koffiesoorten, bezocht vele boeren en bezorgde met zijn collega’s overheerlijke koffies voor hun alsmaar groeiende klandizie.
                    De heer Karnaz ontdekte daarmee hoeveel invloed het productieproces op de smaak en aroma’s van koffie heeft. De plant, boon, het klimaat, de ligging, droging en het branden.

                    <br><br>
                    Na zijn studie en hij het hele proces in kaart te hebben gebracht kon het niet anders dan dat heer Karnaz zijn expertise zou gaan delen. Hij trainde zijn personeel, collega ondernemers en klanten.
                    Toch kon het hier niet bij blijven. Om te komen tot een eigen en unieke koffie startte hij de ambachtelijke koffiebranderij Lorando en Morini.

                    Vanuit deze Rotterdamse koffiebranderij bedient heer Karnaz vele barista’s, restaurants, hotels en bedrijven van de meest verse koffie die er in de wijde omtrek te verkrijgen zijn.


                </p>
                <img src="/img/lorandomorini-5.jpg" alt="picture" class="width-100" style="padding-top: 20px">



{{--                <a href="about.html" class="btn btn-lg btn-yellow">OUR RECEIPES</a>--}}

            </div>
            <!--end col-md-6-->

            <!--begin col-md-6-->
            <div class="col-md-6 wow slideInRight">

                <img src="/img/lorandomorini-over.jpg" alt="picture" class="width-100" style="padding-top: 170px">

                <h2 class="section-title" style="color: antiquewhite; padding-top: 170px">Innovatie op smaak </h2>

                <p style="font-size: 16px; color: whitesmoke;">

                    Lorando en Morini is altijd op zoek naar nieuwe soorten, smaken en aroma’s.
                    Hiervoor werken wij samen met een netwerk van internationale distributeurs en eigen koffieboeren.
                    Zij bieden ons toegang tot de beste kwaliteit single state origin Arabica’s. De bekende maar ook minder bekende gewassen, landen en gebieden.
                    Zo vergroten wij niet alleen ons assortiment maar ook de expertise die wij nodig hebben om te komen tot de unieke smaak die u zoekt. Voor uzelf, uw klanten of collega’s.

                    <br><br>
                    De kracht van Lorando en Morini is de praktijkervaring uit de horeca en vele jaren opgebouwde expertise van koffie cuppen en branden.
                    Deze expertise wordt met passie toegepast op een breed en uitdagend scala aan koffiesoorten. Wij maken gebruik van de nieuwste high-tech apparatuur en koffiebranders.
                    Met onze faciliteiten bieden wij daarmee een nieuwe plek voor koffie liefhebbers, klanten en professionele partners die op zoek zijn naar de beste koffie met een eigen en unieke smaak.


                </p>


            </div>
            <!--end col-md-6-->

        </div>
        <!--end row-->

    </div>
    <!--end container-->

</section>
