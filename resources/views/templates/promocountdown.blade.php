

<section class="container-fluid padding-top-3x">
    <div class="row justify-content-center">
        <div class="col-xl-5 col-lg-6 mb-30">
            <div class="rounded bg-faded position-relative padding-top-3x padding-bottom-3x">
{{--                 <span class="product-badge text-danger" style="top: 24px; left: 24px;">Limited Offer</span>--}}
                <div class="text-center">
                    <h3 class="h2 text-normal mb-1">Maak je eigen studio!!</h3>
                    <h2 class="display-2 text-bold mb-2">Studioflitsset </h2>
                    <h4 class="h3 text-normal mb-4">Linkstar Studioflitsset MTGK-3150U</h4>
                    {{--<div class="countdown mb-3" data-date-time="11/01/2019 12:00:00">--}}
                        {{--<div class="item">--}}
                            {{--<div class="days"></div><span class="days_ref">Dagen</span>--}}
                        {{--</div>--}}
                        {{--<div class="item">--}}
                            {{--<div class="hours">00</div><span class="hours_ref">Uren</span>--}}
                        {{--</div>--}}
                        {{--<div class="item">--}}
                            {{--<div class="minutes">00</div><span class="minutes_ref">Minuten</span>--}}
                        {{--</div>--}}
                        {{--<div class="item">--}}
                            {{--<div class="seconds">00</div><span class="seconds_ref">Secondes</span>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    <a class="btn btn-primary margin-bottom-none" href="/category/subcategory-list/single-product/560110">Bestel nu!</a>

                </div>
            </div>
        </div>
        <div class="col-xl-5 col-lg-6 mb-30" style="min-height: 270px;">
            <div class="img-cover rounded" style="background-size:auto; background-image: url(https://www.benel.nl/images/webshop/linkstar-studioflitsset-mtgk-3150u-full-560110-1-32176-185.jpg)"></div>
        </div>
    </div>
</section>
