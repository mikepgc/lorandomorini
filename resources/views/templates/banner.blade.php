<div id="hero-section" class="home-hero-section">

    <div class="rev_slider_wrapper fullscreen-container" data-alias="agency-website" id="rev_slider_280_1_wrapper" style="background-color:#222222;padding:0px;height:20%">

        <!-- START REVOLUTION SLIDER 5.1.4 fullscreen mode -->
        <div class="rev_slider fullscreenbanner" data-version="5.1.4" id="rev_slider_280_1" style="display:none;">

            <ul>

                <!-- SLIDE  -->
                <li data-description="" data-easein="Power2.easeInOut" data-easeout="default" data-index="rs-898" data-masterspeed="2000" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-title="Slide" data-transition="fade">

                    <!-- MAIN IMAGE -->
                    {{--                    <img alt="" class="rev-slidebg" data-bgparallax="3" data-bgposition="center center" data-duration="30000" data-ease="Linear.easeNone" data-kenburns="on" data-no-retina="" data-offsetend="0 0" data-offsetstart="0 0" data-rotateend="0" data-rotatestart="0" data-scaleend="100" data-scalestart="140" src="http://placehold.it/1920x1280">--}}
                    <img alt="" class="rev-slidebg" data-bgparallax="3" data-bgposition="center center" data-duration="30000" data-ease="Linear.easeNone" data-kenburns="on" data-no-retina="" data-offsetend="0 0" data-offsetstart="0 0" data-rotateend="0" data-rotatestart="0" data-scaleend="100" data-scalestart="140" src="/img/lorandomorini-115.jpg">

                    <!-- LAYERS -->
                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption tp-shape tp-shapewrapper" data-basealign="slide" data-height="full" data-hoffset="['0','0','0','0']" data-responsive="off" data-responsive_offset="off" data-start="0" data-transform_idle="o:1;" data-transform_in="opacity:0;s:2000;e:Power2.easeInOut;" data-transform_out="opacity:0;s:500;s:500;" data-voffset="['0','0','0','0']" data-whitespace="nowrap" data-width="full" data-x="['center','center','center','center']" data-y="['middle','middle','middle','middle']" id="slide-898-layer-1" style="z-index: 5;background-color:rgba(34, 34, 34, 0.45);border-color:rgba(0, 0, 0, 0);">
                    </div>

                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption tp-shape tp-shapewrapper" data-basealign="slide" data-end="bytrigger" data-height="full" data-hoffset="['0','0','0','0']" data-lasttriggerstate="reset" data-responsive="off" data-responsive_offset="off" data-start="bytrigger" data-transform_idle="o:1;" data-transform_in="opacity:0;s:2000;e:Power2.easeInOut;" data-transform_out="opacity:0;s:500;s:500;" data-voffset="['0','0','0','0']" data-whitespace="nowrap" data-width="full" data-x="['center','center','center','center']" data-y="['middle','middle','middle','middle']" id="slide-898-layer-19" style="z-index: 6;background-color:rgba(0, 0, 0, 0.75);border-color:rgba(0, 0, 0, 0);">
                    </div>

                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption Concept-SubTitle   tp-resizeme" id="slide-674-layer-17" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['-178','-178','-178','-178']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power2.easeInOut;" data-transform_out="opacity:0;s:300;s:300;" data-start="500" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 6; white-space: nowrap;font-style:italic;"><img src="img/rs-logo.png" alt="" width="102" height="115" data-no-retina>
                    </div>

                    <!-- LAYER NR. 3 -->
                    <div class="tp-caption tp-shape tp-shapewrapper tp-resizeme" data-height="2" data-hoffset="['0','0','0','0']" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-responsive_offset="on" data-start="1000" data-transform_idle="o:1;" data-transform_in="x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power3.easeInOut;" data-transform_out="opacity:0;s:300;s:300;" data-voffset="['-106','-106','-106','-106']" data-whitespace="nowrap" data-width="100" data-x="['center','center','center','center']" data-y="['middle','middle','middle','middle']" id="slide-898-layer-6" style="z-index: 7;background-color:rgba(255, 255, 255, 1.00);border-color:rgba(0, 0, 0, 0);">
                    </div>

                    <!-- LAYER NR. 4 -->
                    <div class="tp-caption Agency-Title tp-resizeme" data-fontsize="['70','70','50','50']" data-height="none" data-hoffset="['0','0','0','0']" data-lineheight="['70','70','50','50']" data-mask_in="x:0px;y:0px;" data-responsive_offset="on" data-splitin="none" data-splitout="none" data-start="1250" data-transform_idle="o:1;" data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power3.easeInOut;" data-transform_out="opacity:0;s:300;s:300;" data-voffset="['-35','-35','-42','-41']" data-whitespace="nowrap" data-width="none" data-x="['center','center','center','center']" data-y="['middle','middle','middle','middle']" id="slide-898-layer-2" style="z-index: 8; white-space: nowrap;">

                        Gallery

                        </div>

                    <!-- LAYER NR. 5 -->
                    <div class="tp-caption Agency-SubTitle tp-resizeme" data-height="none" data-hoffset="['0','0','0','0']" data-responsive_offset="on" data-splitin="none" data-splitout="none" data-start="1500" data-transform_idle="o:1;" data-transform_in="y:50px;opacity:0;s:2000;e:Power2.easeInOut;" data-transform_out="opacity:0;s:300;s:300;" data-voffset="['29','29','25','25']" data-whitespace="['nowrap','nowrap','normal','normal']" data-width="['none','none','480','360']" data-x="['center','center','center','center']" data-y="['middle','middle','middle','middle']" id="slide-898-layer-4" style="z-index: 9; white-space: nowrap;text-align:center;font-style:italic;">
                        Langskomen? Neem contact op met ons!                    </div>

                    <!-- LAYER NR. 5 -->
                    <div class="tp-caption Dining-BtnLight rev-btn" id="slide-898-layer-17" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']" data-voffset="['537','477','566','425']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;" data-style_hover="c:rgba(255, 255, 255, 1.00);bg:rgba(255, 255, 255, 0.15);cursor:pointer;" data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1000;e:Power3.easeInOut;" data-transform_out="auto:auto;s:2;" data-start="2100" data-splitin="none" data-splitout="none" data-actions='[{"event":"click","action":"startlayer","layer":"slide-898-layer-18","delay":""},{"event":"click","action":"scrollbelow","offset":"-76px"}]' data-responsive_offset="on" data-responsive="off" data-end="bytrigger" data-lasttriggerstate="reset" style="z-index: 9; white-space: nowrap;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box; background-color: #C8584E">
                        Toon Gallery
                    </div>

                </li>
            </ul>
            <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
        </div>
    </div>
    <!-- END REVOLUTION SLIDER -->

</div>
<!--end home-hero-section -->
