<!-- Services-->
<section class="container padding-top-3x padding-bottom-2x">
    <div class="row">
        <div class="col-md-4 col-sm-6 text-center mb-30"><img class="d-block w-90 img-thumbnail rounded-circle mx-auto mb-3" src="img/services/05.png" alt="Shipping">
            <h6>Gratis verzending</h6>
            <p class="text-muted margin-bottom-none">Gratis verzending bij bestelling boven €30,-</p>
        </div>
{{--        <div class="col-md-3 col-sm-6 text-center mb-30"><img class="d-block w-90 img-thumbnail rounded-circle mx-auto mb-3" src="img/services/02.png" alt="Money Back">--}}
{{--            <h6>2 Jaar Garantie</h6>--}}
{{--            <p class="text-muted margin-bottom-none">We geven 2 jaar garantie op onze producten</p>--}}
{{--        </div>--}}
        <div class="col-md-4 col-sm-6 text-center mb-30"><img class="d-block w-90 img-thumbnail rounded-circle mx-auto mb-3" src="img/services/03.png" alt="Support">
            <h6>Klanten Support</h6>
            <p class="text-muted margin-bottom-none">Wij staat voor u klaar</p>
        </div>
        <div class="col-md-4 col-sm-6 text-center mb-30"><img class="d-block w-90 img-thumbnail rounded-circle mx-auto mb-3" src="img/services/04.png" alt="Payment">
            <h6>Veilig betalen</h6>
            <p class="text-muted margin-bottom-none">Betalingen via SSL / beveiligde certificaat</p>

        </div>
    </div>
</section>

<div style="border-bottom: 1px solid ghostwhite"></div>

</body>
