<div id="table-wrapper" style="">
    <div id="table-scroll" >


        <table class="table table-bordered table-dark text-center" id="vrd" >
            <thead>
            <tr>
                <th scope="col"><img src="/img/logo/logo-white3.png" width="100px"></th>
                {{--                        <th scope="col">Datum</th>--}}
                {{--                        <th scope="col">Gegevens</th>--}}
                {{--                        <th scope="col">Betaling</th>--}}
                {{--                        <th scope="col">Orderverzend bevestiging</th>--}}
                {{--                        <th scope="col">Ordertotaal</th>--}}
            </tr>
            </thead>

            <tbody>

            {{---------------- With the below @IF statement the testorders are only visible in test.lorandomorini.nl---------------}}


            @if(env('TEST_ENV') == TRUE)
                @foreach ($order->sortByDesc('id')->where('payment', '1') as $item)
                    <tr>
                        <td>
                            <form  id="button" onclick="window.location = 'dos/' + search.value; return false;" style="margin: 0 ">
                                @if($item->order_sent  == 1 )
                                    <input name="myButton" id="search" style="background-color: green; border-radius: 10px; color: black" value="{{$item->ordernumber}}" type="button" disabled>
                                @else
                                    <input name="myButton" id="search" style="background-color: red" value="{{$item->ordernumber}}" type="button" hidden>
                                    <button  name="myButton"  style="background-color: red; border-radius: 10px;" value="{{$item->ordernumber}}" id="myBtn">{{$item->ordernumber}}</button>

                                @endif
                            </form>
                            {{$item->user_email }}<br>{{date_format($item->created_at, 'd-M-y')}}<br>{{round($item->ordertotal,2) }}
                          </td>
                    </tr>
                @endforeach
            @else
                @foreach ($order->sortByDesc('id')->where('payment', '1')->where('test', Null) as $item)
                    <tr>
                        <td>
                            <form  id="button" onclick="window.location = 'dos/' + search.value; return false;" style="margin: 0 ">
                                @if($item->order_sent  == 1 )
                                    <input name="myButton" id="search" style="background-color: green; border-radius: 10px;" value="{{$item->ordernumber}}" type="button" disabled>
                                @else
                                    <input name="myButton" id="search" style="background-color: red; border-radius: 10px;" value="{{$item->ordernumber}}" type="button" hidden>
                                    <button  name="myButton"  style="background-color: red; border-radius: 10px;" value="{{$item->ordernumber}}" id="myBtn">{{$item->ordernumber}}</button>                                    @endif
                            </form>
                            {{$item->user_email }}<br>{{date_format($item->created_at, 'd-M-y')}}<br>{{round($item->ordertotal,2) }}</td>
                    </tr>
                @endforeach
            @endif





            </tbody>
        </table>
    </div>
</div>
