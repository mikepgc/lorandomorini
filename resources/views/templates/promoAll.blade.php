<!-- Promo #2-->

<!-- Featured Products Carousel-->
{{--------------------------Landen---------------------------}}
<section class="container padding-top-3x padding-bottom-3x">
    <h3 class="text-center mb-30">Verse koffiebonen uit verschillende landen</h3>
    <div class="owl-carousel" data-owl-carousel="{ &quot;nav&quot;: false, &quot;dots&quot;: true, &quot;margin&quot;: 30, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:1},&quot;576&quot;:{&quot;items&quot;:2},&quot;768&quot;:{&quot;items&quot;:3},&quot;991&quot;:{&quot;items&quot;:4},&quot;1200&quot;:{&quot;items&quot;:4}} }">
        <!-- Product-->
        <div class="grid-item">
            <div class="product-card">
                {{--<div class="product-badge text-danger">22% Off</div>--}}
                <a class="product-thumb" href="/category/subcategory-grid/single-product/LM-002"><img src="/img/coffee/8a95eef2985dfeafd83593ab76f6a014.png" alt="Product"></a>
                <a href="/category/subcategory-grid/single-product/LM-002">
                    <h3 class="product-title" style=" margin-bottom: 10px; color: whitesmoke; font-size: 20px; font-weight: 500; text-align: center;">Arabica Brazilië </h3>
                </a>
                <h4 class="product-price">
                    Vanaf €12,50
                </h4>
                <div class="product-buttons">
                    {{--<button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip" title="Whishlist"><i class="icon-heart"></i></button>--}}
                    <button class="btn btn-outline-primary btn-sm" ><a href="/category/subcategory-grid/single-product/LM-002" style="color: white">Opties</a></button>
                </div>
            </div>
        </div>
        <!-- Product-->
        <!-- Product-->
        <div class="grid-item">
            <div class="product-card">
                {{--<div class="product-badge text-danger">22% Off</div>--}}
                <a class="product-thumb" href="/category/subcategory-grid/single-product/LM-004"><img src="/img/coffee/fe0b7564cd959c85b80fb8f58346d0c5.png" alt="Product"></a>
                <a href="/category/subcategory-grid/single-product/LM-004">
                    <h3 class="product-title" style=" margin-bottom: 10px; color: whitesmoke; font-size: 20px; font-weight: 500; text-align: center;">Arabica Colombia</h3>
                </a>
                <h4 class="product-price">
                    Vanaf €12,50
                </h4>
                <div class="product-buttons">
                    {{--<button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip" title="Whishlist"><i class="icon-heart"></i></button>--}}
                    <button class="btn btn-outline-primary btn-sm" ><a href="/category/subcategory-grid/single-product/LM-004" style="color: white">Opties</a></button>
                </div>
            </div>
        </div>
        <!-- Product-->
        <!--<div class="grid-item">
            <div class="product-card">
                {{--<div class="product-badge text-danger">22% Off</div>--}}
                <a class="product-thumb" href="/category/subcategory-grid/single-product/LM-006"><img src="/img/coffee/1c70e4b418e6324db5ef880a1ad2fa11.png" alt="Product"></a>
                <a href="/category/subcategory-grid/single-product/LM-006">
                    <h3 class="product-title" style=" margin-bottom: 10px; color: whitesmoke; font-size: 20px; font-weight: 500; text-align: center;">Arabica Guatemala</h3>
                </a>
                <h4 class="product-price">
                    Vanaf €12,50
                </h4>
                <div class="product-buttons">
                    {{--<button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip" title="Whishlist"><i class="icon-heart"></i></button>--}}
                    <button class="btn btn-outline-primary btn-sm" ><a href="/category/subcategory-grid/single-product/LM-006" style="color: white">Opties</a></button>
                </div>
            </div>
        </div>-->
        <!-- Product-->
        <!-- Product-->
      <!-- <div class="grid-item">
            <div class="product-card">
                {{--<div class="product-badge text-danger">22% Off</div>--}}
                <a class="product-thumb" href="/category/subcategory-grid/single-product/LM-008"><img src="/img/coffee/aa24b6153b27c206a71d82187239a450.png" alt="Product"></a>
               <a href="/category/subcategory-grid/single-product/LM-008">
                   <h3 class="product-title" style=" margin-bottom: 10px; color: whitesmoke; font-size: 20px; font-weight: 500; text-align: center;">Arabica Honduras</h3>

               </a>
                <h4 class="product-price">
                    Vanaf €12,50
                </h4>
                <div class="product-buttons">
                    {{--<button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip" title="Whishlist"><i class="icon-heart"></i></button>--}}
                    <button class="btn btn-outline-primary btn-sm" ><a href="/category/subcategory-grid/single-product/LM-008" style="color: white">Opties</a></button>
                </div>
            </div>
        </div> -->
        <!-- Product-->


    </div>
</section>


<section class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-xl-10 col-lg-12">
            <div class="fw-section rounded padding-top-4x padding-bottom-4x"  style=" background-image: url(/img/LMfooter2.jpg)"><span class="overlay rounded" style="opacity: .35;"></span>
                <div class="text-center">
                    <h3 style="color:gold" class="display-4 text-normal  text-shadow mb-1" >Gratis VIP Membership</h3>
                    <h2 class="display-2 text-bold text-white text-shadow">Voor de koffie liefhebbers!</h2>
                    <h4 class="d-inline-block h2 text-normal text-white text-shadow border-default border-left-0 border-right-0 mb-4">Altijd 10% korting op uw bestelling</h4>
                    <br><a class="btn btn-primary margin-bottom-none" href="/contacts">Bel of mail ons voor meer info</a>
                </div>
            </div>
        </div>
    </div>
</section>

{{--------------------------Seizoen---------------------------}}
{{--<section class="container padding-top-3x padding-bottom-3x">--}}
{{--    <h3 class="text-center mb-30">Verse koffiebonen voor elke seizoen</h3>--}}
{{--    <div class="owl-carousel" data-owl-carousel="{ &quot;nav&quot;: false, &quot;dots&quot;: true, &quot;margin&quot;: 30, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:1},&quot;576&quot;:{&quot;items&quot;:2},&quot;768&quot;:{&quot;items&quot;:3},&quot;991&quot;:{&quot;items&quot;:4},&quot;1200&quot;:{&quot;items&quot;:4}} }">--}}

{{--        <!-- Product-->--}}
{{--        <div class="grid-item">--}}
{{--            <div class="product-card">--}}
{{--                --}}{{--<div class="product-badge text-danger">22% Off</div>--}}
{{--                <a class="product-thumb" href="/category/subcategory-grid/single-product/LM-025"><img src="/img/coffee/f044ea4727e7d7748295aacd80a2cc9b.png" alt="Product"></a>--}}
{{--                <a href="/category/subcategory-grid/single-product/LM-025">--}}
{{--                    <h3 class="product-title" style=" margin-bottom: 10px; color: whitesmoke; font-size: 20px; font-weight: 500; text-align: center;">De Zomer</h3>--}}

{{--                </a>--}}
{{--                <h4 class="product-price">--}}
{{--                    Vanaf €12,50--}}
{{--                </h4>--}}
{{--                <div class="product-buttons">--}}
{{--                    --}}{{--<button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip" title="Whishlist"><i class="icon-heart"></i></button>--}}
{{--                    <button class="btn btn-outline-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Winkelwagen</button>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <!-- Product-->--}}
{{--        <!-- Product-->--}}
{{--        <div class="grid-item">--}}
{{--            <div class="product-card">--}}
{{--                --}}{{--<div class="product-badge text-danger">22% Off</div>--}}
{{--                <a class="product-thumb" href="/category/subcategory-grid/single-product/LM-028"><img src="/img/coffee/43b58c5c8062e1af6c60f9816bff52bf.png" alt="Product"></a>--}}
{{--                <a href="/category/subcategory-grid/single-product/LM-028">--}}
{{--                    <h3 class="product-title" style=" margin-bottom: 10px; color: whitesmoke; font-size: 20px; font-weight: 500; text-align: center;">De Herfst</h3>--}}

{{--                </a>--}}
{{--                <h4 class="product-price">--}}
{{--                    Vanaf €12,50--}}
{{--                </h4>--}}
{{--                <div class="product-buttons">--}}
{{--                    --}}{{--<button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip" title="Whishlist"><i class="icon-heart"></i></button>--}}
{{--                    <button class="btn btn-outline-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Winkelwagen</button>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <!-- Product-->--}}
{{--        <!-- Product-->--}}
{{--        <div class="grid-item">--}}
{{--            <div class="product-card">--}}
{{--                --}}{{--<div class="product-badge text-danger">22% Off</div>--}}
{{--                <a class="product-thumb" href="/category/subcategory-grid/single-product/LM-023"><img src="/img/coffee/be1e9d938ff73ae12d4a5fcdec47c907.png" alt="Product"></a>--}}
{{--                <a href="/category/subcategory-grid/single-product/LM-023">--}}
{{--                    <h3 class="product-title" style=" margin-bottom: 10px; color: whitesmoke; font-size: 20px; font-weight: 500; text-align: center;">De Winter</h3>--}}

{{--                </a>--}}
{{--                <h4 class="product-price">--}}
{{--                    Vanaf €12,50--}}
{{--                </h4>--}}
{{--                <div class="product-buttons">--}}
{{--                    --}}{{--<button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip" title="Whishlist"><i class="icon-heart"></i></button>--}}
{{--                    <button class="btn btn-outline-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Winkelwagen</button>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <!-- Product-->--}}
{{--        <!-- Product-->--}}
{{--        <div class="grid-item">--}}
{{--            <div class="product-card">--}}
{{--                --}}{{--<div class="product-badge text-danger">22% Off</div>--}}
{{--                <a class="product-thumb" href="/category/subcategory-grid/single-product/LM-027"><img src="/img/coffee/551a865129aedab8e3f94b81a55bd09c.png" alt="Product"></a>--}}
{{--                <a href="/category/subcategory-grid/single-product/LM-027">--}}
{{--                    <h3 class="product-title" style=" margin-bottom: 10px; color: whitesmoke; font-size: 20px; font-weight: 500; text-align: center;">De Lente</h3>--}}

{{--                </a>--}}
{{--                <h4 class="product-price">--}}
{{--                    Vanaf €12,50--}}
{{--                </h4>--}}
{{--                <div class="product-buttons">--}}
{{--                    --}}{{--<button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip" title="Whishlist"><i class="icon-heart"></i></button>--}}
{{--                    <button class="btn btn-outline-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Winkelwagen</button>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <!-- Product-->--}}


{{--    </div>--}}
{{--</section>--}}


{{--------------------------end Seizoen---------------------------}}





<!-- Product Widgets-->
{{--<section class="container padding-bottom-2x">--}}
    {{--<div class="row">--}}
        {{--<div class="col-md-4 col-sm-6">--}}
            {{--<div class="widget widget-featured-products">--}}
                {{--<h3 class="widget-title">Top Sellers</h3>--}}
                {{--<!-- Entry-->--}}
                {{--<div class="entry">--}}
                    {{--<div class="entry-thumb"><a href="losse templates/shop-single.html"><img src="img/shop/widget/01.jpg" alt="Product"></a></div>--}}
                    {{--<div class="entry-content">--}}
                        {{--<h4 class="entry-title"><a href="losse templates/shop-single.html">Oakley Kickback</a></h4><span class="entry-meta">$155.00</span>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<!-- Entry-->--}}
                {{--<div class="entry">--}}
                    {{--<div class="entry-thumb"><a href="losse templates/shop-single.html"><img src="img/shop/widget/03.jpg" alt="Product"></a></div>--}}
                    {{--<div class="entry-content">--}}
                        {{--<h4 class="entry-title"><a href="losse templates/shop-single.html">Vented Straw Fedora</a></h4><span class="entry-meta">$49.50</span>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<!-- Entry-->--}}
                {{--<div class="entry">--}}
                    {{--<div class="entry-thumb"><a href="losse templates/shop-single.html"><img src="img/shop/widget/04.jpg" alt="Product"></a></div>--}}
                    {{--<div class="entry-content">--}}
                        {{--<h4 class="entry-title"><a href="losse templates/shop-single.html">Big Wordmark Tote</a></h4><span class="entry-meta">$29.99</span>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="col-md-4 col-sm-6">--}}
            {{--<div class="widget widget-featured-products">--}}
                {{--<h3 class="widget-title">New Arrivals</h3>--}}
                {{--<!-- Entry-->--}}
                {{--<div class="entry">--}}
                    {{--<div class="entry-thumb"><a href="losse templates/shop-single.html"><img src="img/shop/widget/05.jpg" alt="Product"></a></div>--}}
                    {{--<div class="entry-content">--}}
                        {{--<h4 class="entry-title"><a href="losse templates/shop-single.html">Union Park</a></h4><span class="entry-meta">$49.99</span>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<!-- Entry-->--}}
                {{--<div class="entry">--}}
                    {{--<div class="entry-thumb"><a href="losse templates/shop-single.html"><img src="img/shop/widget/06.jpg" alt="Product"></a></div>--}}
                    {{--<div class="entry-content">--}}
                        {{--<h4 class="entry-title"><a href="losse templates/shop-single.html">Cole Haan Crossbody</a></h4><span class="entry-meta">$200.00</span>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<!-- Entry-->--}}
                {{--<div class="entry">--}}
                    {{--<div class="entry-thumb"><a href="losse templates/shop-single.html"><img src="img/shop/widget/07.jpg" alt="Product"></a></div>--}}
                    {{--<div class="entry-content">--}}
                        {{--<h4 class="entry-title"><a href="losse templates/shop-single.html">Skagen Holst Watch</a></h4><span class="entry-meta">$145.00</span>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="col-md-4 col-sm-6">--}}
            {{--<div class="widget widget-featured-products">--}}
                {{--<h3 class="widget-title">Best Rated</h3>--}}
                {{--<!-- Entry-->--}}
                {{--<div class="entry">--}}
                    {{--<div class="entry-thumb"><a href="losse templates/shop-single.html"><img src="img/shop/widget/08.jpg" alt="Product"></a></div>--}}
                    {{--<div class="entry-content">--}}
                        {{--<h4 class="entry-title"><a href="losse templates/shop-single.html">Jordan's City Hoodie</a></h4><span class="entry-meta">$65.00</span>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<!-- Entry-->--}}
                {{--<div class="entry">--}}
                    {{--<div class="entry-thumb"><a href="losse templates/shop-single.html"><img src="img/shop/widget/09.jpg" alt="Product"></a></div>--}}
                    {{--<div class="entry-content">--}}
                        {{--<h4 class="entry-title"><a href="losse templates/shop-single.html">Palace Shell Track Jacket</a></h4><span class="entry-meta">$36.99</span>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<!-- Entry-->--}}
                {{--<div class="entry">--}}
                    {{--<div class="entry-thumb"><a href="losse templates/shop-single.html"><img src="img/shop/widget/10.jpg" alt="Product"></a></div>--}}
                    {{--<div class="entry-content">--}}
                        {{--<h4 class="entry-title"><a href="losse templates/shop-single.html">Off the Shoulder Top</a></h4><span class="entry-meta">$128.00</span>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div></section>--}}



