<!-- Main Slider-->


      <section class="hero-slider" style="background-image: url(/img/LMfooter.jpg);">

        <div class="owl-carousel large-controls dots-inside" data-owl-carousel="{ &quot;nav&quot;: true, &quot;dots&quot;: true, &quot;loop&quot;: true, &quot;autoplay&quot;: true, &quot;autoplayTimeout&quot;: 7000 }">

            @foreach ($sliders->where('id', '==', '12') as $slider)


            <div class="item">
              <div class="container padding-top-3x">
                <div class="row justify-content-center align-items-center">
                  <div class="col-lg-5 col-md-6 padding-bottom-2x text-md-left text-center">
                    <div class="from-bottom">
                        <img src="{{$slider->logoBrand}}" height="60" width="120" alt="Lorando Morini">
                      <div class="h2  text-normal mb-2 pt-1" style="color: whitesmoke">{{$slider->productName}}</div>
                      <div class="h2  text-normal mb-4 pb-1"><span class="text-bold" style="color: gold">&euro;{{$slider->price}}
                                        <del style="color: red; font-size: 20px" >€{{$slider->price_discount}}</del></span></div>
                    </div><a class="btn btn-primary scale-up delay-1" href="{{$slider->urlProduct}}">Toon product</a>
                  </div>
                  <div class="col-md-6 padding-bottom-2x mb-3"><img class="d-block mx-auto" src="{{$slider->imageProduct}}" width="{{$slider->imageWidth}}px"></div>
                </div>
              </div>
            </div>

          @endforeach

        </div>
      </section>
