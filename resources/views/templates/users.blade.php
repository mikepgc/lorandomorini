<div id="table-wrapper">
    <div id="table-scroll" >

        @if (session('alert2'))
            <div class="alert alert-success  text-center">
                {{ session('alert2') }}
            </div>
        @endif

        <table class="table table-bordered table-dark text-center" id="vrd" style="max-width: 100%">
            <thead>
            <tr>
                <th scope="col"><img src="/img/logo/logo-white3.png" width="100px"></th>

            </tr>
            </thead>

            <tbody>

            {{---------------- With the below @IF statement the testorders are only visible in test.lorandomorini.nl---------------}}

                @foreach ($users as $user)
                    <tr>
                        <td>
                            <form action="/getvip/{{$user->id}}"  method="POST">
                                {{ csrf_field() }}
                                @if($user->vip)
                                    <button  name="myButton" style="background-color: green; border-radius: 10px;" value="{{$user->id}}" id="myBtn">
                                        VIP
                                    </button>
                                @else
                                    <button  name="myButton" style="background-color: red; border-radius: 10px;" value="{{$user->id}}" id="myBtn">
                                        Geen VIP
                                    </button>
                                @endif
                            </form>


                            <span style="font-weight: bold">{{$user->name}} {{$user->surname}}</span><br>
                            <span style="font-size: 14px">{{$user->email}}</span><br>

                            @if($user->account)
                            <span style="color: green">Heeft account</span><br>
                            @else
                            <span style="color: red">Geen account</span><br>
                            @endif


                        </td>
                    </tr>
                @endforeach

            </tbody>
        </table>
    </div>
</div>

