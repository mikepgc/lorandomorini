
@extends('layouts.master2')


@section('content')


    @include('layouts.sidebar')

    @include('layouts.topbar')

    @include ('layouts.navbar')

    <!-- Off-Canvas Wrapper-->
    <div class="offcanvas-wrapper" style="background-color: #6e5e4e; border-bottom: 1px solid ghostwhite" >
      <!-- Page Title-->
      <div class="page-title"  style="background-color: #6e5e4e; border-bottom: 1px solid ghostwhite" >
        <div class="container">
          <div class="column">
            <h1 style="color: whitesmoke">Contact</h1>
          </div>
          <div class="column">
            <ul class="breadcrumbs">
              <li><a href="/">Home</a>
              </li>
              <li class="separator">&nbsp;</li>
              <li>Contact</li>
            </ul>
          </div>
        </div>
      </div>
      <!-- Page Content-->
      <div class="container padding-bottom-2x mb-2" >
        <div class="row">
          <div class="col-md-7">
            <div class="display-3 mb-30" style="color: whitesmoke">Klantenservice</div>
          </div>
          <div class="col-md-5">
            <ul class="list-icon" style="color: whitesmoke">
              <li> <i class="icon-mail"></i><a class="navi-link" href="mailto:info@lorandomorini.nl" style="color: whitesmoke">Lorando & Morini mail</a></li>
              <li > <i class="icon-bell"></i>+31 6 23 23 55 09</li>
              <li> <i class="icon-clock"></i>2 uur</li>
            </ul>
          </div>
        </div>
        <hr class="margin-top-2x">
        <div class="row margin-top-2x">
          <div class="col-md-7">
            <div class="display-3 mb-30">Technische Support</div>
          </div>
          <div class="col-md-5">
            <ul class="list-icon" style="color: whitesmoke">
              <li> <i class="icon-mail"></i><a class="navi-link" href="mailto:info@jae-studio.nl" style="color: whitesmoke">JAE-Studio mail</a></li>
                {{--<li> <i class="icon-bell"></i>010 - 8222993</li>--}}
                <li> <i class="icon-clock"></i>2 uur</li>
            </ul>
          </div>
        </div>
      </div>


        <div style="border-bottom: 1px solid ghostwhite"></div>
        <!-- Site Footer-->
        @include('layouts.footer')

        @endsection    </div>
    <!-- Back To Top Button--><a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
    <!-- Backdrop-->
    <div class="site-backdrop"></div>
    <!-- JavaScript (jQuery) libraries, plugins and custom scripts-->
    <script src="js/vendor.min.js"></script>
    <script src="js/scripts.min.js"></script>
  </body>
</html>
