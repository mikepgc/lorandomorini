
@extends('layouts.master2')

@section('content')

    @include('layouts.sidebar')

    @include('layouts.topbar')

    @include ('layouts.navbar')

    <!-- Off-Canvas Wrapper-->
    <div class="offcanvas-wrapper">
      <!-- Page Title-->
      <div class="page-title">
        <div class="container">
          <div class="column">
            <h1>Winkelwagen</h1>
          </div>
          <div class="column">
            <ul class="breadcrumbs">
              <li><a href="/">Home</a>
              </li>
              <li class="separator">&nbsp;</li>
              <li>Winkelwagen</li>
            </ul>
          </div>
        </div>
      </div>
      <!-- Page Content-->
        @if(!\App\Governor::$agent->isMobile())

        <div class="container padding-bottom-3x mb-1">
        <!-- Alert-->
        {{--<div class="alert alert-info alert-dismissible fade show text-center" style="margin-bottom: 30px;"><span class="alert-close" data-dismiss="alert"></span><img class="d-inline align-center" src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeD0iMHB4IiB5PSIwcHgiIHZpZXdCb3g9IjAgMCA1MTIuMDAzIDUxMi4wMDMiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDUxMi4wMDMgNTEyLjAwMzsiIHhtbDpzcGFjZT0icHJlc2VydmUiIHdpZHRoPSIxNnB4IiBoZWlnaHQ9IjE2cHgiPgo8Zz4KCTxnPgoJCTxnPgoJCQk8cGF0aCBkPSJNMjU2LjAwMSw2NGMtNzAuNTkyLDAtMTI4LDU3LjQwOC0xMjgsMTI4czU3LjQwOCwxMjgsMTI4LDEyOHMxMjgtNTcuNDA4LDEyOC0xMjhTMzI2LjU5Myw2NCwyNTYuMDAxLDY0eiAgICAgIE0yNTYuMDAxLDI5OC42NjdjLTU4LjgxNiwwLTEwNi42NjctNDcuODUxLTEwNi42NjctMTA2LjY2N1MxOTcuMTg1LDg1LjMzMywyNTYuMDAxLDg1LjMzM1MzNjIuNjY4LDEzMy4xODQsMzYyLjY2OCwxOTIgICAgIFMzMTQuODE3LDI5OC42NjcsMjU2LjAwMSwyOTguNjY3eiIgZmlsbD0iIzUwYzZlOSIvPgoJCQk8cGF0aCBkPSJNMzg1LjY0NCwzMzMuMjA1YzM4LjIyOS0zNS4xMzYsNjIuMzU3LTg1LjMzMyw2Mi4zNTctMTQxLjIwNWMwLTEwNS44NTYtODYuMTIzLTE5Mi0xOTItMTkycy0xOTIsODYuMTQ0LTE5MiwxOTIgICAgIGMwLDU1Ljg1MSwyNC4xMjgsMTA2LjA2OSw2Mi4zMzYsMTQxLjE4NEw2NC42ODQsNDk3LjZjLTEuNTM2LDQuMTE3LTAuNDA1LDguNzI1LDIuODM3LDExLjY2OSAgICAgYzIuMDI3LDEuNzkyLDQuNTY1LDIuNzMxLDcuMTQ3LDIuNzMxYzEuNjIxLDAsMy4yNDMtMC4zNjMsNC43NzktMS4xMDlsNzkuNzg3LTM5Ljg5M2w1OC44NTksMzkuMjMyICAgICBjMi42ODgsMS43OTIsNi4xMDEsMi4yNCw5LjE5NSwxLjI4YzMuMDkzLTEuMDAzLDUuNTY4LTMuMzQ5LDYuNjk5LTYuNGwyMy4yOTYtNjIuMTQ0bDIwLjU4Nyw2MS43MzkgICAgIGMxLjA2NywzLjE1NywzLjU0MSw1LjYzMiw2LjY3Nyw2LjcyYzMuMTM2LDEuMDY3LDYuNTkyLDAuNjQsOS4zNjUtMS4yMTZsNTguODU5LTM5LjIzMmw3OS43ODcsMzkuODkzICAgICBjMS41MzYsMC43NjgsMy4xNTcsMS4xMzEsNC43NzksMS4xMzFjMi41ODEsMCw1LjEyLTAuOTM5LDcuMTI1LTIuNzUyYzMuMjY0LTIuOTIzLDQuMzczLTcuNTUyLDIuODM3LTExLjY2OUwzODUuNjQ0LDMzMy4yMDV6ICAgICAgTTI0Ni4wMTcsNDEyLjI2N2wtMjcuMjg1LDcyLjc0N2wtNTIuODIxLTM1LjJjLTMuMi0yLjExMi03LjMxNy0yLjM4OS0xMC42ODgtMC42NjFMOTQuMTg4LDQ3OS42OGw0OS41NzktMTMyLjIyNCAgICAgYzI2Ljg1OSwxOS40MzUsNTguNzk1LDMyLjIxMyw5My41NDcsMzUuNjA1TDI0Ni43LDQxMS4yQzI0Ni40ODcsNDExLjU2MywyNDYuMTY3LDQxMS44NCwyNDYuMDE3LDQxMi4yNjd6IE0yNTYuMDAxLDM2Mi42NjcgICAgIEMxNjEuOSwzNjIuNjY3LDg1LjMzNSwyODYuMTAxLDg1LjMzNSwxOTJTMTYxLjksMjEuMzMzLDI1Ni4wMDEsMjEuMzMzUzQyNi42NjgsOTcuODk5LDQyNi42NjgsMTkyICAgICBTMzUwLjEwMywzNjIuNjY3LDI1Ni4wMDEsMzYyLjY2N3ogTTM1Ni43NTksNDQ5LjEzMWMtMy40MTMtMS43MjgtNy41MDktMS40NzItMTAuNjg4LDAuNjYxbC01Mi4zNzMsMzQuOTIzbC0zMy42NDMtMTAwLjkyOCAgICAgYzQwLjM0MS0wLjg1Myw3Ny41ODktMTQuMTg3LDEwOC4xNi0zNi4zMzFsNDkuNTc5LDEzMi4yMDNMMzU2Ljc1OSw0NDkuMTMxeiIgZmlsbD0iIzUwYzZlOSIvPgoJCTwvZz4KCTwvZz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8L3N2Zz4K" width="18" height="18" alt="Medal icon">&nbsp;&nbsp;With this purchase you will earn <strong>290</strong> Reward Points.</div>--}}
            <div class="checkout-steps">

            @if (empty (Session :: get ('cart')))
                        <a>Compleet</a>
                        <a><span class="angle"></span>Afronding</a>
                        <a><span class="angle"></span>Adresgegevens</a>
                        <a class="active" href="shoppingcart"><span class="angle"></span>Mijn Winkelwagen</a>
                     </div>

                @elseif(!Auth::user())
                <a>Compleet</a>
                <a><span class="angle"></span>Afronding</a>
                <a href="checkout-address"><span class="angle"></span>Adresgegevens</a>
                <a class="active" href="shoppingcart"><span class="angle"></span>Mijn Winkelwagen</a>


                     @else
                      <a href="checkout-complete">Compleet</a>
                      <a href="checkout-afronding"><span class="angle"></span>Afronding</a>
                      <a href="checkout-address"><span class="angle"></span>Adresgegevens</a>
                      <a class="active" href="shoppingcart"><span class="angle"></span>Mijn Winkelwagen</a>
                  @endif
        </div>
        @endif


        <!-- Shopping Cart-->
          @if(Session::has('cart'))
        <div class="table-responsive shopping-cart">
            <table class="table">
            <thead>
              <tr>
                  <th>Product naam</th>
                  <th class="text-center">Aantal</th>
                  <th class="text-center ">Subtotaal</th>

                  <th class="text-center"><a class="btn btn-sm btn-outline-danger" href="{{ route('product.destroyCart') }}">Winkelwagen leegmaken</a></th>
              </tr>
            </thead>
            <tbody>
            @foreach ($products as $product)
            <tr id="cartProduct_{{ $product['item'] ['id'] }}">
                <td>
                  <div class="product-item"><a class="product-thumb" href="category/subcategory-grid/single-product/{{$product['item']['artnr']}}"><img src="{{$product['item'] ['image_1']}}" alt="Product"></a>
                    <div class="product-info" style="width: 200px">
                      <h4 class="product-title" ><a style="color:whitesmoke" href="category/subcategory-grid/single-product/{{$product['item']['arntr']}}">{{$product['item'] ['name']}}</a></h4>

{{--                        <span><em>Merk:</em> {{$product['item'] ['brand']}}</span>--}}
                    </div>
                  </div>
                </td>

                      <td class="text-center  text-lg text-medium " style="color: whitesmoke">
                          <a style="text-decoration: blink; color: whitesmoke;font-size: 30px" href="javascript:;" onclick="change_qty(this,'decrese')">
                              -
                          </a>
                        <input type="text" name="qty" value="{{$product['qty']}}" style="width: 40px;height: 40px;display: inline-block; border:0 ; padding: 5px; text-decoration: blink; color: whitesmoke; font-size: 20px; background-color: transparent; text-align: center"/>
                        <input type="hidden" name="product" value="{{ $product['item'] ['id'] }}" />

                          <!--<span class="badge" style="color: whitesmoke; font-size: 20px">{{$product['qty']}}</span>-->
                          <a style="text-decoration: blink; color: whitesmoke; font-size: 30px" href="javascript:;" onclick="change_qty(this,'increase')">
                              +
                          </a>
                      </td>

{{--                <td class="text-center text-lg text-medium">€{{$product['item']['gross_incbtw']}}</td>--}}
                <td class="text-center text-lg text-medium productPrice" style="color:whitesmoke">€{{(round($product['item']['price'] , 2 ) * $product['qty'])}}</td>
                  {{--@if($discount > '0' )--}}

                      {{--<td class="text-center text-lg text-medium">€{{(($product['item']['discountPercentage'] / 100 * $product['item']['price'] )) * $product['qty'] }}</td>--}}
                  {{--@else--}}
                      <td class="text-center text-lg text-medium"></td>
                  {{--@endif               --}}
                  <td class="text-center"><a class="remove-from-cart" href="{{ route('product.remove', ['id'=> $product['item']['id']])}}" data-toggle="tooltip" title="Remove item"><i class="icon-cross"></i></a></td>
              </tr>
              @endforeach

            </tbody>
          </table>
        </div>
        @else
              <div class="container" style="padding: 20px">
              <h1>Winkelwagen is leeg</h1><br>
          <h3>Indien je een account hebt kun je  <a href="/account-login" style="color: whitesmoke">hier</a> aanmelden om artikelen te bekijken die je eerder hebt toegevoegd.</h3>

                  <button class="btn btn-outline-primary btn-sm" type="button" onclick="window.location.href = '/';" style="height: 60px">Verder winkelen</button><br><br>

              </div>

        @endif
        <div class="shopping-cart-footer">

{{--------------------------------------------   Coupon function-------------------------------------------------}}
{{--          <div class="column">--}}
{{--            <form class="coupon-form" method="post">--}}
{{--              <input class="form-control form-control-sm" type="text" placeholder="Coupon code" required>--}}
{{--              <button class="btn btn-outline-primary btn-sm" type="submit">Apply Coupon</button>--}}
{{--            </form>--}}
{{--          </div>--}}

  {{--------------------------------------------  End Coupon function-------------------------------------------------}}
            @if(!empty($totalPrice))
                <div class="column text-lg" id="cartSubtotal" style="color:whitesmoke;">Subtotaal:

                    <span class="text-medium">€{{ round((Session::has('cart')) ? Session::get('cart')->totalPrice : '' , 2) }}</span>
                </div>

            @else
{{--            <span class="text-medium">€ 0</span>--}}
            @endif
        </div>
        <div class="shopping-cart-footer">
          <div class="column"><a class="btn btn-outline-secondary" href="/"><i class="icon-arrow-left"></i>&nbsp;Verder winkelen</a></div>
          <div class="column">
            {{--<a class="btn btn-primary" href="shoppingcart" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Your cart" data-toast-message="is updated successfully!">Update Cart</a>--}}
            @if (Session::has('cart'))
            <a class="btn btn-success" href="/checkout-address">Ga verder met bestellen</a>
            @endif
          </div>
        </div>
        <!-- Related Products Carousel-->
        {{--<h3 class="text-center padding-top-2x mt-2 padding-bottom-1x">You May Also Like</h3>--}}
        <!-- Carousel-->
        {{--<div class="owl-carousel" data-owl-carousel="{ &quot;nav&quot;: false, &quot;dots&quot;: true, &quot;margin&quot;: 30, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:1},&quot;576&quot;:{&quot;items&quot;:2},&quot;768&quot;:{&quot;items&quot;:3},&quot;991&quot;:{&quot;items&quot;:4},&quot;1200&quot;:{&quot;items&quot;:4}} }">--}}
          {{--<!-- Product-->--}}
          {{--<div class="grid-item">--}}
            {{--<div class="product-card">--}}
              {{--<div class="product-badge text-danger">22% Off</div><a class="product-thumb" href="losse templates/shop-single.html"><img src="img/shop/products/09.jpg" alt="Product"></a>--}}
              {{--<h3 class="product-title"><a href="losse templates/shop-single.html">Rocket Dog</a></h3>--}}
              {{--<h4 class="product-price">--}}
                {{--<del>$44.95</del>$34.99--}}
              {{--</h4>--}}
              {{--<div class="product-buttons">--}}
                {{--<button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip" title="Whishlist"><i class="icon-heart"></i></button>--}}
                {{--<button class="btn btn-outline-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Add to Cart</button>--}}
              {{--</div>--}}
            {{--</div>--}}
          {{--</div>--}}
          {{--<!-- Product-->--}}
          {{--<div class="grid-item">--}}
            {{--<div class="product-card">--}}
                {{--<div class="rating-stars"><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star"></i>--}}
                {{--</div><a class="product-thumb" href="losse templates/shop-single.html"><img src="img/shop/products/03.jpg" alt="Product"></a>--}}
              {{--<h3 class="product-title"><a href="losse templates/shop-single.html">Oakley Kickback</a></h3>--}}
              {{--<h4 class="product-price">$155.00</h4>--}}
              {{--<div class="product-buttons">--}}
                {{--<button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip" title="Whishlist"><i class="icon-heart"></i></button>--}}
                {{--<button class="btn btn-outline-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Add to Cart</button>--}}
              {{--</div>--}}
            {{--</div>--}}
          {{--</div>--}}
          {{--<!-- Product-->--}}
          {{--<div class="grid-item">--}}
            {{--<div class="product-card"><a class="product-thumb" href="losse templates/shop-single.html"><img src="img/shop/products/12.jpg" alt="Product"></a>--}}
              {{--<h3 class="product-title"><a href="losse templates/shop-single.html">Vented Straw Fedora</a></h3>--}}
              {{--<h4 class="product-price">$49.50</h4>--}}
              {{--<div class="product-buttons">--}}
                {{--<button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip" title="Whishlist"><i class="icon-heart"></i></button>--}}
                {{--<button class="btn btn-outline-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Add to Cart</button>--}}
              {{--</div>--}}
            {{--</div>--}}
          {{--</div>--}}
          {{--<!-- Product-->--}}
          {{--<div class="grid-item">--}}
            {{--<div class="product-card">--}}
                {{--<div class="rating-stars"><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star filled"></i>--}}
                {{--</div><a class="product-thumb" href="losse templates/shop-single.html"><img src="img/shop/products/11.jpg" alt="Product"></a>--}}
              {{--<h3 class="product-title"><a href="losse templates/shop-single.html">Top-Sider Fathom</a></h3>--}}
              {{--<h4 class="product-price">$90.00</h4>--}}
              {{--<div class="product-buttons">--}}
                {{--<button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip" title="Whishlist"><i class="icon-heart"></i></button>--}}
                {{--<button class="btn btn-outline-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Add to Cart</button>--}}
              {{--</div>--}}
            {{--</div>--}}
          {{--</div>--}}
          {{--<!-- Product-->--}}
          {{--<div class="grid-item">--}}
            {{--<div class="product-card"><a class="product-thumb" href="losse templates/shop-single.html"><img src="img/shop/products/04.jpg" alt="Product"></a>--}}
              {{--<h3 class="product-title"><a href="losse templates/shop-single.html">Waist Leather Belt</a></h3>--}}
              {{--<h4 class="product-price">$47.00</h4>--}}
              {{--<div class="product-buttons">--}}
                {{--<button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip" title="Whishlist"><i class="icon-heart"></i></button>--}}
                {{--<button class="btn btn-outline-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Add to Cart</button>--}}
              {{--</div>--}}
            {{--</div>--}}
          {{--</div>--}}
          {{--<!-- Product-->--}}
          {{--<div class="grid-item">--}}
            {{--<div class="product-card">--}}
              {{--<div class="product-badge text-danger">50% Off</div><a class="product-thumb" href="losse templates/shop-single.html"><img src="img/shop/products/01.jpg" alt="Product"></a>--}}
              {{--<h3 class="product-title"><a href="losse templates/shop-single.html">Unionbay Park</a></h3>--}}
              {{--<h4 class="product-price">--}}
                {{--<del>$99.99</del>$49.99--}}
              {{--</h4>--}}
              {{--<div class="product-buttons">--}}
                {{--<button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip" title="Whishlist"><i class="icon-heart"></i></button>--}}
                {{--<button class="btn btn-outline-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Add to Cart</button>--}}
              {{--</div>--}}
            {{--</div>--}}
          {{--</div>--}}
        {{--</div>--}}
      <!-- Site Footer-->
    </div>

        @include('layouts.footer')

        @endsection
    <!-- Back To Top Button-->
<a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
    <!-- Backdrop-->
    <div class="site-backdrop"></div>
    <!-- JavaScript (jQuery) libraries, plugins and custom scripts-->
    <script src="js/vendor.min.js"></script>
    <script src="js/scripts.min.js"></script>
@section('js')
<script type="text/javascript">
    $(document).ready(function(){
        if($('input[name="qty"]').length > 0){
            $('input[name="qty"]').on('change',function(){
                var qty = $(this).val();
                if(qty < 0){
                    alert('Please enter proper quantity');
                    location.reload();
                    return false;
                }else{
                    var product = $(this).siblings('input[name="product"]').val();
                    changeQty(product,qty);
                }

            });
        }
    });
    function changeQty(product,qty){
        var url = "{{ route('product.changeProductQuantity') }}";
        $('#loader').fadeIn(100);
        $.ajax({
           type: "POST",
           url: url,
           data: {
               product:product,
               qty:qty,
               _token: "{{ csrf_token() }}",
            },
           success: function(data)
           {
                $('#cart-toolbar').html(data.response.cartData); // show response from the php script.
                $('#cartProduct_'+product).children('td.productPrice').html('€'+data.cartData.price);
                $('#cartSubtotal .text-medium').html('€'+data.cartSubtotal);
                $('#loader').fadeOut(100);
           }
        });

    }
    function change_qty(obj,type){
        var target_ele = $(obj).siblings('input[name="qty"]');
        var product = $(obj).siblings('input[name="product"]').val();
        var current_qty = target_ele.val();
        if(type == 'increase'){
            current_qty++;
            target_ele.val(current_qty);
            changeQty(product,current_qty)
        }else{
            if(current_qty == 1){
                return ;
            }else{
                current_qty--;
                target_ele.val(current_qty);
                changeQty(product,current_qty)
            }
        }

    }
</script>
@endsection
