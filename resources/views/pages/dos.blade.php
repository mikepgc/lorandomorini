<!-- Web Application Manifest -->
{{--<link rel="manifest" href="/manifest.json">--}}
<!-- Chrome for Android theme color -->
<meta name="theme-color" content="black">

<!-- Add to homescreen for Chrome on Android -->
<meta name="mobile-web-app-capable" content="yes">
<meta name="application-name" content="Dos">
<link rel="icon" sizes="512x512" href="/img/logo/logo-white3.png">
{{--<link rel="manifest" href="/manifest.json">--}}

<!-- Add to homescreen for Safari on iOS -->
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="apple-mobile-web-app-title" content="Dos">
<link rel="apple-touch-icon" href="/img/logo/logomobile.png">
{{--<link rel="manifest" href="/manifest.json">--}}
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">
<link href="/img/icons/splash-640x1136.png"
      media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)"
      rel="apple-touch-startup-image"/>
<link href="/img/icons/splashdos-750x1334.png"
      media="(device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2)"
      rel="apple-touch-startup-image"/>
<link href="/img/icons/splash-1242x2208.png"
      media="(device-width: 621px) and (device-height: 1104px) and (-webkit-device-pixel-ratio: 3)"
      rel="apple-touch-startup-image"/>
<link href="/img/icons/splashdos-1125x2436.png"
      media="(device-width: 375px) and (device-height: 812px) and (-webkit-device-pixel-ratio: 3)"
      rel="apple-touch-startup-image"/>
<link href="/img/icons/splash-828x1792.png"
      media="(device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 2)"
      rel="apple-touch-startup-image"/>
<link href="/img/icons/splash-1242x2688.png"
      media="(device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 3)"
      rel="apple-touch-startup-image"/>
<link href="/img/icons/splash-1536x2048.png"
      media="(device-width: 768px) and (device-height: 1024px) and (-webkit-device-pixel-ratio: 2)"
      rel="apple-touch-startup-image"/>
<link href="/img/icons/splash-1668x2224.png"
      media="(device-width: 834px) and (device-height: 1112px) and (-webkit-device-pixel-ratio: 2)"
      rel="apple-touch-startup-image"/>
<link href="/img/icons/splash-1668x2388.png"
      media="(device-width: 834px) and (device-height: 1194px) and (-webkit-device-pixel-ratio: 2)"
      rel="apple-touch-startup-image"/>
<link href="/img/icons/splash-2048x2732.png"
      media="(device-width: 1024px) and (device-height: 1366px) and (-webkit-device-pixel-ratio: 2)"
      rel="apple-touch-startup-image"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">
<script
    src="https://code.jquery.com/jquery-3.4.1.min.js"
    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
    crossorigin="anonymous"
></script>
<link
    href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
    rel="stylesheet"
    type="text/css"
/>

<link href="/css/bottombar.css" rel="stylesheet">

@extends('layouts.master2')

@section('content')

{{--    @include('layouts.sidebar')--}}

{{--    @include('layouts.topbar')--}}

{{--    @include ('layouts.navbar')--}}


{{--    <h1>PDF generator</h1>--}}
{{--<form onsubmit="window.location = 'pdf/' + search.value; return false;">--}}

{{--    <input id="search" placeholder="Voer ordernummer na JPS-" type="search" name="search" style="width: 300px" value="LM-">--}}
{{--    <input type="submit"  value="Maak PDF">--}}
{{--</form>--}}

{{--    <H1>Orderhistorie</H1>--}}
    @if (session('alert'))
        <div class="alert alert-success">
            {{ session('alert') }}
        </div>
    @endif
<div id="lorando" class="tabcontent">
    @include('templates.lorando')

</div>
<div id="users" class="tabcontent">
    @include('templates.users')

</div>

<div id="caffeitalia" class="tabcontent" style="margin-bottom: 100px;">

    @include('templates.caffeitalia')
{{--    <iframe src="https://caffeitalia.nl/controlpanel" title="reserveringen" height="100%"></iframe>--}}


</div>

@include('layouts.bottombar')

<!-- The Modal -->
<div id="myModal" class="modal">

    <!-- Modal content -->
    <div class="modal-content">
        <span class="close">&times;</span>
        <p style="color: black">Factuur is succesvol verstuurd..<br> pagina wordt ververst.. geduld..</p>
    </div>
</div>
    <style>
        body {font-family: Arial, Helvetica, sans-serif;}

        /* The Modal (background) */
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        /* Modal Content */
        .modal-content {
            background-color: #fefefe;
            margin: auto;
            padding: 20px;
            border: 1px solid #888;
            width: 80%;
        }

        /* The Close Button */
        .close {
            color: #aaaaaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        .close:hover,
        .close:focus {
            color: #000;
            text-decoration: none;
            cursor: pointer;
        }

    .button {
        background-color: #008cba; /* Green */
        border: none;
        color: white;
        padding: 15px 32px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 4px 2px;
        cursor: pointer;
    }
    .button:disabled {
        opacity: 0.5;
    }
    .hide {
        display: none;
    }
</style>

<script>
    // Get the modal
    var modal = document.getElementById("myModal");

    // Get the button that opens the modal
    var btn = document.getElementById("myBtn");

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks the button, open the modal
    btn.onclick = function() {
        modal.style.display = "block";
    }

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
</script>
    <script>

        $(document).ready(function () {
            $("#button").submit(function () {
                $(".myButton").attr("disabled", true);
                return true;
            });
        });

        $("#searchOrdernr").on('submit', function(){

            window.open($(this).attr("src") + "/" + $("#ordernr").val(), "_self");

            return false;
        });


    </script>

{{--Pull to refresh--}}
<script src="/js/pulltorefresh.js" type="text/javascript"></script>
<script src="//unpkg.com/hammer-touchemulator@0.0.2/touch-emulator.js"></script>
<script>TouchEmulator()</script>
<script type='text/javascript'>

    PullToRefresh.init({
        mainElement: 'body',
        onRefresh: function(){ window.location.reload(); }
    });

</script>
{{--End Pull to refresh--}}
    <script>
        function checkForm(form)
        {

            form.submit.disabled = true;
            form.submit2.disabled = true;
            return true;
        }

    </script>
