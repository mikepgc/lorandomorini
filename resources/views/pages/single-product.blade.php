
@extends('layouts.master2')

@section('content')

    @include('layouts.sidebar')

    @include('layouts.topbar')

    @include ('layouts.navbar')


    {{---------------    Start Facebook productfeed Pixel-----------------}}

    <div itemscope itemtype="http://schema.org/Product">
        <meta itemprop="brand" content="Lorando & Morini">
        <meta itemprop="name" content="{{$productId->title}}">
        <meta itemprop="description" content="{{$productId->description}}">
        <meta itemprop="productID" content="{{$productId->artnr}}">
        <meta itemprop="url" content="{{url()->current()}}">
        <meta itemprop="image" content="{{$productId->image_1}}">
        <div itemprop="value" itemscope itemtype="http://schema.org/PropertyValue">
            <span itemprop="propertyID" content="1"></span>
            <meta itemprop="value" content="Coffee"></meta>
        </div>
        <div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
            <link itemprop="availability" href="100">
            <link itemprop="itemCondition" href="new">
            <meta itemprop="price" content="{{$productId->price}}">
            <meta itemprop="priceCurrency" content="EUR">
        </div>
    </div>


    {{--------------   End Facebook productfeed Pixel-----------------}}

    <!-- Off-Canvas Wrapper-->
<div class="offcanvas-wrapper">
    <!-- Page Title-->
    <div class="page-title">
        <div class="container">
            <div class="column">
                <h1>{{$productId->title}}</h1>
            </div>
            <div class="column">
                <ul class="breadcrumbs">
                    <li><a href="/">Home</a>
                    </li>
                     <li class="separator">&nbsp;</li>
                    <li><a href="/category/subcategory-grid/{{$productId->maincat}}">{{$productId->maincat_url}}</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- Page Content-->
    <div class="container padding-bottom-3x mb-1">
        <div class="row">
            <!-- Poduct Gallery-->
            <div class="col-md-6">
                <div  style="padding-right: 0; padding-left: 0" class="product-gallery">
                    {{--<span class="product-badge text-danger">30% Off</span>--}}
{{--                    <div class="gallery-wrapper">--}}
{{--                        <div class="gallery-item  active"><a href="{{$productId->image_1}}" data-hash="one" data-size="1000x667" ></a></div>--}}
{{--                        @if($productId->npictures >= 2)--}}
{{--                        <div class="gallery-item"><a href="{{$productId->image_2}}" data-hash="two" data-size="1000x667"></a></div>--}}
{{--                        @endif--}}
{{--                        @if($productId->npictures >= 3)--}}
{{--                        <div class="gallery-item"><a href="{{$productId->image_3}}" data-hash="three" data-size="1000x667"></a></div>--}}
{{--                        @endif--}}
{{--                        @if($productId->npictures >= 4)--}}
{{--                        <div class="gallery-item"><a href="{{$productId->image_4}}" data-hash="four" data-size="1000x667"></a></div>--}}
{{--                        @endif--}}
{{--                        @if($productId->npictures >= 5)--}}
{{--                        <div class="gallery-item"><a href="{{$productId->image_5}}" data-hash="five" data-size="1000x667"></a></div>--}}
{{--                        @endif--}}



{{--                    </div>--}}
                    <div style="border-bottom: 1px solid white" class="product-carousel owl-carousel">


                        <div data-hash="one"><img src="{{$productId->image_1}}" class="center" alt="Product" style="height: auto; width: 400px">
                        </div>
                        @if($productId->npictures >= 2)
                            <div data-hash="two"><img src="{{$productId->image_2}}" class="center"  alt="Product"></div>
                        @endif
                        @if($productId->npictures >= 3)
                            <div data-hash="three"><img src="{{$productId->image_3}}" class="center"  alt="Product"></div>
                        @endif
                        @if($productId->npictures >= 4)
                            <div data-hash="four"><img src="{{$productId->image_4}}" class="center" alt="Product"></div>
                        @endif
                        @if($productId->npictures >= 5)
                            <div data-hash="five"><img src="{{$productId->image_5}}" class="center"  alt="Product"></div>
                        @endif


                    </div>
                    <ul class="product-thumbnails">
                            <li class="active"><a href="#one"><img src="{{$productId->image_1}}" alt="Product"></a>
                        @if($productId->npictures >= 2)
                            <li><a href="#two"><img src="{{$productId->image_2}}" alt="Product"></a>
                        @endif
                        @if($productId->npictures >= 3)
                        <li><a href="#three"><img src="{{$productId->image_3}}" alt="Product"></a>
                        @endif
                        @if($productId->npictures >= 4)
                            <li><a href="#four"><img src="{{$productId->image_4}}" alt="Product"></a>
                        @endif
                        @if($productId->npictures >= 5)
                            <li><a href="#five"><img src="{{$productId->image_5}}" alt="Product"></a>
                        @endif


                    </ul>
                </div>
            </div>
            <!-- Product Info-->
            <div class="col-md-6">
                <div class="padding-top-2x mt-2 hidden-md-up"></div>
{{--                <div class="rating-stars"><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star"></i>--}}
{{--                </div>--}}
{{--                <span class="text-muted align-middle">&nbsp;&nbsp;4.2 | 3 customer reviews</span>--}}
                <h2 class="padding-top-1x text-normal" style="color: whitesmoke">{{$productId->name}} </h2>
                <span class="h2 d-block" style="color: whitesmoke">
                    €{{$productId->price}} (incl btw)

                </span>
                <br>
                @if(!empty($relatedProduct))
                <?php
                $relatedProductStock = explode('-',$relatedProduct->name);
                $relatedProductName = $relatedProductStock[1]??'';
                $currentProductStock = explode('-',$productId->name);
                $currentProductName = $currentProductStock[1]??'';
                if(\Request::segment(2) == 'subcategory-grid'){
                    $url = route('pages.single-product-grid',['page'=>$relatedProduct->artnr]);
                }else{
                    $url = route('pages.single-product-list',['page'=>$relatedProduct->artnr]);
                }
                ?>
                @if($relatedProductName != '')
                        <a href="javascript:;" style="color: #fff;text-decoration: none;" > <button style="color: whitesmoke" class="btn btn-secondary" disabled>{{ $currentProductName }}</button> </a>

                        <a href="{{ $url }}" style="color: #fff;text-decoration: none;" > <button  class="btn btn-primary" >{{ $relatedProductName }}</button> </a>
                @endif
                @endif
                {{--<div class="col-sm-5">--}}
                        {{--<div class="form-group">--}}
                            {{--<label for="color">Choose color</label>--}}
                            {{--<select class="form-control" id="color">--}}
                                {{--<option>White/Red/Blue</option>--}}
                                {{--<option>Black/Orange/Green</option>--}}
                                {{--<option>Gray/Purple/White</option>--}}
                            {{--</select>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-sm-3">--}}
                        {{--<div class="form-group">--}}
                            {{--<label for="quantity">Quantity</label>--}}
                            {{--<select class="form-control" id="quantity">--}}
                                {{--<option>1</option>--}}
                                {{--<option>2</option>--}}
                                {{--<option>3</option>--}}
                                {{--<option>4</option>--}}
                                {{--<option>5</option>--}}
                            {{--</select>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}



                <div class="pt-1 mb-2"  style="color: whitesmoke"><span class="text-medium"  style="color: whitesmoke">Artikelnummer:</span> {{$productId->artnr}}</div>
{{--                <div class="pt-1 mb-2"><span class="text-medium">Merk:</span> {{$productId->brand}}</div>--}}


                @if($productId->subcat)
                <div class="mb-2"><span class="text-medium"  style="color: whitesmoke">Categorie:&nbsp;</span><a class="navi-link" href="#"  style="color: whitesmoke">{{$productId->maincat_url}}</a></div>
                @endif
                {{--@if($productId->size)--}}
                {{--<div class="mb-2"><span class="text-medium">Maat:&nbsp;</span><a class="navi-link" href="#">{{$productId->size}}</a></div>--}}
                {{--@endif--}}


                <br>
                @if($productId->stock >= 1 && $productId->stock <= 5)
                    @if(env('TEST_ENV'))
                    <div style="color: green; font-weight: bold; font-size: 20px" title="beperkt op voorraad {{$productId->stock}}">
                        <i class="fa fa-square" style="font-size:20px;color:green"></i>
                        <i class="fa fa-square" style="font-size:20px;color:grey"></i>
                        <i class="fa fa-square" style="font-size:20px;color:grey"></i>

                        In Voorraad</div>
                    @else

                    <div style="color: green; font-weight: bold; font-size: 20px" title="beperkt op voorraad">
                        <i class="fa fa-square" style="font-size:20px;color:green"></i>
                        <i class="fa fa-square" style="font-size:20px;color:grey"></i>
                        <i class="fa fa-square" style="font-size:20px;color:grey"></i>

                        Op voorraad</div>
                    @endif
                @elseif($productId->stock < 1)
                    @if(env('TEST_ENV'))
                    <div style="color: red;  font-weight: bold; font-size: 20px" title="{{$productId->stock}}">
                        <i class="fa fa-square" style="font-size:20px;color:red"></i>
                        <i class="fa fa-square" style="font-size:20px;color:red"></i>Geen Voorraad</div>
                    @elseif($productId->stock < 1)
                    <div style="color: red;  font-weight: bold; font-size: 20px" >Geen Voorraad</div>
                    @endif

                @endif

                @if($productId->stock >= 6  && $productId->stock <= 10)
                    @if(env('TEST_ENV'))
                        <div style="color: green; font-weight: bold; font-size: 20px" title="voldoende op voorraad {{$productId->stock}}">
                            <i class="fa fa-square" style="font-size:20px;color:green"></i>
                            <i class="fa fa-square" style="font-size:20px;color:green"></i>
                            <i class="fa fa-square" style="font-size:20px;color:grey"></i>

                            Op voorraad</div>
                    @else

                        <div style="color: green; font-weight: bold; font-size: 20px" title="voldoende op voorraad">
                            <i class="fa fa-square" style="font-size:20px;color:green"></i>
                            <i class="fa fa-square" style="font-size:20px;color:green"></i>
                            <i class="fa fa-square" style="font-size:20px;color:grey"></i>

                            Op voorraad</div>
                    @endif


                @endif


                @if($productId->stock >= 10)
                    @if(env('TEST_ENV'))
                        <div style="color: green; font-weight: bold; font-size: 20px" title="ruim op voorraad {{$productId->stock}}">
                            <i class="fa fa-square" style="font-size:20px;color:green"></i>
                            <i class="fa fa-square" style="font-size:20px;color:green"></i>
                            <i class="fa fa-square" style="font-size:20px;color:green"></i>

                            Op voorraad</div>
                    @else


                    <div style="color: green; font-weight: bold; font-size: 20px" title="ruim op voorraad">
                            <i class="fa fa-square" style="font-size:20px;color:green"></i>
                            <i class="fa fa-square" style="font-size:20px;color:green"></i>
                            <i class="fa fa-square" style="font-size:20px;color:green"></i>

                            Op voorraad</div>
                    @endif


                @endif

                <br>

                @if(env('TEST_ENV'))

                <div class="padding-bottom-1x mb-2"><span class="text-medium">Voorraad :&nbsp;</span>{{$productId->stock}} stuks</div>
                @endif
                <form action="javascript:;" id="addToCartForm_{{ $productId->id }}">
                    <div class="change_quantity" >
                        <a style="text-decoration: blink; color: whitesmoke;font-size: 35px" onclick="change_qty(this,'decrese')" href="javascript:;">
                            -
                        </a>
                        <input type="text" name="qty" value="1" style="width: 40px;height: 20px;display: inline-block; border:0 ; padding: 5px; text-decoration: blink; color: whitesmoke; font-size: 25px; background-color: transparent; text-align: center"/>
                        <input type="hidden" name="product" value="{{ $productId->id }}" />
                        {{ csrf_field() }}
                        <a style="text-decoration: blink; color: whitesmoke; font-size: 35px" onclick="change_qty(this,'increase')" href="javascript:;">
                            +
                        </a>
                    </div>

                    <a class="page-sroll" href="#productdescription" style="color: #fff"><button class="btn btn-primary" >Meer over product</button></a>
                    <a href="javascript:;" style="color: #fff" onclick="addToCart(this)"> <button class="btn btn-primary"   data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Product" data-toast-message="{{$productId->name}} toegevoegd in winkelwagen!"><i class="icon-bag"></i> Voeg aan winkelwagen </button> </a>
                </form>

             <span style="color: whitesmoke; padding-right: 20px"> Deel dit product:  </span>
                <a class="social-button shape-circle sb-facebook" href="https://www.facebook.com/sharer/sharer.php?u={{url()->current()}}" target="_blank" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="socicon-facebook"></i></a>
                                            <a class="social-button shape-circle sb-twitter" href="https://twitter.com/intent/tweet?text=LorandoMorini&url={{url()->current()}}" target="_blank"data-toggle="tooltip" data-placement="top" title="Twitter"><i class="socicon-twitter"></i></a>

                <br><br>
                <hr class="mb-3">
{{--                <div class="d-flex flex-wrap justify-content-between">--}}
{{--                    <div class="entry-share mt-2 mb-2"><span class="text-muted">Delen:</span>--}}
{{--                        <div class="share-links">--}}
{{--                            <a class="social-button shape-circle sb-facebook" href="" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="socicon-facebook"></i></a>--}}
{{--                            <a class="social-button shape-circle sb-twitter" href="#" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="socicon-twitter"></i></a>--}}
{{--                            <a class="social-button shape-circle sb-instagram" href="#" data-toggle="tooltip" data-placement="top" title="Instagram"><i class="socicon-instagram"></i></a><a class="social-button shape-circle sb-google-plus" href="#" data-toggle="tooltip" data-placement="top" title="Google +"><i class="socicon-googleplus"></i></a></div>--}}
{{--                    </div>--}}
{{--                    <div class="sp-buttons mt-2 mb-2">--}}
{{--                        <button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip" title="Whishlist"><i class="icon-heart"></i></button>--}}

{{--                        <br><br><br><br><br><br><a href="{{ url()->previous() }}" style="color: #fff"><button class="btn btn-primary" > Ga terug</button></a>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>
        </div>
        <!-- Product Tabs-->
        <div class="row padding-top-3x mb-3"  id="productdescription" style="margin-top: 50px">
            <div class="col-lg-10 offset-lg-1" style="margin-left: 0">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item"><a class="nav-link active" href="#description" data-toggle="tab" role="tab">Product omschrijving</a></li>
{{--                    <li class="nav-item"><a class="nav-link" href="#reviews" data-toggle="tab" role="tab">Reviews (3)</a></li>--}}
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="description" role="tabpanel" style="color:whitesmoke; font-size: 16px">
                        {!!html_entity_decode($productId->description)!!}
                        <br><br>
                        <a href="{{ route('product.addToCart', ['id' => $productId->id]) }}" style="color: #fff">  <button class="btn btn-primary" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Product" data-toast-message="{{$productId->description}} toegevoegd in winkelwagen!"><i class="icon-bag"></i>Voeg aan winkelwagen</button></a>  <br>

                    </div>
{{--                    <div class="tab-pane fade" id="reviews" role="tabpanel">--}}
{{--                        <!-- Review-->--}}
{{--                        <div class="comment">--}}
{{--                            <div class="comment-author-ava"><img src="img/reviews/01.jpg" alt="Review author"></div>--}}
{{--                            <div class="comment-body">--}}
{{--                                <div class="comment-header d-flex flex-wrap justify-content-between">--}}
{{--                                    <h4 class="comment-title">Average quality for the price</h4>--}}
{{--                                    <div class="mb-2">--}}
{{--                                        <div class="rating-stars"><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star"></i><i class="icon-star"></i>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <p class="comment-text">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</p>--}}
{{--                                <div class="comment-footer"><span class="comment-meta">Francis Burton</span></div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <!-- Review-->--}}
{{--                        <div class="comment">--}}
{{--                            <div class="comment-author-ava"><img src="img/reviews/02.jpg" alt="Review author"></div>--}}
{{--                            <div class="comment-body">--}}
{{--                                <div class="comment-header d-flex flex-wrap justify-content-between">--}}
{{--                                    <h4 class="comment-title">My husband love his new...</h4>--}}
{{--                                    <div class="mb-2">--}}
{{--                                        <div class="rating-stars"><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star filled"></i>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <p class="comment-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>--}}
{{--                                <div class="comment-footer"><span class="comment-meta">Maggie Scott</span></div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <!-- Review-->--}}
{{--                        --}}{{--<div class="comment">--}}
{{--                            --}}{{--<div class="comment-author-ava"><img src="img/reviews/03.jpg" alt="Review author"></div>--}}
{{--                            --}}{{--<div class="comment-body">--}}
{{--                                --}}{{--<div class="comment-header d-flex flex-wrap justify-content-between">--}}
{{--                                    --}}{{--<h4 class="comment-title">Soft, comfortable, quite durable...</h4>--}}
{{--                                    --}}{{--<div class="mb-2">--}}
{{--                                        --}}{{--<div class="rating-stars"><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star"></i>--}}
{{--                                        --}}{{--</div>--}}
{{--                                    --}}{{--</div>--}}
{{--                                --}}{{--</div>--}}
{{--                                --}}{{--<p class="comment-text">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>--}}
{{--                                --}}{{--<div class="comment-footer"><span class="comment-meta">Jacob Hammond</span></div>--}}
{{--                            --}}{{--</div>--}}
{{--                        --}}{{--</div>--}}
{{--                        <!-- Review Form-->--}}
{{--                        --}}{{--<h5 class="mb-30 padding-top-1x">Leave Review</h5>--}}
{{--                        --}}{{--<form class="row" method="post">--}}
{{--                            --}}{{--<div class="col-sm-6">--}}
{{--                                --}}{{--<div class="form-group">--}}
{{--                                    --}}{{--<label for="review_name">Your Name</label>--}}
{{--                                    --}}{{--<input class="form-control form-control-rounded" type="text" id="review_name" required>--}}
{{--                                --}}{{--</div>--}}
{{--                            --}}{{--</div>--}}
{{--                            --}}{{--<div class="col-sm-6">--}}
{{--                                --}}{{--<div class="form-group">--}}
{{--                                    --}}{{--<label for="review_email">Your Email</label>--}}
{{--                                    --}}{{--<input class="form-control form-control-rounded" type="email" id="review_email" required>--}}
{{--                                --}}{{--</div>--}}
{{--                            --}}{{--</div>--}}
{{--                            --}}{{--<div class="col-sm-6">--}}
{{--                                --}}{{--<div class="form-group">--}}
{{--                                    --}}{{--<label for="review_subject">Subject</label>--}}
{{--                                    --}}{{--<input class="form-control form-control-rounded" type="text" id="review_subject" required>--}}
{{--                                --}}{{--</div>--}}
{{--                            --}}{{--</div>--}}
{{--                            --}}{{--<div class="col-sm-6">--}}
{{--                                --}}{{--<div class="form-group">--}}
{{--                                    --}}{{--<label for="review_rating">Rating</label>--}}
{{--                                    --}}{{--<select class="form-control form-control-rounded" id="review_rating">--}}
{{--                                        --}}{{--<option>5 Stars</option>--}}
{{--                                        --}}{{--<option>4 Stars</option>--}}
{{--                                        --}}{{--<option>3 Stars</option>--}}
{{--                                        --}}{{--<option>2 Stars</option>--}}
{{--                                        --}}{{--<option>1 Star</option>--}}
{{--                                    --}}{{--</select>--}}
{{--                                --}}{{--</div>--}}
{{--                            --}}{{--</div>--}}
{{--                            --}}{{--<div class="col-12">--}}
{{--                                --}}{{--<div class="form-group">--}}
{{--                                    --}}{{--<label for="review_text">Review </label>--}}
{{--                                    --}}{{--<textarea class="form-control form-control-rounded" id="review_text" rows="8" required></textarea>--}}
{{--                                --}}{{--</div>--}}
{{--                            --}}{{--</div>--}}
{{--                            --}}{{--<div class="col-12 text-right">--}}
{{--                                --}}{{--<button class="btn btn-outline-primary" type="submit">Submit Review</button>--}}
{{--                            --}}{{--</div>--}}
{{--                        --}}{{--</form>--}}
{{--                    </div>--}}
                </div>
            </div>
        </div>
        <!-- Related Products Carousel-->
        {{--<h3 class="text-center padding-top-2x mt-2 padding-bottom-1x">You May Also Like</h3>--}}
        {{--<!-- Carousel-->--}}
        {{--<div class="owl-carousel" data-owl-carousel="{ &quot;nav&quot;: false, &quot;dots&quot;: true, &quot;margin&quot;: 30, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:1},&quot;576&quot;:{&quot;items&quot;:2},&quot;768&quot;:{&quot;items&quot;:3},&quot;991&quot;:{&quot;items&quot;:4},&quot;1200&quot;:{&quot;items&quot;:4}} }">--}}
            {{--<!-- Product-->--}}
            {{--<div class="grid-item">--}}
                {{--<div class="product-card">--}}
                    {{--<div class="product-badge text-danger">22% Off</div><a class="product-thumb" href="shop-single.html"><img src="img/shop/products/09.jpg" alt="Product"></a>--}}
                    {{--<h3 class="product-title"><a href="shop-single.html">Rocket Dog</a></h3>--}}
                    {{--<h4 class="product-price">--}}
                        {{--<del>$44.95</del>$34.99--}}
                    {{--</h4>--}}
                    {{--<div class="product-buttons">--}}
                        {{--<button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip" title="Whishlist"><i class="icon-heart"></i></button>--}}
                        {{--<button class="btn btn-outline-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Add to Cart</button>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<!-- Product-->--}}
            {{--<div class="grid-item">--}}
                {{--<div class="product-card">--}}
                    {{--<div class="rating-stars"><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star"></i>--}}
                    {{--</div><a class="product-thumb" href="shop-single.html"><img src="img/shop/products/03.jpg" alt="Product"></a>--}}
                    {{--<h3 class="product-title"><a href="shop-single.html">Oakley Kickback</a></h3>--}}
                    {{--<h4 class="product-price">$155.00</h4>--}}
                    {{--<div class="product-buttons">--}}
                        {{--<button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip" title="Whishlist"><i class="icon-heart"></i></button>--}}
                        {{--<button class="btn btn-outline-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Add to Cart</button>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<!-- Product-->--}}
            {{--<div class="grid-item">--}}
                {{--<div class="product-card"><a class="product-thumb" href="shop-single.html"><img src="img/shop/products/12.jpg" alt="Product"></a>--}}
                    {{--<h3 class="product-title"><a href="shop-single.html">Vented Straw Fedora</a></h3>--}}
                    {{--<h4 class="product-price">$49.50</h4>--}}
                    {{--<div class="product-buttons">--}}
                        {{--<button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip" title="Whishlist"><i class="icon-heart"></i></button>--}}
                        {{--<button class="btn btn-outline-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Add to Cart</button>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<!-- Product-->--}}
            {{--<div class="grid-item">--}}
                {{--<div class="product-card">--}}
                    {{--<div class="rating-stars"><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star filled"></i><i class="icon-star filled"></i>--}}
                    {{--</div><a class="product-thumb" href="shop-single.html"><img src="img/shop/products/11.jpg" alt="Product"></a>--}}
                    {{--<h3 class="product-title"><a href="shop-single.html">Top-Sider Fathom</a></h3>--}}
                    {{--<h4 class="product-price">$90.00</h4>--}}
                    {{--<div class="product-buttons">--}}
                        {{--<button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip" title="Whishlist"><i class="icon-heart"></i></button>--}}
                        {{--<button class="btn btn-outline-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Add to Cart</button>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<!-- Product-->--}}
            {{--<div class="grid-item">--}}
                {{--<div class="product-card"><a class="product-thumb" href="shop-single.html"><img src="img/shop/products/04.jpg" alt="Product"></a>--}}
                    {{--<h3 class="product-title"><a href="shop-single.html">Waist Leather Belt</a></h3>--}}
                    {{--<h4 class="product-price">$47.00</h4>--}}
                    {{--<div class="product-buttons">--}}
                        {{--<button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip" title="Whishlist"><i class="icon-heart"></i></button>--}}
                        {{--<button class="btn btn-outline-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Add to Cart</button>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<!-- Product-->--}}
            {{--<div class="grid-item">--}}
                {{--<div class="product-card">--}}
                    {{--<div class="product-badge text-danger">50% Off</div><a class="product-thumb" href="shop-single.html"><img src="img/shop/products/01.jpg" alt="Product"></a>--}}
                    {{--<h3 class="product-title"><a href="shop-single.html">Unionbay Park</a></h3>--}}
                    {{--<h4 class="product-price">--}}
                        {{--<del>$99.99</del>$49.99--}}
                    {{--</h4>--}}
                    {{--<div class="product-buttons">--}}
                        {{--<button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip" title="Whishlist"><i class="icon-heart"></i></button>--}}
                        {{--<button class="btn btn-outline-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Product" data-toast-message="successfuly added to cart!">Add to Cart</button>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    </div>

    <!-- Site Footer-->
    @include('layouts.footer')

    <script>
        $(function(){
            // bind change event to select
            $('#dynamic_select').bind('change', function () {
                var url = $(this).val(); // get selected value
                if (url) { // require a URL
                    window.location = url; // redirect
                }
                return false;
            });
        });
    </script>

    @endsection</div>
<!-- Photoswipe container-->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="pswp__bg"></div>
    <div class="pswp__scroll-wrap">
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
            <div class="pswp__top-bar">
                <div class="pswp__counter"></div>
                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
                <button class="pswp__button pswp__button--share" title="Share"></button>
                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                        <div class="pswp__preloader__cut">
                            <div class="pswp__preloader__donut"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div>
            </div>
            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>
        </div>
    </div>
</div>
<!-- Back To Top Button--><a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
<!-- Backdrop-->
<div class="site-backdrop"></div>


<!-- JavaScript (jQuery) libraries, plugins and custom scripts-->
<script src="js/vendor.min.js"></script>
<script src="js/scripts.min.js"></script>

<style>

    .center {
        display: block;
        margin-left: auto;
        margin-right: auto;
        width: 50%;
    }
</style>

</body>
</html>
@section('js')
<script type="text/javascript">
    $(document).ready(function(){
        $('input[name=qty]').change(function(){
            if($(this).val() < 0){
                $(this).val('1')
            }
        });
    });
    function addToCart(obj){
        var url = "{{ route('product.addingToCart') }}";
        var form = $(obj).parent('form');
        $('#loader').fadeIn(100);
        $.ajax({
           type: "POST",
           url: url,
           data: form.serialize(), // serializes the form's elements.
           success: function(data)
           {
                $('#cart-toolbar').html(data.response.cartData); // show response from the php script.
                $('#loader').fadeOut(100);
           }
        });
    }
    function change_qty(obj,type){
        var target_ele = $(obj).siblings('input[name="qty"]');
        var current_qty = target_ele.val();
        if(type == 'increase'){
            current_qty++;
            target_ele.val(current_qty);
        }else{
            if(current_qty == 1){
                return ;
            }else{
                current_qty--;
                target_ele.val(current_qty);
            }
        }
    }
</script>
@endsection
