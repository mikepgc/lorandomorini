
@extends('layouts.master-analytics')


@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
                <div class="box-header with-border">

                    <div class="col-lg-12" style=" padding-bottom:20px;  border-bottom: 1px solid lightgrey">
                        <h2>Monitor Jouwerotiekshop.nl</h2>

                    </div>


                    <style>
                        div.scroll {

                            width: responsive;
                            height: 500px;
                            overflow: auto;
                        }

                        div.scroll2 {

                            width: responsive;
                            height: 265px;
                            overflow: auto;
                        }
                    </style>

                    <div class="row" style="margin-top: 20px">

                        {{--Aantal bezoekers periode--}}
                        <div class="col-md-6">
                            <!-- MAP & BOX PANE -->
                            <div class="box box-success" style="border-top-color:#d2d6de">


                                <!-- /.box-header -->
                                <div class="box-body no-padding">
                                    <div class="row">
                                        <div class="col-md-5 col-sm-4">

                                            <!-- small box -->
                                            <div class="small-box bg-purple" style="min-height: 280px; margin-bottom:0px" >
                                                <div class="inner">
                                                    <h2 style="color: white">Stockupdates</h2>
                                                    <div class="scroll2">

                                                    @foreach ($stockupdate->sortByDesc('id') as $data)

                                                        {{--Company:  {{$data['pageTitle']}}<br>--}}
                                                        updates:  {{$data->updated_at}}<br>

                                                        <br>

                                                    @endforeach
                                                    </div>

                                                </div>
                                                <div class="icon" style="padding-top: 10px">
                                                    <i class="ion ion-android-contacts"></i>
                                                </div>

                                            </div>
                                        </div>
                                        <!-- /.col -->



                                        <!-- /.col -->

                                    </div>
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->

                        {{--Aparaat Categorie--}}
                    </div>

                    {{--Tabellen--}}
                    <!-- ./col -->


                </div>
            </div>
        </div>


@endsection

