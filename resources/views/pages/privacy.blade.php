




@extends('layouts.master2')


@section('content')


    @include('layouts.sidebar')

    @include('layouts.topbar')

    @include ('layouts.navbar')
    <style>
        p {
            color: whitesmoke;
        }
    </style>
    <!-- Off-Canvas Wrapper-->
    <div class="offcanvas-wrapper">
        <!-- Page Title-->
        <div class="page-title">
            <div class="container">
                <div class="column">
                    {{--Takes from end of url and remove underscore--}}
                    <h1>Privacy Policy</h1>
                </div>
                <div class="column">
                    <ul class="breadcrumbs">
                        <li><a href="index.html">Home</a>
                        </li>
                        <li class="separator">&nbsp;</li>
                        <li>Privacy Policy</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Page Content-->
        <div class="container padding-bottom-3x mb-1">
            <div class="row">
                <!-- Products-->
                <div class="col-xl-9 col-lg-8 order-lg-2">
                    <!-- Shop Toolbar-->


                    <p>Bij bestellingen die via deze website worden geplaatst, verzamelen wij bepaalde gegevens. Op
                        deze pagina vindt u meer informatie over ons privacybeleid.</p>

                    <h3> Over Lorando & Morini </h3>

                    <p>Voor meer informatie over Lorando & Morini kunt u kijken onder het kopje "bedrijfsgegevens" op deze
                        website. Voor contactgegevens kunt u onder het kopje "klantenservice Lorando & Morini" kijken. </p>

                    <h3> Persoonsgegevens en gegevensverwerking </h3>

                    <p>  Om u een goede en persoonlijke service te kunnen bieden, heeft Lorando & Morini enkele gegevens van u nodig. We vragen u deze gegevens in te vullen bij het aanmaken van een account omdat dit voor ons noodzakelijk is om de bestelling goed af te kunnen handelen; dit is zonder uw gegevens niet mogelijk. Wanneer u een bestelling bij ons plaatst, worden de volgende gegevens verzameld:
                    </p>
   <h3> Bedrijfs- en identiteitsgegevens</h3>

                    <p>Dit zijn gegevens die u zelf invult tijdens het aanmaken van een account:<br><br>

                        Bedrijfsnaam<br>
                        Adres<br>
                        Postcode<br>
                        Plaatsnaam<br>
                        Land<br>
                        BTW-nummer<br>
                        Naam contactpersoon<br>
                        E-mailadres<br>
                        Telefoonnummer<br><br>
                        Deze gegevens gebruiken wij om uw bestelling te verwerken en het pakketje zo snel mogelijk bij u te bezorgen. Uw e-mailadres is daarbij nodig om de orderbevestiging te verzenden, zodat u altijd in bezit bent van een bewijs van uw bestelling. Verder wordt het e-mailadres gebruikt om met u te kunnen communiceren over uw bestelling. Uw e-mailadres wordt niet voor marketingdoeleinden gebruikt.<br><br>
                    </p>
                    <h3>Betalingsgegevens</h3>
                    <p> Bij registratie van betalingsgegevens noteren we enkel het totaalbedrag en uw betaalwijze. Uiteraard worden bankgegevens niet opgeslagen en is elke betaalmogelijkheid beveiligd.</p>

                    <h3>Verkeersgegevens</h3>
                    <p> Bij verkeersgegevens kunt u denken aan het registreren van een IP-adres *, het installeren van een cookie *, het registreren van het klikgedrag en het analyseren van uw navigatie op onze website.
                        <br><br>

                        Met de verkeersgegevens analyseren we het surfgedrag van onze bezoekers. We bekijken hoe de navigatie wordt gebruikt, welke pagina's worden bezocht en welke artikelen worden besteld, zodat we hiermee de bekendheid, functionaliteit en inhoud van onze site kunnen verbeteren.
                        <br><br>

                        * Een IP-adres is een Internet Protocol adres dat verwijst naar een unieke bestemming op het Internet. Dit kan betekenen dat het in sommige gevallen direct naar de computer van de bezoeker is te herleiden. Deze informatie is essentieel om de pagina's via Internet naar de juiste computer te sturen.
<br><br>
                        ** Een cookie is een verzameling gegevens, een registratiebestandje, die een website overbrengt naar de harde schijf van een computer om ze daar op te slaan. Een cookie identificeert de bezoeker weliswaar niet, maar wel zijn computer. Onze website installeert een cookie om de door u bestelde producten te koppelen aan uw computer (op deze manier wordt uw winkelmandje alleen voor u zichtbaar). Om te kijken welke cookies precies worden geplaatst op onze website, kunt u kijken op onze cookiepagina.</p>

                    <h3>Doelen en gegevensverwerking</h3>

                    <p>Hieronder wordt aangegeven voor welke doel(en) Lorando & Morini uw gegevens verzamelt. Lorando & Morini heeft de volgende gespecificeerde doelen voor ogen met het verwerken van uw gegevens:

                        Lorando & Morini verwerkt identiteitsgegevens voor administratieve doeleinden, zoals:

                        het verwerken van uw bestelling
                        de bezorging van uw bestelling op het door u gekozen afleveradres
                        Lorando & Morini verwerkt betalingsgegevens voor administratieve doeleinden, namelijk:

                        de betaling van uw bestelling
                        Verkeersgegevens verwerkt Lorando & Morini voor communicatieve doeleinden, zoals:

                        het bevorderen van de gebruiksvriendelijkheid van de site

                    </p>


                    <h3>Doorlinken via onze website</h3>

                    <p>De website van Lorando & Morini kan advertenties van derden of koppelingen naar andere sites bevatten die niet onder het beheer van Lorando & Morini vallen. Lorando & Morini is op geen enkele wijze verantwoordelijk voor de inhoud of naleving van de privacyregels door de beheerders van de betreffende sites. Het opnemen van links naar andere sites impliceert daarnaast ook geen goedkeuring van Lorando & Morini met betrekking tot de inhoud van dergelijke sites.
                    </p>

                    <h3>Beveiligde pagina's</h3>

                    <p>De online betaalmogelijkheden zijn beveiligd. De pagina's zijn beschermd met SSL, waardoor uw gegevens versleuteld worden verzonden en het onmogelijk is voor derden om uw (creditcard)gegevens te zien. U kunt de beveiliging checken door op de betalingspagina op de rechtermuisknop te drukken en te kiezen voor 'Eigenschappen'. U krijgt nu een venster met de verbindingsgegevens. Bij 'Verbinding' staat vermeld dat het om een SSL-verbinding gaat. Uw creditcardgegevens, zoals het nummer en de vervaldatum, worden uiteraard niet opgeslagen.
                    </p>

                    <h3>Periode van opslag</h3>
                    <p>Om onze diensten zo goed mogelijk aan te kunnen bieden, slaan we de gegevens die u invult op. De gegevens worden opgeslagen voor de periode dat uw account bestaat.
                    </p>

                    <h3> Herkomst van gegevens</h3>
                    <p>De gegevens die Lorando & Morini verwerkt worden verkregen van bezoekers en klanten, die door de bezoeker zelf worden verstrekt bij het aanvragen van een account.
                    </p>

                    <h3>Verstrekking van gegevens</h3>
                    <p>Uw gegevens kunnen door ons gedeeld worden binnen de verschillende BV’s van Lorando & Morini. Dit doen we als dit voor een zo soepel mogelijke afhandeling van uw bestelling nodig is.
                    <br><br>
                        Lorando & Morini verstrekt uw gegevens aan een beperkte kring van derden die de gegevens voor administratieve doeleinden verwerken:

                        Betaalproviders: voor het verwerken van betalingsverkeer verstrekken wij uw gegevens, afhankelijk van de betaalmethode aan Mollie.
                        Transporteurs: om uw pakket te verzenden dragen wij uw adresgegevens over aan de transporteur van uw pakket: PostNL, FedEx, DHL of UPS.
                        Data-analyse: om onze website continu te kunnen verbeteren, kunnen wij gegevens doorgeven aan partijen waarmee wij data-analyse uitvoeren (o.a. Google Analytics).
                        Hiernaast verstrekt Lorando & Morini uw gegevens niet aan derden, behoudens voor zover wet- en regelgeving ons hiertoe verplichten. De gegevens worden uitsluitend voor de in deze privacypolicy omschreven doelen gebruikt.

                    </p>

                    <h3> Rechten van bezoekers en onze cliënten</h3>
                    <p>Als bezoeker of klant van onze website heeft u het recht om zich tot onze klantenservice te richten met het verzoek om u mede te delen of onze organisatie uw gegevens verwerkt. De klantenservice zal binnen vier weken na het verzoek schriftelijk mededeling  doen welke gegevens worden verwerkt, voor welke doeleinden deze gegevens worden verwerkt, hoe lang de gegevens zullen worden opgeslagen of aan de hand van welke criteria dit gebeurt en waar deze gegevens vandaan komen, aan wie ze worden verstrekt en voor welke doelen dit gebeurt.

                        Als duidelijk is dat wij persoonsgegevens van u verwerken en welke gegevens dit zijn heeft u een aantal mogelijkheden, die hieronder worden genoemd.

                        Als de gegevens feitelijk onjuist, onnodig of onvolledig blijken te zijn, kunt u bij ons een verzoek tot rectificatie indienen; wij zullen uw gegevens dan zo snel mogelijk verbeteren, aanvullen of afschermen.

                        Daarnaast mag u een verzoek tot gegevenswissing indienen. Wij checken vervolgens of uw gegevens inderdaad gewist moeten worden. Dit doen we aan de hand van alle toepasselijke privacywetgeving. Blijkt dat u hier recht op heeft, dan wissen we zo snel mogelijk uw gegevens.

                        Ook kunt u een verzoek tot beperking van de verwerking doen. Dit betekent dat uw persoonsgegevens tijdelijk slechts opgeslagen worden, en niet verder worden verwerkt. U kunt zo’n verzoek bijvoorbeeld doen als u meent dat wij onjuiste persoonsgegevens van u verwerken, of u niet wil dat uw gegevens gewist worden. Wij checken vervolgens of u recht heeft op beperking van de verwerking. Dit doen we aan de hand van alle toepasselijke privacywetgeving. Als de eventuele beperking van persoonsgegevens later weer door ons wordt opgeheven, krijg u hier altijd bericht van.

                        Alle bovenstaande verzoeken kunt u indienen door zich te richten tot onze klantenservice.



                        Wanneer u geen prijs meer stelt op het ontvangen van onze nieuwsbrief, dan kunt u ons hiervan per e-mail op de hoogte stellen of afmelden via de afmeldlink in de nieuwsbrief.



                        Als u klachten heeft over de manier waarop wij met uw gegevens omgaan, laat ons dit dan weten, zodat wij hier mee aan de slag kunnen. Ben u na contact met ons nog niet tevreden? U heeft het recht om een klacht in te dienen bij de Autoriteit Persoonsgegevens.
                    </p>

                    <h3>Geheimhouding</h3>
                    <p>  De personen die handelen onder het gezag van de verantwoordelijke, alsmede de verantwoordelijke zelf en zij die toegang hebben tot persoonsgegevens, zijn verplicht tot geheimhouding van de persoonsgegevens waarvan zij kennis nemen, behoudens voor zover zij door wet- of regelgeving verplicht worden mededelingen te doen.
                    </p>

                    <h3>Beveiliging</h3>
                    <p> Om inzage van uw persoonsgegevens door onbevoegden te voorkomen, heeft Lorando & Morini verschillende beveiligingsmaatregelen getroffen, waaronder maatregelen tegen ongeoorloofde toegang, ongeoorloofd gebruik, ongeoorloofde wijziging, onrechtmatige en onbedoelde vernietiging en onbedoeld verlies.
                    </p>

                    <h3>Tot slot</h3>
                    <p> Mocht u na het lezen van deze privacy policy toch nog vragen hebben over de bescherming van uw privacy tijdens uw bezoeken op onze website, neem dan gerust schriftelijk, telefonisch of per e-mail contact met ons op.

                        Lorando & Morini kan tussentijds deze privacy policy aanpassen wanneer zich ontwikkelingen voordoen op het gebied van bijvoorbeeld technologie, wetgeving, verwerkingen of verstrekkingen. Wijzigingen zullen bekend worden gemaakt via deze pagina.
                    </p>


                </div>

            </div>
        </div>
        <!-- Site Footer-->
        @include('layouts.footer')

        @endsection
    </div>
    <!-- Back To Top Button--><a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
    <!-- Backdrop-->
    <div class="site-backdrop"></div>
    <!-- JavaScript (jQuery) libraries, plugins and custom scripts-->
    <script src="js/vendor.min.js"></script>
    <script src="js/scripts.min.js"></script>




