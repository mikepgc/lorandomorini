
@extends('layouts.master-analytics')


@section('content')

    <div class="row">
        <div class="col-md-12" >
            <div class="box box-default" style="background-color: #6e5e4e; color: whitesmoke">
                <div class="box-header with-border">

                    <div class="col-lg-12" style=" padding-bottom:20px;  border-bottom: 1px solid lightgrey">
                        <form>
                            <div class="col-md-7"  style="color: whitesmoke">
                                <h2>Dashboard Lorando Morini</h2>{{$startdate->format('d-m-Y')}} t/m {{$enddate->format('d-m-Y')}}
                                <a href="/dashboard" ><img src="https://pbs.twimg.com/media/C6UNxMdWUAImm9N.png" style="width:auto;height:25px;" type="submit"></a>

                                <input   style="color: black" id="startDate" name="startDate" type="date" value="{{date('Y-m-d', \Carbon\Carbon::now()->startOfMonth()->timestamp)}}">
                                &nbsp;t/m &nbsp;
                                <input   style="color: black "id="endDate" name="endDate" type="date" value="{{date('Y-m-d', \Carbon\Carbon::now()->addDays(1)->timestamp )}}">
                                <input class="btn btn-success" type="submit" value="submit">
                            </div>
                        </form>
                    </div>


                    <style>
                        div.scroll {

                            width: responsive;
                            height: 500px;
                            overflow: auto;
                        }

                        div.scroll2 {

                            width: responsive;
                            height: 265px;
                            overflow: auto;
                        }

                       h1,h2, h3, h4, h5{
                            color: whitesmoke;
                        }

                    </style>

                    <div class="row" style="margin-top: 20px">

                        {{--Aantal bezoekers periode--}}
                        <div class="col-md-6">
                            <!-- MAP & BOX PANE -->
                            <div class="box box-success" style="border-top-color:#d2d6de">
                                <div class="box-header with-border">
                                    <h3  style="color: black" class="box-title" >Aantal bezoekers periode</h3>

                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                        </button>
                                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body no-padding">
                                    <div class="row">
                                        <div class="col-md-5 col-sm-4">

                                            <!-- small box -->
                                            <div class="small-box bg-purple" style="min-height: 280px; margin-bottom:0px" >
                                                <div class="inner">
                                                    <h4>Bezoekers vandaag</h4>

                                                    @foreach ($analyticsData7  as $data)
                                                        <h3>{{$data['visitors']}}<sup style="font-size: 20px"></sup></h3>
                                                        Bezoekers
                                                    @endforeach

                                                    @foreach ($analyticsData14  as $data)
                                                        <h3>{{$analyticsData14->name}}{{($data[1])}}<sup style="font-size: 20px"></sup></h3>
                                                        Pageviews

                                                        <h3>{{$analyticsData14->name}}{{($data[2])}}<sup style="font-size: 20px"></sup></h3>
                                                        Bounced
                                                    @endforeach

                                                </div>
                                                <div class="icon" style="padding-top: 10px">
                                                    <i class="ion ion-android-contacts"></i>
                                                </div>

                                            </div>
                                        </div>
                                        <!-- /.col -->

                                        <div class="col-md-7 col-sm-8">
                                            <div class="pad">
                                                <!-- Map will be created here -->
                                                <div id="world-map-markers" style="height: 260px;" >
                                                    <div class="col-sm-12" >
                                                        <div class="scroll2">
                                                            @foreach ($analyticsData as $data)

                                                                {{--Company:  {{$data['pageTitle']}}<br>--}}
                                                                Bezoekers:  {{$data['visitors']}}<br>
                                                                Datum: {{$data['date']}}<br>
                                                                Pageviews: {{$data['pageViews']}}<br>
                                                                <br>

                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- /.col -->

                                    </div>
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->

                        {{--Aparaat Categorie--}}
                        <div class="col-md-6">

                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title" style="margin-bottom:16px; color:BLACK;">Apparaat Categorie</h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body no-padding" >
                                    <table class="table table-condensed">
                                        <tr>
                                            <th>Apparaat</th>
                                            <th>Bezoekers</th>
                                            <th>Bounce percentage</th>

                                        </tr>
                                        @foreach ($analyticsData11 as $data)
                                            <tr>

                                                <td>{{$analyticsData11->name}}{{$data[0]}}</td>

                                                <td>{{$analyticsData11->name}}{{$data[1]}}</td>


                                                <td>
                                                    <div class="progress progress-xs progress-striped active">
                                                        <div class="progress-bar progress-bar-success" style="width: {{$analyticsData11->name}}{{Round($data[2],2)}}%"></div>
                                                    </div>
                                                </td>
                                                <td><span class="badge bg-green">{{$analyticsData11->name}}{{Round($data[2],2)}}%</span></td>

                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>

                        </div>
                    </div>

                    {{--Tabellen--}}
                    <div class="row" style="margin-top: 20px">
                    {{--type bezoekers--}}
                        <!-- ./col -->
                        <div class="col-lg-6 col-xs-6">

                            <!-- small box -->
                            <div class="small-box bg-green" >
                                <div class="inner">
                                    <h4>Type bezoekers</h4>
                                    @foreach ($analyticsData5 as $data)

                                        <h3>{{$data['sessions']}}</h3>
                                        {{$data['type']}}
                                    @endforeach
                                </div>
                                <div class="icon" style="padding-top: 10px">
                                    <i class="ion ion-android-contacts"></i>
                                </div>
                            </div>
                        </div>
                        <!-- ./col -->
                        {{--Bezoekers pageviews--}}
                        <div class="col-lg-6 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h4>Bezoekers & Pageviews</h4>
                                    @foreach ($analyticsData13 as $data)

                                        <h3>{{$analyticsData13->name}}{{($data[0])}}</h3>
                                        Bezoekers

                                        <h3>{{$analyticsData13->name}}{{$data[1]}}</h3>
                                        Pageviews
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <!-- ./col -->
                    </div>
                    <!-- ./col -->



                    <div class="row"  style="color: whitesmoke">

                        <div  class="col-sm-2 col-md-2 col-lg-2" >

                            <h3>Top Exit pagina</h3>
                            <hr>
                            <div class="scroll">

                                @foreach ($analyticsData10 as $data)

                                    URL: <b>{{$analyticsData10->name}}{{$data[0]}}</b><br>

                                    Aantal exits:  {{$analyticsData10->name}}{{$data[1]}}<br>


                                    <br>

                                @endforeach

                            </div>
                        </div>

                        <div class="col-sm-2 col-md-2 col-lg-2" >
                            <h3>Info mobiel apparaat</h3>
                            <hr>

                            <div class="scroll">

                                @foreach ($analyticsData12 as $data)

                                    <b>{{$analyticsData12->name}}{{$data[0]}}</b><br>
                                    Source: {{$analyticsData12->name}}{{$data[1]}}<br>
                                    Sessions: {{$analyticsData12->name}}{{$data[2]}}<br>
                                    Pageviews: {{$analyticsData12->name}}{{$data[3]}}<br>
                                    <br>

                                @endforeach

                            </div>
                        </div>

                        <div class="col-sm-2 col-md-2 col-lg-2" >
                            <h3>Bezoekers per Land</h3>
                            <hr>

                            <div class="scroll">

                                @foreach ($analyticsData9 as $data)


                                    <b>{{$analyticsData9->name}}{{$data[0]}}</b><br>
                                    Aantal: {{$analyticsData9->name}}{{$data[1]}}
                                    <br>
                                        <br>

                                @endforeach
                            </div>
                        </div>

                        <div class="col-sm-2 col-md-2 col-lg-2" >

                            <h3>Top verwijswebsites</h3>
                            <hr>
                            <div class="scroll">
                                @foreach ($analyticsData4 as $data)
                                    <b> {{$data['url']}}</b><br>
                                    Pageviews:  {{$data['pageViews']}}<br>
                                    <br>
                                @endforeach

                            </div>
                        </div>

                        <div class="col-sm-2 col-md-2 col-lg-2" >


                            <h3>Top Browsers</h3>
                            <hr>
                            <div class="scroll">

                                @foreach ($analyticsData2 as $data)
                                    Browser:  <b>{{$data['browser']}}</b><br>
                                    Aantal:  {{$data['sessions']}}<br>
                                    <br>
                                @endforeach

                            </div>
                        </div>

                        <div class="col-sm-2 col-md-2 col-lg-2" >

                            <h3>Meest bezochte pagina</h3>
                            <hr>
                            <div class="scroll">

                                @foreach ($analyticsData3 as $data)
                                    URL:   <b> {{$analyticsData3->name}}{{$data[0]}}</b><br>
                                    Pageviews:  {{$analyticsData3->name}}{{$data[1]}}<br>
                                    Bounced:  {{$analyticsData3->name}}{{$data[2]}}<br>

                                    <br>
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

