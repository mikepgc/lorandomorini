@extends('layouts.master2')

@section('content')

    @include('layouts.topbar')

    @include ('layouts.navbar')

    @include('layouts.gallery-layout')

    @include('layouts.footer')

@endsection

