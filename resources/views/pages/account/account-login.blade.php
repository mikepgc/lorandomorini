
@extends('layouts.master2')


@section('content')


    @include('layouts.sidebar')

    @include('layouts.topbar')

    @include ('layouts.navbar')

    <!-- Off-Canvas Wrapper-->
    <div class="offcanvas-wrapper">
      <!-- Page Title-->
      <div class="page-title">
        <div class="container">
          <div class="column">
            <h1>Login Account</h1>
          </div>
          <div class="column">
            <ul class="breadcrumbs">
              <li><a href="/">Home</a>
              </li>
              <li class="separator">&nbsp;</li>
              <li><a href="account-orders">Account</a>
              </li>
              <li class="separator">&nbsp;</li>
              <li>Login </li>
            </ul>
          </div>
        </div>
      </div>
      <!-- Page Content-->
      <div class="container padding-bottom-3x mb-2">
        <div class="row">
          <div class="col-md-6">
            <form action="{{route('account-login')}}" class="login-box" method="post">
              {{--<div class="row margin-bottom-1x">--}}
                {{--<div class="col-xl-4 col-md-6 col-sm-4"><a class="btn btn-sm btn-block facebook-btn" href="#"><i class="socicon-facebook"></i>&nbsp;Facebook login</a></div>--}}
                {{--<div class="col-xl-4 col-md-6 col-sm-4"><a class="btn btn-sm btn-block twitter-btn" href="#"><i class="socicon-twitter"></i>&nbsp;Twitter login</a></div>--}}
                {{--<div class="col-xl-4 col-md-6 col-sm-4"><a class="btn btn-sm btn-block google-btn" href="#"><i class="socicon-googleplus"></i>&nbsp;Google+ login</a></div>--}}
              {{--</div>--}}
              <h4 class="margin-bottom-1x">Log in</h4>

                @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            <p>{{ $error }}</p>

                        @endforeach
                    </div>
                    <br>
                @endif

              <div class="form-group input-group">
                  <input class="form-control" type="email" id="email" name="email" required>
               <span class="input-group-addon"><i class="icon-mail"></i></span>
              </div>
              <div class="form-group input-group">
                  <input class="form-control" type="password" id="password" name="password" required>
                  <span class="input-group-addon"><i class="icon-lock"></i></span>
              </div>
              <div class="d-flex flex-wrap justify-content-between padding-bottom-1x">
                <div class="custom-control custom-checkbox">
                  <input class="custom-control-input" type="checkbox" id="remember_me" checked>
                  <label class="custom-control-label" for="remember_me">Onthouden</label>
                </div><a class="navi-link" href="account-password-recovery">Wachtwoord vergeten?</a>
              </div>
              <div class="text-center text-sm-right">
                <button class="btn btn-primary margin-bottom-none" type="submit">Inloggen</button>
              </div>
                {{ csrf_field() }}

            </form>
          </div>
          <div class="col-md-6">
            <div class="padding-top-3x hidden-md-up"></div>
            <h3 class="margin-bottom-1x">Geen account? Registeer hier</h3>
            <p style="color: whitesmoke">Registratie duurt minder dan een minuut en geeft u volledige controle en overzicht over uw bestellingen.</p>

               <button class="btn btn-primary margin-bottom-none" onclick="window.location.href = 'account-register';" >Registeer hier</button>

          </div>
        </div>
      </div>
      <!-- Site Footer-->
        <!-- Site Footer-->
        @include('layouts.footer')

        @endsection
    </div>
    <!-- Back To Top Button--><a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
    <!-- Backdrop-->
    <div class="site-backdrop"></div>
    <!-- JavaScript (jQuery) libraries, plugins and custom scripts-->
    <script src="js/vendor.min.js"></script>
    <script src="js/scripts.min.js"></script>
  </body>
</html>

