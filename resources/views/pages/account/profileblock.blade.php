
<div class="col-lg-4">
    <aside class="user-info-wrapper">
        <div class="user-cover" style="background-image: url(img/account/user-cover-img.jpg);">
            {{--<div class="info-label" data-toggle="tooltip" title="You currently have 290 Reward Points to spend"><i class="icon-medal"></i>290 points</div>--}}
        </div>
        <div class="user-info">
            @if(Auth::user()->profilePic)
            <div class="user-avatar"><a class="edit-avatar" href="#"></a><img src="{{Auth::user()->profilePic}}" alt="User"></div>
            @endif
            <div class="user-data">
                <h4>{{Auth::user()->name}} {{Auth::user()->surname}}</h4><span>Gebuiker sinds {{date('d-m-Y', strtotime(Auth::user()->created_at))}}</span>
            </div>
        </div>
    </aside>

    <?php $orders_count = \DB::table('orders')->where('user_id' , Auth::user()->id)->count(); ?>

    @if(\Request::is('account-orders'))
        <nav class="list-group"><a class="list-group-item with-badge active" href="account-orders"><i class="icon-bag"></i>Mijn orders<span class="badge badge-primary badge-pill">{{$orders_count}}</span></a><a class="list-group-item " href="account-profile"><i class="icon-head"></i>Mijn profiel</a><a class="list-group-item" href="account-address"><i class="icon-map"></i>Mijn Adresgegevens</a>
            {{--<a class="list-group-item with-badge" href="account-wishlist"><i class="icon-heart"></i>Mijn wensenlijst<span class="badge badge-primary badge-pill">3</span></a><a class="list-group-item with-badge" href="account-tickets"><i class="icon-tag"></i>Mijn Tickets<span class="badge badge-primary badge-pill">4</span></a>--}}
        </nav>
    @endif

    @if(\Request::is('account-profile'))
        <nav class="list-group"><a class="list-group-item with-badge" href="account-orders"><i class="icon-bag"></i>Mijn orders<span class="badge badge-primary badge-pill">{{$orders_count}}</span></a><a class="list-group-item active" href="account-profile"><i class="icon-head"></i>Mijn profiel</a><a class="list-group-item" href="account-address"><i class="icon-map"></i>Mijn Adresgegevens</a>
            {{--<a class="list-group-item with-badge " href="account-wishlist"><i class="icon-heart"></i>Mijn wensenlijst<span class="badge badge-primary badge-pill">3</span></a><a class="list-group-item with-badge" href="account-tickets"><i class="icon-tag"></i>Mijn Tickets<span class="badge badge-primary badge-pill">4</span></a>--}}
        </nav>
    @endif
    @if(\Request::is('account-address'))
        <nav class="list-group"><a class="list-group-item with-badge" href="account-orders"><i class="icon-bag"></i>Mijn orders<span class="badge badge-primary badge-pill">{{$orders_count}}</span></a><a class="list-group-item" href="account-profile"><i class="icon-head"></i>Mijn profiel</a><a class="list-group-item active" href="account-address"><i class="icon-map"></i>Mijn Adresgegevens</a>
            {{--<a class="list-group-item with-badge " href="account-wishlist"><i class="icon-heart"></i>Mijn wensenlijst<span class="badge badge-primary badge-pill">3</span></a><a class="list-group-item with-badge" href="account-tickets"><i class="icon-tag"></i>Mijn Tickets<span class="badge badge-primary badge-pill">4</span></a>--}}
        </nav>
    @endif
    @if(\Request::is('account-wishlist'))
        <nav class="list-group"><a class="list-group-item with-badge" href="account-orders"><i class="icon-bag"></i>Mijn orders<span class="badge badge-primary badge-pill">{{$orders_count}}</span></a><a class="list-group-item" href="account-profile"><i class="icon-head"></i>Mijn profiel</a><a class="list-group-item" href="account-address"><i class="icon-map"></i>Mijn Adresgegevens</a>
            {{--<a class="list-group-item with-badge active" href="account-wishlist"><i class="icon-heart"></i>Mijn wensenlijst<span class="badge badge-primary badge-pill">3</span></a><a class="list-group-item with-badge" href="account-tickets"><i class="icon-tag"></i>Mijn Tickets<span class="badge badge-primary badge-pill">4</span></a>--}}
        </nav>
    @endif
    @if(\Request::is('account-tickets'))
        <nav class="list-group"><a class="list-group-item with-badge" href="account-orders"><i class="icon-bag"></i>Mijn orders<span class="badge badge-primary badge-pill">{{$orders_count}}</span></a><a class="list-group-item" href="account-profile"><i class="icon-head"></i>Mijn profiel</a><a class="list-group-item" href="account-address"><i class="icon-map"></i>Mijn Adresgegevens</a>
            {{--<a class="list-group-item with-badge " href="account-wishlist"><i class="icon-heart"></i>Mijn wensenlijst<span class="badge badge-primary badge-pill">3</span></a><a class="list-group-item with-badge active" href="account-tickets"><i class="icon-tag"></i>Mijn Tickets<span class="badge badge-primary badge-pill">4</span></a>--}}
        </nav>
    @endif

</div>