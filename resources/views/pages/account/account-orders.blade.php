
@extends('layouts.master2')


@section('content')


    @include('layouts.sidebar')

    @include('layouts.topbar')

    @include ('layouts.navbar')


    <!-- Off-Canvas Wrapper-->
    <div class="offcanvas-wrapper">
      <!-- Page Title-->
      <div class="page-title">
        <div class="container">
          <div class="column">
            <h1>Mijn Orders</h1>
          </div>
          <div class="column">
            <ul class="breadcrumbs">
              <li><a href="/">Home</a>
              </li>
              <li class="separator">&nbsp;</li>
              <li><a href="account-orders">Account</a>
              </li>
              <li class="separator">&nbsp;</li>
              <li>Mijn Orders</li>
            </ul>
          </div>
        </div>
      </div>
      <!-- Page Content-->
      <div class="container padding-bottom-3x mb-2">
        <div class="row">
            @include('pages.account.profileblock')
          <div class="col-lg-8">
            <div class="padding-top-2x mt-2 hidden-lg-up"></div>
            <div class="table-responsive">
                <?php $orders_count = \DB::table('orders')->where('user_id' , Auth::user()->id)->count(); ?>
              @if($orders_count >= 1)
              <table class="table table-hover margin-bottom-none">
                <thead>
                  <tr>
                    <th>Ordernummer</th>
                    <th>Datum</th>
                    <th>Status</th>
                    <th>Totaal</th>
                  </tr>
                </thead>
                <tbody>

                @foreach($orders->where('user_id' , Auth::user()->id)->sortByDesc('created_at') as $order)
                  <tr>
                    <td><a class="openPopup text-medium navi-link " style="cursor: pointer" data-href="/order-tracking/{{$order->ordernumber}}" data-toggle="modal" data-target="#orderDetails">{{$order->ordernumber}}</a></td>
                    <td>{{date('d-m-Y', strtotime($order->created_at))}}</td>
                      @if($order->payment == 1)
                    <td><span style="color: green">Betaald</span></td>
                      @else
                    <td><span class="text-danger">Niet betaald</span></td>
                      @endif
                    <td><span class="text-medium">€{{number_format($order->ordertotal, 2)}}</span></td>
                  </tr>

                  @endforeach

                </tbody>
              </table>
                @else<h1>Nog geen orders</h1>
                @endif
            </div>
            <hr>
            {{--<div class="text-right"><a class="btn btn-link-primary margin-bottom-none" href="#"><i class="icon-download"></i>&nbsp;Order Details</a></div>--}}
          </div>
        </div>
      </div>
      <!-- Site Footer-->
        @include('layouts.footer')

        @endsection
    </div>
    <!-- Back To Top Button--><a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
    <!-- Backdrop-->
    <div class="site-backdrop"></div>




    {{--<div class="modal fade" id="orderDetails" tabindex="-1">--}}
      {{--<div class="modal-dialog modal-lg">--}}
        {{--<div class="modal-content">--}}
          {{--<div class="modal-header">--}}
            {{--@foreach($orders->where('ordernumber' , $order->ordernumber ) as $order)--}}
            {{--<h4 class="modal-title">Order Nummer  - {{$order->ordernumber}} </h4>--}}

            {{--@endforeach--}}
            {{--<button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>--}}
          {{--</div>--}}
          {{--<div class="modal-body">--}}
            {{--<div class="table-responsive shopping-cart mb-0">--}}
              {{--<table class="table">--}}
                {{--<thead>--}}
                {{--<tr>--}}
                  {{--<th>Product Name</th>--}}
                  {{--<th class="text-center">Subtotal</th>--}}
                {{--</tr>--}}
                {{--</thead>--}}
                {{--<tbody>--}}
                {{--<tr>--}}
                  {{--<td>--}}
                    {{--<div class="product-item"><a class="product-thumb" href="shop-single.html"><img src="img/shop/cart/01.jpg" alt="Product"></a>--}}
                      {{--<div class="product-info">--}}
                        {{--<h4 class="product-title"><a href="shop-single.html">--}}



                            {{--                                  @foreach($orders->orderdata  as $order)--}}


                            {{--<li class="list-group-item">--}}
                            {{--<span class="badge">{{$order}}</span>--}}

                            {{--</li>--}}


                            {{--@endforeach--}}

                            {{--<small>x 1</small></a></h4><span><em>Size:</em> 10.5</span><span><em>Color:</em> Dark Blue</span>--}}
                      {{--</div>--}}
                    {{--</div>--}}
                  {{--</td>--}}
                  {{--<td class="text-center text-lg text-medium">$43.90</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                  {{--<td>--}}
                    {{--<div class="product-item"><a class="product-thumb" href="shop-single.html"><img src="img/shop/cart/02.jpg" alt="Product"></a>--}}
                      {{--<div class="product-info">--}}
                        {{--<h4 class="product-title"><a href="shop-single.html">Daily Fabric Cap<small>x 2</small></a></h4><span><em>Size:</em> XL</span><span><em>Color:</em> Black</span>--}}
                      {{--</div>--}}
                    {{--</div>--}}
                  {{--</td>--}}
                  {{--<td class="text-center text-lg text-medium">$24.89</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                  {{--<td>--}}
                    {{--<div class="product-item"><a class="product-thumb" href="shop-single.html"><img src="img/shop/cart/03.jpg" alt="Product"></a>--}}
                      {{--<div class="product-info">--}}
                        {{--<h4 class="product-title"><a href="shop-single.html">Cole Haan Crossbody<small>x 1</small></a></h4><span><em>Size:</em> -</span><span><em>Color:</em> Turquoise</span>--}}
                      {{--</div>--}}
                    {{--</div>--}}
                  {{--</td>--}}
                  {{--<td class="text-center text-lg text-medium">$200.00</td>--}}
                {{--</tr>--}}
                {{--</tbody>--}}
              {{--</table>--}}
            {{--</div>--}}
            {{--<hr class="mb-3">--}}
            {{--<div class="d-flex flex-wrap justify-content-between align-items-center pb-2">--}}
              {{--<div class="px-2 py-1">Subtotal: <span class='text-medium'>$289.68</span></div>--}}
              {{--<div class="px-2 py-1">Shipping: <span class='text-medium'>$22.50</span></div>--}}
              {{--<div class="px-2 py-1">Tax: <span class='text-medium'>$3.42</span></div>--}}
              {{--<div class="text-lg px-2 py-1">Total: <span class='text-medium'>$315.60</span></div>--}}
            {{--</div>--}}
          {{--</div>--}}
        {{--</div>--}}
      {{--</div>--}}
    {{--</div>--}}
    <div class="modal fade" id="myModal" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header"></div>
          <div class="modal-body"></div>
        </div>
      </div>
    </div>

    <!-- JavaScript (jQuery) libraries, plugins and custom scripts-->
    <script src="js/vendor.min.js"></script>
    <script src="js/scripts.min.js"></script>
    <script>
        $(document).ready(function(){
            $('.openPopup').on('click',function(){
                var dataURL = $(this).attr('data-href');
                $('.modal-body').load(dataURL,function(){
                    $('#myModal').modal({show:true});
                });
            });
        });
    </script>
