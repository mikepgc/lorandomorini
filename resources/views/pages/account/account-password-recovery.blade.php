@extends('layouts.master2')

@section('content')

    @include('layouts.sidebar')

    @include('layouts.topbar')

    @include ('layouts.navbar')
    <!-- Off-Canvas Wrapper-->
    <div class="offcanvas-wrapper">
      <!-- Page Title-->
      <div class="page-title">
        <div class="container">
          <div class="column">
            <h1>Wachtwoord herstellen</h1>
          </div>
          <div class="column">
            <ul class="breadcrumbs">
              <li><a href="/">Home</a>
              </li>
              <li class="separator">&nbsp;</li>
              <li><a href="#">Account</a>
              </li>
              <li class="separator">&nbsp;</li>
              <li>Wachtwoord herstellen</li>
            </ul>
          </div>
        </div>
      </div>
      <!-- Page Content-->
      <div class="container padding-bottom-3x mb-2">
        <div class="row justify-content-center">
          <div class="col-lg-8 col-md-10">
            <h2>Wachtwoord vergeten?</h2>
            <p style="color: whitesmoke;">Geen paniek! Vraag hier uw wachtwoord aan.</p>
{{--            <ol class="list-unstyled">--}}
{{--              <li><span class="text-primary text-medium">1. </span>Fill in your email address below.</li>--}}
{{--              <li><span class="text-primary text-medium">2. </span>We'll email you a temporary code.</li>--}}
{{--              <li><span class="text-primary text-medium">3. </span>Use the code to change your password on our secure website.</li>--}}
{{--            </ol>--}}
            <form class="card mt-4">
              <div class="card-body">
                <div class="form-group">
                  <label for="email-for-pass" style="color: #9da9b9">Vul uw email adres in</label>
                  <input class="form-control" type="text" id="email-for-pass" required><small class="form-text text-muted">Vul hier uw email adres in waarmee u, uw account heeft geregistreerd bij Lorandomorini.nl</small>
                </div>
              </div>
              <div class="card-footer">
                <button class="btn btn-primary" type="submit">Vraag uw wachtwoord aan</button>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- Site Footer-->
        @include('layouts.footer')

        @endsection</div>
