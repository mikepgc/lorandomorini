@extends('layouts.master2')


@section('content')


    @include('layouts.sidebar')

    @include('layouts.topbar')

    @include ('layouts.navbar')

    <!-- Off-Canvas Wrapper-->
    <div class="offcanvas-wrapper">
        <!-- Page Title-->
        <div class="page-title">
            <div class="container">
                <div class="column">
                    <h1>Registreer Account</h1>
                </div>
                <div class="column">
                    <ul class="breadcrumbs">
                        <li><a href="/">Home</a>
                        </li>
                        <li class="separator">&nbsp;</li>
                        <li><a href="account-orders">Account</a>
                        </li>
                        <li class="separator">&nbsp;</li>
                        <li>Registreer</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Page Content-->
        <div class="container padding-bottom-3x mb-2">
            <div class="row">

                <div class="col-md-12">
                    <div class="padding-top-3x hidden-md-up"></div>
                    <h3 class="margin-bottom-1x">Geen account? Registeer hier</h3>
                    <p>Registratie duurt minder dan een minuut en geeft u volledige controle en overzicht over uw
                        bestellingen.</p>
                    @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $error)
                                <p>{{ $error }}</p>
                            @endforeach
                        </div>
                    @endif
                    <form action="account-register" class="row" method="post">


                        <div class="col-sm-2">
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input class="status person custom-control-input" type="checkbox" id="person"
                                           name="person" checked>

                                    <label class="custom-control-label" for="person">Particulier</label>
                                </div>

                            </div>
                        </div>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">

                                    <input class="status company custom-control-input" type="checkbox" id="company"
                                           name="company">

                                    <label class="custom-control-label" for="company">Bedrijf</label>
                                </div>
                            </div>
                        </div>


                        <div class="col-sm-6" id="company-details" style="display: none;">
                            <div class="form-group">
                                <label for="checkout-ln">Bedrijfsnaam</label>

                                <input class="form-control" type="text" id="companyname" name="companyname">

                            </div>
                        </div>

                        <div class="col-sm-6" id="company-details2" style="display: none;">
                            <div class="form-group">
                                <label for="checkout-ln">BTW-nummer</label>

                                <input class="form-control" type="text" id="btw" name="btw">
                            </div>
                        </div>


                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="reg-fn">Voornaam</label>
                                <input class="form-control" type="text" id="name" name="name" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="reg-ln">Achternaam</label>
                                <input class="form-control" type="text" id="surname" name="surname" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="reg-email">E-mail Addres</label>
                                <input class="form-control" type="email" id="email" name="email" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="reg-phone">Telefoon</label>
                                <input class="form-control" type="text" id="phone" name="phone" required>
                            </div>
                        </div>


                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="reg-pass">Wachtwoord</label>
                                <input class="form-control" type="password" id="password" name="password" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="reg-pass-confirm">Herhaal wachtwoord</label>
                                <input class="form-control" type="password" id="password" name="password" required>
                            </div>
                        </div>

                        <div class="col-12 text-center text-sm-right">
                            <button class="btn btn-primary margin-bottom-none" type="submit">Registreer</button>
                        </div>


                        {{ csrf_field() }}

                    </form>
                </div>
            </div>
        </div>
        <!-- Site Footer-->
        <!-- Site Footer-->
        @include('layouts.footer')
        <script>

            //        when company is checked, then show company fields

            $(".company").change(function () {
                if (this.checked) {
                    $('#company-details').show();
                }
                else {
                    $('#company-details').hide();
                }

            });
            $(".company").change(function () {
                if (this.checked) {
                    $('#company-details2').show();
                }
                else {
                    $('#company-details2').hide();
                }
            });

            //        when person is checked, then uncheck company
            $(".person").change(function () {
                if (this.checked) {
                    $('#company-details').hide();
                }
            });

            $('input.status').on('change', function () {
                $('input.status').not(this).prop('checked', false);
            });

            $(".person").change(function () {
                if (this.checked) {
                    $('#company-details2').hide();
                }
            });

            $('input.status').on('change', function () {
                $('input.status').not(this).prop('checked', false);
            });

        </script>

        @endsection
    </div>
    <!-- Back To Top Button--><a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
    <!-- Backdrop-->

    <div class="site-backdrop"></div>
    <!-- JavaScript (jQuery) libraries, plugins and custom scripts-->
    <script src="js/vendor.min.js"></script>
    <script src="js/scripts.min.js"></script>
    </body>
    </html>