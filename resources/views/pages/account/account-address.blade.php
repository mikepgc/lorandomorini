
@extends('layouts.master2')


@section('content')


    @include('layouts.sidebar')

    @include('layouts.topbar')

    @include ('layouts.navbar')

    <!-- Off-Canvas Wrapper-->
    <div class="offcanvas-wrapper">
      <!-- Page Title-->
      <div class="page-title">
        <div class="container">
          <div class="column">
            <h1>Contact adres</h1>
          </div>
          <div class="column">
            <ul class="breadcrumbs">
              <li><a href="/">Home</a>
              </li>
              <li class="separator">&nbsp;</li>
              <li><a href="/account-orders">Account</a>
              </li>
              <li class="separator">&nbsp;</li>
              <li>Contact Adres</li>
            </ul>
          </div>
        </div>
      </div>
      <!-- Page Content-->
      <div class="container padding-bottom-3x mb-1">
        <div class="row">
            @include('pages.account.profileblock')
          <div class="col-lg-8">
            <div class="padding-top-2x mt-2 hidden-lg-up"></div>
            <h4>Contact Adres</h4>
            <hr class="padding-bottom-1x">
            <form action="account-address" method="post">
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">

                    <label for="checkout-zip">Postcode</label>

                    @if(Auth::check())
                      <input class="form-control" type="text" id="postcode" name="postcode" value="{{Auth::user()->postcode}}" >
                    @else
                      <input class="form-control" type="text" id="postcode" name="postcode" >
                    @endif
                  </div>
                </div>

                <div class="col-sm-3">
                  <div class="form-group">
                    <label for="checkout-address1">Huisnummer </label>
                    @if(Auth::check())
                      <input class="form-control" type="text" id="housenumber" name="housenumber" value="{{Auth::user()->housenumber}}" >
                    @else
                      <input class="form-control" type="text" id="housenumber" name="housenumber" >
                    @endif
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <label for="checkout-address1">Toevoeging </label>
                    @if(Auth::check())
                      <input class="form-control" type="text" id="housenumber2" name="housenumber2" value="{{Auth::user()->housenumber2}}" >
                    @else
                      <input class="form-control" type="text" id="housenumber2" name="housenumber2" >
                    @endif
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="checkout-address1">Address </label>
                    @if(Auth::check())
                      <input class="form-control" type="text" id="address" name="address" value="{{Auth::user()->address}}"  >
                    @else
                      <input class="form-control" type="text" id="address" name="address" >
                    @endif
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="checkout-city">Stad</label>
                    @if(Auth::check())
                      <input class="form-control" type="text" id="city" name="city" value="{{Auth::user()->city}}" >
                    @else
                      <input class="form-control" type="text" id="city" name="city" >
                    @endif
                  </div>
                </div>

              </div>

              <div class="col-12 padding-top-1x">

                <div class="text-right">
                  <button class="btn btn-primary margin-bottom-none"type="submit" href="/account-address" data-toast data-toast-position="topRight" data-toast-type="success" data-toast-icon="icon-circle-check" data-toast-title="Success!" data-toast-message="Your address updated successfuly.">Update Address</button>
                </div>
                {{ csrf_field() }}

              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- Site Footer-->
        @include('layouts.footer')

        @endsection
    </div>
    <!-- Back To Top Button--><a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
    <!-- Backdrop-->
    <div class="site-backdrop"></div>
    <!-- JavaScript (jQuery) libraries, plugins and custom scripts-->
    <script src="js/vendor.min.js"></script>
    <script src="js/scripts.min.js"></script>
