<!-- Open Ticket Modal-->

<div class="modal-header" style="color: black">
    <h4 class="modal-title" style="color: black">Ordernummer - {{$orders->ordernumber}}</h4>
    <button class="close"  style="color: black" type="button" data-dismiss="modal" aria-label="Close"  ><span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="table-responsive shopping-cart mb-0">
        <table class="table">
            <thead>
            <tr>
                <th style="color: black">Product</th>
                <th class="text-center" style="color: black">Subtotaal</th>
            </tr>
            </thead>
            <tbody>
            @foreach($orderdata as  $order)
                <tr>
                    <td>
                        <div class="product-item"><a class="product-thumb" href="/category/subcategory-grid/single-product/{{$order['item']['artnr']}}"><img
                                        src="{{$order['item']['image_1']}}" alt="Product"></a>
                            <div class="product-info">
                                <h4 class="product-title"><a
                                            href="/category/subcategory-grid/single-product/{{$order['item']['artnr']}}">{{$order['item']['name']}}
                                        <small>x {{$order['qty']}}</small>
                                    </a></h4>
                                <span style="color: black"><em>Artnr:</em> {{$order['item']['artnr']}}</span>
                            </div>
                        </div>
                    </td>
                    <td class="text-center text-lg text-medium" style="color: black">€ {{number_format($order['item']['price'], 2)}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <hr class="mb-3">
    <div class="d-flex flex-wrap justify-content-between align-items-center pb-2">


        {{--                {{$orders->orderdata}}--}}
        <div class="text-lg px-2 py-1" style="color: black">Totaal: <span class='text-medium'>€{{number_format($orders->ordertotal, 2)}}</span></div>
    </div>
</div>

