
@extends('layouts.master2')


@section('content')


    @include('layouts.sidebar')

    @include('layouts.topbar')

    @include ('layouts.navbar')



    <!-- Off-Canvas Wrapper-->
    <div class="offcanvas-wrapper">
      <!-- Page Title-->
      <div class="page-title">
        <div class="container">
          <div class="column">
            <h1>Mijn Profiel</h1>
          </div>
          <div class="column">
            <ul class="breadcrumbs">
              <li><a href="/">Home</a>
              </li>
              <li class="separator">&nbsp;</li>
              <li><a href="account-orders.html">Account</a>
              </li>
              <li class="separator">&nbsp;</li>
              <li>Mijn Profiel</li>
            </ul>
          </div>
        </div>
      </div>
      <!-- Page Content-->
      <div class="container padding-bottom-3x mb-2">
        <div class="row">
   @include('pages.account.profileblock')

          <div class="col-lg-8">
            <div class="padding-top-2x mt-2 hidden-lg-up"></div>

            <form  action="account-profile-update" method="post" class="row">

                <div class="col-md-2">
                  <div class="form-group">
                    <div class="custom-control custom-checkbox">
                      @if(Auth::user()->person == '0')
                      <input class="status person custom-control-input" type="checkbox" id="person" name="person" checked>
                     @else
                      <input class="status person custom-control-input" type="checkbox" id="person" name="person"  >
                      @endif
                      <label class="custom-control-label" for="person">Particulier</label>
                    </div>

                  </div>
                </div>
                <div class="col-md-10">
                  <div class="form-group">
                    <div class="custom-control custom-checkbox">
                      @if(Auth::user()->company == 'on')

                      <input class="status company custom-control-input" type="checkbox" id="company" name="company"  checked>
                      @else
                        <input class="status company custom-control-input" type="checkbox" id="company" name="company"  >
                      @endif
                        <label class="custom-control-label" for="company">Bedrijf</label>
                    </div>
                  </div>
                </div>

              @if(Auth::user()->company == 'on')
                <div class="col-md-6" id="company-details">
                  @else

                <div class="col-md-6" id="company-details" style="display: none">
                  @endif
                  <div class="form-group">
                    <label for="checkout-ln">Bedrijfsnaam</label>
                    @if(Auth::check())
                      <input class="form-control" type="text" id="companyname" name="companyname" value="{{Auth::user()->companyname}}" >
                    @else
                      <input class="form-control" type="text" id="companyname" name="companyname" >
                    @endif
                  </div>
                </div>

                    @if(Auth::user()->company == 'on')
                      <div class="col-md-6" id="company-details2">
                        @else

                          <div class="col-md-6" id="company-details2" style="display: none">
                            @endif
                            <div class="form-group">
                    <label for="checkout-ln">BTW-nummer</label>
                    @if(Auth::check())
                      <input class="form-control" type="text" id="btw" name="btw" value="{{Auth::user()->btw}}" >
                    @else
                      <input class="form-control" type="text" id="btw" name="btw" >
                    @endif
                  </div>
                </div>


              <div class="col-md-6">
                <div class="form-group">
                  <label for="account-fn">Voornaam</label>
                  <input class="form-control" type="text" id="account-fn" name="name"  value="{{Auth::user()->name}}" required>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="account-ln">Achternaam</label>
                  <input class="form-control" type="text" id="account-ln" name="surname" value="{{Auth::user()->surname}}" required>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="account-email">Email adres</label>
                  <input class="form-control" type="email" id="account-email" name="email" value="{{Auth::user()->email}}" disabled>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="account-phone">Telefoonnummer</label>
                  <input class="form-control" type="text" id="account-phone" name="phone" value="{{Auth::user()->phone}}" required>
                </div>
              </div>

              {{--<div class="col-md-6">--}}
                {{--<div class="form-group">--}}
                  {{--<label for="account-pass">Nieuwe wachtwoord</label>--}}
                  {{--<input class="form-control" type="password" name="password" id="account-pass">--}}
                {{--</div>--}}
              {{--</div>--}}
              {{--<div class="col-md-6">--}}
                {{--<div class="form-group">--}}
                  {{--<label for="account-confirm-pass">Herhaal wachtwoord</label>--}}
                  {{--<input class="form-control" type="password"  id="account-confirm-pass">--}}
                {{--</div>--}}
              {{--</div>--}}


              <div class="col-12">

                <hr class="mt-2 mb-3">


                  <div class="form-group">
                    <div class="custom-control custom-checkbox">
                      @if(Auth::user()->newsletter == '0')
                        <input class="custom-control-input" type="checkbox" id="newsletter" name="newsletter" >
                      @else
                        <input class=" custom-control-input" type="checkbox" id="newsletter" name="newsletter"  checked>
                      @endif
                      <label class="custom-control-label" for="newsletter">Schrijf mij in voor nieuwsbrief</label>
                    </div>


                </div>


                  <button class="btn btn-primary margin-right-none" type="submit" data-toast data-toast-position="topRight" data-toast-type="success" data-toast-icon="icon-circle-check" data-toast-title="Success!" data-toast-message="Your profile updated successfuly.">Wijzig Profiel gegevens</button>
                </div>
                {{ csrf_field() }}
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- Site Footer-->

    </div>
        @include('layouts.footer')
      <script>




          //        when company is checked, then show company fields

          $(".company").change(function() {
              if(this.checked) {
                  $('#company-details').show();
              }
              else {
                  $('#company-details').hide();

              }


          });

          //        when person is checked, then uncheck company
          $(".person").change(function() {
              if(this.checked) {
                  $('#company-details').hide();
              }
          });

          $('input.status').on('change', function() {
              $('input.status').not(this).prop('checked', false);
          });


   $(".company").change(function() {
              if(this.checked) {
                  $('#company-details2').show();
              }
              else {
                  $('#company-details2').hide();

              }


          });

          //        when person is checked, then uncheck company
          $(".person").change(function() {
              if(this.checked) {
                  $('#company-details2').hide();
              }
          });

          $('input.status').on('change', function() {
              $('input.status').not(this).prop('checked', false);
          });

      </script>

        @endsection

    </div>
    <!-- Back To Top Button--><a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
    <!-- Backdrop-->
    <div class="site-backdrop"></div>
    <!-- JavaScript (jQuery) libraries, plugins and custom scripts-->
