@extends('layouts.master2')

@section('content')


    @include('layouts.sidebar')


    @include('layouts.topbar')


    @include ('layouts.navbar')

    @if(!$agent->isMobile() && !$agent->isTablet())
        @include ('templates.slider')
    @endif

    @include('templates.video')



{{--    @include ('templates.cartoon')--}}

{{--    @include ('templates.topCategories')--}}

{{--    @include('templates.promocountdown')--}}
        @include('templates.promoAll')


    @include('templates.services')



    @include('layouts.footer')


@endsection

