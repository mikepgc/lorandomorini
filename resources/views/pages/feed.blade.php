id|title|description|google product category|product type |link |image link |condition |availability|price|sale price | sale price effective date|gtin |brand
@foreach ($products as $product)
    <?
    $link = "https://www.jouwfotografieshop.nl/category/subcategory-list/single-product/".$product->artnr;
    $link = trim(preg_replace('/\s\s+/', '', $link))
    ?>
{{$product->id}}|{{$product->art_description_NL}}|{{$product->art_description_NL}}|Camera's en optiek|{{$product->category_NL}}|{{$link}}|{{$product->image_url_1}}|new|@if($product->stock >= 1)in stock @else out of stock @endif|{{$product->price}} EUR|{{$product->price}} EUR|2018-03-01T16:00-08:00/2020-03-03T16:00-08:00|{{$product->ean}}|{{$product->brand}}
@endforeach
