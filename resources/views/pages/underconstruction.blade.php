@extends('layouts.master2')

@section('content')

  <!-- Body-->
  <body>
    <!-- Off-Canvas Category Menu-->

    <!-- Off-Canvas Wrapper-->
    <div class="offcanvas-wrapper">
      <!-- Page Content-->
      <div class="row no-gutters">
        <div class="col-md-12 fh-section" style="background-image: url('/img/gallery/gallery2.jpg');"><span class="overlay" style="background-color: #374250; opacity: .85;"></span>
          <div class="d-flex flex-column fh-section py-5 px-3 justify-content-between">
            <div class="w-100 text-center">
              <div class="d-inline-block mb-5" style="width: 129px;">                      <a href="/"><img src="/img/logo/logo-white.png" class="img-fluid" style="width: 120px; padding-left: 20px" alt="Responsive image"> </a>
              </div>
              <h1 class="text-white text-normal mb-2">Wij zijn op vakantie</h1>
              <h5 class="text-white text-normal opacity-80 mb-4">We zijn terug op 30 juli<br>
{{--                  Tegelijkertijd gaan wij ons fabriek een groot onderhoud plegen om de kwaliteit van uw koffie te waarborgen <br>Het gaat live in:</h5>--}}
{{--              <div class="countdown countdown-inverse" data-date-time="30/07/2024 12:00:00">--}}
{{--                <div class="item">--}}
{{--                  <div class="days">00</div><span class="days_ref">Dagen</span>--}}
{{--                </div>--}}
{{--                <div class="item">--}}
{{--                  <div class="hours">00</div><span class="hours_ref">Uren</span>--}}
{{--                </div>--}}
{{--                <div class="item">--}}
{{--                  <div class="minutes">00</div><span class="minutes_ref">Mins</span>--}}
{{--                </div>--}}
{{--                <div class="item">--}}
{{--                  <div class="seconds">00</div><span class="seconds_ref">Secs</span>--}}
{{--                </div>--}}
{{--              </div>--}}
{{--              <div class="pt-3 hidden-md-up"><a class="btn btn-primary scroll-to" href="#notify"><i class="icon-bell"></i>&nbsp;Hou mij op de hoogte!</a></div>--}}
            </div>
            <div class="w-100 text-center">
              <p class="text-white mb-2"></p><a class="navi-link-light" href="info@lorandomorini.nl">info@lorandomorini.nl</a>
              <div class="pt-3"><a class="social-button shape-circle sb-facebook sb-light-skin" href="#"><i class="socicon-facebook"></i></a><a class="social-button shape-circle sb-twitter sb-light-skin" href="#"><i class="socicon-twitter"></i></a><a class="social-button shape-circle sb-instagram sb-light-skin" href="#"><i class="socicon-instagram"></i></a><a class="social-button shape-circle sb-google-plus sb-light-skin" href="#"><i class="socicon-googleplus"></i></a></div>
            </div>
          </div>
        </div>
{{--        <div class="col-md-6 fh-section" id="notify" data-offset-top="-1">--}}
{{--          <div class="d-flex flex-column fh-section py-5 px-3 justify-content-center align-items-center">--}}
{{--            <div class="text-center" style="max-width: 500px;">--}}
{{--              <img style="width: 150%; padding-bottom: 300px;"  src="/img/logo.png" alt="">--}}
{{--              <div class="h1 text-normal mb-2">&nbsp;Hou mij op de hoogte!</div>--}}
{{--              <h5 class="text-normal text-muted mb-4">Mocht u vragen hebben dan kunt u zich opgeven voor de nieuwsbrief. </h5>--}}

{{--              <form action="storeNieuwsbrief" class="popup-modal" method="post">--}}

{{--                <div class="form-group">--}}
{{--                  <input class="form-control" type="text" name="naam" placeholder="Uw Naam" id="naam" required>--}}
{{--                </div>--}}
{{--                <div class="form-group">--}}
{{--                  <input class="form-control" type="email" id="email" name="email" placeholder="Uw Email Adres" required>--}}

{{--                </div>--}}

{{--                <input type="hidden" name="_token" value="{{ csrf_token() }}">--}}


{{--                <button class="btn btn-primary" type="submit"><i class="icon-mail"></i>&nbsp;Vraag aan</button>--}}
{{--              </form>--}}
{{--            </div>--}}
{{--          </div>--}}
{{--        </div>--}}
      </div>
    </div>

@endsection

