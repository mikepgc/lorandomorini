<!-- Web Application Manifest -->
<link rel="manifest" href="/manifest.json">
<!-- Chrome for Android theme color -->
<meta name="theme-color" content="#000000">

<!-- Add to homescreen for Chrome on Android -->
<meta name="mobile-web-app-capable" content="yes">
<meta name="application-name" content="LorandoMorini">
<link rel="icon" sizes="512x512" href="/img/logo/logo-white.png">
<link rel="manifest" href="/manifest.json">
    @laravelPWA


<!-- Add to homescreen for Safari on iOS -->
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="apple-mobile-web-app-title" content="Richman">
<link rel="apple-touch-icon" href="/img/logo/logo-white.png">
{{--<link rel="manifest" href="/manifest.json">--}}
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">
<link href="/images/icons/splash-640x1136.png"
      media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)"
      rel="apple-touch-startup-image"/>
<link href="/images/icons/splash-750x1334.png"
      media="(device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2)"
      rel="apple-touch-startup-image"/>
<link href="/images/icons/splash-1242x2208.png"
      media="(device-width: 621px) and (device-height: 1104px) and (-webkit-device-pixel-ratio: 3)"
      rel="apple-touch-startup-image"/>
<link href="/images/icons/splashh-1125x2436.png"
      media="(device-width: 375px) and (device-height: 812px) and (-webkit-device-pixel-ratio: 3)"
      rel="apple-touch-startup-image"/>
<link href="/images/icons/splash-828x1792.png"
      media="(device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 2)"
      rel="apple-touch-startup-image"/>
<link href="/images/icons/splash-1242x2688.png"
      media="(device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 3)"
      rel="apple-touch-startup-image"/>
<link href="/images/icons/splash-1536x2048.png"
      media="(device-width: 768px) and (device-height: 1024px) and (-webkit-device-pixel-ratio: 2)"
      rel="apple-touch-startup-image"/>
<link href="/images/icons/splash-1668x2224.png"
      media="(device-width: 834px) and (device-height: 1112px) and (-webkit-device-pixel-ratio: 2)"
      rel="apple-touch-startup-image"/>
<link href="/images/icons/splash-1668x2388.png"
      media="(device-width: 834px) and (device-height: 1194px) and (-webkit-device-pixel-ratio: 2)"
      rel="apple-touch-startup-image"/>
<link href="/images/icons/splash-2048x2732.png"
      media="(device-width: 1024px) and (device-height: 1366px) and (-webkit-device-pixel-ratio: 2)"
      rel="apple-touch-startup-image"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">


@extends('layouts.master-analytics')

@section('content')

{{--    <header class="header-page parallax dark" style="padding-bottom: 10px">--}}
{{--        <div class="container">--}}
{{--            <div class="header-content text-center has-sticky-menu">--}}
{{--                <h1 class="page-title wow fadeInDown first"><span class="text-primary" style="color:black; font-weight:bold">Order Bevestigen</span></h1>--}}
{{--                --}}{{--<h4 class="header-subtitle mt-0 wow fadeInUp first">Make a Reservation</h4>--}}
{{--            </div><!-- / header-content -->--}}
{{--        </div><!-- / container -->--}}
{{--    </header>--}}

    <section id="features" class="p-0 bg-light-body-secondary">
        <div class="container-fluid">
            <div class="row">


                <div class="col-md-6">
                    <div class="promo-box text-center inner-space-2x">
                        <span style="color:black; font-weight:bold; font-size:20px; ">Order nog te bevestigen</span>
                        <hr>
                        @foreach ($orders->where('order_sent', NULL)->where('payment', '1')->where('test', NULL)  as $order)

                            <div class="row line">
                                <div class="col-md-12">
                                    <form method="post" onsubmit="return checkForm(this);" action="/succes">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="id" value="{{$order->ordernumber}}"/>
                                        <button class="button-orange btn btn-lg" type="submit"  id="submit" >
                                        <p class="box-description mb-3">{{$order->user_email}}
                                                <br> {{date('D d-m-Y', strtotime($order->created_at))}}
                                            </p>
                                            <p style="white-space: normal; font-size: 12px" >&euro;{{$order->ordertotal}}</p>
                                        </button>
                                    </form>

                                </div>
                                <hr>
                            </div>

                        @endforeach
                    </div><!-- / icon-block -->
                </div><!-- / column -->

                <div class="col-md-6">
                    <div class="promo-box text-center inner-space-2x">
                         <span style="color:black; font-weight:bold; font-size:20px">Order Bevestigen</span>
                        <hr>

                        @foreach ($orders->where('order_sent','1')->sortByDesc('created_at')  as $order)

                            <div class="row line">
                                <div class="col-md-12">
                            <button class="button-green btn btn-lg" type="button" >
                                <p class="box-description mb-3">{{$order->user_email}}  <br>
                                    {{date('D d-m-Y', strtotime($order->created_at))}} </p>

                                    <p style="white-space: normal; font-size: 10px" >&euro;{{$order->ordertotal}} </p>
                            </button>
                                </div>

                            <br>

                                </div>


                        @endforeach
                    </div><!-- / icon-block -->
                </div><!-- / column -->

            </div><!-- / row -->
        </div><!-- / container -->
    </section>


    <script>
        function checkForm(form)
        {

            form.submit.disabled = true;
            form.submit2.disabled = true;
            return true;
        }

    </script>

<script type="text/javascript">
    // Initialize the service worker
    if ('serviceWorker' in navigator) {
        navigator.serviceWorker.register('/serviceworker.js', {
            scope: '.'
        }).then(function (registration) {
            // Registration was successful
            console.log('Laravel PWA: ServiceWorker registration successful with scope: ', registration.scope);
        }, function (err) {
            // registration failed :(
            console.log('Laravel PWA: ServiceWorker registration failed: ', err);
        });
    }
</script>
@endsection

