@extends('layouts.master')

@section('content')
    <script>
        function initMap() {
            var myStyle = [
                {
                    "featureType": "all",
                    "elementType": "all",
                    "stylers": [
                        {
                            "saturation": -100
                        },
                        {
                            "gamma": 0.5
                        }
                    ]
                }, {
                    "featureType": "poi",
                    "elementType": "labels",
                    "stylers": [
                        { visibility: "off" }
                    ]
                }
            ];

            var features = [
                {
                    position: new google.maps.LatLng(51.75128, 4.5547842),
                    type: 'info'
                }
            ];
            // Create a map object and specify the DOM element for display.
            var map = new google.maps.Map(document.getElementById('gmap'), {
                center: {lat: 51.75128, lng: 4.5547842},
                scrollwheel: false,
                zoom: 17,
                disableDefaultUI: true,
                draggable: false,
                keyboardShortcuts: false,
                disableDoubleClickZoom: true,
                mapTypeId: 'mystyle'
            });

            features.forEach(function(feature) {
                var marker = new google.maps.Marker({
                    position: feature.position,
                    map: map
                });
            });
            map.mapTypes.set('mystyle', new google.maps.StyledMapType(myStyle, { name: 'My Style' }));
        }
        //51.7522347,4.5510667
    </script>
    <div class="col-lg-12" id="container">
        @include('pages.infobanner')
        <div class="small " data-cycle-log="false" id="banner">
            <div class="normal contact_banner"><script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBh4Hws61liDZaZZdNrzpwk_s46Myru3fc&callback=initMap"></script>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="gmap"></div>
            </div>
        </div>

        <section class="container no_padding" id="content">&nbsp;</section>

        <div class="container contact" id="maincontent">
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 collapse_sidemenu" id="sidemenu">
                <div class="inner">
                    <h3>{{trans( 'menu.Navigeer naar')}}</h3>

                    <ul>
                        <a href="#employees"><li>{{ trans('menu.onze verkopers')}}</li></a>
                        <a href="#storage"><li>{{ trans('menu.Wissel en Opslag')}}</li></a>
                        <a href="#administration"><li>{{ trans('menu.administratie')}}</li></a>
                        <a href="#website"><li>{{ trans('menu.website')}}</li></a>
                        {{--	<a href="#retour"><li>{{ trans('menu.retour formulier')}}</li></a>--}}
                    </ul>
                </div>
            </div>

            <div class="contact col-xs-12 col-sm-12 col-md-9 col-lg-9 col-xl-9" id="content">


                <div class="address_container col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12"><!--		<div id="gmap" class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-xl-8"></div>-->
                    <div class="address_info col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6">

                            <?=trans('menu.contactblock1')?>

                        </div>
                    </div>
                </div>

                @include('pages.OLD templates.tijdenblock')

                <div class="cards_container">
                    <h2 id="employees">{{trans('menu.onze verkopers')}}</h2>

                    <div class="explanation">{{trans('menu.contact-aanschaf')}}</div>


                    @include('pages.OLD templates.salesblock')

                </div>


                <div class="cards_container">
                    <h2 id="storage">{{trans('menu.Wissel en Opslag')}}</h2>

                    <div class="explanation">{{trans('menu.contact-vragen')}}</div>

                    <div class="contact_card col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <div class="inner">
                            <div class="title">{{trans('menu.Wissel en Opslag')}}</div>

                            <div class="function">&nbsp;</div>

                            <div class="email"><a href="mailto:">bandenhotel@reedijkgroup.com</a></div>

                            <div class="phone">078 - 674 4544</div>
                        </div>
                    </div>
                </div>


                <div class="cards_container">
                    <h2 id="administration">{{trans('menu.finance')}}</h2>

                    <div class="explanation">{{trans('menu.contact-vragen')}}</div>


                    <div class="contact_card col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <div class="inner">
                            <div class="title">	{{trans('menu.administratie1')}}</div>


                            <div class="email"><a href="mailto:">administratie@reedijkgroup.com</a></div>
                            <br><br><br><br>

                            <div class="contact_card col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">

                                {{trans('menu.administratie2')}}

                                <br><br>

                                <div class="title">	{{trans('menu.administratie3')}}</div>

                                <div class="email"><a href="mailto:">invoice@reedijkgroup.com</a></div>
                            </div>
                        </div>
                    </div>




                    @include('pages.OLD templates.administrationblock')

                </div>

                <div class="cards_container">
                    <h2 id="website">MARKETING / ICT</h2>

                    <div class="explanation">{{trans('menu.contact-vragen2')}}</div>

                    @include('pages.OLD templates.websiteblock')

                </div>

                {{--<div class="cards_container">
                <h2 id="retour">{{trans('menu.retour formulier')}}</h2>

                <div class="explanation">{{trans('menu.klik')}} <a href="/retour">{{trans('menu.hier')}}</a></div>
                </div>--}}

            </div>
        </div>
    </div>


@endsection


