@extends('layouts.master')

@section('content')

	<div class="col-lg-12 " id="container">
@include('pages.infobanner')
		
		<div class="small " data-cycle-log="false" id="banner">
			<div class="normal" style="background: #000 url('/img/header_contentpage.jpg') no-repeat center !important;">&nbsp;</div>
		</div>


		<section class="container no_padding" id="content">&nbsp;</section>

		<div class="container accesoires" id="maincontent">
			@include('layouts.configurator')
			<div class="col-md-3 col-lg-3 col-xl-3 no_padding" id="sidemenu">
				<div class="inner">
					<h3>{{trans( 'menu.Navigeer naar')}}</h3>

					<ul>
						<a href="#tpms"><li>{{ trans('menu.tpms')}}</li></a>
						<a href="#bimecc"><li>{{ trans('menu.Bimecc')}}</li></a>
						<!--	<a href="./reinigingsproducten">--><!--		<li>--><!--</li>--><!--	</a>--><!--	<a href="./wielsloten">--><!--		<li>--><!--</li>--><!--	</a>-->
					</ul>
				</div>
			</div>


			<div class="col-md-9 col-lg-9 col-xl-9" id="content">

                <?=trans('menu.accesoiresblock')?>
			</div>


		</div>
	</div>


@endsection

