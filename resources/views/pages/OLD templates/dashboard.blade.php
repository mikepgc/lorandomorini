@extends('backpack::layout')

@section('header')
    <section class="content-header">
        <h1>
            Dashboard<!-- <small>{{ trans('backpack::base.first_page_you_see') }}</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url(config('backpack.base.route_prefix', 'admin')) }}">{{ config('backpack.base.project_name') }}</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>
@endsection


@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    <div class="box-title">Goedendag <b></b>!
                        <br>
                        <h5>Je bent succesvol ingelogd!</h5>
                    </div>

                    <hr>

<div class="col-sm-6" >
    <h1> Afgelopen 30 dagen</h1>
        @foreach ($analyticsData as $data)

            Company:  {{$data['pageTitle']}}<br>

            Bezoekers:  {{$data['visitors']}}<br>

            Datum: {{$data['date']}}<br>

            Pageviews: {{$data['pageViews']}}<br>


            <br>


        @endforeach
</div>


<div class="col-sm-6" >

    <h1> Afgelopen 30 dagen</h1>
@foreach ($analyticsData2 as $data)

    Browser:  {{$data['browser']}}<br>

    Aantal:  {{$data['sessions']}}<br>
<br>

@endforeach

</div>


                </div>
            </div>
        </div>
    </div>


@endsection
