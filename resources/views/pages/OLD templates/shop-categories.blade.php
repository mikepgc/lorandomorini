@extends('layouts.master2')

@section('content')

    <div class="offcanvas-header">
        <h3 class="offcanvas-title">{{$categorie}}</h3>
    </div>
      <!-- Page Content-->
      <div class="container padding-bottom-2x mb-2">
        <div class="row">
          <!-- Sidebar          -->
          <div class="col-lg-3">
            <button class="sidebar-toggle position-left" data-toggle="modal" data-target="#modalShopCategories"><i class="icon-layout"> </i></button>
            <aside class="sidebar sidebar-offcanvas">
              <section class="widget widget-categories">
                <h3 class="widget-title">Popular Brands</h3>
                <ul>
                  <li><a href="#">Adidas{{$categorie}}</a><span>(254)</span></li>
                  <li><a href="#">Bilabong</a><span>(39)</span></li>
                  <li><a href="#">Brooks</a><span>(205)</span></li>
                  <li><a href="#">Calvin Klein</a><span>(128)</span></li>
                  <li><a href="#">Cole Haan</a><span>(104)</span></li>
                  <li><a href="#">Columbia</a><span>(217)</span></li>
                  <li><a href="#">New Balance</a><span>(95)</span></li>
                  <li><a href="#">Nike</a><span>(310)</span></li>
                  <li><a href="#">Nine West</a><span>(134)</span></li>
                  <li><a href="#">Oakley</a><span>(73)</span></li>
                  <li><a href="#">Puma</a><span>(446)</span></li>
                  <li><a href="#">Scechers</a><span>(87)</span></li>
                  <li><a href="#">Tommy Bahama</a><span>(42)</span></li>
                  <li><a href="#">Tommy Hilfiger</a><span>(289)</span></li>
                  <li><a href="#">Valentino</a><span>(68)</span></li>
                </ul>
              </section>
            </aside>
          </div>
          <!-- Categories-->
          <div class="col-lg-9">
            <!-- Promo banner-->
            <div class="alert alert-image-bg alert-dismissible fade show text-center mb-4" style="background-image: url(img/banners/alert-bg.jpg);"><span class="alert-close text-white" data-dismiss="alert"></span>
              <div class="h3 text-medium text-white padding-top-1x padding-bottom-1x"><i class="icon-clock" style="font-size: 33px; margin-top: -5px;"></i>&nbsp;&nbsp;Check our Limited Offers. Save up to 50%&nbsp;&nbsp;&nbsp;
                <div class="mt-3 hidden-xl-up"></div><a class="btn btn-primary" href="#">View Offers</a>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-6">
                <div class="card mb-30"><a class="card-img-tiles" href="losse templates/shop-grid-ls.html">
                    <div class="inner">
                      <div class="main-img"><img src="img/shop/categories/01.jpg" alt="Category"></div>
                      <div class="thumblist"><img src="img/shop/categories/02.jpg" alt="Category"><img src="img/shop/categories/03.jpg" alt="Category"></div>
                    </div></a>
                  <div class="card-body text-center">
                    <h4 class="card-title">Clothing</h4>
                    <p class="text-muted">Starting from $49.99</p><a class="btn btn-outline-primary btn-sm" href="losse templates/shop-grid-ls.html">View Products</a>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="card mb-30"><a class="card-img-tiles" href="losse templates/shop-grid-ls.html">
                    <div class="inner">
                      <div class="main-img"><img src="img/shop/categories/04.jpg" alt="Category"></div>
                      <div class="thumblist"><img src="img/shop/categories/05.jpg" alt="Category"><img src="img/shop/categories/06.jpg" alt="Category"></div>
                    </div></a>
                  <div class="card-body text-center">
                    <h4 class="card-title">Shoes</h4>
                    <p class="text-muted">Starting from $56.00</p><a class="btn btn-outline-primary btn-sm" href="losse templates/shop-grid-ls.html">View Products</a>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="card mb-30"><a class="card-img-tiles" href="losse templates/shop-grid-ls.html">
                    <div class="inner">
                      <div class="main-img"><img src="img/shop/categories/07.jpg" alt="Category"></div>
                      <div class="thumblist"><img src="img/shop/categories/08.jpg" alt="Category"><img src="img/shop/categories/09.jpg" alt="Category"></div>
                    </div></a>
                  <div class="card-body text-center">
                    <h4 class="card-title">Bags</h4>
                    <p class="text-muted">Starting from $27.00</p><a class="btn btn-outline-primary btn-sm" href="losse templates/shop-grid-ls.html">View Products</a>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="card mb-30"><a class="card-img-tiles" href="losse templates/shop-grid-ls.html">
                    <div class="inner">
                      <div class="main-img"><img src="img/shop/categories/10.jpg" alt="Category"></div>
                      <div class="thumblist"><img src="img/shop/categories/11.jpg" alt="Category"><img src="img/shop/categories/12.jpg" alt="Category"></div>
                    </div></a>
                  <div class="card-body text-center">
                    <h4 class="card-title">Hats</h4>
                    <p class="text-muted">Starting from $14.50</p><a class="btn btn-outline-primary btn-sm" href="losse templates/shop-grid-ls.html">View Products</a>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="card mb-30"><a class="card-img-tiles" href="losse templates/shop-grid-ls.html">
                    <div class="inner">
                      <div class="main-img"><img src="img/shop/categories/13.jpg" alt="Category"></div>
                      <div class="thumblist"><img src="img/shop/categories/14.jpg" alt="Category"><img src="img/shop/categories/15.jpg" alt="Category"></div>
                    </div></a>
                  <div class="card-body text-center">
                    <h4 class="card-title">Sunglasses</h4>
                    <p class="text-muted">Starting from $35.99</p><a class="btn btn-outline-primary btn-sm" href="losse templates/shop-grid-ls.html">View Products</a>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="card mb-30"><a class="card-img-tiles" href="losse templates/shop-grid-ls.html">
                    <div class="inner">
                      <div class="main-img"><img src="img/shop/categories/16.jpg" alt="Category"></div>
                      <div class="thumblist"><img src="img/shop/categories/17.jpg" alt="Category"><img src="img/shop/categories/18.jpg" alt="Category"></div>
                    </div></a>
                  <div class="card-body text-center">
                    <h4 class="card-title">Watches</h4>
                    <p class="text-muted">Starting from $79.99</p><a class="btn btn-outline-primary btn-sm" href="losse templates/shop-grid-ls.html">View Products</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Site Footer-->
    <!-- Back To Top Button--><a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
    <!-- Backdrop-->
    <!-- JavaScript (jQuery) libraries, plugins and custom scripts-->
    @include('layouts.footer')
    </div>

@endsection
<div class="site-backdrop"></div>
