
<form method="GET" class="col-xs-12" id="galleryFilter" data-url="{{URL::route('gallery')}}">
    @foreach($filters as $key => $options)
    <div class="col-xs-3" style="padding: 5px;">
    <label>{{trans('menu.' . $key)}}</label>
      <div class="select_wrapper">
          <select name="{{$key}}" class="col-xs-12">
              @if(($key == 'wheelModel' || $key == 'carModel') && empty($options))
                  <option value="All">{{trans('menu.selecteer eerst een merk')}}</option>
              @else
                  @if(($key == 'wheelModel' || $key == 'carModel'))
                      <option value="All">All</option>
                  @endif
                  @foreach($options as $option)
                      <option @if(($_GET[$key] ?? '') == $option) selected @endif value="{{$option}}">{{$option}}</option>
                  @endforeach
              @endif
          </select>
          <span class="arrow">▼</span>
      </div>
    </div>
  @endforeach
</form>
<div class="clearfix"></div>
<div class="gallery-images col-xs-12">
@foreach ($galleryImages as $galleryImage)
<a data-fancybox="gallery" data-toolbar="true" href="{{URL::asset($galleryImage->src)}}" class="col-xs-12 col-sm-4 col-lg-3" data-caption="{{$galleryImage->photoName1}}">
  <div style="background-image: url('{{URL::asset($galleryImage->getThumb())}}')">
  
</div> 

</a>
@endforeach
</div>
