@extends('layouts.master')

@section('content')




<div class="col-lg-12 " id="container">
@include('pages.infobanner')

	<div class="small " data-cycle-log="false" id="banner">
<div class="normal" style="background: #000 url('/img/header_contentpage.jpg') no-repeat center !important;">&nbsp;</div>
</div>

<section class="container no_padding" id="content">&nbsp;</section>

<div class="container page" id="maincontent">
<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 collapse_sidemenu" id="sidemenu">
<div id="open_sidemenu"><!--?xml version="1.0" encoding="utf-8"?--><!-- Generator: Adobe Illustrator 19.2.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  --><svg id="Layer_1" style="enable-background:new 0 0 50 50;" version="1.1" viewbox="0 0 50 50" x="0px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" y="0px"><g><polygon points="1,10 25,40.3 49,10 	" style="fill:#5D5D5D;"></polygon></g></svg></div>

<div class="inner">
<h3>{{trans( 'menu.Navigeer naar')}}</h3>

<ul>
	<a href="#over_reedijk"><li>{{ trans('menu.over Reedijk')}}</li></a>
	<a href="#reedijk_kenmerken"><li>{{ trans('menu.Reedijk kenmerken')}}</li></a>
	<a href="#infographic"><li>{{ trans('menu.infographic')}}</li> </a>
	<a href="#concept"><li>{{ trans('menu.concept')}}</li> </a>
	<a class="page-scroll" href="#collectie"><li>{{ trans('menu.collectie')}}</li> </a>
	<a class="page-scroll" href="#productgroepen"><li>{{ trans('menu.We hanteren de volgende productgroepen')}}</li></a>
	<a class="page-scroll" href="#team"><li>{{ trans('menu.team')}}</li></a>
	<a class="page-scroll" href="#netwerk"><li>{{ trans('menu.netwerk')}}</li></a>
	<a class="page-scroll" href="#kenniscentrum"><li>{{ trans('menu.kenniscentrum')}}</li></a>
	<a class="page-scroll" href="#sets"><li>{{ trans('menu.sets (kits)')}}</li> </a>
	<a class="page-scroll" href="#design"><li>{{ trans('menu.design')}}</li></a>
	<a class="page-scroll" href="#exclusiviteit"><li>{{ trans('menu.exclusiviteit')}}</li></a>
	<a class="page-scroll" href="#verkoop_marketing"><li>{{ trans('menu.verkoop & marketing')}}</li></a>
	<a class="page-scroll" href="#strategische_allianties"><li>{{ trans('menu.Strategische Allianties')}}</li></a>
	<a class="page-scroll" href="#toonaangevende_merken"><li>{{ trans('menu.toonaangevende merken')}}</li></a>
	<a class="page-scroll" href="#reedijks_keuze"><li>{{ trans('menu.reedijks keuze')}}</li> </a>
	<a class="page-scroll" href="#academy"><li>{{ trans('menu.academy')}}</li> </a>
	<a class="page-scroll" href="#merken"><li>{{ trans('menu.merken')}}</li></a>
</ul>
</div>
</div>


	<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 col-xl-9" id="content">
        <?=trans('menu.aboutusblock')?>
	</div>





</div>
</div>

@endsection



