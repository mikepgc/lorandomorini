
 <div class="store_times">
     <h4>{{trans('menu.openingstijden:')}}</h4>

     <table class="time_table">
         <tbody>
         <tr>
             <td>{{trans('menu.ma/vr')}}</td>
             <td>08:00 - 17:00</td>
         </tr>
         <tr>
             <td>{{trans('menu.zaterdag')}}</td>
             <td>08:00 - 14:00</td>
         </tr>
         <tr>
             <td>{{trans('menu.zondag')}}</td>
             <td>{{trans('menu.gesloten')}}</td>
         </tr>
         </tbody>
     </table>


     <h4 class="col-xs-12"><small>{{trans('menu.aangepaste tijden')}}:</small></h4>
     @foreach ($Tijden->where('active','1')->sortBy('datum') as $times)

     <table class="time_table">
         <tbody>
             <tr>


                 <td>{{Carbon\Carbon::parse($times->datum)->timezone('Europe/Amsterdam')->format('d-m-Y')}}</td>
                 <td>{{trans('menu.'.$times->type)}} / {{$times->feestdag}}</td>
             </tr>

         </tbody>
     </table>
     @endforeach
 </div>

