@extends('layouts.master')


@section('content')


    <div class="col-lg-12 " id="container">
        @include('pages.infobanner')

        <div class="small " data-cycle-log="false" id="banner">

            <div class="normal" style="background: #000 url('/img/header_contentpage.jpg') no-repeat center !important;">&nbsp;</div>

        </div>

        <section class="container no_padding" id="content">&nbsp;</section>

        <div class="container page" id="maincontent">
            <div class="col-xs-12 col-sm-12" id="content">
                <div class="inner text-center" id="pageContent">
                    <h3>
                        Bedankt voor uw bestelling
                    </h3>
                    <br>
                    <form action="{{URL::route('wheels')}}">
                        <button type="submit" class="button" style="color: #333">Terug naar de webshop</button>
                    </form>
                </div>
            </div>
        </div>

    </div>



@endsection

