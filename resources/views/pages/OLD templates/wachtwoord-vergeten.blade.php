@extends('layouts.master')
@section('content')
    <section id="content" class="container  no_padding">
        {{--<a href="{{URL::asset('/')}}" id="logo" style="background-image: url('{{URL::asset(\App\Governor::$website->logo)}}')">
        </a>--}}

        <h1 class="intro-text text-center">{{trans('menu.Wachtwoord vergeten')}}?</h1>
        <h5 class="intro-text text-center">{{trans('menu.Vraag hier uw wachtwoord op')}}</h5>
        <br>

        <div id="maincontent" class="container loginBox form" >
            <div id="loginform">
                <fieldset id="fieldset-form-wachtwoordvergeten"><form id="form-wachtwoordvergeten" method="POST" class="intro-text text-center">
                        {{ csrf_field() }}
                        @if($sent)
                            <span class="remark prio2 text-center">
                                    {{trans('menu.U ontvangt binnenkort van ons een email')}}
                                </span>
                        @endif
                        <ol><li class="inputContainer input-email">
                                <p id="email-label" class="formLabel">
                                    <label for="email" class="required">{{trans('menu.E-mail adres')}}</label>
                                </p>
                                <div class="formInput">
                                    <input type="text" name="email" id="email" value="" placeholder="{{trans('menu.E-mail adres')}}" required="required">
                                </div>
                            </li>
                            <li class="inputContainer input-vraagaan input-button" >
                                <div class="formInput" >
                                    <input type="submit" name="vraagaan" id="vraagaan" value="{{trans('menu.Vraag nieuwe wachtwoord aan')}}" placeholder="E-mail adres">
                                </div>
                            </li>
                        </ol>
                    </form>
                </fieldset>
            </div>
        </div>
    </section>
@endsection
