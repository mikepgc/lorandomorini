@extends('layouts.master')

@section('content')

<div class="col-lg-12 " id="container">
@include('pages.infobanner')
<div class="small " data-cycle-log="false" id="banner">
<div class="normal" style="background: #000 url('/img/header_contentpage.jpg') no-repeat center !important;">&nbsp;</div>
</div>

<section class="container no_padding" id="content">&nbsp;</section>

<div class="container page" id="maincontent">
<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 collapse_sidemenu" id="sidemenu">
<div id="open_sidemenu"><!--?xml version="1.0" encoding="utf-8"?--><!-- Generator: Adobe Illustrator 19.2.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  --><svg id="Layer_1" style="enable-background:new 0 0 50 50;" version="1.1" viewbox="0 0 50 50" x="0px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" y="0px"><g><polygon points="1,10 25,40.3 49,10 	" style="fill:#5D5D5D;"></polygon></g></svg></div>

<div class="inner">
<h3>{{trans( 'menu.Navigeer naar')}}</h3>

<ul>
	<a href="/privacy"><li>{{ trans('menu.Privacy') }}</li></a>
	<a href="/privacy_policy"><li>{{ trans('menu.Privacy Policy') }}</li></a>
	<a href="/disclaimer"><li>{{ trans('menu.Disclaimer') }}</li></a>
</ul>
</div>
</div>

	<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 col-xl-9" id="content">
		<div class="inner " id="pageContent">
			<h2>Privacy</h2>

			<p>Reedijk Wheels &amp; Tyres respecteert de privacy van gebruikers van haar site en draagt er zorg voor dat de persoonlijke informatie die u ons eventueel verschaft vertrouwelijk wordt behandeld. Wij gebruiken uw gegevens om bestellingen zo snel en gemakkelijk mogelijk te laten verlopen. Voor het overige zullen wij deze gegevens uitsluitend gebruiken na uw schriftelijke toestemming. Reedijk Wheels &amp; Tyres zal uw persoonlijke gegevens niet aan derden verkopen en zal deze uitsluitend aan derden ter beschikking stellen die zijn betrokken bij het uitvoeren van uw bestelling.</p>

			<p>Reedijk Wheels &amp; Tyres gebruikt de verzamelde gegevens om haar klanten de volgende diensten te leveren:</p>

			<ul>
				<li><strong>Als u een offerte aanvraagt, hebben we uw naam, e-mailadres, telefoonnummer, merk en type auto en bandentype nodig om aan uw offerteaanvraag tegemoet te komen en u van het verloop daarvan op de hoogte te houden.</strong></li>
				<li><strong>Wij gebruiken uw e-mailadres om u te informeren over de ontwikkeling van c.q. op de website en over speciale aanbiedingen en promotionele acties. Als u hier niet langer prijs op stelt, kunt u zich uitschrijven via unsubscribe@reedijkgroup.com.</strong></li>
				<li><strong>Gegevens over het gebruik van onze site en de feedback die we krijgen van onze bezoekers helpen ons om onze website verder te ontwikkelen en te verbeteren.</strong></li>
			</ul>
		</div>
	</div>


</div>
</div>



@endsection

