@extends('layouts.master')

@section('content')

<div class="col-lg-12 " id="container">
@include('pages.infobanner')
<div class="small " data-cycle-log="false" id="banner">
<div class="normal" style="background: #000 url('/img/header_contentpage.jpg') no-repeat center !important;">&nbsp;</div>
</div>

<section class="container no_padding" id="content">&nbsp;</section>

<div class="container page" id="maincontent">
<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 collapse_sidemenu" id="sidemenu">
<div id="open_sidemenu"><!--?xml version="1.0" encoding="utf-8"?--><!-- Generator: Adobe Illustrator 19.2.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  --><svg id="Layer_1" style="enable-background:new 0 0 50 50;" version="1.1" viewbox="0 0 50 50" x="0px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" y="0px"><g><polygon points="1,10 25,40.3 49,10 	" style="fill:#5D5D5D;"></polygon></g></svg></div>

<section class="container no_padding" id="content">&nbsp;</section>

<div class="inner">
	<h3>{{trans( 'menu.Navigeer naar')}}</h3>

	<ul>
		<a href="/privacy"><li>{{ trans('menu.Privacy') }}</li></a>
		<a href="/privacy_policy"><li>{{ trans('menu.Privacy Policy') }}</li></a>
		<a href="/disclaimer"><li>{{ trans('menu.Disclaimer') }}</li></a>
</ul>
</div>
</div>

	<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 col-xl-9" id="content">
		<div class="inner " id="pageContent">
			<h2>Privacy Policy</h2>

			<p>Reedijk Wheels &amp; Tyres respecteert de privacy van gebruikers van haar site en draagt er zorg voor dat de persoonlijke informatie die u ons eventueel verschaft vertrouwelijk wordt behandeld.</p>

			<p>Alle wijzigingen in prijzen, tekst, foto&#39;s en tekeningen zijn voorbehouden aan Reedijk Wheels &amp; Tyres. Aan (de inhoud van) deze website kunnen geen rechten worden ontleend.</p>

			<p>De informatie op deze website is met de grootst mogelijke zorg samengesteld. Desondanks is het mogelijk dat de gepubliceerde informatie achterhaald, onvolledig of onjuist is. Reedijk Wheels &amp; Tyres aanvaardt hiervoor nimmer aansprakelijkheid. Reedijk Wheels &amp; Tyres sluit elke aansprakelijkheid uit voor schade die voortvloeit uit of verband houdt met het gebruik van de gepubliceerde informatie op deze website.</p>
		</div>
	</div>


</div>
</div>



@endsection

