
@extends('layouts.master2')


@section('content')


  @include('layouts.sidebar')

  @include('layouts.topbar')

  @include ('layouts.navbar')
    <!-- Off-Canvas Wrapper-->
    <div class="offcanvas-wrapper">
      <!-- Page Title-->
      <div class="page-title">
        <div class="container">
          <div class="column">
            <h1>Shop Grid Left Sidebar</h1>
          </div>
          <div class="column">
            <ul class="breadcrumbs">
              <li><a href="index.html">Home</a>
              </li>
              <li class="separator">&nbsp;</li>
              <li>Shop Grid Left Sidebar</li>
            </ul>
          </div>
        </div>
      </div>
      <!-- Page Content-->
      <div class="container padding-bottom-3x mb-1">
        <div class="row">
          <!-- Products-->
          <div class="col-xl-9 col-lg-8 order-lg-2">
            <!-- Shop Toolbar-->
            <div class="shop-toolbar padding-bottom-1x mb-2">
              <div class="column">
                <div class="shop-sorting">
                  <label for="sorting">Sort by:</label>
                  <select class="form-control" id="sorting">
                    <option>Popularity</option>
                    <option>Low - High Price</option>
                    <option>High - Low Price</option>
                    <option>Avarage Rating</option>
                    <option>A - Z Order</option>
                    <option>Z - A Order</option>
                  </select><span class="text-muted">Showing:&nbsp;</span><span>1 - 12 items</span>
                </div>
              </div>
              <div class="column">
                <div class="shop-view"><a class="grid-view active" href="shop-grid-ls"><span></span><span></span><span></span></a><a class="list-view" href="shop-list-ls"><span></span><span></span><span></span></a></div>
              </div>
            </div>

            <!-- Products Grid-->
            <div class="isotope-grid cols-3 mb-2">
              <div class="gutter-sizer"></div>
              <div class="grid-sizer"></div>
              <!-- Product-->

              @foreach ($products as  $product)
                <div class="grid-item">
                  <div class="product-card">
                    {{--@if(!empty($product->btw_percentage))--}}
                      {{--<div class="product-badge text-danger">{{$product->btw_percentage}}% Korting</div>--}}

                    {{--@endif--}}
                    <br>
                    <a class="product-thumb" href="single-product/{{$product->id}}"><img src="{{$product->image_url_1}}" alt="Product"></a>
                    <h3 class="product-title"><a href="losse templates/shop-single">{{$product->art_description_NL}}</a></h3>
                    <h4 class="product-price">
                      {{--@if(!empty($product->btw_percentage))--}}

                        {{--<del>€{{$product->gross_incbtw}}</del>--}}

                        {{--€{{$product->gross_incbtw - ($product->btw_percentage / 100 *$product->gross_incbtw )}}--}}
                      {{--@else--}}
                        €{{$product->price}}<br>
                      {{--@if(env('TEST_ENV'))--}}
                        {{--€{{$product->purchase_exbtw}}--}}
                      {{--@endif--}}
                      {{$product->category_url}}<br>
                      {{$product->category}}<br>
                      {{$product->sub_category}}<br>

                      {{--@endif--}}
                    </h4>
                    <div class="product-buttons">
                      <button class="btn btn-outline-secondary btn-sm btn-wishlist" data-toggle="tooltip" title="Whishlist"><i class="icon-heart"></i></button>
                      <button class="btn btn-outline-primary btn-sm" data-toast data-toast-type="success" data-toast-position="topRight" data-toast-icon="icon-circle-check" data-toast-title="Product" data-toast-message="successfuly added to cart!"><a href="{{ route('product.addToCart', ['id' => $product->id]) }}">Winkelwagen</a></button>
                    </div>
                  </div>
                </div>
              @endforeach
            </div>
            <!-- Pagination-->


            <nav class="pagination">
                <ul class="pages">
                 {{ $products->links()}}
                </ul>
              <div class="column text-right hidden-xs-down"><a class="btn btn-outline-secondary btn-sm" href="#">Next&nbsp;<i class="icon-arrow-right"></i></a></div>
            </nav>
          </div>
          <!-- Sidebar          -->
        @include('layouts.menusidebar')
        </div>
      </div>
      <!-- Site Footer-->
      @include('layouts.footer')

      @endsection
    </div>
    <!-- Back To Top Button--><a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
    <!-- Backdrop-->
    <div class="site-backdrop"></div>
    <!-- JavaScript (jQuery) libraries, plugins and custom scripts-->
    <script src="js/vendor.min.js"></script>
    <script src="js/scripts.min.js"></script>
