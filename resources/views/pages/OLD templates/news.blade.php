@extends('layouts.master')


@section('content')


    <div class="col-lg-12 " id="container">
@include('pages.infobanner')

<div class="small " data-cycle-log="false" id="banner">

<div class="normal" style="background: #000 url('/img/header_contentpage.jpg') no-repeat center !important;">&nbsp;</div>

</div>

<section class="container no_padding" id="content">&nbsp;</section>

<div class="container page" id="maincontent">
<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 collapse_sidemenu" id="sidemenu">
<div id="open_sidemenu"><!--?xml version="1.0" encoding="utf-8"?--><!-- Generator: Adobe Illustrator 19.2.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  --></div>

<div class="inner">
<h3>{{trans( 'menu.Navigeer naar')}} </h3>

<ul>
@foreach ($news as $nieuws)
    <a href="{{ route('pages.news', $nieuws->slug) }}"><li>{{$nieuws->title}}</li></a>
@endforeach
</ul>
</div>
</div>
    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 col-xl-9" id="content">
        <div class="inner " id="pageContent">

            <h2 id="{{$show->title}}">{{$show->title}}</h2>

            {!!html_entity_decode($show->text)!!}

        </div>
    </div>
</div>

</div>



@endsection

