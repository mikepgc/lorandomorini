@extends('layouts.master')

@section('content')


<div class="col-lg-12 " id="container">
@include('pages.infobanner')
<div class="small " data-cycle-log="false" id="banner">
<div class="normal" style="background: #000 url('/img/header_contentpage.jpg') no-repeat center !important;">&nbsp;</div>
</div>

<section class="container no_padding" id="content">&nbsp;</section>

<div class="container page" id="maincontent">
<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 collapse_sidemenu" id="sidemenu">
<div id="open_sidemenu"><!--?xml version="1.0" encoding="utf-8"?--><!-- Generator: Adobe Illustrator 19.2.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  --><svg id="Layer_1" style="enable-background:new 0 0 50 50;" version="1.1" viewbox="0 0 50 50" x="0px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" y="0px"><g><polygon points="1,10 25,40.3 49,10 	" style="fill:#5D5D5D;"></polygon></g></svg></div>

<div class="inner">
<h3>Downloads</h3>

<nav id="downloads">
<ul>
</ul>
</nav>
</div>
</div>

    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 col-xl-9" id="content">
        <div class="inner " id="pageContent">
            <p>{{trans('menu.downloads 1')}}.</p>

            <ul>
                <li><a href="https://issuu.com/reedijkwheelstyres/docs/reedijk_gmp_catalogo_2017_nuovo">GMP Catalog 2017</a></li>
                <li><a href="https://issuu.com/reedijkwheelstyres/docs/catalogus_reedijk_def_lr" target="_blank">{{trans('menu.downloads 2')}}</a>&nbsp;</li>
                <li><a href="https://issuu.com/reedijkwheelstyres/docs/gmp_catalog_2016_reifen_essen_-_low" target="_blank">GMP Catalog 2016</a></li>
            </ul>

            <p>{{trans('menu.downloads 3')}}:</p>

            <ul>
                <li><a href="http://static.fitmentguide.nl/downloads/high-resolution.zip" target="_blank">High resolution images</a></li>
            </ul>
        </div>
    </div>


</div>
</div>

@endsection



