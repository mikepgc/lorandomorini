@extends('layouts.master')

@section('content')

<div class="col-lg-12 " id="container">
@include('pages.infobanner')
<div class="small " data-cycle-log="false" id="banner">
<div class="normal" style="background: #000 url('/img/header_contentpage.jpg') no-repeat center !important;">&nbsp;</div>
</div>

<section class="container no_padding" id="content">&nbsp;</section>

 <div id="maincontent" class="container retour">
                                            <div id="sidemenu" class="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 collapse_sidemenu">
	<div id="open_sidemenu"></div>
	<div class="inner">
		<h3>navigeer naar</h3>
		<ul><a href="./contact#employees">
				<li>onze verkopers</li>
			</a>
			<a href="./contact#storage">
				<li>Wissel en Opslag</li>
			</a>
			<a href="./contact#administration">
				<li>administratie</li>
			</a>
			<a href="./contact#website">
				<li>website</li>
			</a>
			<a href="https://www.reedijkgroup.com/nl_NL/retour">
				<li>retour formulier</li>
			</a>
		</ul></div>
</div>

<div id="content" class="col-xs-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 retour">
	<div class="inner">
        		<h2>retour formulier</h2>


		<fieldset id="fieldset-form-retour">

			<form id="form-retour" enctype="application/x-www-form-urlencoded" action="/nl_NL/retour" novalidate="novalidate" method="post">

				<ol>


					<li class="inputContainer input-contactPerson"><p id="contactPerson-label" class="formLabel">
							<label for="contactPerson" class="required">Naam</label></p>

						<div class="formInput">
							<input type="text" name="contactPerson" id="contactPerson" value="" required="required"></div>
					</li>

					<li class="inputContainer input-invoiceNumber"><p id="invoiceNumber-label" class="formLabel">
							<label for="invoiceNumber" class="required">Debiteurnummer</label></p>

						<div class="formInput">
							<input type="text" name="invoiceNumber" id="invoiceNumber" value="" required="required"></div></li>
					<li class="inputContainer input-reason"><p id="reason-label" class="formLabel"><label for="reason" class="required">Reden van retour</label></p>

						<div class="formInput">
							<textarea name="reason" id="reason" rows="10" cols="50" required="required"></textarea></div></li>
					<li class="inputContainer input-discussedWith"><p id="discussedWith-label" class="formLabel">
							<label for="discussedWith" class="required">Besproken met</label></p>

						<div class="formInput">
							<select name="discussedWith" id="discussedWith" required="required"><option value="Bert Reedijk" label="Bert Reedijk">Bert Reedijk</option><option value="Bas Lems" label="Bas Lems">Bas Lems</option><option value="Frank Nootenboom" label="Frank Nootenboom">Frank Nootenboom</option><option value="Lennart Monster" label="Lennart Monster">Lennart Monster</option><option value="Jurrien van der Struik" label="Jurrien van der Struik">Jurrien van der Struik</option><option value="Semira Sarfaty" label="Semira Sarfaty">Semira Sarfaty</option><option value="Bert Pico" label="Bert Pico">Bert Pico</option><option value="Carlo Marinus" label="Carlo Marinus">Carlo Marinus</option><option value="Desi Sanderse" label="Desi Sanderse">Desi Sanderse</option></select></div></li>
					<li class="inputContainer input-agree input-checkbox"><p id="agree-label" class="formLabel">
							<label for="agree" class="required">Ik ga akkoord met de algemene voorwaarden</label></p>

						<div class="formInput">
							<input type="hidden" name="agree" value="0"><input type="checkbox" name="agree" id="agree" value="1" required="required"></div></li>

					<li class="inputContainer input-submitRetour input-button"><div class="formInput">


							<input type="submit" name="submitRetour" id="submitRetour" value="Versturen" class="button"></div>
					</li>
				</ol>

			</form>
		</fieldset>

		<form action="onjuist_geleverd" class="popup-modal" method="POST" title="Banden">

			<div class="formInput">
			<p>Datum van Levering</p>

			<input type="date" name="bday">
			</div>
			<br><br>

			<p>Debiteurnummer<font color="red">*</font> </p> <input type="text" name="debiteurnummer" placeholder="Debiteurnummer" required>

			<br><br>

			<p>Naam<font color="red">*</font> </p>  <input type="text" name="Naam" placeholder="Uw naam" required>

			<br><br>

			<p>Ophalen standaard adres?</p><input type="radio" name="Naam" placeholder="Uw naam" required>

			<p>Ophaaldres<font color="red">*</font> </p> <input type="text" id="field1" name="adres" placeholder=""  required>

			<br><br>


			<p>Ophaal postcode<font color="red">*</font> </p> <input type="text" id="field2" name="postcode" placeholder=""  required>

			<br><br>

			<p>Ophaal Plaats<font color="red">*</font> </p> <input type="text" id="field3" name="plaats" placeholder=""  required>

			<br><br>

			<p>Geholpen door:<font color="red">*</font></p>


			<select name="sales" size="1" required>


				@foreach ($Werknemers->where('afdeling','sales') as $Werknemer)

					<option value='{{ $Werknemer->title}}'>{{ $Werknemer->title}}</option>

				@endforeach</select>
			<br><br>

				<p>Reden retour <font color="red">*</font></p> <input type="text" id="redenretour" name="redenretour" placeholder="Redenretour"  required>

			<br><br>
				<p>Productgegevens<font color="red">*</font> </p> <input type="text" id="productgegevens" name="productgegevens" placeholder="productgegevens"  required>

			<br><br>
				<p>Akkoord</p><input type="checkbox" name="Naam" placeholder="Uw naam" required>

			<input type="submit" name="submitRetour" id="submitRetour" value="Versturen" class="button"></div>
		</form>
        		

</div></div></div></div>
            
</div>





@endsection



