@extends('layouts.master')

@section('content')






<div class="col-lg-12 " id="container">
@include('pages.infobanner')
<div class="small " data-cycle-log="false" id="banner">
<div class="normal" style="background: #000 url('/img/header_contentpage.jpg') no-repeat center !important;">&nbsp;</div>
</div>

<section class="container no_padding" id="content">&nbsp;</section>

<div class="container page" id="maincontent">
<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 collapse_sidemenu" id="sidemenu">
<div id="open_sidemenu"><!--?xml version="1.0" encoding="utf-8"?--><!-- Generator: Adobe Illustrator 19.2.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  --></div>

<div class="inner">
<h3>{{trans( 'menu.Navigeer naar')}}</h3>

<ul>
	<a href="#uitlijnen"><li>{{ trans('menu.uitlijnen')}}</li></a>
	<a href="#balanceren"><li>{{ trans('menu.balanceren')}}</li></a>
	<a href="#montage"><li>{{ trans('menu.Montage')}}</li></a>
	<a href="#winterwissel"><li>{{ trans('menu.Winterwissel')}}</li></a>
	<a href="#bandenopslag"><li>{{ trans('menu.banden Opslag')}}</li></a>
	<a href="#verlagen"><li>{{ trans('menu.verlagen')}}</li></a>
	<a href="#reparatie"><li>{{ trans('menu.reparatie banden')}}</li></a>
	<a href="#wielenwassen"><li>{{ trans('menu.wielen Wassen')}}</li></a>
</ul>
</div>
</div>




	<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 col-xl-9" id="content">
        <?=trans('menu.servicesblock')?>
	</div>



</div>
</div>



@endsection

