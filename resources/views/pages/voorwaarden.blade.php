




@extends('layouts.master2')


@section('content')


    @include('layouts.sidebar')

    @include('layouts.topbar')

    @include ('layouts.navbar')
    <!-- Off-Canvas Wrapper-->
    <div class="offcanvas-wrapper">
        <!-- Page Title-->
        <div class="page-title">
            <div class="container">
                <div class="column">
                    {{--Takes from end of url and remove underscore--}}
                    <h1>Algemene Voorwaarden</h1>
                </div>
                <div class="column">
                    <ul class="breadcrumbs">
                        <li><a href="index.html">Home</a>
                        </li>
                        <li class="separator">&nbsp;</li>
                        <li>Algemene Voorwaarden</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Page Content-->
        <div class="container padding-bottom-3x mb-1">
            <div class="row">
                <!-- Products-->
                <div class="col-xl-9 col-lg-8 order-lg-2">
                    <!-- Shop Toolbar-->
                  <h3> ALGEMENE VOORWAARDEN – Coffee Roasting Lorando-Morini</h3>

                    <br>
                    Dit zijn onze algemene voorwaarden. Deze zijn samen met de verwijzingen die ze bevatten, van toepassing op elke overeenkomst die wij, Coffee Roasting Lorando-Morini (met KvK-nummer64137325), met u sluiten. Mocht u vragen hebben, aarzel dan niet met ons contact te zoeken via info@lorandomorini.nl of per post: Coffee Roasting Lorando-Morini Kiotoweg 133 Unit 25/26 3047BG Rotterdam.  Wij hebben het recht deze algemene voorwaarden te wijzigen. U stemt ermee in dat steeds de laatste versie van deze algemene voorwaarden op onze overeenkomst van toepassing zal zijn.
                    <br>
                    <br>
                    <h5>Artikel 1 – Producten</h5>

<br>
                    Wij verkopen Verse Koffie.
                    <br>
                    <br>
                   <h5> Artikel 2 – Betalingen</h5>
                    <br>
                    De (totale) prijs van de producten staat in onze webshop aangegeven. U kunt in onze webshop betalen met iDEAL, Wij vergoeden de verzendkosten boven bestellingen van 30 euro (incl. btw).
                    <br>
                    <br>
                    <h5>Artikel 3 – Leveringen</h5>
                    <br>
                    Wij leveren uw product af op het adres dat u bij de bestelling hebt opgegeven. U kunt het adres niet meer wijzigen als wij het pakket al hebben verzonden. DHL PostNL DPD UPS verzorgt de distributie. Wanneer het pakket is verzonden, krijgt u van ons een track & trace code, waarmee u de bestelling kunt volgen. Wij doen ons best om de bestelling zo snel mogelijk te leveren. Als de bestelling vertraagd is, stellen wij u hiervan schriftelijk op de hoogte. We garanderen dat u de bestelling uiterlijk 2 dagen na bevestiging van de bestelling zal ontvangen. Lukt dit onverhoopt niet, dan kunt u ons verzoeken om (a) het bedrag dat u voor het product betaald hebt meteen terug te storten, of (b) een vervangend product (van gelijke waarde) te sturen.
                    <br>
                    <br>
                    <h5>Artikel 4 – Retour</h5>
                    <br>
                    U mag het product altijd binnen 14 dagen, nadat u het hebt ontvangen, terugsturen . Dit kunt u doen met het retourformulier of door een e-mail te sturen naar info@lorandomorini.nl Nadat u van ons een bevestiging hebt ontvangen, moet u het product binnen 14 dagen opsturen. Als wij het product hebben ontvangen, storten wij binnen 5 werkdagen dagen het volledige aankoopbedrag terug. De kosten voor de retourzending komen voor uw rekening. Tijdens de termijn die u heeft om te bedenken of u het product wilt houden, moet u zorgvuldig omgaan met het product en de verpakking. U mag het product alleen uitpakken en gebruiken voor zover dat nodig is om te bepalen of u het product wilt houden.
                    <br>
                    <br>
                    <h5>Artikel 5 – Aansprakelijkheid</h5>
                    <br>
                    Wij zijn niet aansprakelijk voor de schade die veroorzaakt is door gebruik van een product. Dit geldt niet als die schade is ontstaan door onze opzet, grove nalatigheid of bewuste roekeloosheid. Uw recht op schadevergoeding zal in ieder geval maximaal het bedrag zijn van het bestelde en betaalde product.
                    <br>
                    <br>
                    <h5> Artikel 6 – Intellectuele eigendomsrechten</h5>

                    <br>
                    Alle intellectuele eigendomsrechten (zoals: auteursrechten, beeldmerken, woordmerken) op onze teksten, foto’s, beelden en andere materialen, zijn ons eigendom (of hebben wij in beheer met toestemming van de rechthebbende). Uw gebruik mag hier geen inbreuk op maken.
                    <br>
                    <br>
                    <h5>Artikel 7 – Klachtenregeling</h5>
                    <br>
                    Als u een klacht heeft over de uitvoering van de overeenkomst, horen wij dat graag. Stuur uw klacht dan zo snel mogelijk (en zo volledig mogelijk omschreven) naar info@zen-simple.com. Wij zullen uw klacht zo snel mogelijk afhandelen, uiterlijk binnen 5 werkdagen dagen nadat we uw klacht hebben ontvangen. Als het langer duurt om de klacht af te wikkelen, ontvangt u binnen 3 werkdagen dagen een bevestiging van uw klacht en laten we weten wanneer we een inhoudelijk antwoord verwachten. Als we de klacht niet onderling kunnen oplossen, ontstaat er een geschil dat valt onder de geschillenregeling.
                    <br>
                    <br>
                    <h5>Artikel 8 – Geschillenregeling</h5>
                    <br>
                    Op deze algemene voorwaarden en de overeenkomsten die wij met u sluiten is het Nederlands recht van toepassing. In het geval van een geschil is de rechter te Rotterdam bevoegd.
                    <br>
                    <br>
                    <h5>Contact</h5>
                    Coffee Roasting Lorando-Morini<br><br>

                    Hoofdvestiging   Coffee Roasting Lorando-Morini KVK 64137325  Vestigingsnr. 000032990103 Kiotoweg 133 Unit 25/26    3047BG Rotterdam  info@lorandomorini.nl





                </div>

            </div>
        </div>
        <!-- Site Footer-->
        @include('layouts.footer')

        @endsection
    </div>
    <!-- Back To Top Button--><a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
    <!-- Backdrop-->
    <div class="site-backdrop"></div>
    <!-- JavaScript (jQuery) libraries, plugins and custom scripts-->
    <script src="js/vendor.min.js"></script>
    <script src="js/scripts.min.js"></script>



