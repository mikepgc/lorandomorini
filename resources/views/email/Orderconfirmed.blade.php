<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">

<head>
    <title>
    </title>
    <!--[if !mso]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--<![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style type="text/css">
        #outlook a {
            padding: 0;
        }

        body {
            margin: 0;
            padding: 0;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }

        table,
        td {
            border-collapse: collapse;
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: none;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
        }

        p {
            display: block;
            margin: 13px 0;
        }
    </style>
    </style>
    <!--[if mso]>
                <xml>
                <o:OfficeDocumentSettings>
                <o:AllowPNG/>
                            <o:PixelsPerInch>96</o:PixelsPerInch>
                                                 </o:OfficeDocumentSettings>
                                                   </xml>
                                                     <![endif]-->
    <!--[if lte mso 11]>
                       <style type="text/css">
                                             .outlook-group-fix { width:100% !important; }
    </style>
    <![endif]-->
    <!--[if !mso]><!-->
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,700" rel="stylesheet" type="text/css">
    <style type="text/css">
        @import url(https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700);
        @import url(https://fonts.googleapis.com/css?family=Nunito+Sans:400,700);
    </style>
    <!--<![endif]-->
    <style type="text/css">
        @media only screen and (min-width:480px) {
            .mj-column-per-100 {
                width: 100% !important;
                max-width: 100%;
            }

            .mj-column-per-50 {
                width: 50% !important;
                max-width: 50%;
            }
        }
    </style>
    <style type="text/css">
        @media only screen and (max-width:480px) {
            table.full-width-mobile {
                width: 100% !important;
            }

            td.full-width-mobile {
                width: auto !important;
            }
        }
    </style>
</head>

<body style="background-color:#ffffff;">
<div style="display:none;font-size:1px;color:#ffffff;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;"> Hartelijk dank voor uw bestelling bij Lorandomorini.nl. </div>
<div style="background-color:#ffffff;">

    <div style="margin:0px auto;max-width:600px;">
        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
            <tbody>
            <tr>
                <td style="vertical-align: top; direction: ltr; font-size: 0px; padding: 20px 0; padding-bottom: 0; padding-top: 12px; text-align: center;" align="center" valign="top">
                    <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                        <tr>

                            <td
                                class="" style="vertical-align:top;width:600px;"
                            >
                    <![endif]-->
                    <div class="mj-column-per-100 outlook-group-fix" style="font-size: 0px; text-align: left; direction: ltr; display: inline-block; vertical-align: top; width: 100%;">
                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align: top;" width="100%" valign="top">
                            <tr style="vertical-align: top;" valign="top">
                                <td align="left" style="vertical-align: top; font-size: 0px; padding: 10px 25px; padding-bottom: 0; word-break: break-word;" valign="top">
                                    <div style="vertical-align: top; font-family: Nunito Sans, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px; text-align: left; color: #231F20;">
                                        @if($data['company'] == 'on')
                                        <p style="vertical-align: top;">Beste {{$data['companyName']}},</p>
                                        @else
                                        <p style="vertical-align: top;">Beste {{$data['userName']}}  {{$data['surName']}},</p>
                                        @endif
                                        <p style="vertical-align: top;">Uw order <strong style="vertical-align: top;">{{$data['orderId']}}</strong> is verstuurd. </p>

                                        <p>Uw factuur kunt u vinden in de bijlage. </p>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <!--[if mso | IE]>
                    </td>

                    </tr>

                    </table>
                    <![endif]-->
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <!--[if mso | IE]>
    </td>
    </tr>
    </table>

    <table
        align="center" border="0" cellpadding="0" cellspacing="0" class="order-outlook" style="width:600px;" width="600"
    >
        <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
    <![endif]-->
    <div class="order" style="padding: 26px; margin: 0px auto; max-width: 600px;">
        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="max-width: 540px; width: 100%;" width="100%">
            <tbody>
            <tr>
                <td style="vertical-align: top; border: 1px solid #929497; direction: ltr; font-size: 0px; padding: 0 14px; text-align: center;" align="center" valign="top">
                    <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                        <tr>
                            <td
                                class="" width="600px"
                            >

                                <table
                                    align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:570px;" width="570"
                                >
                                    <tr>
                                        <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                    <![endif]-->
                    <div style="vertical-align: top; margin: 0px auto; max-width: 570px;">
                        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align: top; max-width: 540px; width: 100%;" width="100%" valign="top">
                            <tbody style="vertical-align: top;" valign="top">
                            <tr style="vertical-align: top;" valign="top">
                                <td style="vertical-align: top; border-bottom: 1px solid #979797; direction: ltr; font-size: 0px; padding: 16px 0; text-align: center;" align="center" valign="top">
                                    <!--[if mso | IE]>
                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                                        <tr>

                                            <td
                                                class="" style="vertical-align:top;width:570px;"
                                            >
                                    <![endif]-->
                                    <div class="mj-column-per-100 outlook-group-fix" style="font-size: 0px; text-align: left; direction: ltr; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="max-width: 540px; vertical-align: top;" width="100%" valign="top">
                                            <tr style="vertical-align: top;" valign="top">
                                                <td align="center" style="vertical-align: top; font-size: 0px; padding: 0; word-break: break-word;" valign="top">
                                                    <div style="vertical-align: top; font-family: Nunito Sans, Helvetica, Arial, sans-serif; font-size: 14px; font-weight: bold; line-height: 20px; text-align: center; color: #231F20;">Uw order details</div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <!--[if mso | IE]>
                                    </td>

                                    </tr>

                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]>
                    </td>
                    </tr>
                    </table>

                    </td>
                    </tr>

                    <tr>
                        <td
                            class="" width="600px"
                        >

                            <table
                                align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:570px;" width="570"
                            >
                                <tr>
                                    <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                    <![endif]-->
                    <div style="vertical-align: top; margin: 0px auto; max-width: 570px;">
                        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align: top; max-width: 540px; width: 100%;" width="100%" valign="top">
                            <tbody style="vertical-align: top;" valign="top">
                            <tr style="vertical-align: top;" valign="top">
                                <td style="vertical-align: top; direction: ltr; font-size: 0px; padding: 16px 0 0 0; text-align: center;" align="center" valign="top">
                                    <!--[if mso | IE]>
                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                                        <tr>

                                            <td
                                                class="" style="vertical-align:top;width:570px;"
                                            >
                                    <![endif]-->
                                    <div class="mj-column-per-100 outlook-group-fix" style="font-size: 0px; text-align: left; direction: ltr; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="max-width: 540px; vertical-align: top;" width="100%" valign="top">
                                            <tr style="vertical-align: top;" valign="top">
                                                <td align="left" style="vertical-align: top; font-size: 0px; padding: 0; word-break: break-word;" valign="top">
                                                    <div style="vertical-align: top; font-family: Nunito Sans, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px; text-align: left; color: #231F20;">Ordernummer: {{$data['orderId']}} </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <!--[if mso | IE]>
                                    </td>

                                    </tr>

                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]>
                    </td>
                    </tr>
                    </table>

                    </td>
                    </tr>

                    <tr>
                        <td
                            class="" width="600px"
                        >

                            <table
                                align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:570px;" width="570"
                            >
                                <tr>
                                    <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                    <![endif]-->
                    <div style="vertical-align: top; margin: 0px auto; max-width: 570px;">
                        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align: top; max-width: 540px; width: 100%;" width="100%" valign="top">
                            <tbody style="vertical-align: top;" valign="top">
                            <tr style="vertical-align: top;" valign="top">
                                <td style="vertical-align: top; direction: ltr; font-size: 0px; padding: 16px 0; text-align: center;" align="center" valign="top">
                                    <!--[if mso | IE]>
                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                                        <tr>

                                            <td
                                                class="" style="vertical-align:top;width:285px;"
                                            >
                                    <![endif]-->
                                    <div class="mj-column-per-50 outlook-group-fix" style="font-size: 0px; text-align: left; direction: ltr; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="max-width: 540px; vertical-align: top;" width="100%" valign="top">
                                            <tr style="vertical-align: top;" valign="top">
                                                <td align="left" style="vertical-align: top; font-size: 0px; padding: 0 0 8px 0; word-break: break-word;" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align: top; max-width: 540px; border-collapse: collapse; border-spacing: 0px;" valign="top">
                                                        <tbody style="vertical-align: top;" valign="top">
                                                        <tr style="vertical-align: top;" valign="top">
                                                            <td style="vertical-align: top; width: 20px;" width="20" valign="top">
                                                                <img alt="Factuuradres" height="auto" src="https://www.lorandomorini.nl/img/factuur.png" style="vertical-align: top; border: 0; display: block; outline: none; text-decoration: none; height: auto; width: 100%; font-size: 13px;" width="20">
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr style="vertical-align: top;" valign="top">
                                                <td align="left" style="vertical-align: top; font-size: 0px; padding: 0; word-break: break-word;" valign="top">
                                                    <div style="vertical-align: top; font-family: Nunito Sans, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 30px; text-align: left; color: #231F20;"><strong style="vertical-align: top;">Factuuradres</strong><br style="vertical-align: top;"> @if($data['company'] == 'on') {{$data['companyName']}} <br style="vertical-align: top;">  @endif {{$data['userName']}} {{$data['surName']}}<br style="vertical-align: top;"> {{$data['address']}} {{$data['housenumber']}}{{$data['housenumber2']}} <br style="vertical-align: top;"> {{$data['postcode']}} {{$data['city']}}<br style="vertical-align: top;"> E-mail: <a href="mailto:{{$data['userEmail']}}" class="link-style" target="_blank" style="vertical-align: top; color: #c8584e; text-decoration: none; font-weight: bold;">{{$data['userEmail']}}</a></div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <!--[if mso | IE]>
                                    </td>

                                    <td
                                        class="" style="vertical-align:top;width:285px;"
                                    >
                                    <![endif]-->
                                    <div class="mj-column-per-50 outlook-group-fix" style="font-size: 0px; text-align: left; direction: ltr; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="max-width: 540px; vertical-align: top;" width="100%" valign="top">
                                            <tr style="vertical-align: top;" valign="top">
                                                <td align="left" style="vertical-align: top; font-size: 0px; padding: 0 0 8px 0; word-break: break-word;" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align: top; max-width: 540px; border-collapse: collapse; border-spacing: 0px;" valign="top">
                                                        <tbody style="vertical-align: top;" valign="top">
                                                        <tr style="vertical-align: top;" valign="top">
                                                            <td style="vertical-align: top; width: 20px;" width="20" valign="top">
                                                                <img alt="Afleveradres" height="auto" src="https://www.lorandomorini.nl/img/adres.png" style="vertical-align: top; border: 0; display: block; outline: none; text-decoration: none; height: auto; width: 100%; font-size: 13px;" width="20">
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr style="vertical-align: top;" valign="top">
                                                <td align="left" style="vertical-align: top; font-size: 0px; padding: 0; word-break: break-word;" valign="top">
                                                    <div style="vertical-align: top; font-family: Nunito Sans, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 30px; text-align: left; color: #231F20;"><strong style="vertical-align: top;">Afleveradres</strong><br style="vertical-align: top;"> @if($data['company'] == 'on') {{$data['companyName']}} <br style="vertical-align: top;">  @endif  {{$data['userName']}} {{$data['surName']}}<br style="vertical-align: top;"> {{$data['shippingAddress']}} {{$data['shippinghousenumber']}}{{$data['shippinghousenumber2']}} <br style="vertical-align: top;"> {{$data['shippingPostcode']}} {{$data['shippingCity']}}<br style="vertical-align: top;"> E-mail: <a href="mailto:{{$data['userEmail']}}" class="link-style" target="_blank" style="vertical-align: top; color: #c8584e; text-decoration: none; font-weight: bold;">{{$data['userEmail']}}</a></div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>

                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>


                    <div style="vertical-align: top; margin: 0px auto; max-width: 570px;">
                        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align: top; max-width: 540px; width: 100%;" width="100%" valign="top">
                            <tbody style="vertical-align: top;" valign="top">
                            <tr style="vertical-align: top;" valign="top">
                                <td style="vertical-align: top; direction: ltr; font-size: 0px; padding: 8px 0 0 0; text-align: center;" align="center" valign="top">

                                    <div class="mj-column-per-100 outlook-group-fix" style="font-size: 0px; text-align: left; direction: ltr; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="max-width: 540px; vertical-align: top;" width="100%" valign="top">
                                            <tr style="vertical-align: top;" valign="top">
                                                <td align="left" style="vertical-align: top; font-size: 0px; padding: 0; word-break: break-word;" valign="top">
                                                    <table cellpadding="0" cellspacing="14px" width="100%" border="0" style="vertical-align: top; max-width: 540px; color: #231F20; font-family: Ubuntu, Helvetica, Arial, sans-serif; font-size: 13px; line-height: 22px; table-layout: auto; width: 100%; border: none;" valign="top">
                                                        <tr style="vertical-align: top; text-align: left;" align="left" valign="top">
                                                            <th  style="vertical-align: top; padding: 0 14px 7px 0;" valign="top">Artikel omschrijving</th>
                                                            <th style="vertical-align: top; padding: 6px 6px 7px 0; text-align: left;" align="left" valign="top">Aantal</th>
{{--                                                            <th style="vertical-align: top; padding: 6px 6px 7px 0; text-align: right;" align="right" valign="top">Prijs</th>--}}
                                                            <th style="vertical-align: top; padding: 6px 6px 7px 0; text-align: right;" align="right" valign="top">Totaal</th>
                                                        </tr>
                                                        <!-- ITEM PRODUCT GROUP -->
                                                        @foreach($data['orderdata'] as  $order)

                                                        <tr style="vertical-align: top; text-align: left; border-top: 1px solid #979797;" align="left" valign="top">

                                                            <td style="vertical-align: top; padding: 6px 14px 14px 0;" valign="top"><a><img
                                                                    src="https://www.lorandomorini.nl{{$order['item']['image_1']}}" alt="Product" width="40px"></a><br>
                                                                <strong style="vertical-align: top;">{{$order['item']['name']}}</strong><br style="vertical-align: top;"> Artikelnummer: {{$order['item']['artnr']}}
                                                            </td>
                                                            <td style="vertical-align: top; padding: 6px 6px 14px 0; text-align: right;" align="left" valign="top">{{$order['qty']}}</td>
{{--                                                            <td style="vertical-align: top; padding: 6px 6px 14px 0; text-align: right;" align="right" valign="top">€ {{$order['item']['price']}}</td>--}}
                                                            <td style="vertical-align: top; padding: 6px 6px 14px 0; text-align: right;" align="right" valign="top">€ {{$order['qty'] * $order['item']['price']}}</td>
                                                        </tr>
                                                        @endforeach
                                                        <!-- /ITEM PRODUCT GROUP -->

                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <!--[if mso | IE]>
                                    </td>

                                    </tr>

                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>



                    <div style="vertical-align: top; margin: 0px auto; max-width: 570px;">
                        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align: top; max-width: 540px; width: 100%;" width="100%" valign="top">
                            <tbody style="vertical-align: top;" valign="top">
                            <tr style="vertical-align: top;" valign="top">
                                <td style="vertical-align: top; direction: ltr; font-size: 0px; padding: 0 0 14px 0; text-align: center;" align="center" valign="top">
                                    <!--[if mso | IE]>
                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                                        <tr>

                                            <td
                                                class="" style="vertical-align:top;width:570px;"
                                            >
                                    <![endif]-->
                                    <div class="mj-column-per-100 outlook-group-fix" style="font-size: 0px; text-align: left; direction: ltr; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="max-width: 540px; vertical-align: top;" width="100%" valign="top">
                                            <tr style="vertical-align: top;" valign="top">
                                                <td align="left" style="vertical-align: top; font-size: 0px; padding: 0; word-break: break-word;" valign="top">
                                                    <table cellpadding="0" cellspacing="14px" width="100%" border="0" style="vertical-align: top; max-width: 540px; color: #231F20; font-family: Ubuntu, Helvetica, Arial, sans-serif; font-size: 13px; line-height: 22px; table-layout: auto; width: 100%; border: none;" valign="top">

                                                        </tr>
                                                        <tr style="vertical-align: top;" valign="top">
                                                            <td style="vertical-align: top; padding: 4px 6px 4px 0;" valign="top"><strong style="vertical-align: top;">Verzendkosten</strong></td>
                                                            @if($data['shippingcost'] > 1 )
                                                                <td style="vertical-align: top; padding: 4px 6px 4px 0; text-align: right;" align="right" valign="top">€ {{$data['shippingcost']}}</td>
                                                            @else
                                                                <td style="vertical-align: top; padding: 4px 6px 4px 0; text-align: right; color: green" align="right" valign="top">Gratis</td>
                                                            @endif
                                                        </tr>
{{--                                                        <tr style="vertical-align: top;" valign="top">--}}
{{--                                                            <td style="vertical-align: top; padding: 4px 6px 4px 0;" valign="top"><strong style="vertical-align: top;">Totaal excl. BTW</strong></td>--}}
{{--                                                            <td style="vertical-align: top; padding: 4px 6px 4px 0; text-align: right;" align="right" valign="top">€ 800.43</td>--}}
{{--                                                        </tr>--}}
                                                        <tr style="vertical-align: top;" valign="top">
                                                            <td style="vertical-align: top; padding: 4px 6px 4px 0;" valign="top"><strong style="vertical-align: top;">Totaal BTW (9%)</strong></td>
                                                            <td style="vertical-align: top; padding: 4px 6px 4px 0; text-align: right;" align="right" valign="top">€ {{(round(($data['ordertotal'] - $data['shippingcost']) / 109 * 9, 2))}}</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr style="vertical-align: top;" valign="top">
                                                <td style="vertical-align: top; font-size: 0px; padding: 14px 0; word-break: break-word;" valign="top">
                                                    <p style="vertical-align: top; border-top: solid 1px #979797; font-size: 1; margin: 0px auto; width: 100%;">
                                                    </p>
                                                    <!--[if mso | IE]>
                                                    <table
                                                        align="center" border="0" cellpadding="0" cellspacing="0" style="border-top:solid 1px #979797;font-size:1;margin:0px auto;width:570px;" role="presentation" width="570px"
                                                    >
                                                        <tr>
                                                            <td style="height:0;line-height:0;">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <![endif]-->
                                                </td>
                                            </tr>
                                            <tr style="vertical-align: top;" valign="top">
                                                <td align="left" style="vertical-align: top; font-size: 0px; padding: 0; word-break: break-word;" valign="top">
                                                    <table cellpadding="0" cellspacing="14px" width="100%" border="0" style="vertical-align: top; max-width: 540px; color: #231F20; font-family: Ubuntu, Helvetica, Arial, sans-serif; font-size: 13px; line-height: 22px; table-layout: auto; width: 100%; border: none;" valign="top">
                                                        <tr style="vertical-align: top;" valign="top">
                                                            <td style="vertical-align: top; padding: 4px 6px 4px 0;" valign="top">Totaalbedrag incl. BTW</td>
                                                            <td style="vertical-align: top; padding: 4px 6px 4px 0; text-align: right;" align="right" valign="top">€ {{number_format($data['ordertotal'], 2, '.', '')}}</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <!--[if mso | IE]>
                                    </td>

                                    </tr>

                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]>
                    </td>
                    </tr>
                    </table>

                    </td>
                    </tr>
                    <![endif]-->
                    <!-- PRICING LINES -->
                    <!--[if mso | IE]>
                    </table>
                    <![endif]-->
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <!--[if mso | IE]>
    </td>
    </tr>
    </table>

    <table
        align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
    >
        <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
    <![endif]-->
    <div style="margin:0px auto;max-width:600px;">
        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
            <tbody>
            <tr>
                <td style="vertical-align: top; direction: ltr; font-size: 0px; padding: 0; text-align: center;" align="center" valign="top">
                    <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                        <tr>

                            <td
                                class="" style="vertical-align:top;width:600px;"
                            >
                    <![endif]-->
{{--                    <div class="mj-column-per-100 outlook-group-fix" style="font-size: 0px; text-align: left; direction: ltr; display: inline-block; vertical-align: top; width: 100%;">--}}
{{--                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align: top;" width="100%" valign="top">--}}
{{--                            <tr style="vertical-align: top;" valign="top">--}}
{{--                                <td align="left" style="vertical-align: top; font-size: 0px; padding: 0 28px 14px 28px; word-break: break-word;" valign="top">--}}
{{--                                    <div style="vertical-align: top; font-family: Nunito Sans, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px; text-align: left; color: #231F20;">Please contact us with any questions, we are very happy to help.</div>--}}
{{--                                </td>--}}
{{--                            </tr>--}}
{{--                        </table>--}}
{{--                    </div>--}}
                    <!--[if mso | IE]>
                    </td>

                    </tr>

                    </table>
                    <![endif]-->
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <!--[if mso | IE]>
    </td>
    </tr>
    </table>

    <table
        align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
    >
        <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
    <![endif]-->
    <div style="margin:0px auto;max-width:600px;">
        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
            <tbody>
            <tr>
                <td style="vertical-align: top; direction: ltr; font-size: 0px; padding: 20px 0; text-align: center;" align="center" valign="top">
                    <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                        <tr>

                            <td
                                class="" style="vertical-align:top;width:600px;"
                            >
                    <![endif]-->
                    <div class="mj-column-per-100 outlook-group-fix" style="font-size: 0px; text-align: left; direction: ltr; display: inline-block; vertical-align: top; width: 100%;">
                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align: top;" width="100%" valign="top">
                            <tr style="vertical-align: top;" valign="top">
                                <td align="center" style="vertical-align: top; font-size: 0px; padding: 0; word-break: break-word;" valign="top">
                                    <!--[if mso | IE]>
                                    <table
                                        align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
                                    >
                                        <tr>

                                            <td>
                                    <![endif]-->
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align: top; float: none; display: inline-table;" valign="top">
                                        <tr style="vertical-align: top;" valign="top">
                                            <td style="vertical-align: top; padding: 4px;" valign="top">
                                                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align: top; background: #231F20; border-radius: 0; width: 32px;" width="32" valign="top">
                                                    <tr style="vertical-align: top;" valign="top">
                                                        <td style="font-size: 0; height: 32px; vertical-align: middle; width: 32px;" width="32" height="32" valign="middle">
                                                            <a href="" target="_blank" style="vertical-align: top;">
                                                                <img height="32" src="https://www.mailjet.com/images/theme/v1/icons/ico-social/facebook.png" style="vertical-align: top; border-radius: 0; display: block;" width="32">
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!--[if mso | IE]>
                                    </td>

                                    <td>
                                    <![endif]-->
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align: top; float: none; display: inline-table;" valign="top">
                                        <tr style="vertical-align: top;" valign="top">
                                            <td style="vertical-align: top; padding: 4px;" valign="top">
                                                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align: top; background: #231F20; border-radius: 0; width: 32px;" width="32" valign="top">
                                                    <tr style="vertical-align: top;" valign="top">
                                                        <td style="font-size: 0; height: 32px; vertical-align: middle; width: 32px;" width="32" height="32" valign="middle">
                                                            <a href="" target="_blank" style="vertical-align: top;">
                                                            <img height="32" src="https://www.mailjet.com/images/theme/v1/icons/ico-social/twitter.png" style="vertical-align: top; border-radius: 0; display: block;" width="32">
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!--[if mso | IE]>
                                    </td>

                                    <td>
                                    <![endif]-->
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align: top; float: none; display: inline-table;" valign="top">
                                        <tr style="vertical-align: top;" valign="top">
                                            <td style="vertical-align: top; padding: 4px;" valign="top">
                                                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align: top; background: #231F20; border-radius: 0; width: 32px;" width="32" valign="top">
                                                    <tr style="vertical-align: top;" valign="top">
                                                        <td style="font-size: 0; height: 32px; vertical-align: middle; width: 32px;" width="32" height="32" valign="middle">
                                                            <a href="" target="_blank" style="vertical-align: top;">
                                                                <img height="32" src="https://www.mailjet.com/images/theme/v1/icons/ico-social/linkedin.png" style="vertical-align: top; border-radius: 0; display: block;" width="32">
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!--[if mso | IE]>
                                    </td>

                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                            <tr style="vertical-align: top;" valign="top">
                                <td align="center" style="vertical-align: top; font-size: 0px; padding: 10px 25px; padding-bottom: 0; word-break: break-word;" valign="top">
                                    <div style="vertical-align: top; font-family: Nunito Sans, Helvetica, Arial, sans-serif; font-size: 12px; line-height: 16px; text-align: center; color: #c8584e">Email: <a style="vertical-align: top; color: #c8584e; text-decoration: none;" href="mailto:info@lorandomorini.nl">info@lorandomorini.nl</a> </div>
                                </td>
                            </tr>
                            <tr style="vertical-align: top;" valign="top">
                                <td align="center" style="vertical-align: top; font-size: 0px; padding: 10px 25px; padding-top: 0; padding-bottom: 28px; word-break: break-word;" valign="top">
                                    <div style="vertical-align: top; font-family: Nunito Sans, Helvetica, Arial, sans-serif; font-size: 12px; line-height: 16px; text-align: center; color: #929497;">Lorando&Morini by JAE-Studio</div>
                                    <div style="vertical-align: top; font-family: Nunito Sans, Helvetica, Arial, sans-serif; font-size: 12px; line-height: 16px; text-align: center; color: #929497;">Kiotoweg 133<br> 3047 BG  Rotterdam<br>Tel: 06 2323 5509</div>
                                </td>
                            </tr>
                            <tr style="vertical-align: top;" valign="top">
                                <td align="center" style="vertical-align: top; font-size: 0px; padding: 10px 25px; word-break: break-word;" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align: top; border-collapse: collapse; border-spacing: 0px;" valign="top">
                                        <tbody style="vertical-align: top;" valign="top">
                                        <tr style="vertical-align: top;" valign="top">
                                            <td style="vertical-align: top; width: 128px;" width="128" valign="top">
                                                <img alt="logo small" height="auto" src="https://lorandomorini.nl/img/logo/logo-black.png" style="vertical-align: top; border: 0; display: block; outline: none; text-decoration: none; height: auto; width: 100%; font-size: 13px;" width="128">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <!--[if mso | IE]>
                    </td>

                    </tr>

                    </table>
                    <![endif]-->
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <!--[if mso | IE]>
    </td>
    </tr>
    </table>
    <![endif]-->
</div>
</body>

</html>
